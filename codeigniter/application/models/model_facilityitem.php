<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Model_facilityitem extends CI_Model 
    {
        function __construct(){
        parent::__construct();
    }
        public function cek_item($id = ''){
        $data = array(
            'msi.itemid',
            'itemname',
            'specification'
        );

        $this->db->select($data);
        $this->db->from('msitem msi');
        $this->db->where('status', 'Active');

        if( $id != '' )
            $this->db->where('itemid', $id);

        return $this->db->get();
    }


    public function insert($itemname='',$specification=''){
        $data = array(
           'itemname' => $this->input->post('itemname'),
           'specification' => $this->input->post('specification')
        );

        $this->db->trans_begin();
        $this->db->insert('msitem', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update($itemname='',$specification=''){
          $data = array(
           'itemname' => $this->input->post('itemname'),
           'specification' => $this->input->post('specification')
        );
          
        $this->db->trans_begin();
        $this->db->where('itemid', $this->input->post('UpdateItem'));
        $this->db->update('msitem', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();
        $this->db->where('itemid', $this->input->post('itemid'));
        $this->db->update('msitem', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

}