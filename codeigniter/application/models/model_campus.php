<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Model_campus extends CI_Model {

        public function cek_campus($id = ''){
        $data = array(
            'campusid',
            'campuscode',
            'campusname',
            'campusaddress',
            'campusemail',
            'phoneoffice',
            'detail',
        
        );
        $this->db->select($data);
        $this->db->from('mscampus');
        $this->db->where('status', 'Active');

        if( $id != '' )
            $this->db->where('campusid', $id);

        return $this->db->get();
    }

   
    public function insert($campuscode='',$campusname='',$campusaddress='',$campusemail='',$phoneoffice='',$detail=''){
    $data = array(
        
           'campuscode' => $this->input->post('campuscode'),
           'campusname' => $this->input->post('campusname'),
           'campusaddress' => $this->input->post('campusaddress'),
           'campusemail' =>  $this->input->post('campusemail'),
           'phoneoffice' =>  $this->input->post('phoneoffice'),
           'detail' => $this->input->post('detail'),
        );
            
    
        $this->db->trans_begin();
        $this->db->insert('mscampus', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
    public function update($campuscode2='',$campusname2='',$campusaddress2='',$campusemail2='',$phoneoffice2='',$detail2=''){
        $data = array(
            
           'campuscode' => $this->input->post('campuscode'),
           'campusname' => $this->input->post('campusname'),
           'campusaddress' => $this->input->post('campusaddress'),
           'campusemail' => $this->input->post('campusemail'),
           'phoneoffice' => $this->input->post('phoneoffice'),
           'detail' => $this->input->post('detail'),
          
        );

        $this->db->trans_begin();
        $this->db->where('campusid', $this->input->post('CampusId'));
        $this->db->update('mscampus', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
              
     public function delete(){
        $data = array( 'status' => 'Inactive' );
        $this->db->trans_begin();
        $this->db->where('campusid', $this->input->post('campusId'));
        $this->db->update('mscampus', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
}