 <?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Model_detailbooking extends CI_Model {
    //Booking Periode//
        public function cek_detailbookingperiode($id = ''){
        $data = array(
            'ms.shiftid',
            'ms.shift',
            'mu.userid',
            'mu.nama',
            'mu.username',
            'mu.phone',
            'mu.email',
            'msb.bookingperiodeid',
            'msb.day',
            'msb.name',
            'msb.description',
            'msb.bookingby',
            'msc.campusid as pickupid',
            'msc2.campusid as dropoffid',
            'msc.campusname as pickupLoc',
            'msc2.campusname as dropoffLoc',
            'mt.termid',
            'mt.termcode',
            'msb.bookingperiodeid',
            'msb.termid',
        );

        $this->db->select($data);
        $this->db->from('msbookingperiode msb');
        $this->db->join('msshift ms','ms.shiftid=msb.shiftid');
        $this->db->join('msuser mu','mu.userid=msb.userid');
        $this->db->join('mscampus msc','msc.campusid=msb.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=msb.dropoffid');
        $this->db->join('msterm mt','mt.termid=msb.termid');
        $this->db->where('msb.status', 'Active');
        if( $id != '' )
            $this->db->where('bookingperiodeid', $id);
        return $this->db->get();
        }       

      public function get_term(){        
        $this->db->select('termcode');
        $this->db->from('msterm');
        $this->db->where('status','Active');

        $query = $this->db->get();

        return $query->result();
    }


    public function get_shift(){        
        $this->db->select('*');
        $this->db->from('msshift');
        $this->db->where('status','Active');

        $query = $this->db->get();

        return $query->result();
    }

        public function get_dataday(){        
        $this->db->select('*');
        $this->db->from('msbookingperiode');
        $this->db->where('status','Active');

        $query = $this->db->get();

        return $query->result();
    }

        public function get_datapickup(){
        $data = array(
        'msc.campuscode as pickupLoc',
        'msc.campusid as pickupid'
        );
        $this->db->select($data);
        $this->db->from('mscampus msc');
        $query = $this->db->get();
        return $query->result();
    }

        public function get_datadropoff(){        
        $data = array(
            'msc.campuscode as dropoffLoc',
            'msc.campusid as dropoffid'
        );
        $this->db->select($data);
        $this->db->from('mscampus msc');
        $query = $this->db->get();
        return $query->result();
        }

          
    
        public function deleteperiode(){ 
        $data = array( 'status' => 'Inactive' );
        $this->db->trans_begin();
        $this->db->where('bookingperiodeid', $this->input->post('bookingperiodeId'));
        $this->db->update('msbookingperiode', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
        }

         //booking date//
            public function cek_detailbooking($id = ''){
            $data = array(
            'ms.shiftid',
            'ms.shift',
            'mu.userid',
            'mu.nama',
            'mu.username',
            'mu.phone',
            'mu.email',
            'tb.date',
            'tb.name', 
            'tb.description',
            'tb.trbookingid',
            'tb.bookingby',
            'msc.campusid as pickupid',
            'msc2.campusid as dropoffid',
            'msc.campusname as pickupLoc',
            'msc2.campusname as dropoffLoc',
        );

        $this->db->select($data);
        $this->db->from('trbooking tb');
        $this->db->join('msshift ms','ms.shiftid=tb.shiftid');
        $this->db->join('msuser mu','mu.userid=tb.userid');
        $this->db->join('mscampus msc','msc.campusid=tb.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=tb.dropoffid');
        $this->db->where('tb.status', 'Active');
        if( $id != '' )
            $this->db->where('trbookingid', $id);
        return $this->db->get();
     }

        public function get_datashift(){        
        $this->db->select('*');
        $this->db->from('msshift');
        $this->db->where('status','Active');

        $query = $this->db->get();

        return $query->result();
    }
     public function get_date(){        
        $this->db->select('*');
        $this->db->from('trdetailshuttle');
        $this->db->where('status','Active');

        $query = $this->db->get();

        return $query->result();
    }
        public function get_pickup(){
        $data = array(
        'msc.campuscode as pickupLoc',
        'msc.campusid as pickupid'
        );
        $this->db->select($data);
        $this->db->from('mscampus msc');
        $query = $this->db->get();
        return $query->result();
    }

        public function get_dropoff(){        
        $data = array(
            'msc.campuscode as dropoffLoc',
            'msc.campusid as dropoffid'
        );
        $this->db->select($data);
        $this->db->from('mscampus msc');
        $query = $this->db->get();
        return $query->result();
    }

        public function insert(){
        $data = array(
            'date' => $this->input->post('date'),
            'pickupid' => $this->input->post('pickupid'),
            'dropoffid' => $this->input->post('dropoffid'),
            'nama' => $this->input->post('username'),
            'name' => $this->input->post('name'),
        );
       $data2 = array(  
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
        );
        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
        } 
            public function delete($date='',$shift='',$nama='',$email='',$phone='',$campusid='',$campusid1='',$name='',$description='',$id='',$userid=''){
              
                  $date = $this->input->post('date');
                  $shiftid = $this->input->post('shiftid');
                  $userid=$this->input->post('userid');
                  $totalorder=$this->input->post('totalorder');
                  $campusid= $this->input->post('pickupid');
                  $campusid1 = $this->input->post('dropoffid');
                  $name = $this->input->post('name');
                  $description = $this->input->post('description');
                  $id = $this->input->post('id');
                  $pid = $this->input->post('pid');
               
              if ($id[0] == 'P') {
                  echo "bookingperiode";
                  $id = str_replace('P','',$id);
                  $pid = str_replace('H','',$pid);
                  // $date = date('l');
                  $data=array(
                  'date'=>$date,
                  'shiftid'=>$shiftid,
                  'userid'=>$userid,
                  'totalorder'=>$totalorder,
                  'pickupid' => $campusid,
                  'dropoffid' => $campusid1,
                  'name' => $name,
                  'description' => $description,
                  'bookingperiodeid' => $id,
                  'headershuttleid' => $pid
             

                );
                 $this->db->select($data); 
                 $this->db->insert('trupdateperiode', $data);
                // print_r($data);

              } else if ($id[0] == 'T') {
                echo "tr booking";
                  $id = str_replace('T','',$id);
                  $data2=array(
                  'status'=> 'Inactive'
                );
                  $this->db->where('trbookingid',$id);
                  $this->db->update('trbooking',$data2);
                    // print_r($data2);
               }
               }

               
            public function get_queryall(){
            $query = $this->db->query('SELECT CONCAT("P",mbp.bookingperiodeid) as id,CONCAT("H",mbp.headershuttleid) as pid,date, ms.shift,ms.shiftid, mu.nama, mu.email, mu.phone, mcp.campusid as pickupid,mcd.campusid as dropoffid ,mcp.campusname as pickupLoc, mcd.campusname as dropoffLoc,mbp.name, mbp.description,mbp.bookingby,mu.userid,mbp.totalorder,mbp.status
            FROM msbookingperiode mbp JOIN msuser mu ON mbp.userid = mu.userid 
                                JOIN msshift ms ON ms.shiftid = mbp.shiftid 
                                JOIN mscampus mcp ON mbp.pickupid = mcp.campusid 
                                JOIN mscampus mcd ON mbp.dropoffid = mcd.campusid
                                JOIN headershuttle hs ON mbp.headershuttleid = hs.headershuttleid
                                JOIN trdetailshuttle tds ON tds.headershuttleid = hs.headershuttleid  

            WHERE date NOT IN
             (
              SELECT date
              FROM msbookingperiode mbp2 JOIN trupdateperiode tp on mbp2.bookingperiodeid = tp.bookingperiodeid 
              WHERE mbp.bookingperiodeid = mbp2.bookingperiodeid 
             )

            UNION

            SELECT  CONCAT("T",tb.trbookingid) as id,CONCAT("TRD",tb.trdetailshuttleid) as pid,date, ms.shift,ms.shiftid, mu.nama, mu.email, mu.phone, mcp.campusid as pickupid,mcd.campusid as dropoffid,mcp.campusname as pickupLoc,mcd.campusname as dropoffLoc,tb.name, tb.description,tb.bookingby,mu.userid,tb.totalorder,tb.status
            FROM trbooking tb JOIN msuser mu ON tb.userid = mu.userid
                                JOIN msshift ms ON ms.shiftid = tb.shiftid
                                JOIN mscampus mcp ON tb.pickupid = mcp.campusid
                                JOIN mscampus mcd ON tb.dropoffid = mcd.campusid
            WHERE tb.status = "Active"
            ');

            return $query;          
        }
        }
     



