<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_subject extends CI_Model{
	public function get_subject(){
		$data = array(
			'ms.subjectid',
			'ms.subject',
			'mc.categoryid',
			'mc.category'
		);
		$this->db->select($data);
		$this->db->from('mssubject ms');
        $this->db->join('mscategory mc','mc.categoryid=ms.categoryid');
        $this->db->where('ms.status','Active')->where('mc.category !=',"Others")->where('mc.status','Active');
		return $this->db->get();
	}

	public function get_category(){
		$data = array(
			'categoryid',
			'category',
			'status'
		);
		$this->db->select($data);
		$this->db->from('mscategory');
        $this->db->where('status','Active')->where('category !=',"Others");
		return $this->db->get();
	}

	public function insert(){
		$data = array(
			'categoryid' => $this->input->post('category'),
			'subject' => $this->input->post('subject'),
			'status' => "Active"
		);
		$this->db->insert('mssubject',$data);
	}

	public function update(){
		$data = array(
			'categoryid' => $this->input->post('category'),
			'subject' => $this->input->post('subject')
		);
		$this->db->where('subjectid',$this->input->post('SubjectId'));
		$this->db->update('mssubject',$data);
	}

	    public function cekvalidasi($tempcategory=''){
        $tempcategory = $this->input->post('category');
        $data = $this->model->insert($tempcategory);
        echo json_encode($tempcategory);
        // redirect('index.php/tambahshift');
    }

	public function delete(){
		$data = array(
			'status' => 'Inactive'
		);
		$this->db->where('subjectid',$this->input->post('delete'));
		$this->db->update('mssubject',$data);
	}
}