<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Model_driver extends CI_Model {

        function __construct(){
        parent::__construct();
    }

        public function cek_driver($id = ''){
        $data = array(
             
            'driverid',
            'drivername',
            'dob',
            'address',
            'phone'
            

        );

        $this->db->select($data);
        $this->db->from('msdriver');
          $this->db->where('status', 'Active');


        if( $id != '' )
            $this->db->where('driverid', $id);

        return $this->db->get();
    }

    public function insert($drivername='',$dob='',$address='',$phone=''){
        $data = array(
        
           'drivername' => $this->input->post('drivername'),
           'dob' => date('Y/m/d', strtotime($this->input->post('dob'))),
           'address' => $this->input->post('address'),
           'phone' => $this->input->post('phone'),
          
        );
            
    
        $this->db->trans_begin();
        $this->db->insert('msdriver', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

         public function update($drivername='',$dob='',$address='',$phone=''){
        $data = array(
            
           'drivername' => $this->input->post('drivername'),
           'dob' => date('Y/m/d ', strtotime($this->input->post('dob'))),
           'address' => $this->input->post('address'),
           'phone' => $this->input->post('phone'),
          
        );

        $this->db->trans_begin();
        $this->db->where('driverid', $this->input->post('driverId'));
        $this->db->update('msdriver', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
                        

    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();

        $this->db->where('driverid', $this->input->post('driverId'));
        $this->db->update('msdriver',$data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
} 
