<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_shift extends CI_Model{

    public function get_shifts($id = ''){
        $data = array(
            'shiftid',
            'shift',
            'shiftType'
        );

        $this->db->select($data);
        $this->db->from('msshift');
        $this->db->where('status', 'Active');

        if( $id != '' )
            $this->db->where('shiftid', $id);

        return $this->db->get();
    }

    public function insert($tempshift=''){
        $data = array(
            'shift' => $tempshift
        );

        $this->db->trans_begin();
        $this->db->insert('msshift', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

     public function update(){
        $shift = $this->input->post('shift');
        $shiftid = $this->input->post('Shiftid');

        if (!empty($shift)){
            if(!empty($shiftid)){
                $this->db->trans_begin();
                $this->db->set('shift',$shift);
                $this->db->where('shiftid', $shiftid);
                $this->db->update('msshift');

                if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    return true;
                } else {
                    $this->db->trans_rollback();
                    return false;
                }   
            }
        }       
   }

    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();

        $this->db->where('shiftid', $this->input->post('Shiftid'));
        $this->db->update('msshift',$data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
}