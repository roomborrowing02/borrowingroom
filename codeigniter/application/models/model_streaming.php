<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_streaming extends CI_Model{

    public function get_data($id = ''){
        $data = array(
            'streamingid',
            'content',
            'description',
        );

        $this->db->select($data);
        $this->db->from('streaming');
        $this->db->where('status', 'Active');

        if( $id != '' )
            $this->db->where('streamingid', $id);

        return $this->db->get();
    }
    public function fullscreen($streamingid){
        $data = array(
            'streamingid',
            'content',
            'description',
        );

        $this->db->select($data);
        $this->db->from('streaming');
        $this->db->where('status', 'Active')->where('streamingid',$streamingid);

        $query = $this->db->get();
        return $query->result();
    }

    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();

        $this->db->where('pengumumanid', $this->input->post('DeleteId'));
        $this->db->update('mspengumuman',$data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }


}