<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_admin extends CI_Model {

		function __construct(){
        parent::__construct();
    }

		public function cek_terms($id = ''){
        $data = array(
            'userid',
            'nim',
            'username',
            'nama',
            'email',
            'password',
        );

        $this->db->select($data);
        $this->db->from('msuser');
        $this->db->where('status','Active');
        if( $id != '' )
            $this->db->where('userid', $id);

        return $this->db->get();
    }

        public function update(){
        $data = array(
            'nim' => $this->input->post('nim'),
            'username' => $this->input->post('username'),
            'nama' => $this->input->post('nama'),
            'email' => $this->input->post('email'),
            'roleid' => $this->input->post('role')
        );

        $this->db->trans_begin();
        $this->db->where('userid', $this->input->post('updateuser'));
        $this->db->update('msuser', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

        public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();
        $this->db->where('vehicleid', $this->input->post('vehicleId'));
        $this->db->update('msvehicle', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

}