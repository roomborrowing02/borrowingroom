<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Model_link extends CI_Model 
    {
        function __construct(){
        parent::__construct();
    }
        public function cek_link($id = ''){
        $data = array(
            'msl.linkid',
            'name',
            'description',
            'link',
            'status'
        );

        $this->db->select($data);
        $this->db->from('mslink msl');
        
        if( $id != '' )
            $this->db->where('linkid', $id);

        return $this->db->get();
    }

  //untuk passing data dari controller dan insert ke table mslink//
   public function insert($name='',$description='',$link=''){
        $data = array(
           'name' => $this->input->post('name'),
           'description' => $this->input->post('description'),
           'link' => $this->input->post('link')

        );

        $this->db->trans_begin();
        $this->db->insert('mslink', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
 
    //untuk passing data dari controller dan update ke table mslink//
    public function update($name='',$description='',$link=''){
          $data = array(
          'name' => $this->input->post('name'),
          'description' => $this->input->post('description'),
          'link' => $this->input->post('link')
        );
          
        $this->db->trans_begin();
        $this->db->where('linkid', $this->input->post('UpdateLink'));
        $this->db->update('mslink', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    //untuk delete table msitem secara soft delete//
    public function delete(){
        $data = array( 'status' => 'Expired' );

        $this->db->trans_begin();
        $this->db->where('linkid', $this->input->post('linkid'));
        $this->db->update('mslink', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    //untuk delete table msitem secara soft delete//
    public function activate(){
        $data = array( 'status' => 'Active' );

        $this->db->trans_begin();
        $this->db->where('linkid', $this->input->post('activeName'));
        $this->db->update('mslink', $data);
        
        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

}