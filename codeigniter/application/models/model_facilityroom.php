<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_facilityroom extends CI_Model {
        function __construct(){
        parent::__construct();
    }

    public function get_campuscode(){        
        $this->db->select('*');
        $this->db->from('mscampus');
        $this->db->where('status', 'Active');

        $query = $this->db->get();

        return $query->result();
    }

    public function get_dataspec($specification=''){        
        $this->db->select('*');
        $this->db->from('msitem');
        $this->db-> where ('itemid', $specification) -> where('status', 'Active');

        return $this->db->get();
    }
    
    public function get_dataspecupdate($specification=''){        
        $this->db->select('*');
        $this->db->from('msitem');
        $this->db-> where ('itemid', $specification) -> where('status', 'Active');

        return $this->db->get();
    }

    public function get_itemname(){        
        $this->db->select('*');
        $this->db->from('msitem');
        $this->db->where('status', 'Active');

        return $this->db->get();
    }
    public function get_itemdata(){        
        $this->db->select('*');
        $this->db->like('itemname', $q);
        $query = $this->db->get('msitem');
        if($query->num_rows() > 0){
             $list = array();
             $key=0;
             while($row = $result->fetch_assoc()) {
                 $list[$key]['id'] = $row['itemid'];
                 $list[$key]['text'] = $row['itemname']; 
             $key++;
             }
             echo json_encode($list);
            echo json_encode($row_set); //format the array into json data
        }
    }

    public function get_room($facilityid=''){        
        $this->db->select('*');
        $this->db->from('msfacility');
        $this->db->where('status', 'Active')->where('facilityid',$facilityid);
        return $this->db->get();

    }

    public function cek_facility($id = ''){
        $data = array(
            'msf.facilityid',
            'msc.campusid as campusid',
            'facilityname',
            'capacity',
            'description',
            'msc.campuscode',
            'msf.status'
        );

        $this->db->select($data);
        $this->db->from('msfacility msf');
        $this->db->join('mscampus msc','msc.campusid=msf.campusid');
        $this->db->where('msf.status', 'Active');

        if( $id != '' )
            $this->db->where('facilityid', $id);

        return $this->db->get();
    }

      public function insert($facilityname='',$campusid='',$capacity='',$description=""){
       $data = array(
           'facilityname' => $this->input->post('facilityname'),
           'campusid' => $this->input->post('campuscode'),
           'capacity' => $this->input->post('capacity'),
           'description' => $this->input->post('description'),
          
        );           
    
        $this->db->trans_begin();
        $this->db->insert('msfacility', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function insertDetail($facilityid ='',$itemname='',$qty='',$condition='',$specification=''){
        $this->db->set('itemid',$this->input->post('itemname'));
        $this->db->set('facilityid',$this->input->post('facility'));
        $this->db->set('qty',$this->input->post('qty'));
        $this->db->set('condition',$this->input->post('condition'));

        $this->db->set('status', "Active");
        $this->db->where('trfacilityitem', $this->input->post('insertDetail'));      
        $this->db->insert('trfacilityitem');

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }


    public function update($facilityname='',$campusid='',$capacity='',$description=""){
      $data = array(
           'facilityname' => $this->input->post('facilityname'),
           'campusid' => $this->input->post('campusid'),
           'capacity' => $this->input->post('capacity'),
           'description' => $this->input->post('description'),
        );

        $this->db->trans_begin();
        $this->db->where('facilityId', $this->input->post('facilityId'));
        $this->db->update('msfacility', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function updateDetail($facilityid ='',$itemname='',$qty='',$condition='',$specification=''){
        $this->db->set('itemid',$this->input->post('itemnameupdate'));
        $this->db->set('qty',$this->input->post('qty'));
        $this->db->set('condition',$this->input->post('condition'));

        $this->db->where('trfacilityitemid', $this->input->post('updatedetail'));
        $this->db->update('trfacilityitem');

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
                        
    public function view($facilityid=''){
            $data = array(
            'tfi.trfacilityitemid',
            'tfi.facilityid',
            // 'tfi.description',
            'msf.facilityname',
            'msi.itemid',
            'msi.itemname as itemname',
            'msi.specification as specification',
            'tfi.qty',
            'tfi.condition',
            // 'tfi.content',
            'tfi.status'
        );

        $this->db->select($data);
        $this->db->from('trfacilityitem tfi');
        $this->db->join('msitem msi','msi.itemid=tfi.itemid');
        $this->db->join('msfacility msf','msf.facilityid=tfi.facilityid');
        $this->db->where('tfi.facilityid',$facilityid)->where('tfi.status', 'Active');

        return $this->db->get();
    }

    public function image($facilityid=''){
            $data = array(
                'tri.trfacilityimageid',
                'tri.facilityid',
                'tri.content',
                'tri.description',
                'tri.status'
        );
            $this->db->select($data);
            $this->db->from('trfacilityimage tri');
            $this->db->join('msfacility msf','msf.facilityid=tri.facilityid');
            $this->db->where('tri.facilityid',$facilityid)->where('tri.status','Active');
            
            return $this->db->get();
    }

    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();
        $this->db->where('facilityid', $this->input->post('deletefacilityId'));
        $this->db->update('msfacility', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

     public function deleteDetail(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();
        $this->db->where('trfacilityitemid', $this->input->post('deletedetail'));
        $this->db->update('trfacilityitem', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function deleteImg(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();
        $this->db->where('trfacilityimageid', $this->input->post('ImgDelete'));
        $this->db->update('trfacilityimage',$data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
}
