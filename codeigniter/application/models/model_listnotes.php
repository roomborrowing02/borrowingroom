<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_listnotes extends CI_Model{

	public function getnotes(){
		$data = array(
			'mn.notesid',
			'mn.date',
			'mn.others',
			'mu.userid',
			'mu.nama as usernama',
			'mu2.userid',
			'mu2.nama as notesuser',
			'mc.categoryid',
			'mc.category',
			'ms.subjectid',
			'ms.subject',
			'mn.roomid',
			'mf.facilityname',
			'mn.description',
			'mn.solution',
			'mn.dateline',
			'mn.status'
		);
		$this->db->select($data);
		$this->db->from('msnotes mn');
        $this->db->join('msuser mu','mu.userid=mn.userid');
        $this->db->join('msuser mu2','mu2.userid=mn.notesuserid');
        $this->db->join('mscategory mc','mc.categoryid=mn.categoryid');
        $this->db->join('mssubject ms','ms.subjectid=mn.subjectid');
        $this->db->join('msfacility mf','mf.facilityid=mn.roomid','left');
        $this->db->where('mn.status !=',"Inactive");
        return $this->db->get();
	}

	public function delete(){
		$data = array(
			'status' => "Inactive"
		);
		$this->db->where('notesid',$this->input->post('Notesid'));
		$this->db->update('msnotes',$data);
	}
}