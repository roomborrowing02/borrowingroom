<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Model_lecturer extends CI_Model {

        function __construct(){
        parent::__construct();
    }


        public function getLecturer($id = ''){
        $data = array(
            'kddsn',
            'nmKTP',
            'telrm',
            'hp',
            'updatedhp',
            'email'
        );

        $this->db->select($data);
        $this->db->from('mslecturer');
        $this->db->where('status', 'Active');
        $this->db->where('nmstat', 'Aktif');


        if( $id != '' )
            $this->db->where('driverid', $id);

        return $this->db->get();
    }
    public function getLecturerName($id){
        $data = array('nmKTP');

        $this->db->select($data);
        $this->db->from('mslecturer');
        $this->db->where('kddsn', $id);
        $this->db->where('status', 'Active');
        return $this->db->get();
    }

    public function insert($drivername='',$driverdob='',$driveraddress='',$driverphone=''){
        $data = array(
        
            'drivername' => $drivername,
            'driverdob' => $driverdob,
            'driveraddress' => $driveraddress,
            'driverphone' => $driverphone
           
        );
            
    
        $this->db->trans_begin();
        $this->db->insert('msdriver', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

         public function update($driverid2='',$drivername2='',$driverdob2='',$driveraddress2='',$driverphone2=''){
        $data = array(
            'driverid2' => $driverid2,
            'drivername2' => $drivername2,
            'driverdob2' => $driverdob2,
            'driveraddress2' => $driveraddress2,
            'driverphone2' => $driverphone2
        );
        $this->db->trans_begin();
        $this->db->where('driverid', $this->input->post('driverId'));
        $this->db->update('msdriver', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();

        $this->db->where('driverid', $this->input->post('driverId'));
        $this->db->update('msdriver',$data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
} 
