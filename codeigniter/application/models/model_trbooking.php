<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_trbooking extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    public function get_pickup(){
        $data = array(
        'msc.campuscode as pickupLoc',
        'msc.campusid as pickupid'
        );
        $this->db->select($data);
        $this->db->from('mscampus msc');
        $this->db->where('status','Active');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_term(){
        $data = array(
        'mt.termcode',
        'mt.description',
        'cm.termid'
        );
        $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
        $this->db->group_by('cm.termid');       
        $this->db->where('cm.status','Active');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_datauser($datauser=''){
        $data = array(
        'userid',
        'username',
        'nama',
        'email',
        'phone'      
         );
        $this->db->select($data);
        $this->db->from('msuser');
        $this->db->where('userid',$datauser)->where('status','Active');
        // $this->db->where('status','Active');
        return $this->db->get();

    }

    public function get_campus($pickupdata=''){
        $data = array(
        'msc2.campusid',
        'msc2.campusname',
        'msc2.campuscode'       
         );
        $this->db->select($data);
        $this->db->from('trdetailshuttle trd');
        $this->db->join('msshift ms','ms.shiftid=trd.shiftid');
        $this->db->join('mscampus msc','msc.campusid=trd.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=trd.dropoffid');
        $this->db->join('msvehicle msv','msv.vehicleid=trd.vehicleid');
        $this->db->group_by('msc2.campusid');
        $this->db->where('trd.pickupid',$pickupdata)->where('trd.status','Active');
        return $this->db->get();

    }

    public function get_data2($pickupdata='',$dropoffdata='',$tempdate=''){
      $data = array(
        'trd.date',
        'trd.status'
      );

      $this->db->select($data);
      $this->db->from('trdetailshuttle trd');
      $this->db->join('msshift ms','ms.shiftid=trd.shiftid');
      $this->db->join('mscampus msc','msc.campusid=trd.pickupid');
      $this->db->join('mscampus msc2','msc2.campusid=trd.dropoffid');
      $this->db->join('msvehicle msv','msv.vehicleid=trd.vehicleid');
      $this->db->where('trd.pickupid', $pickupdata)->where('trd.dropoffid',$dropoffdata)->where('trd.date >=',$tempdate)->where('trd.status','Active');

      return $this->db->get();
    }

    public function get_data3($pickupdata='',$dropoffdata='',$tanggal=''){
        $data = array(
          'trd.shiftid',
          'ms.shift',
          'trd.status'
        );

        $this->db->select($data);
        $this->db->from('trdetailshuttle trd');
        $this->db->join('msshift ms','ms.shiftid=trd.shiftid');
        $this->db->join('mscampus msc','msc.campusid=trd.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=trd.dropoffid');
        $this->db->join('msvehicle msv','msv.vehicleid=trd.vehicleid');
        $this->db->group_by('trd.shiftid');
        $this->db->where('trd.pickupid', $pickupdata)->where('trd.dropoffid',$dropoffdata)->where('trd.status','Active')->where('trd.date',$tanggal);

        return $this->db->get();
    }

    public function get_data4($pickupdata='',$dropoffdata='',$tanggal='',$tempshift=''){
        $data = array(
          'msv.vehicleid',
          'msv.vehiclename',
          'msv.vehicleplate',
          'msv.capacity',
          'trd.status'
        );

        $this->db->select($data);
        $this->db->from('trdetailshuttle trd');
        $this->db->join('msshift ms','ms.shiftid=trd.shiftid');
        $this->db->join('mscampus msc','msc.campusid=trd.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=trd.dropoffid');
        $this->db->join('msvehicle msv','msv.vehicleid=trd.vehicleid');
        $this->db->group_by('msv.vehicleid');
        $this->db->where('trd.pickupid', $pickupdata)->where('trd.dropoffid',$dropoffdata)->where('msv.status','Active')->where('trd.date',$tanggal)->where('trd.shiftid',$tempshift);

        return $this->db->get();
    }
    function cek1($data,$vehicledata){
        $this->db->select($data);
        $this->db->from('msvehicle');
        $this->db->where('vehicleid',$vehicledata)->where('status','Active');
        $query = $this->db->get();
        $tempData = $query->result();
        $tempcapacity = array();
        foreach ($tempData as $temp) {
            $tempcapacity[] = $temp->capacity;
        }
        return $tempcapacity;
    }

    function cek2($vehicledata,$pickupdata,$dropoffdata,$tempshift){
        $data = array('cm.headershuttleid');
        $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');       
        $this->db->where('cm.status','Active')->where('cm.vehicleid',$vehicledata)->where('cm.pickupid',$pickupdata)->where('cm.dropoffid',$dropoffdata)->where('cm.shiftid',$tempshift);
        $query = $this->db->get();
        return $query->result();
    }

    function cek3($check2,$vehicledata,$pickupdata,$dropoffdata,$tempshift,$tanggal){
        $data = array(
          'trd.headershuttleid',
          'trd.trdetailshuttleid',
          'trd.shiftid',
          'trd.pickupid',
          'trd.dropoffid',
          'trd.vehicleid'
        );

        $this->db->select($data);
        $this->db->from('trdetailshuttle trd');
        $this->db->join('msshift ms','ms.shiftid=trd.shiftid');
        $this->db->join('mscampus msc','msc.campusid=trd.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=trd.dropoffid');
        $this->db->join('msvehicle msv','msv.vehicleid=trd.vehicleid');
        foreach ($check2 as $temp) {
            $this->db->where('trd.headershuttleid',$temp->headershuttleid)->where('trd.pickupid', $pickupdata)->where('trd.dropoffid',$dropoffdata)->where('trd.date',$tanggal)->where('trd.shiftid',$tempshift)->where('trd.vehicleid',$vehicledata);
        }
       
        $query = $this->db->get();
        return $query->result();
    }

    function cek4($check3,$tanggal,$check2){
        $bb = array();
        foreach ($check2 as $key => $aa) {
            $bb[] = $aa->headershuttleid;
        }
        $cc = ['headershuttleid'=>$bb];
         $data = array('totalorder');
         $this->db->select($data);
        $this->db->from('msbookingperiode');
        foreach ($cc as $key => $dd) {
            $this->db->where_in($key,$dd);
        }
        foreach ($check3 as $temp2) {
            $this->db->where('shiftid',$temp2->shiftid)->where('pickupid',$temp2->pickupid)->where('dropoffid',$temp2->dropoffid);
        }

        $query = $this->db->get();
        $tempData = $query->result();
        $tempperiode = array();
        foreach ($tempData as $temp3) {
            $tempperiode[] =  $temp3->totalorder;
        }
        $allperiode = array_sum($tempperiode);
        return $allperiode;

    }

    function cek5($check3,$tanggal){
        $data = array('totalorder');
        $this->db->select($data);
        $this->db->from('trbooking');
        foreach ($check3 as $temp) {
            $this->db->where('date',$tanggal)->where('pickupid',$temp->pickupid)->where('dropoffid',$temp->dropoffid)->where('shiftid',$temp->shiftid)->where('trdetailshuttleid',$temp->trdetailshuttleid);
        }
        $query = $this->db->get();
        $tempData = $query->result();
        $tempdate = array();
        foreach ($tempData as $temp3) {
            $tempdate[] =  $temp3->totalorder;
        }
        $alldateperiode = array_sum($tempdate);
        return $alldateperiode;
        // print_r($alldateperiode);
    }
    function cek6($cek2){
        $this->db->select('*');
        $this->db->from('trupdateperiode tup');
        $this->db->join('msbookingperiode mbp','mbp.bookingperiodeid=tup.bookingperiodeid');
        $this->db->join('msshift s','s.shiftid=tup.shiftid');
        $this->db->join('mscampus msc','msc.campusid=tup.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=tup.dropoffid');
        foreach ($cek2 as $hs) {
            $this->db->where('tup.headershuttleid',$hs->headershuttleid);
        }

        $query = $this->db->get();
        return $query->result();
    }

    function cek7($check,$check4,$check5,$check6,$check2,$tanggal){
        $tempcapacity = array();
        $bookingperiode = $check4 ? $check4 : 0;
        $bookingdate = $check5 ? $check5 : 0;
        $capacity = $check[0];
        // print_r($bookingperiode);
        // print_r($bookingdate);
        // print_r($capacity);
        if (empty($check6)) {
            $tempcapacity[] = $capacity-$bookingdate-$bookingperiode;
        }
        else{
            foreach ($check6 as $temp) {
                if ($temp->date == $tanggal) {
                    $tempcapacity[] = $capacity-$bookingdate-$bookingperiode+$temp->totalorder;
                }
                else{
                    $tempcapacity[] = $capacity-$bookingdate-$bookingperiode;
                }
            }
        }
        



        return $tempcapacity;
        // print_r($tempcapacity);
    }

    public function get_data5($vehicledata='',$pickupdata='',$dropoffdata='',$tanggal='',$tempshift=''){
        $data = array('capacity');
        $check = $this->cek1($data,$vehicledata);
        $check2 = $this->cek2($vehicledata,$pickupdata,$dropoffdata,$tempshift);
        $check3 = $this->cek3($check2,$vehicledata,$pickupdata,$dropoffdata,$tempshift,$tanggal);
        $check4 = $this->cek4($check3,$tanggal,$check2);
        $check5 = $this->cek5($check3,$tanggal);
        $check6 = $this->cek6($check2);
        $check7 = $this->cek7($check,$check4,$check5,$check6,$check2,$tanggal);
        // print_r($check);
        // print_r($check2);
        // print_r($check3);
        // print_r($check4);
        // print_r($check5);
        // print_r($check6);
        // print_r($check7);
        // $tempcapacity = array();
        // $tempcapacity[] = $check[0]-$a-$b;
        // return $tempcapacity;
        return $check7;
    }

    public function get_data6($vehicledata='',$pickupdata='',$dropoffdata='',$tanggal='',$tempshift=''){
        $data = array('trd.trdetailshuttleid');
        $this->db->select($data);
        $this->db->from('trdetailshuttle trd');
        $this->db->join('msshift ms','ms.shiftid=trd.shiftid');
        $this->db->join('mscampus msc','msc.campusid=trd.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=trd.dropoffid');
        $this->db->join('msvehicle msv','msv.vehicleid=trd.vehicleid');     
        $this->db->where('trd.status','Active')->where('trd.vehicleid',$vehicledata)->where('trd.pickupid',$pickupdata)->where('trd.dropoffid',$dropoffdata)->where('trd.shiftid',$tempshift)->where('trd.date',$tanggal);
        $query = $this->db->get();
        return $query->result();
    }


    public function getdata1($termdata=''){
        $data = array(
        'msc.campusid',
        'msc.campusname',
        'msc.campuscode'       
         );
        $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
        $this->db->group_by('cm.pickupid');       
        $this->db->where('cm.termid',$termdata)->where('cm.status','Active');
        // $this->db->where('status','Active');
        return $this->db->get();

    }

    public function getdata2($pickupdata='',$termdata=''){
        $data = array(
        'msc2.campusid',
        'msc2.campusname',
        'msc2.campuscode'       
         );
        $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
        $this->db->group_by('cm.dropoffid');       
        $this->db->where('cm.termid',$termdata)->where('cm.dropoffid !=',$pickupdata)->where('cm.status','Active');
        // $this->db->where('status','Active');
        return $this->db->get();

    }
    public function getdata3($pickupdata='',$dropoffdata='',$termdata=''){
      $data = array(
        'cm.day',
        'cm.status'
      );

      $this->db->select($data);
      $this->db->from('headershuttle cm');
      $this->db->join('msterm mt','mt.termid=cm.termid');
      $this->db->join('msshift s','s.shiftid=cm.shiftid');
      $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
      $this->db->join('msdriver msd','msd.driverid=cm.driverid');
      $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
      $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
      $this->db->group_by('cm.day');
      $this->db->where('cm.status','Active')->where('cm.termid',$termdata)->where('cm.pickupid',$pickupdata)->where('cm.dropoffid',$dropoffdata);

      return $this->db->get();
  }

    public function getdata4($pickupdata='',$dropoffdata='',$hari='',$termdata=''){
        $data = array(
          'cm.shiftid',
          's.shift',
          'cm.status'
        );

        $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
        $this->db->group_by('cm.shiftid');       
        $this->db->where('cm.pickupid', $pickupdata)->where('cm.dropoffid',$dropoffdata)->where('cm.status','Active')->where('cm.day',$hari)->where('cm.termid',$termdata);

        return $this->db->get();
    }


    public function getdata5($pickupdata='',$dropoffdata='',$hari='',$tempshift='',$termdata=''){
        $data = array(
          'cm.vehicleid',
          'msv.vehiclename',
          'msv.vehicleplate',
          'msv.capacity'
        );

        $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
        $this->db->group_by('cm.vehicleid');       
        $this->db->where('cm.pickupid', $pickupdata)->where('cm.dropoffid',$dropoffdata)->where('cm.status','Active')->where('cm.day',$hari)->where('cm.termid',$termdata);

        return $this->db->get();

    }

        function cekdata0($data,$pickupdata,$dropoffdata,$hari,$tempshift,$termdata,$vehicledata){
        $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
        $this->db->where('cm.status','Active')->where('cm.vehicleid',$vehicledata)->where('cm.pickupid',$pickupdata)->where('cm.dropoffid',$dropoffdata)->where('cm.termid',$termdata)->where('cm.shiftid',$tempshift);

        $query = $this->db->get();
        return $query->result();
        // return $this->db->get();
    }

    public function cekdata1($check,$pickupdata,$dropoffdata,$hari,$tempshift,$termdata,$vehicledata){
        $data = array('trd.trdetailshuttleid','trd.headershuttleid','trd.date');
        $this->db->select($data);
        $this->db->from('trdetailshuttle trd');
        $this->db->join('msshift ms','ms.shiftid=trd.shiftid');
        $this->db->join('mscampus msc','msc.campusid=trd.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=trd.dropoffid');
        $this->db->join('msvehicle msv','msv.vehicleid=trd.vehicleid');
        $tempheader = array();
        foreach ($check as $key => $temp) {
            $tempheader[] = $temp->headershuttleid;
        }
        $tempheadershuttle = ['headershuttleid' => $tempheader];
        foreach ($tempheadershuttle as $key => $temp2) {
            $this->db->where_in($key,$temp2);
            $this->db->where('trd.pickupid',$pickupdata)->where('trd.dropoffid',$dropoffdata)->where('trd.shiftid',$tempshift)->where('trd.vehicleid',$vehicledata);
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function cekdata2($check){
        $bb = array();
        foreach ($check as $key => $aa) {
            $bb[] = $aa->headershuttleid;
        }
        $cc = ['headershuttleid'=>$bb];
         $data = array('totalorder','headershuttleid');
         $this->db->select($data);
        $this->db->from('msbookingperiode');
        foreach ($cc as $key => $dd) {
            $this->db->where_in($key,$dd);
        }
        $query = $this->db->get();
        $tempData = $query->result();
        $temparray = array();
        foreach ($tempData as $temp) {
            $temparray[] = $temp->totalorder;
        }
        // return $temparray;
        $temphasilarray = array_sum($temparray);
        $hasilarray = $temphasilarray ? $temphasilarray : 0;
        // print_r($hasilarray);
        return $hasilarray;
    }

    function cekdata3($check1,$check2,$check){
        $bb = array();
        foreach ($check1 as $key => $aa) {
            $bb[] = $aa->trdetailshuttleid;
        }
        $cc = ['trdetailshuttleid'=>$bb];
         $data = array('totalorder','trdetailshuttleid');
         $this->db->select($data);
        $this->db->from('trbooking');
        foreach ($cc as $key => $dd) {
            $this->db->where_in($key,$dd);
        }
        $query = $this->db->get();
        $tempData = $query->result();
        $temparray;

        if (empty($tempData)) {
            foreach ($check as $temp) {
                foreach ($check1 as $temp1) {
                        $temparray[] = array('headershuttleid'=>$temp->headershuttleid,'vehicleid'=>$temp->vehicleid,'vehiclename'=>$temp->vehiclename,'vehicleplate'=>$temp->vehicleplate,'capacity'=>$temp->capacity - $check2,'date'=>$temp1->date);
                }
            }
        }else{
        $b = array ();
        foreach ($tempData as $key)
        {
            if (isset($b[$key->trdetailshuttleid]))
            {
                $b[$key->trdetailshuttleid] += $key->totalorder;
            }
            else
            {
                $b[$key->trdetailshuttleid] = $key->totalorder;
            }
        }
        
        foreach ($check as $temp) {
            foreach ($check1 as $temp1) {
                foreach ($b as $trdetailshuttleid => $totalorder) {
                    if ($temp1->trdetailshuttleid == $trdetailshuttleid) {
                        $temparray[] = array('headershuttleid'=>$temp->headershuttleid,'trdetailshuttleid'=>$trdetailshuttleid,'vehicleid'=>$temp->vehicleid,'vehiclename'=>$temp->vehiclename,'vehicleplate'=>$temp->vehicleplate,'capacity'=>$temp->capacity - $totalorder - $check2,'date'=>$temp1->date);
                    }
                }
            }
        }
    }
        return $temparray;
    }

    function cekdata4($check3){
        $this->db->select('*');
        $this->db->from('trupdateperiode tup');
        $this->db->join('msbookingperiode mbp','mbp.bookingperiodeid=tup.bookingperiodeid');
        $this->db->join('msshift s','s.shiftid=tup.shiftid');
        $this->db->join('mscampus msc','msc.campusid=tup.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=tup.dropoffid');
        $query = $this->db->get();
        return $query->result();
    }

    function cekdata5($check3,$check4){
        foreach ($check3 as $key => $value) {
            if (empty($check4)) {
                // print_r("kosong");
                $temparray[] = array(
                    'headershuttleid'=>$value['headershuttleid'],
                    'vehicleid'=>$value['vehicleid'],
                    'capacity'=>$value['capacity'],
                    'date'=>date('M-d-Y',strtotime($value['date'])),
                    'vehiclename'=>$value['vehiclename'],
                    'vehicleplate'=>$value['vehicleplate'],
                );
            }
            else{
                foreach ($check4 as $temp) {
                    if ($value['headershuttleid'] == $temp->headershuttleid) {
                        if ($value['date'] == $temp->date) {
                            $temparray[] = array(
                                'headershuttleid'=>$value['headershuttleid'],
                                'vehicleid'=>$value['vehicleid'],
                                'capacity'=>$value['capacity'] + $temp->totalorder,
                                'date'=>date('M-d-Y',strtotime($value['date'])),
                                'vehiclename'=>$value['vehiclename'],
                                'vehicleplate'=>$value['vehicleplate'],
                            );
                        }
                        else{
                            $temparray[] = array(
                                'headershuttleid'=>$value['headershuttleid'],
                                'vehicleid'=>$value['vehicleid'],
                                'capacity'=>$value['capacity'],
                                'date'=>date('M-d-Y',strtotime($value['date'])),
                                'vehiclename'=>$value['vehiclename'],
                                'vehicleplate'=>$value['vehicleplate'],
                            );
                        }
                    }
                }
            }
        }
        
        
        // print_r($temparray);
        return $temparray;
    }

    public function getdata6($vehicledata='',$hari='',$dropoffdata ='',$pickupdata='',$tempshift='',$termdata=''){
         $data = array(
            'cm.headershuttleid',
            'cm.vehicleid',
            'msv.vehiclename',
            'msv.vehicleplate',
            'msv.capacity'
        );

        $check = $this->cekdata0($data,$pickupdata,$dropoffdata,$hari,$tempshift,$termdata,$vehicledata);
        $check1 = $this->cekdata1($check,$pickupdata,$dropoffdata,$hari,$tempshift,$termdata,$vehicledata);
        $check2 = $this->cekdata2($check);
        $check3 = $this->cekdata3($check1,$check2,$check);
        $check4 = $this->cekdata4($check3);
        $check5 = $this->cekdata5($check3,$check4);

        // print_r($check3);
        $numbers = array_column($check5, 'capacity');
        $min = min($numbers);
        return $min;
    }

    public function getdata7($vehicledata='',$hari='',$dropoffdata ='',$pickupdata='',$tempshift='',$termdata=''){
        $data = array(
            'cm.headershuttleid',
            'cm.vehicleid',
            'msv.vehiclename',
            'msv.vehicleplate',
            'msv.capacity'
        );
         $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
        $this->db->where('cm.status','Active')->where('cm.vehicleid',$vehicledata)->where('cm.pickupid',$pickupdata)->where('cm.dropoffid',$dropoffdata)->where('cm.termid',$termdata)->where('cm.shiftid',$tempshift);

        $query = $this->db->get();
        return $query->result();
    }

    function list($data,$pickupdata,$dropoffdata,$hari,$tempshift,$termdata,$vehicledata){
        $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
        $this->db->where('cm.status','Active')->where('cm.vehicleid',$vehicledata);

        $query = $this->db->get();
        return $query->result();
        // return $this->db->get();
    }

    public function list1($check,$pickupdata,$dropoffdata,$hari,$tempshift,$termdata,$vehicledata){
        $data = array('trd.trdetailshuttleid','trd.headershuttleid','trd.date','msd.drivername','trd.shiftid','trd.pickupid','trd.dropoffid');
        $this->db->select($data);
        $this->db->from('trdetailshuttle trd');
        $this->db->join('msshift ms','ms.shiftid=trd.shiftid');
        $this->db->join('mscampus msc','msc.campusid=trd.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=trd.dropoffid');
        $this->db->join('msvehicle msv','msv.vehicleid=trd.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=trd.driverid');
        $tempheader = array();
        foreach ($check as $key => $temp) {
            $tempheader[] = $temp->headershuttleid;
        }
        $tempheadershuttle = ['headershuttleid' => $tempheader];
        foreach ($tempheadershuttle as $key => $temp2) {
            $this->db->where_in($key,$temp2);
            $this->db->where('trd.pickupid',$pickupdata)->where('trd.dropoffid',$dropoffdata)->where('trd.shiftid',$tempshift)->where('trd.vehicleid',$vehicledata);
        }
        
        $query = $this->db->get();
        return $query->result();
    }


    function list2($check){
        $bb = array();
        foreach ($check as $key => $aa) {
            $bb[] = $aa->headershuttleid;
        }
        $cc = ['headershuttleid'=>$bb];
         $data = array('totalorder','headershuttleid');
         $this->db->select($data);
        $this->db->from('msbookingperiode');
        foreach ($cc as $key => $dd) {
            $this->db->where_in($key,$dd);
        }
        $query = $this->db->get();
        $tempData = $query->result();
        $temparray = array();
        foreach ($tempData as $temp) {
            $temparray[] = $temp->totalorder;
        }
        // return $temparray;
        $temphasilarray = array_sum($temparray);
        $hasilarray = $temphasilarray ? $temphasilarray : 0;
        // print_r($hasilarray);
        return $hasilarray;
    }

    function list3($check1,$check2,$check){
        $bb = array();
        foreach ($check1 as $key => $aa) {
            $bb[] = $aa->trdetailshuttleid;
        }
        $cc = ['trdetailshuttleid'=>$bb];
         $data = array('totalorder','trdetailshuttleid');
         $this->db->select($data);
        $this->db->from('trbooking');
        foreach ($cc as $key => $dd) {
            $this->db->where_in($key,$dd);
        }
        $query = $this->db->get();
        $tempData = $query->result();
        $temparray;
        $maxloop = count($check1);
        $count = 0;
        if (empty($tempData)) {
            foreach ($check as $temp) {
                foreach ($check1 as $temp1) {
                        $temparray[] = array('headershuttleid'=>$temp->headershuttleid,'vehicleid'=>$temp->vehicleid,'vehiclename'=>$temp->vehiclename,'vehicleplate'=>$temp->vehicleplate,'capacity'=>$temp->capacity - $check2,'date'=>$temp1->date,'drivername'=>$temp1->drivername,'day'=>date('l',strtotime($temp1->date)),'shiftid'=>$temp1->shiftid,'pickupid'=>$temp1->pickupid,'dropoffid'=>$temp1->dropoffid);
                }
            }
        }else{
        $b = array ();
        foreach ($tempData as $key)
        {
            if (isset($b[$key->trdetailshuttleid]))
            {
                $b[$key->trdetailshuttleid] += $key->totalorder;
            }
            else
            {
                $b[$key->trdetailshuttleid] = $key->totalorder;
            }
        }

        foreach ($check as $temp) {
            foreach ($check1 as $temp1) {
                foreach ($b as $trdetailshuttleid => $totalorder) {
                    if ($count == $maxloop) {
                        break;
                    }
                    else{
                        if ($temp1->trdetailshuttleid == $trdetailshuttleid) {
                            $temparray[] = array('headershuttleid'=>$temp->headershuttleid,'trdetailshuttleid'=>$trdetailshuttleid,'vehicleid'=>$temp->vehicleid,'vehiclename'=>$temp->vehiclename,'vehicleplate'=>$temp->vehicleplate,'capacity'=>$temp->capacity - $totalorder - $check2,'date'=>$temp1->date,'drivername'=>$temp1->drivername,'day'=>date('l',strtotime($temp1->date)),'shiftid'=>$temp1->shiftid,'pickupid'=>$temp1->pickupid,'dropoffid'=>$temp1->dropoffid);
                                $count++;
                        }
                        else{
                            $temparray[] = array('headershuttleid'=>$temp->headershuttleid,'trdetailshuttleid'=>$trdetailshuttleid,'vehicleid'=>$temp->vehicleid,'vehiclename'=>$temp->vehiclename,'vehicleplate'=>$temp->vehicleplate,'capacity'=>$temp->capacity - $check2,'date'=>$temp1->date,'drivername'=>$temp1->drivername,'day'=>date('l',strtotime($temp1->date)),'shiftid'=>$temp1->shiftid,'pickupid'=>$temp1->pickupid,'dropoffid'=>$temp1->dropoffid);
                            $count++;

                        }
                    }
                }
            }
        }
    }
        return $temparray;
    }

    function list4($check3){
        $this->db->select('*');
        $this->db->from('trupdateperiode tup');
        $this->db->join('msbookingperiode mbp','mbp.bookingperiodeid=tup.bookingperiodeid');
        $this->db->join('msshift s','s.shiftid=tup.shiftid');
        $this->db->join('mscampus msc','msc.campusid=tup.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=tup.dropoffid');

        $query = $this->db->get();
        return $query->result();
    }

    function list5($check3,$check4,$check){
        $temparray;
        foreach ($check3 as $key => $value) {
            if (empty($check4)) {
                $temparray[] = array(
                    'headershuttleid'=>$value['headershuttleid'],
                    'vehicleid'=>$value['vehicleid'],
                    'capacity'=>$value['capacity'],
                    'date'=>date('M-d-Y',strtotime($value['date'])),
                    'day'=>date('l',strtotime($value['date'])),
                    'vehiclename'=>$value['vehiclename'],
                    'vehicleplate'=>$value['vehicleplate'],
                    'drivername'=>$value['drivername'],
                    'shiftid'=>$value['shiftid'],
                    'pickupid'=>$value['pickupid'],
                    'dropoffid'=>$value['dropoffid']

                );
            }
            else{
                if ($value['headershuttleid'] == $temp->headershuttleid) {
                    if ($value['date'] == $temp->date) {
                        $temparray[] = array(
                            'headershuttleid'=>$value['headershuttleid'],
                            'vehicleid'=>$value['vehicleid'],
                            'capacity'=>$value['capacity'] + $temp->totalorder,
                            'date'=>date('M-d-Y',strtotime($value['date'])),
                            'day'=>date('l',strtotime($value['date'])),
                            'vehiclename'=>$value['vehiclename'],
                            'vehicleplate'=>$value['vehicleplate'],
                            'drivername'=>$value['drivername'],
                            'shiftid'=>$value['shiftid'],
                            'pickupid'=>$value['pickupid'],
                            'dropoffid'=>$value['dropoffid']
                        );
                    }
                    else{
                        $temparray[] = array(
                            'headershuttleid'=>$value['headershuttleid'],
                            'vehicleid'=>$value['vehicleid'],
                            'capacity'=>$value['capacity'],
                            'date'=>date('M-d-Y',strtotime($value['date'])),
                            'day'=>date('l',strtotime($value['date'])),
                            'vehiclename'=>$value['vehiclename'],
                            'vehicleplate'=>$value['vehicleplate'],
                            'drivername'=>$value['drivername'],
                            'shiftid'=>$value['shiftid'],
                            'pickupid'=>$value['pickupid'],
                            'dropoffid'=>$value['dropoffid']
                        );
                    }
                }
            }
        }
        // print_r($temparray);
        return $temparray;
    }

    public function listcapacity($vehicledata='',$hari='',$dropoffdata ='',$pickupdata='',$tempshift='',$termdata=''){
         $data = array(
            'cm.headershuttleid',
            'cm.vehicleid',
            'msv.vehiclename',
            'msv.vehicleplate',
            'msv.capacity'
        );

        $check = $this->list($data,$pickupdata,$dropoffdata,$hari,$tempshift,$termdata,$vehicledata);
        $check1 = $this->list1($check,$pickupdata,$dropoffdata,$hari,$tempshift,$termdata,$vehicledata);
        $check2 = $this->list2($check);
        $check3 = $this->list3($check1,$check2,$check);
        $check4 = $this->list4($check3);
        $check5 = $this->list5($check3,$check4,$check);

        usort($check5, function($a, $b) {
            return strtotime($a['date']) - strtotime($b['date']);
        });
        return $check5;

        
    }

    function get_bird($q){
      $this->db->select('*');
      $this->db->like('username', $q);
      $query = $this->db->get('msuser');
      if($query->num_rows() > 0){
        foreach ($query->result_array() as $row){
          $new_row['label']=htmlentities(stripslashes($row['username']));
          $new_row['value']=htmlentities(stripslashes($row['username']));
          $new_row['value2']=htmlentities(stripslashes($row['email']));
          $new_row['value3']=htmlentities(stripslashes($row['phone']));
          $row_set[] = $new_row; //build an array
        }
        echo json_encode($row_set); //format the array into json data
      }
    }

    public function cek_data()
        {
            $data = array(
            'ms.shiftid',
            'mu.userid',
            'mu.nama',
            'mu.email',
            'msc.campusname as pickupLoc',
            'msc2.campusname as dropoffLoc',
            'tb.trbookingid'
        );
            $this->db->select($data);
            $this->db->from('trbooking tb');
            $this->db->join('msshift ms','ms.shiftid=tb.shiftid');
            $this->db->join('msuser mu','mu.userid=tb.userid');
            $this->db->join('mscampus msc','msc.campusid=tb.pickupid');
            $this->db->join('mscampus msc2','msc2.campusid=tb.dropoffid');
            $this->db->where('tb.status','Active');
            $query = $this->db->get( );
             return $query->result();
    }

    public function insert($id = '',$date = '',$shift='',$name='',$email='',$phone='',$term='',$pickup='',$dropoff='',$description='',$doid='',$puid='',$sid='',$capacity='',$order1='',$order2='',$order3='',$order4='',$orde5r='',$order6='',$order7='',$order8='',$order9='',$order10='',$day='',$shuttleid=''){
        $order = $this->input->post('order');
        $order1 = $this->input->post('order1');
        $order2 = $this->input->post('order2');
        $order3 = $this->input->post('order3');
        $order4 = $this->input->post('order4');
        $order5 = $this->input->post('order5');
        $order6 = $this->input->post('order6');
        $order7 = $this->input->post('order7');
        $order8 = $this->input->post('order8');
        $order9 = $this->input->post('order9');
        $order10 = $this->input->post('order10');
        $term = $this->input->post('term');
        $date = $this->input->post('date');
        $sid = $this->input->post('sid');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $doid = $this->input->post('doid');
        $puid = $this->input->post('puid');
        $vid = $this->input->post('vid');
        $teco = $this->input->post('teco');
        $capacity = $this->input->post('capacity');
        $description = $this->input->post('description');
        $adminid = $this->session->userdata('userid');
        $roleid = $this->session->userdata('role');
        $day = $this->input->post('day');
        $shuttleid = $this->input->post('shuttleid');
        $datauser;
        $check = array(
            'userid',
            'nim',
            'username',
            'nama',
            'email',
            'password',
        );

        $this->db->select($check);
        $this->db->from('msuser');
        $this->db->where('status', 'Active');
        $query = $this->db->get();
        $tempData = $query->result();

        $check2 = array(
            'vehiclename',
            'vehicleplate',
            'capacity'
        );
        $this->db->select($check2);
        $this->db->from('msvehicle');
        $query2 = $this->db->get();
        // return $query->result();
        $tempData2 = $query2->result();

        $check3 = array(
            'roleid',
            'rolename'
        );
        $this->db->select($check3);
        $this->db->from('msrole');
        $this->db->where('rolename',"member");
        $query3 = $this->db->get();
        $tempData3 = $query3->result();

        foreach ($tempData3 as $role) {
            $datauser = array(
                'email' => $email,
                'phone' => $phone,
                'nama' => $name,
                'username' => $name,
                'password' => md5($name),
                'roleid' => $role->roleid,
                'status' => "Active"
            );
        }

        foreach ($tempData as $test) {
            if ($order == 1) {
                $booking = array(
                    'date'=> $date,
                    'shiftid' => $sid,
                    'pickupid' => $puid,
                    'dropoffid'=>$doid,
                    'userid'=>$adminid,
                    'description'=>$description,
                    'trdetailshuttleid'=>$shuttleid,
                    'bookingby' =>$roleid,
                    'totalorder'=>$order,
                    'name' => $order1,

                 );
                if ($name == $test->username) {
                        $this->db->trans_begin();
                        $this->db->insert('trbooking',$booking);
                        if($this->db->trans_status() === TRUE){
                            $this->db->trans_commit();
                           return true;
                        } else {
                            $this->db->trans_rollback();
                          return false;
                       }
                }else{

                    $this->db->trans_start();
                    $this->db->insert('msuser',$datauser);
                    $insert_id = $this->db->insert_id();
                    $this->db->trans_complete();

                    $booking = array(
                    'date'=> $date,
                    'shiftid' => $sid,
                    'pickupid' => $puid,
                    'dropoffid'=>$doid,
                    'userid'=>$insert_id,
                    'description'=>$description,
                    'trdetailshuttleid'=>$shuttleid,
                    'bookingby' =>$roleid,
                    'totalorder'=>$order,
                    'name' => $order1
                     );
                    $this->db->trans_begin();
                    $this->db->insert('trbooking',$booking);
                    if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                       return true;
                    } else {
                        $this->db->trans_rollback();
                      return false;
                   }
                }
             }elseif ($order == 2) {
                if ($name == $test->username) {
                  $booking = array(
                    'date'=> $date,
                    'shiftid' => $sid,
                    'pickupid' => $puid,
                    'dropoffid'=>$doid,
                    'userid'=>$adminid,
                    'description'=>$description,
                    'trdetailshuttleid'=>$shuttleid,
                    'bookingby' =>$roleid,
                    'totalorder'=>$order,
                    'name' => $order1.";".$order2
                     );

                   $this->db->trans_begin();
                   $this->db->insert('trbooking',$booking);
                   if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                       return true;
                    } else {
                        $this->db->trans_rollback();
                      return false;
                   }
                }else{
                   $this->db->trans_start();
                   $this->db->insert('msuser',$datauser);
                   $insert_id = $this->db->insert_id();
                    $this->db->trans_complete();

                    $booking = array(
                    'date'=> $date,
                    'shiftid' => $sid,
                    'pickupid' => $puid,
                    'dropoffid'=>$doid,
                    'userid'=>$insert_id,
                    'description'=>$description,
                    'trdetailshuttleid'=>$shuttleid,
                    'bookingby' =>$roleid,
                    'totalorder'=>$order,
                    'name' => $order1.";".$order2
                     );
                   $this->db->trans_begin();
                   $this->db->insert('trbooking',$booking);
                    if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                       return true;
                    } else {
                        $this->db->trans_rollback();
                      return false;
                   }
                }
           }elseif ($order == 3) {
              if ($name == $test->username) {
                $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3
                   );

                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                  $this->db->trans_complete();

                  $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3
                   );
                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 4) {
              if ($name == $test->username) {
                $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4
                   );

                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                  $this->db->trans_complete();
                  $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4
                   );
                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 5) {
              if ($name == $test->username) {
                $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5
                   );

                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                  $this->db->trans_complete();
                  $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5
                   );

                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 6) {
              if ($name == $test->username) {
                $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6
                   );

                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();

                  $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6
                   );
                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 7) {
              if ($name == $test->username) {
                $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7
                   );

                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();
                  $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7
                   );
                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 8) {
              if ($name == $test->username) {
                $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8
                   );

                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();
                  $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8
                   );
                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 9) {
              if ($name == $test->username) {
                 $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8.";".$order9
                   );

                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();
                  $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8.";".$order9
                   );
                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 10) {
              if ($name == $test->username) {
                $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8.";".$order9.";".$order10
                   );

                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{
                      
                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();
                  $booking = array(
                  'date'=> $date,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'trdetailshuttleid'=>$shuttleid,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8.";".$order9.";".$order10
                   );
                 $this->db->trans_begin();
                 $this->db->insert('trbooking',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
            }
          }
        
        
    }
    
    public function insertperiode($id = '',$date = '',$shift='',$name='',$email='',$phone='',$term='',$pickup='',$dropoff='',$description='',$doid='',$puid='',$sid='',$capacity='',$order1='',$order2='',$order3='',$order4='',$orde5r='',$order6='',$order7='',$order8='',$order9='',$order10='',$day=''){

        $order = $this->input->post('order');
        $order1 = $this->input->post('order1');
        $order2 = $this->input->post('order2');
        $order3 = $this->input->post('order3');
        $order4 = $this->input->post('order4');
        $order5 = $this->input->post('order5');
        $order6 = $this->input->post('order6');
        $order7 = $this->input->post('order7');
        $order8 = $this->input->post('order8');
        $order9 = $this->input->post('order9');
        $order10 = $this->input->post('order10');
        $term = $this->input->post('term');
        $date = $this->input->post('date');
        $sid = $this->input->post('sid');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $doid = $this->input->post('doid');
        $puid = $this->input->post('puid');
        $vid = $this->input->post('vid');
        $teco = $this->input->post('teco');
        $capacity = $this->input->post('capacity');
        $description = $this->input->post('description');
        $adminid = $this->session->userdata('userid');
        $roleid = $this->session->userdata('role');
        $day = $this->input->post('day');
        $shuttleid = $this->input->post('shuttleid');
        $datauser;
        
        $check = array(
            'userid',
            'nim',
            'username',
            'nama',
            'email',
            'password',
            'role'
        );

        $this->db->select($check);
        $this->db->from('msuser');
        $this->db->where('status', 'Active');
        $query = $this->db->get();
        $tempData = $query->result();

        $check2 = array(
            'vehiclename',
            'vehicleplate',
            'capacity'
        );
        $this->db->select($check2);
        $this->db->from('msvehicle');
        $query = $this->db->get();
        // return $query->result();
        $tempData2 = $query->result();

       $check3 = array(
            'roleid',
            'rolename'
        );
        $this->db->select($check3);
        $this->db->from('msrole');
        $this->db->where('rolename',"member");
        $query3 = $this->db->get();
        $tempData3 = $query3->result();

        foreach ($tempData3 as $role) {
            $datauser = array(
                'email' => $email,
                'phone' => $phone,
                'nama' => $name,
                'username' => $name,
                'password' => md5($name),
                'roleid' => $role->roleid,
                'status' => "Active"
            );
        }

          foreach ($tempData as $test) {
            if ($order == 1) {
                $booking = array(
                    'termid'=> $teco,
                    'day'=> $day,
                    'shiftid' => $sid,
                    'pickupid' => $puid,
                    'dropoffid'=>$doid,
                    'userid'=>$adminid,
                    'description'=>$description,
                    'bookingby' =>$roleid,
                    'name' => $order1,
                    'totalorder'=>$order,
                    'headershuttleid'=>$shuttleid
                 );
                if ($name == $test->username) {
                        $this->db->trans_begin();
                        $this->db->insert('msbookingperiode',$booking);
                        if($this->db->trans_status() === TRUE){
                            $this->db->trans_commit();
                           return true;
                        } else {
                            $this->db->trans_rollback();
                          return false;
                       }
                }else{
                    $this->db->trans_start();
                    $this->db->insert('msuser',$datauser);
                    $insert_id = $this->db->insert_id();
                    $this->db->trans_complete();

                    $booking = array(
                    'termid'=> $teco,
                    'day'=> $day,
                    'shiftid' => $sid,
                    'pickupid' => $puid,
                    'dropoffid'=>$doid,
                    'userid'=>$insert_id,
                    'description'=>$description,
                    'bookingby' =>$roleid,
                    'name' => $order1,
                    'totalorder'=>$order,
                    'headershuttleid'=>$shuttleid
                     );
                    $this->db->trans_begin();
                    $this->db->insert('msbookingperiode',$booking);
                    if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                       return true;
                    } else {
                        $this->db->trans_rollback();
                      return false;
                   }
                }
             }elseif ($order == 2) {
                if ($name == $test->username) {
                  $booking = array(
                    'termid'=> $teco,
                    'day'=> $day,
                    'shiftid' => $sid,
                    'pickupid' => $puid,
                    'dropoffid'=>$doid,
                    'userid'=>$adminid,
                    'description'=>$description,
                    'bookingby' =>$roleid,
                    'totalorder'=>$order,
                    'headershuttleid'=>$shuttleid,
                    'name' => $order1.";".$order2
                     );

                   $this->db->trans_begin();
                   $this->db->insert('msbookingperiode',$booking);
                   if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                       return true;
                    } else {
                        $this->db->trans_rollback();
                      return false;
                   }
                }else{

                   $this->db->trans_start();
                   $this->db->insert('msuser',$datauser);
                   $insert_id = $this->db->insert_id();
                    $this->db->trans_complete();

                    $booking = array(
                    'termid'=> $teco,
                    'day'=> $day,
                    'shiftid' => $sid,
                    'pickupid' => $puid,
                    'dropoffid'=>$doid,
                    'userid'=>$insert_id,
                    'description'=>$description,
                    'bookingby' =>$roleid,
                    'totalorder'=>$order,
                    'headershuttleid'=>$shuttleid,
                    'name' => $order1.";".$order2
                     );
                   $this->db->trans_begin();
                   $this->db->insert('msbookingperiode',$booking);
                    if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                       return true;
                    } else {
                        $this->db->trans_rollback();
                      return false;
                   }
                }
           }elseif ($order == 3) {
              if ($name == $test->username) {
                $booking = array(
                  'termid'=> $teco,
                  'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3
                   );

                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{
                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                  $this->db->trans_complete();

                  $booking = array(
                  'termid'=> $teco,
                  'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 4) {
              if ($name == $test->username) {
                $booking = array(
                  'termid'=> $teco,
                  'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4
                   );

                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                  $this->db->trans_complete();
                  $booking = array(
                  'termid'=> $teco,
                  'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 5) {
              if ($name == $test->username) {
                $booking = array(
                  'termid'=> $teco,
                  'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5
                   );

                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                  $this->db->trans_complete();
                  $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5
                   );

                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 6) {
              if ($name == $test->username) {
                $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{
                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();

                  $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 7) {
              if ($name == $test->username) {
                $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{

                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();
                  $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 8) {
              if ($name == $test->username) {
                $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{
                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();
                  $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 9) {
              if ($name == $test->username) {
                 $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8.";".$order9
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{
                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();
                  $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8.";".$order9
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
           }elseif ($order == 10) {
              if ($name == $test->username) {
                $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$adminid,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8.";".$order9.";".$order10
                   );

                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }else{
                 $this->db->trans_start();
                 $this->db->insert('msuser',$datauser);
                 $insert_id = $this->db->insert_id();
                 // print_r ($insert_id);
                  $this->db->trans_complete();
                  $booking = array(
                  'termid'=> $teco,
                    'day'=> $day,
                  'shiftid' => $sid,
                  'pickupid' => $puid,
                  'dropoffid'=>$doid,
                  'userid'=>$insert_id,
                  'description'=>$description,
                  'bookingby' =>$roleid,
                  'totalorder'=>$order,
                  'headershuttleid'=>$shuttleid,
                  'name' => $order1.";".$order2.";".$order3.";".$order4.";".$order5.";".$order6.";".$order7.";".$order8.";".$order9.";".$order10
                   );
                 $this->db->trans_begin();
                 $this->db->insert('msbookingperiode',$booking);
                 // print_r($booking);
                  if($this->db->trans_status() === TRUE){
                      $this->db->trans_commit();
                     return true;
                  } else {
                      $this->db->trans_rollback();
                    return false;
                 }
              }
            }
        }

    }

    public function insertmember($id = '',$date = '',$shift='',$name='',$email='',$phone='',$term='',$pickup='',$dropoff='',$description='',$doid='',$puid='',$sid='',$order1='',$order2='',$order3='',$order4='',$orde5r='',$order6='',$order7='',$order8='',$order9='',$order10=''){

        $order = $this->input->post('order');
        $order1 = $this->input->post('order1');
        $order2 = $this->input->post('order2');
        $order3 = $this->input->post('order3');
        $order4 = $this->input->post('order4');
        $order5 = $this->input->post('order5');
        $order6 = $this->input->post('order6');
        $order7 = $this->input->post('order7');
        $order8 = $this->input->post('order8');
        $order9 = $this->input->post('order9');
        $order10 = $this->input->post('order10');
        $term = $this->input->post('term');
        $date = $this->input->post('date');
        $sid = $this->input->post('sid');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $doid = $this->input->post('doid');
        $puid = $this->input->post('puid');
        $caid = $this->input->post('caid');
        $description = $this->input->post('description');
        $userid = $this->session->userdata('userid');
        $shuttle = $capacity - $order;

            

         if ($order == 1) {
              // $this->db->trans_start();
              //  $this->db->insert('msuser',$datauser);
              //  $insert_id = $this->db->insert_id();
              //  print_r ($insert_id);
              //   $this->db->trans_complete();
              //   $booking = array(
              //   'date'=> $date,
              //   'shiftid' => $sid,
              //   'pickupid' => $puid,
              //   'dropoffid'=>$doid,
              //   'userid'=>$insert_id,
              //   'description'=>$description,
              //   'bookingby' =>"admin",
              //   'name' => $order1
              //    );
              //  $this->db->trans_begin();
              //  $this->db->insert('trbooking',$booking);
              //  print_r($booking);
              //   if($this->db->trans_status() === TRUE){
              //       $this->db->trans_commit();
              //      return true;
              //   } else {
              //       $this->db->trans_rollback();
              //     return false;
              //  }
            // }
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1);
         }elseif ($order == 2) {
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1.";".$order2);
         }elseif ($order == 3) {
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1.";".$order2.";".$order3);
         }elseif ($order == 4) {
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1.";".$order2.";".$order3.";".$order4);
         }elseif ($order == 5) {
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1.";".$order2.";".$order3.";".$order4.";".$order5);
         }elseif ($order == 6) {
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1.";".$order2.";".$order3.";".$order4.";".$order8.";".$order6);
         }elseif ($order == 7) {
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1.";".$order2.";".$order3.";".$order4.";".$order8.";".$order6.";".$order7);
         }elseif ($order == 8) {
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1.";".$order2.";".$order3.";".$order4.";".$order8.";".$order6.";".$order7.";".$order8);
         }elseif ($order == 9) {
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1.";".$order2.";".$order3.";".$order4.";".$order8.";".$order6.";".$order7.";".$order8.";".$order9);
         }elseif ($order == 10) {
            $this->db->set('date',$date);
            $this->db->set('shiftid',$sid);
            $this->db->set('userid',$userid);
            $this->db->set('pickupid',$puid);
            $this->db->set('dropoffid',$doid);
            $this->db->set('description',$description);
            $this->db->set('bookingby',"member");
            $this->db->set('name',$order1.";".$order2.";".$order3.";".$order4.";".$order8.";".$order6.";".$order7.";".$order8.";".$order9.";".$order10);
         }
        
       $this->db->trans_begin();
       $this->db->insert('trbooking');


       if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
           return true;
        } else {
            $this->db->trans_rollback();
          return false;
       }
   } 
}