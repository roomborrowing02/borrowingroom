<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_term extends CI_Model {

    public function sterm($id = ''){
        $data = array(
            'termid',
            'termcode',
            'startdate',
            'enddate',
            'description'
        );

        $this->db->select($data);
        $this->db->from('msterm');
        $this->db->where('status', 'Active');

        if( $id != '' )
            $this->db->where('termid', $id);

        return $this->db->get();
    }
    public function sdetail($id =''){
        $data = array(
            'mtd.termdetailid',
            'mtd.termid',
            'mt.termcode',
            'mtd.startdate as startdetail',
            'mtd.enddate as enddetail',
            'mtd.description as descTermDetail',
            'mt.description as descTerm',
            'mtd.includeSchedule',
            'mtd.status'
        );
     $this->db->select($data);
     $this->db->from('mstermdetail mtd' );
     $this->db->join('msterm mt','mt.termid=mtd.termid');
     $this->db->where('mtd.status', 'Active');

     if( $id != '' )
            $this->db->where('termdetailid', $id);

        return $this->db->get();
    }
    public function lweek($id = ''){
        $data = array(
            'mls.lectureweekid',
            'mls.weeksession',
            'mt.termid',
            'mt.termcode',
            'mls.day as day',
            'mls.startdate as startdateLWS',
            'mls.enddate as enddateLWS',
            'mls.status'
        );
     $this->db->select($data);
     $this->db->from('mslectureweek mls');
     $this->db->join('msterm mt','mt.termid=mls.termid');
     $this->db->where('mls.status', 'Active');
     if( $id != '' )
            $this->db->where('lectureweekid', $id);

        return $this->db->get();
    }
    public function get_termcode($id = ''){
        $data = array('termid',
            'termcode','description');

        $this->db->select($data);
        $this->db->from('msterm');
        $this->db->where('status', 'Active');

        if( $id != '' )
            $this->db->where('termid', $id);

        return $this->db->get();
    }
    public function get_schedule($id = ''){
        $data = array('termid',
            'includeSchedule');

        $this->db->select($data);
        $this->db->from('mstermdetail');
        $this->db->where('status', 'Active');

        if( $id != '' )
            $this->db->where('termid', $id);

        return $this->db->get();
    }
    public function insert($tempcode='',$tempstart='',$tempend='',$tempdescription=''){
        $data = array(
            'termcode' => $this->input->post('termcode'),
            'startdate' => date('Y/m/d H:i:s', strtotime($this->input->post('startdate'))),
            'enddate' => date('Y/m/d H:i:s', strtotime($this->input->post('enddate'))),
            'description' => $this->input->post('description'),
            'status' => "Active"
        );

        $this->db->trans_begin();
        $this->db->insert('msterm', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function insertDetail($id='' ,$termcode='',$startdetail='',$enddetail='' ,$includeSchedule='',$description='',$Termdetailid=''){
        $this->db->set('termid',$this->input->post('termcode'));
        $this->db->set('startdate',$this->input->post('startdetail'));
        $this->db->set('enddate',$this->input->post('enddetail'));
        $this->db->set('description',$this->input->post('description'));
        $this->db->set('includeSchedule',$this->input->post('includeSchedule'));
        $this->db->set('status',"Active");
        $this->db->where('termdetailid', $this->input->post('InsertDetail'));
        $this->db->insert('mstermdetail');
        
        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function insertlectureweek($id='' ,$termcode='',$startdateLWS='',$enddateLWS='',$weeksession=''){
        $this->db->set('termid',$this->input->post('termcode'));
        $this->db->set('day',$this->input->post('day'));
        $this->db->set('startdate',$this->input->post('startdateLWS'));
        $this->db->set('enddate',$this->input->post('enddateLWS'));
        $this->db->set('weeksession',$this->input->post('weeksession'));
        $this->db->set('status',"Active");
        $this->db->where('lectureweekid', $this->input->post('InsertLecture'));
        $this->db->insert('mslectureweek');
        
        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update(){
        $data = array(
            'termcode' => $this->input->post('termcode'),
            'startdate' => date('Y/m/d H:i:s', strtotime($this->input->post('startdate'))),
            'enddate' => date('Y/m/d H:i:s', strtotime($this->input->post('enddate'))),
            'description' => $this->input->post('description')
        );

        $this->db->trans_begin();
        $this->db->where('termid', $this->input->post('Termid'));
        $this->db->update('msterm', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function updatedetail($id='' ,$termcode='',$startdetail='',$enddetail='',$description='',$includeSchedule='',$Termdetailid=''){
        $this->db->set('termid',$this->input->post('termcode'));
        $this->db->set('startdate',$this->input->post('startdetail'));
        $this->db->set('enddate',$this->input->post('enddetail'));
        $this->db->set('description',$this->input->post('description'));
        $this->db->set('includeSchedule',$this->input->post('includeSchedule'));
        $this->db->where('termdetailid', $this->input->post('Termdetailid'));
        $this->db->update('mstermdetail');

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function updatelecture($id='' ,$termcode='',$startdate='',$enddate='',$weeksession=''){
        $this->db->set('termid',$this->input->post('termcode'));
        $this->db->set('startdate',$this->input->post('startdateLWS'));
        $this->db->set('enddate',$this->input->post('enddateLWS'));
        $this->db->set('weeksession',$this->input->post('weeksession'));
        $this->db->where('lectureweekid', $this->input->post('Lectureweekid'));
        $this->db->update('mslectureweek');

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();

        $this->db->where('termid', $this->input->post('Termid'));
        $this->db->update('msterm',$data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function deletedetail(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();

        $this->db->where('termdetailid', $this->input->post('deletedeldetail'));
        $this->db->update('mstermdetail',$data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function deletelecture(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();

        $this->db->where('lectureweekid', $this->input->post('deletelectureweek'));
        $this->db->update('mslectureweek',$data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function generate($id='' ,$termid='',$termcode='',$startdate='',$enddate='',$detail=''){
        $termid = $this->input->post('termid');
         $startdate = $this->input->post('startdate');
         $enddate = $this->input->post('enddate');
         $code = $this->input->post('termcode');
         $enddate = strtotime($enddate);

        $day = ['Monday', 'Tuesday', 'Wednesday', 
                        'Thursday', 'Friday', 'Saturday', 'Sunday'];

        $check = array(
            'mt.termcode',
            'mtd.startdate as startdetail',
            'mtd.enddate as enddetail',
            'mtd.includeSchedule',
            'mtd.status'
            );
        $this->db->select($check);
        $this->db->from('mstermdetail mtd');
        $this->db->join('msterm mt','mt.termid=mtd.termid');
        $this->db->where('mtd.status', 'Active');
        $query = $this->db->get();
        $tempData = $query->result();
        // $tempData[] = array();
        // print_r($tempData);
        // tampung semua tanggal di termdetail
        // looping tanggal yang includeschedule nya yes

         for($i=1 ; $i<7 ; $i++){
            $weeksession = 1;
            // echo $day[$i-1] . "<br>";        
                        $hasilarray = array();
                        foreach ($tempData as $test) {
                            if ($test->termcode == $code) {
                                for ($b=strtotime($test->startdetail); $b <= strtotime($test->enddetail); $b = strtotime('+1 day', $b)) { 
                                    
                                    if($test->includeSchedule == "No"){
                                        $hasil=date('Y-m-d',$b);
                                        array_push($hasilarray, $hasil);
                                        // print_r($hasilarray);
                                        // echo $hasilarray[0];
                                        // echo "<br>";
                                        //Tampung dalam array abcno
                                    }
                                }
                            }
                        }

                        foreach ($tempData as $test) {
                            if ($test->termcode == $code) {
                                $hasilarraydetail = array();
                                for ($k=strtotime($test->startdetail); $k <= strtotime($test->enddetail); $k = strtotime('+1 day', $k)) { 
                                    if ($test->includeSchedule == "Yes") {
                                        $flag = 0;
                                        for ($j=0; $j < count($hasilarray,COUNT_RECURSIVE);  $j++) { 
                                            if (date('Y-m-d',$k) == $hasilarray[$j]) {
                                                $flag = -1;
                                            }
                                        }
                                        if($flag == 0){
                                            // echo date('Y-m-d',$k)."<br>";
                    for($a=strtotime('Monday', strtotime($test->startdetail)); $a <= strtotime($test->enddetail); $a = strtotime('+1 week', $a)){
                    if ($day[$i-1] == "Monday") {
                        if ($k == $a) {
                                $this->db->set('termid',$termid);
                                $this->db->set('WeekSession',$weeksession);
                                $this->db->set('day',date('l',$k));
                                $this->db->set('startdate',date('Y-m-d',$k));
                                $this->db->set('enddate',date('Y-m-d',$k));
                                $this->db->insert('mslectureweek');
                                $weeksession++;  
                             } 
                        }
                    }

                    for($a=strtotime('Tuesday', strtotime($test->startdetail)); $a <= strtotime($test->enddetail); $a = strtotime('+1 week', $a)){
                    if ($day[$i-1] == "Tuesday") {
                        if ($k == $a) {
                                $this->db->set('termid',$termid);
                                $this->db->set('WeekSession',$weeksession);
                                $this->db->set('day',date('l',$k));
                                $this->db->set('startdate',date('Y-m-d',$k));
                                $this->db->set('enddate',date('Y-m-d',$k));
                                $this->db->insert('mslectureweek');
                                $weeksession++;
                             } 
                         }
                    }

                    for($a=strtotime('Wednesday', strtotime($test->startdetail)); $a <= strtotime($test->enddetail); $a = strtotime('+1 week', $a)){
                    if ($day[$i-1] == "Wednesday") {
                        if ($k == $a) {
                                $this->db->set('termid',$termid);
                                $this->db->set('WeekSession',$weeksession);
                                $this->db->set('day',date('l',$k));
                                $this->db->set('startdate',date('Y-m-d',$k));
                                $this->db->set('enddate',date('Y-m-d',$k));
                                $this->db->insert('mslectureweek');
                                $weeksession++;  
                             } 
                         }
                    }
                    for($a=strtotime('Thursday', strtotime($test->startdetail)); $a <= strtotime($test->enddetail); $a = strtotime('+1 week', $a)){
                    if ($day[$i-1] == "Thursday") {
                        if ($k == $a) {
                               $this->db->set('termid',$termid);
                                $this->db->set('WeekSession',$weeksession);
                                $this->db->set('day',date('l',$k));
                                $this->db->set('startdate',date('Y-m-d',$k));
                                $this->db->set('enddate',date('Y-m-d',$k));
                                $this->db->insert('mslectureweek');
                                $weeksession++;  
                             } 
                         }
                    }
                    for($a=strtotime('Friday', strtotime($test->startdetail)); $a <= strtotime($test->enddetail); $a = strtotime('+1 week', $a)){
                    if ($day[$i-1] == "Friday") {
                        if ($k == $a) {
                                $this->db->set('termid',$termid);
                                $this->db->set('WeekSession',$weeksession);
                                $this->db->set('day',date('l',$k));
                                $this->db->set('startdate',date('Y-m-d',$k));
                                $this->db->set('enddate',date('Y-m-d',$k));
                                $this->db->insert('mslectureweek');
                                $weeksession++;  
                             } 
                         }
                    }

                    for($a=strtotime('Saturday', strtotime($test->startdetail)); $a <= strtotime($test->enddetail); $a = strtotime('+1 week', $a)){
                    if ($day[$i-1] == "Saturday") {
                        if ($k == $a) {
                                $this->db->set('termid',$termid);
                                $this->db->set('WeekSession',$weeksession);
                                $this->db->set('day',date('l',$k));
                                $this->db->set('startdate',date('Y-m-d',$k));
                                $this->db->set('enddate',date('Y-m-d',$k));
                                $this->db->insert('mslectureweek');
                                $weeksession++;  
                             } 
                         }
                    }
                                        }
                                    }
                                }
                            }
                        }
                }
            /*Looping hari, ada looping 6x */
                // $weeksession = 1;
                /*Looping pertemuan, ada looping 13x */
                /*END - Looping pertemuan, ada looping 13x */

            /*END - Looping hari, ada looping 6x */
    }
}