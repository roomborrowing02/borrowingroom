<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModelUsers extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public function cekuser($username,$password) {
		$data = array(

			'mu.userid',
			'mu.roleid',
			'mu.nim',
			'mu.username',
			'mu.nama',
			'mu.email',
			'mu.password',
			// 'mu.role',
			'mr.rolename',
		); 

		$this->db->select($data);
		$this->db->from('msuser mu');
        $this->db->join('msrole mr','mu.roleid = mr.roleid');
		$this->db->where('mu.status','Active')->where('username',$username)->where('password',$password);

		return $this->db->get();


	}

	public function cek_users($id = ''){
		$data = array(

			'mu.userid',
			'mu.roleid',
			'mu.nim',
			'mu.username',
			'mu.nama',
			'mu.email',
			'mu.password',
			// 'mu.role',
			'mr.rolename',
		); 

		$this->db->select($data);
		$this->db->from('msuser mu');
        $this->db->join('msrole mr','mu.roleid = mr.roleid');
		$this->db->where('mu.status','Active');
		if( $id != '' )
			$this->db->where('userid', $id);

		return $this->db->get();
	}
		public function get_role(){        
        $this->db->select('*');
        $this->db->from('msrole');
        $this->db->where('status','Active');

        $query = $this->db->get();

        return $query->result();
    }
	public function insert($nim='',$username='',$nama='',$email='',$rolename='',$role='',
		$roleid=''){
		$data = array(
			'nim' => $this->input->post('nim'),
			'username' => $this->input->post('username'),
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'roleid' => $this->input->post('role')
		);

		$this->db->trans_begin();
        $this->db->insert('msuser', $data);

		if($this->db->trans_status() === TRUE){
			$this->db->trans_commit();
			return true;
		} else {
			$this->db->trans_rollback();
			return false;
		}
	}

	public function update($nim='',$username='',$nama='',$email='',$rolename='',$role='',
		$roleid=''){
		$data = array(
			'nim' => $this->input->post('nim'),
			'username' => $this->input->post('username'),
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'roleid' => $this->input->post('role'),
		);

		$this->db->trans_begin();
		$this->db->where('userid', $this->input->post('updateuser'));
		$this->db->update('msuser', $data);

		if($this->db->trans_status() === TRUE){
			$this->db->trans_commit();
			return true;
		} else {
			$this->db->trans_rollback();
			return false;
		}
	}

	public function delete($nim='',$username='',$nama='',$email='',$rolename=''){
		$data = array( 'status' => 'Inactive' );

		$this->db->trans_begin();
		$this->db->where('userid', $this->input->post('userId'));
		$this->db->update('msuser', $data);

		if($this->db->trans_status() === TRUE){
			$this->db->trans_commit();
			return true;
		} else {
			$this->db->trans_rollback();
			return false;
		}
	}

}