<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cs_master_model extends CI_Model {

    public function get_term_code(){        
        $this->db->select('*');
        $this->db->from('msterm');
        $this->db->where('status', 'Active');
        $this->db->group_by('termid');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_vehicle_name(){        
        $this->db->select('*');
        $this->db->from('msvehicle');
        $this->db->where('status', 'Active');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_driver_name(){        
        $this->db->select('*');
        $this->db->from('msdriver');
        $this->db->where('status', 'Active');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_shift(){        
        $this->db->select('*');
        $this->db->from('msshift');
        $this->db->where('status', 'Active');
        $query = $this->db->get();

        return $query->result();
    }
     public function get_pickup(){        
        $this->db->select('*');
        $this->db->from('mscampus');
        $this->db->where('status', 'Active');
        $query = $this->db->get();

        return $query->result();
    }
     public function get_dropoff(){        
        $this->db->select('*');
        $this->db->from('mscampus');
        $this->db->where('status', 'Active');
        $query = $this->db->get();

        return $query->result();
    }

    public function cekvalidasi($pickup){

        $this->db->select('*');
        $this->db->from('mscampus');
        $this->db->where('campusid !=', $pickup)->where('status','Active');
        $query = $this->db->get();
        return $query->result();
    }

    public function cekvalidasi2($pickup){

        $this->db->select('*');
        $this->db->from('mscampus');
        $this->db->where('campusid !=', $pickup)->where('status','Active');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    public function cek_data($id=''){
        $data = array(
            'cm.headershuttleid',
            'cm.day',
            'mt.termcode',
            'mt.termid',
            'msv.vehicleid',
            'msd.driverid',
            's.shiftid',
            's.shift',
            'msc.campusid as campuspickupid',
            'msc2.campusid as campusdropoffid',
            'msc.campuscode as pickupLoc',
            'msc2.campuscode as dropoffLoc',
            'msv.vehiclename',
            'msd.drivername',
            'cm.status'
        );
        
        $this->db->select($data);
        $this->db->from('headershuttle cm');
        $this->db->join('msterm mt','mt.termid=cm.termid');
        $this->db->join('msshift s','s.shiftid=cm.shiftid');
        $this->db->join('msvehicle msv','msv.vehicleid=cm.vehicleid');
        $this->db->join('msdriver msd','msd.driverid=cm.driverid');
        $this->db->join('mscampus msc','msc.campusid=cm.pickupid');
        $this->db->join('mscampus msc2','msc2.campusid=cm.dropoffid');
        $this->db->where('cm.status', 'Active');

       
     if( $id != '' )
            $this->db->where('headershuttleid', $id);

        return $this->db->get();
    }

    public function insert($day='',$termcode='',$vehiclename='',$drivername='',
        $shift='',$pickup='',$dropoff=''){
        $this->db->set('day',$this->input->post('day'));
        $this->db->set('termid',$this->input->post('termcode'));
        $this->db->set('vehicleid',$this->input->post('vehiclename'));
        $this->db->set('driverid',$this->input->post('drivername'));
        $this->db->set('shiftid',$this->input->post('shift'));
        $this->db->set('pickupid', $this->input->post('pickup'));
        $this->db->set('dropoffid', $this->input->post('dropoff'));
        
        $this->db->set('status', "Active");
        $this->db->where('headershuttleid', $this->input->post('Masterid'));      
        $this->db->insert('headershuttle');

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update($day='',$termcode='',$vehiclename='',$drivername='',
        $shift='',$pickup='',$dropoff=''){
        $day = $this->input->post('day');
        $termid = $this->input->post('termcode');
        $vehicleid = $this->input->post('vehiclename');
        $driverid = $this->input->post('drivername');
        $shiftid = $this->input->post('shift');
        $pickupid = $this->input->post('pickup');
        $dropoffid = $this->input->post('dropoff');
        
        $this->db->set('day',$day);
        $this->db->set('termid',$termid);
        $this->db->set('vehicleid',$vehicleid);
        $this->db->set('driverid',$driverid);
        $this->db->set('shiftid',$shiftid);
        $this->db->set('pickupid', $pickupid);
        $this->db->set('dropoffid', $dropoffid);
        $this->db->where('headershuttleid', $this->input->post('updatemaster'));   
        $this->db->update('headershuttle');    
    }

    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();
        $this->db->where('headershuttleid', $this->input->post('deletemasterId'));
        $this->db->update('headershuttle', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    function generatedata(){
        $check = array(
            'mls.lectureweekid',
            'mls.weeksession',
            'mt.termid',
            'mt.termcode',
            'mls.day as day',
            'mls.startdate as startdateLWS',
            'mls.enddate as enddateLWS',
            'mls.status'
        );
        $this->db->select($check);
        $this->db->from('mslectureweek mls');
        $this->db->join('msterm mt','mt.termid=mls.termid');
        $this->db->where('mls.status', 'Active');
        $query = $this->db->get();
        return $query->result();
    }

    function generatedata2($temp,$day,$shuttleid,$termid,$vehicleid,$driverid,$shiftid,$pickupid,$dropoffid){
        foreach ($temp as $data) {
            if ($termid == $data->termid) {
                if ($day == $data->day) {
                    $cek = array(
                        'day' => $data->day,
                        'headershuttleid' => $shuttleid,
                        'date' => $data->startdateLWS,
                        'termid'=>$termid,
                        'driverid'=>$driverid,
                        'vehicleid'=>$vehicleid,
                        'shiftid'=>$shiftid,
                        'pickupid'=>$pickupid,
                        'dropoffid'=>$dropoffid,
                        'status'=>"Active"
                    );
                    $this->db->insert('trdetailshuttle',$cek);
                    // print_r($cek);
                    // echo "<br>";
                }
            }
        }
    }

    public function generate($termcode='',$vehicle='',$driver='',$day='',$shift='',
        $pickup='',$dropoff='',$shuttleid='')
    {
         $day = $this->input->post('day');
         $shuttleid = $this->input->post('shuttleid');
         $termid = $this->input->post('termcode');
         $vehicleid = $this->input->post('vehicle');
         $driverid = $this->input->post('driver');
         $shiftid = $this->input->post('shift');
         $pickupid = $this->input->post('pickup');
         $dropoffid = $this->input->post('dropoff');
         $temp = $this->generatedata();
         $temp2 = $this->generatedata2($temp,$day,$shuttleid,$termid,$vehicleid,$driverid,$shiftid,$pickupid,$dropoffid);

        // foreach ($tempData as $test) {
        //     if ($test->termid == $termid) {
        //         if ($test->day == $day) {
                    // $this->db->set('day',$test->day);
                    // $this->db->set('headershuttleid',$shuttleid);
                    // $this->db->set('date',$test->startdateLWS);
                    // $this->db->set('termid',$termid);
                    // $this->db->set('vehicleid',$vehicleid);
                    // $this->db->set('driverid',$driverid);
                    // $this->db->set('shiftid',$shiftid);
                    // $this->db->set('pickupid', $pickupid);
                    // $this->db->set('dropoffid', $dropoffid);    
                    // $this->db->set('status', "Active");    
                    // $this->db->insert('trdetailshuttle');
        //             $data = array(
        //                 'day' => $test->day,
        //                 'headershuttleid' => $shuttleid,
        //                 'date' => $test->startdateLWS,
        //                 'termid'=>$termid,
        //                 'vehicleid'=>$vehicleid,
        //                 'shiftid'=>$shiftid,
        //                 'pickupid'=>$pickupid,
        //                 'dropoffid'=>$dropoffid,
        //                 'status'=>"Active"
        //             );
        //             print_r($data);
        //             echo "<br>";
        //         }   
        //     } 
        // }
    }

}