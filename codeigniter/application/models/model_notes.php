<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_notes extends CI_Model{


    function get_user($nimdata){
        $this->db->select('*');
        $this->db->from('msuser');
        $this->db->like('nim', $nimdata);
        return $this->db->get()->result_array();

    }

    public function room(){
        $this->db->select('*');
        $this->db->from('msfacility');
        $this->db->where('status','Active');

        return $this->db->get();

    }

    public function category(){
        $data = array(
            'categoryid',
            'category'
        );
        $this->db->select($data);
        $this->db->from('mscategory');
        $this->db->where('mscategory.status',"Active");
        return $this->db->get();
    }

    public function subject(){
        $data = array(
            'subjectid',
            'subject'
        );
        $this->db->select($data);
        $this->db->from('mssubject ms');
        $this->db->join('mscategory mc','mc.categoryid=ms.categoryid');
        $this->db->where('ms.status',"Active");
        return $this->db->get();
    }

    function getdata($notesid){
        $data = array(
            'subjectid',
            'subject'
        );
        $this->db->select($data);
        $this->db->from('mssubject ms');
        $this->db->join('mscategory mc','mc.categoryid=ms.categoryid');
        $this->db->where('ms.status',"Active")->where('ms.categoryid',$notesid);
        return $this->db->get();
    }

    public function get_list($list){
        $data = array(
            'mn.notesid',
            'mn.date',
            'mu.userid',
            'mu.nama',
            'mu.nim',
            'mc.categoryid',
            'mc.category',
            'ms.subjectid',
            'ms.subject',
            'mn.roomid',
            'mf.facilityid',
            'mf.facilityname',
            'mn.description',
            'mn.solution',
            'mn.dateline',
            'mn.others',
            'mn.status'
        );
        $this->db->select($data);
        $this->db->from('msnotes mn');
        $this->db->join('msuser mu','mu.userid=mn.userid');
        $this->db->join('mscategory mc','mc.categoryid=mn.categoryid');
        $this->db->join('mssubject ms','ms.subjectid=mn.subjectid');
        $this->db->join('msfacility mf','mf.facilityid=mn.roomid','left');
        $this->db->where('mn.notesid',$list);
        return $this->db->get();
    }
    

    public function insert(){
        $room = $this->input->post('room');
        $followup = $this->input->post('followup');
        $adminid = $this->session->userdata('userid');

        if (empty($room)) {
            if (empty($followup)) {
                $data = array(
                    'date' => date('Y-m-d h:i:s', strtotime($this->input->post('dateform'))),
                    'userid' => $this->input->post('user'),
                    'categoryid' => $this->input->post('category'),
                    'subjectid' => $this->input->post('subject'),
                    'description' => $this->input->post('description'),
                    'solution' => $this->input->post('solution'),
                    'others' => $this->input->post('others'),
                    'notesuserid' => $adminid,
                    'status' => $this->input->post('status')
                );
            }
            else{
                $data = array(
                    'date' => date('Y-m-d h:i:s', strtotime($this->input->post('dateform'))),
                    'userid' => $this->input->post('user'),
                    'categoryid' => $this->input->post('category'),
                    'subjectid' => $this->input->post('subject'),
                    'description' => $this->input->post('description'),
                    'solution' => $this->input->post('solution'),
                    'dateline' => date('Y-m-d h:i:s', strtotime($followup)),
                    'others' => $this->input->post('others'),
                    'notesuserid' => $adminid,
                    'status' => $this->input->post('status')
                );
            }
        }else{
            if (empty($followup)) {
                $data = array(
                    'date' => date('Y-m-d h:i:s', strtotime($this->input->post('dateform'))),
                    'userid' => $this->input->post('user'),
                    'categoryid' => $this->input->post('category'),
                    'subjectid' => $this->input->post('subject'),
                    'roomid' => $this->input->post('room'),
                    'description' => $this->input->post('description'),
                    'solution' => $this->input->post('solution'),
                    'others' => $this->input->post('others'),
                    'notesuserid' => $adminid,
                    'status' => $this->input->post('status')
                );
            }
            else{
                $data = array(
                    'date' => date('Y-m-d h:i:s', strtotime($this->input->post('dateform'))),
                    'userid' => $this->input->post('user'),
                    'categoryid' => $this->input->post('category'),
                    'subjectid' => $this->input->post('subject'),
                    'roomid' => $this->input->post('room'),
                    'description' => $this->input->post('description'),
                    'solution' => $this->input->post('solution'),
                    'dateline' => date('Y-m-d h:i:s', strtotime($this->input->post('followup'))),
                    'others' => $this->input->post('others'),
                    'notesuserid' => $adminid,
                    'status' => $this->input->post('status')
                );
            }
        }
        // print_r($data);
        $this->db->insert('msnotes',$data);
    }

    public function update(){
        $room = $this->input->post('room');
        $followup = $this->input->post('followup');
        $notesid = $this->input->post('notesid');
        $date = $this->input->post('dateform');
        if (empty($room)) {
            if (empty($followup)) {
                $data = array(
                    'date' => date('Y-m-d h:i:s', strtotime($this->input->post('dateform'))),
                    'userid' => $this->input->post('user'),
                    'categoryid' => $this->input->post('category'),
                    'subjectid' => $this->input->post('subject'),
                    'description' => $this->input->post('description'),
                    'roomid' => null, 
                    'others' => $this->input->post('others'),
                    'solution' => $this->input->post('solution'),
                    'status' => $this->input->post('status')
                );
            }
            else{
                $data = array(
                    'date' => date('Y-m-d h:i:s', strtotime($this->input->post('dateform'))),
                    'userid' => $this->input->post('user'),
                    'categoryid' => $this->input->post('category'),
                    'subjectid' => $this->input->post('subject'),
                    'description' => $this->input->post('description'),
                    'roomid' => null,
                    'others' => $this->input->post('others'),
                    'solution' => $this->input->post('solution'),
                    'dateline' => date('Y-m-d h:i:s', strtotime($followup)),
                    'status' => $this->input->post('status')
                );
            }
        }else{
            if (empty($followup)) {
                $data = array(
                    'date' => date('Y-m-d h:i:s', strtotime($this->input->post('dateform'))),
                    'userid' => $this->input->post('user'),
                    'categoryid' => $this->input->post('category'),
                    'subjectid' => $this->input->post('subject'),
                    'roomid' => $this->input->post('room'),
                    'description' => $this->input->post('description'),
                    'others' => $this->input->post('others'),
                    'solution' => $this->input->post('solution'),
                    'status' => $this->input->post('status')
                );
            }
            else{
                $data = array(
                    'date' => date('Y-m-d h:i:s', strtotime($this->input->post('dateform'))),
                    'userid' => $this->input->post('user'),
                    'categoryid' => $this->input->post('category'),
                    'subjectid' => $this->input->post('subject'),
                    'roomid' => $this->input->post('room'),
                    'description' => $this->input->post('description'),
                    'others' => $this->input->post('others'),
                    'solution' => $this->input->post('solution'),
                    'dateline' => date('Y-m-d h:i:s', strtotime($this->input->post('followup'))),
                    'status' => $this->input->post('status')
                );
            }
        }
        // print_r($data);
        $this->db->where('notesid',$notesid);
        $this->db->update('msnotes',$data);
    }
}