<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pengumuman extends CI_Model{

    public function get_data($id = ''){
        $data = array(
            'pengumumanid',
            'content',
            'description',
            'startdate',
            'enddate',
            'lastupdate',
            'lastinsert',
            'createdby',
            'status'
        );

        $this->db->select($data);
        $this->db->from('mspengumuman');
        $this->db->where('status', 'Active');

        if( $id != '' )
            $this->db->where('pengumumanid', $id);

        return $this->db->get();
    }
    public function fullscreen($id = ''){
        $tempdate = date('Y-m-d');
        $sql = $this->db->query("SELECT * FROM mspengumuman WHERE ('$tempdate' BETWEEN startdate AND enddate) AND (status ='Active')");
        return $sql->result();
    }

    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();

        $this->db->where('pengumumanid', $this->input->post('DeleteId'));
        $this->db->update('mspengumuman',$data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    // public function get_status(){
    //     $tempdate = date('Y-m-d');
    //     $sql = $this->db->query("SELECT * FROM mspengumuman WHERE ('$tempdate' BETWEEN startdate AND enddate) AND (status ='Active')");
    //     $tempData = $sql->result();
    //     print_r($tempData);
    // }
}