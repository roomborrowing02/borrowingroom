 <?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Model_kendaraan extends CI_Model {

        public function cek_kendaraan($id = ''){
        $data = array(
            'vehicleid',
            'vehiclename',
            'vehicleplate',
            'capacity'
        );

        $this->db->select($data);
        $this->db->from('msvehicle');
        $this->db->where('status', 'Active');

        if( $id != '' )
            $this->db->where('vehicleid', $id);

        return $this->db->get();
    }

    public function insert(){
        $vehiclename = $this->input->post('vehiclename');
        $vehicleplate = $this->input->post('vehicleplate');
        $capacity = $this->input->post('capacity');
        $data = array(
            'vehiclename' =>$vehiclename,
            'vehicleplate' => $vehicleplate,
            'capacity' => $capacity
        );

     
        $this->db->trans_begin();
        $this->db->insert('msvehicle', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update($vehiclename='',$vehicleplate='',$capacity=''){
        
        $data = array(
            
           'vehiclename' => $this->input->post('vehiclename'),
           'vehicleplate' => $this->input->post('vehicleplate'),
           'capacity' => $this->input->post('capacity'),
           
        );

        $this->db->trans_begin();
        $this->db->where('VehicleId', $this->input->post('vehicleId'));
        $this->db->update('msvehicle', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
    
    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();
        $this->db->where('vehicleid', $this->input->post('vehicleId'));
        $this->db->update('msvehicle', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
}