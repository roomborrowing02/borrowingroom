<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Cs_detail_model extends CI_Model {

        function __construct(){
        parent::__construct();
    }

    public function get_term_code(){        
        $this->db->select('*');
        $this->db->from('msterm');
        $this->db->where('status','Active');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_vehicle_name(){        
        $this->db->select('*');
        $this->db->from('msvehicle');
        $this->db->where('status','Active');

        $query = $this->db->get();

        return $query->result();
    }
    public function get_driver_name(){        
        $this->db->select('*');
        $this->db->from('msdriver');
        $this->db->where('status','Active');

        $query = $this->db->get();

        return $query->result();
    }
    public function get_shift(){        
        $this->db->select('*');
        $this->db->from('msshift');
        $this->db->where('status','Active');

        $query = $this->db->get();

        return $query->result();
    }
     public function get_pickup(){        
        $this->db->select('*');
        $this->db->from('mscampus');
        $this->db->where('status', 'Active');

        $query = $this->db->get();

        return $query->result();
    }
     public function get_dropoff(){        
        $this->db->select('*');
        $this->db->from('mscampus');
        $this->db->where('status = "Active"');

        $query = $this->db->get();

        return $query->result();
    }
    public function cekvalidasi($pickup){
        // print_r($pickup);
        $this->db->select('*');
        $this->db->from('mscampus');
        $this->db->where('campusid !=', $pickup);
        $query = $this->db->get();
        return $query->result();
    }
    public function cek_data($id='') {

          $data = array(
            'td.trdetailshuttleid',
            'td.date',
            'td.day',
            'mt.termcode',
            'mt.termid',
            'msv.vehicleid',
            'msd.driverid',
            's.shiftid',
            's.shift',
            'msc.campusid as campuspickupid',
            'msc2.campusid as campusdropoffid',
            'msc.campusname as pickupLoc',
            'msc2.campusname as dropoffLoc',
            'msv.vehiclename',
            'msd.drivername',
            'td.status'
        );
            $this->db->select($data);
            $this->db->from('trdetailshuttle td');
            $this->db->join('msterm mt','mt.termid=td.termid');
            $this->db->join('msshift s','s.shiftid=td.shiftid');
            $this->db->join('msvehicle msv','msv.vehicleid=td.vehicleid');
            $this->db->join('msdriver msd','msd.driverid=td.driverid');
            $this->db->join('mscampus msc','msc.campusid=td.pickupid');
            $this->db->join('mscampus msc2','msc2.campusid=td.dropoffid');
            $this->db->where('td.status', 'Active');
    if( $id != '' )
            $this->db->where('trdetailshuttleid', $id);
        return $this->db->get();
    }
    public function insert($termcode='',$vehiclename='',$drivername='',$day='',$date='',
        $shift='',$pickup='',$dropoff=''){
        $this->db->set('termid',$this->input->post('termcode'));
        $this->db->set('date',$this->input->post('date'));
        $this->db->set('day',$this->input->post('day'));
        $this->db->set('vehicleid',$this->input->post('vehiclename'));
        $this->db->set('driverid',$this->input->post('drivername'));
        $this->db->set('shiftid',$this->input->post('shift'));
        $this->db->set('pickupid', $this->input->post('pickup'));
        $this->db->set('dropoffid', $this->input->post('dropoff'));

        $this->db->set('status', "Active");
        $this->db->where('trdetailshuttleid', $this->input->post('insertdetail'));      
        $this->db->insert('trdetailshuttle');

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function update($termcode='',$vehiclename='',$drivername='',$day='',
        $date='',$shift='',$pickup='',$dropoff=''){
        $day = $this->input->post('day');
        $date = $this->input->post('date');
        $termid = $this->input->post('termcode');
        $vehicleid = $this->input->post('vehiclename');
        $driverid = $this->input->post('drivername');
        $shiftid = $this->input->post('shift');
        $pickupid = $this->input->post('pickup');
        $dropoffid = $this->input->post('dropoff');

        $this->db->set('day',$day);
        $this->db->set('date',$date);
        $this->db->set('termid',$termid);
        $this->db->set('vehicleid',$vehicleid);
        $this->db->set('driverid',$driverid);
        $this->db->set('shiftid',$shiftid);
        $this->db->set('pickupid', $pickupid);
        $this->db->set('dropoffid', $dropoffid);

        $this->db->where('trdetailshuttleid', $this->input->post('updatedetail'));   
        $this->db->update('trdetailshuttle');

    }
    public function delete(){
        $data = array( 'status' => 'Inactive' );

        $this->db->trans_begin();
        $this->db->where('trdetailshuttleid', $this->input->post('trdetailshuttleid'));
        $this->db->update('trdetailshuttle', $data);

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }
}


