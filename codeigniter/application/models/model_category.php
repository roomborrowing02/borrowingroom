<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_category extends CI_Model{
	public function get_category(){
		$this->db->select('*');
		$this->db->from('mscategory');
		$this->db->where('status','Active')->where('category !=',"Others");
		return $this->db->get();
	}

	public function showlist($subdata=''){
		$data = array(
			'ms.subjectid',
			'ms.subject',
			'mc.categoryid',
			'mc.category'
		);
		$this->db->select($data);
		$this->db->from('mssubject ms');
        $this->db->join('mscategory mc','mc.categoryid=ms.categoryid');
        $this->db->where('ms.status','Active')->where('ms.categoryid',$subdata);
		$query = $this->db->get();
		return $query->result();
	}

	public function insert(){
		$data = array(
			'category' => $this->input->post('category'),
			'status' => "Active"
		);

		$this->db->insert('mscategory',$data);
	}

	public function update(){
		$data = array(
			'category' => $this->input->post('category')
		);
		$this->db->where('categoryid',$this->input->post('CategoryId'));
		$this->db->update('mscategory',$data);
	}


	public function delete(){
		$data = array(
			'status' => 'Inactive'
		);
		$this->db->where('categoryid',$this->input->post('delete'));
		$this->db->update('mscategory',$data);
	}
}