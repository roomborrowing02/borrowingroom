<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_role extends CI_Model
{

    // melakukan pengeceka data-data yang ada pada table mscampus//
    public function cek_role($id = '')
    {
        $data = array(
            'roleid',
            'rolename',
            'description',
        );
        $this->db->select($data);
        $this->db->from('msrole');
        $this->db->where('status', 'Active');

        if ($id != '')
            $this->db->where('roleid', $id);

        return $this->db->get();
    }

    //untuk insert data ke table mscampus//
    public function insert($rolename = '')
    {
        $data = array(

            'rolename' => $this->input->post('rolename'),
            'description' => $this->input->post('description'),

        );

        $this->db->trans_begin();
        $this->db->insert('msrole', $data);

        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    //untuk passing data dari controller dan insert ke table mscampus//
    public function update($rolename = '')
    {
        $data = array(

            'rolename' => $this->input->post('rolename'),
            'description' => $this->input->post('description'),

        );
        $this->db->where('rolename',$this->input->post('rolename'));
        $query = $this->db->get('msrole');
        if ($query->num_rows() > 0){
          echo "<script>alert('Role has Registered');history.go(-1);</script>";
        }
        else{
            $this->db->where('roleid', $this->input->post('RoleId'));
            $this->db->update('msrole', $data);
          echo "<script>history.go(-1);</script>";

            return true;
        }
    }

    public function showname($name = '')
    {
        $data = array(
            'nama',

        );
        $this->db->select($data);
        $this->db->from('msuser');
        $this->db->where('status', 'Active')->where('userid', $name);
        $query = $this->db->get();
        return $query->result();
    }


    //untuk delete table mscampus secara soft delete//
    public function delete()
    {
        $data = array('status' => 'Inactive');
        $this->db->trans_begin();
        $this->db->where('roleid', $this->input->post('roleId'));
        $this->db->update('msrole', $data);

        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }


   public function getrole($role)
     {
        $this->db->select('rolename');
        $this->db->from('msrole');
        $this->db->where('rolename',$role)->where('status','Active');
        $query = $this->db->get();
        return $query->result();
     }
}
    
