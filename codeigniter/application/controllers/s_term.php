<?php
class S_term extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
         $this->load->model('model_term', 'model');
    }

	public function index() {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 4;
        $data['sterm'] = $this->model->sterm();
        $data['sdetail'] = $this->model->sdetail();
        $data['lweek'] = $this->model->lweek();
        $data['g_termcode']= $this->model->get_termcode();
        $data['get_schedule']= $this->model->get_schedule();
        $this->template->load('template', 's_term', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

	}
    
    public function insert($tempcode='',$tempstart='',$tempend='',$tempdescription=''){
        // $tempcode = $this->input->post('termcode');
        // $tempstart = date('Y/m/d H:i:s', strtotime($this->input->post('startdate')));
        // $tempend = date('Y/m/d H:i:s', strtotime($this->input->post('enddate')));
        // $tempdescription = $this->input->post('description');
        // $data = $this->model->insert($tempcode,$tempstart,$tempend,$tempdescription);
        $this->model->insert();
        // echo json_encode($data);
        redirect('s_term');
    }

    public function insertDetail($id='' ,$tempcode='',$tempstart='',$tempend='',$tempdescription='',$tempschedule=''){
        // $tempcode = $this->input->post('tempcode');
        // $tempstart = date('Y/m/d H:i:s', strtotime($this->input->post('tempstart')));
        // $tempend = date('Y/m/d H:i:s', strtotime($this->input->post('tempend')));
        // $tempdescription = $this->input->post('tempdescription');
        // $tempschedule = $this->input->post('tempschedule');
        // $data = $this->model->insertDetail($tempcode,$tempstart,$tempend,$tempdescription,$tempschedule);
        // echo json_encode($data);
        $this->model->insertDetail();
        redirect('s_term'); 
    }

    public function insertlectureweek(){
        $this->model->insertlectureweek();
        redirect('s_term');
    }

    public function update(){
        $this->model->update();
        redirect('s_term');
    }
    public function updatedetail($id='' ,$termcode='',$startdate='',$enddate='',$description='',$Termdetailid='' ,$includeSchedule=''){
        $this->model->updatedetail();
        redirect('s_term');
    }

    public function updatelecture($id='' ,$termcode='',$startdate='',$enddate='',$weeksession=''){
        $this->model->updatelecture();
        redirect('s_term');
    }

    public function delete(){
        $this->model->delete();
        redirect('s_term'); 
    }

    public function deletedetail(){
        $this->model->deletedetail();
        redirect('s_term');
    }

    public function deletelecture(){
        $this->model->deletelecture();
        redirect('s_term');
    }

     public function generate($id='' ,$termid='',$termcode='',$startdate='',$enddate='',$detail=''){
        // $temp2 = $this->model->sdetail($detail);
        // $temp = $this->model->generate();
        $this->model->generate();
        // print_r($temp);
        redirect('s_term');
    }



    public function getData(){
        echo json_encode( $this->model->sterm( $this->input->post('id') )->result()[0] );
    }

    public function getDatadetail(){
        echo json_encode( $this->model->sdetail( $this->input->post('id') )->result()[0] );
    }
    public function getDatalecture(){
        echo json_encode( $this->model->lweek( $this->input->post('id') )->result()[0] );
    }
}
?>