<?php
class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('modelUsers', 'model');
	}

	public function index() {
        if ($this->session->userdata('username') != "") {
		$data['page'] = 2;
		$data['data'] = $this->model->cek_users();
		$data['dataRole'] = $this->model->get_role();
		$this->template->load('template', 'viewUsers', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

	}

	public function insert($rolename='') {
		$data['page'] = 2;
		$this->model->insert();
		redirect('user');
	}

	public function update($rolename=''){
		$data['page'] = 2;
		$this->model->update();
		redirect('user');
	} 

	public function delete()
    {		
    	$data['page'] = 2;
        $this->model->delete();
        redirect('user');
    }

	public function getData(){
		echo json_encode( $this->model->cek_users( $this->input->post('id') )->result()[0] );
	}
}
?>