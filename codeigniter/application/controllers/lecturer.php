
<?php
class Lecturer extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_lecturer', 'model');
    }

    public function index() {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 14;
        $data['data'] = $this->model->getLecturer();
        $this->template->load('template', 'viewlecturer', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

    }

    public function insertFullscreen() {
        $data['page'] = 14;
        $this->load->view('viewLecturerFullscreen');
        //$this->template->load('template', 'viewlecturer', $data);
    }

    public function getLecturerName(){
        $lecturerCode = $this->input->post('lecturerCode');
        if($this->model->getLecturerName($lecturerCode)->num_rows() > 0)
            echo json_encode($this->model->getLecturerName($lecturerCode)->result()[0]);
        else 
            die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
    }

   public function insert($name='',$dob='',$address='',$phone=''){
     
        $drivername= $this->input->post('name');
        $driverdob= $this->input->post('dob');
        $driveraddress= $this->input->post('address');
        $driverphone= $this->input->post('phone');

        $data = $this->model->insert($drivername,$driverdob,$driveraddress,$driverphone);

        echo json_encode($data);
    }

     public function update($id2='',$name2='',$dob2='',$address2='',$phone2=''){
        $driverid2 =$this->input->post('id2');
        $drivername2= $this->input->post('name2');
        $driverdob2= $this->input->post('dob2');
        $driveraddress = $this->input->post('address2');
        $driverphone2= $this->input->post('phone2');

        $data = $this->model->insert($driverid2,$drivername2,$driverdob2,$driveraddress2,$driverphone2);

        echo json_encode($data);
    }


    public function delete(){
        $this->model->delete();
        // echo '<script>alert("You Have Successfully delete this Record!");</script>';
        redirect('index.php/driver','refresh');
    }

    public function getData(){
         echo json_encode( $this->model->cek_driver( $this->input->post('id') )->result()[0] );
     }

}
?>