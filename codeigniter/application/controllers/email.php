<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller {

  public function __construct() {
      parent::__construct();
      $this->load->library('email');
      $this->load->model('model_email','model');
  }

  public function index(){

        date_default_timezone_set('Asia/Jakarta');
        $current_date = date('Y-m-d H:i:s');
        print_r($current_date);
    echo "<br>";


    $data = $this->model->getdata();
    foreach ($data as $row) {
      if (empty($row->dateline) || $row->dateline == '0000-00-00 00:00:00') {
        // do nothing
      }
      else{
        if ($current_date <= $row->dateline) {
          if (empty($row->lastsend) || $row->lastsend == '0000-00-00 00:00:00') {
            $message = "Dear Mr./Ms. " . $row->notesuser."<br><br>";
            $message .='
            <table cellpadding="5">
            <tr>
              <th align="left">Date</th>
              <th> :
                <td align="left">'.date('d-M-Y H:i:s',strtotime($row->date)).'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Name</th>
              <th> :
                <td align="left">'.$row->usernama.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Category</th>
              <th> :
                <td align="left">'.$row->category.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Sub-Category</th>
              <th> :
                <td align="left">'.$row->subject.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Subject</th>
              <th> :
                <td align="left">'.$row->others.'
                </td>
              </th>
            </tr>
            <tr>
              <th align="left">Room</th>
              <th> :
                <td align="left">'.$row->facilityname.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Description</th>
              <th> :
                <td align="left">'.$row->description.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Solution</th>
              <th> :
                <td align="left">'.$row->solution.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Follow Up</th>
              <th> :
                <td align="left">'.date('d-M-Y H:i:s',strtotime($row->dateline)).'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Created By</th>
              <th> :
                <td align="left">'.$row->notesuser.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Status</th>
              <th> :
                <td align="left">'.$row->status.'</td>
              </th>
            </tr>
            ';
            $message .='</table><br><br>';
            $message .= "System sending you this note as a reminder to Follow up your note above."."<br><br>";
            $message .= "<strong>This email is automatically sent by the system. Please do not reply this email. Thank you.</strong>"."<br><br>";
            $message .= "Best Regards";
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "smtp.gmail.com";
            $config['smtp_port'] = "587";
            $config['smtp_user'] = "lscalsut@gmail.com";
            $config['smtp_crypto'] = 'tls';
            $config['smtp_pass' ] = "lscalsut123"; 
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);
            $this->email->from('lscalsut@gmail.com','LSC Alam Sutera Binus University');
            // $this->email->to('asaputra@binus.edu');
            $this->email->to($row->notesemail);
            // $this->email->cc('lecturer_service@binus.edu');
            // $this->email->bcc('them@their-example.com');

            $this->email->subject("[No Reply] Reminder");
            $this->email->message($message);
            if ($this->email->send()) {
            print_r("Email Send");
            $data = array('lastsend' => date('Y-m-d H:i:s',strtotime('+1 day',strtotime($current_date))) );
            $this->db->where('notesid', $row->notesid);
            $this->db->update('msnotes', $data);
            } else {
            show_error($this->email->print_debugger());
            }
          }
        }
        else{
          if (empty($row->lastsend) || $row->lastsend == '0000-00-00 00:00:00') {
            $message = "Dear Mr./Ms. " . $row->notesuser."<br><br>";
            $message .='
            <table cellpadding="5">
            <tr>
              <th align="left">Date</th>
              <th> :
                <td align="left">'.date('d-M-Y H:i:s',strtotime($row->date)).'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Name</th>
              <th> :
                <td align="left">'.$row->usernama.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Category</th>
              <th> :
                <td align="left">'.$row->category.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Sub-Category</th>
              <th> :
                <td align="left">'.$row->subject.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Subject</th>
              <th> :
                <td align="left">'.$row->others.'
                </td>
              </th>
            </tr>
            <tr>
              <th align="left">Room</th>
              <th> :
                <td align="left">'.$row->facilityname.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Description</th>
              <th> :
                <td align="left">'.$row->description.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Solution</th>
              <th> :
                <td align="left">'.$row->solution.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Follow Up</th>
              <th> :
                <td align="left">'.date('d-M-Y H:i:s',strtotime($row->dateline)).'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Created By</th>
              <th> :
                <td align="left">'.$row->notesuser.'</td>
              </th>
            </tr>
            <tr>
              <th align="left">Status</th>
              <th> :
                <td align="left">'.$row->status.'</td>
              </th>
            </tr>
            ';
            $message .='</table><br><br>';
            $message .= "System sending you this note as a reminder to Follow up your note above."."<br><br>";
            $message .= "<strong>This email is automatically sent by the system. Please do not reply this email. Thank you.</strong>"."<br><br>";
            $message .= "Best Regards";
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "smtp.gmail.com";
            $config['smtp_port'] = "587";
            $config['smtp_user'] = "lscalsut@gmail.com";
            $config['smtp_crypto'] = 'tls';
            $config['smtp_pass' ] = "lscalsut123"; 
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);
            $this->email->from('lscalsut@gmail.com','LSC Alam Sutera Binus University');
            // $this->email->to('asaputra@binus.edu');
            $this->email->to($row->notesemail);
            // $this->email->cc('lecturer_service@binus.edu');
            // $this->email->bcc('them@their-example.com');

            $this->email->subject("[No Reply] Reminder");
            $this->email->message($message);
            if ($this->email->send()) {
            print_r("Email Send");
            $data = array('lastsend' => date('Y-m-d H:i:s',strtotime('+1 day',strtotime($current_date))) );
            $this->db->where('notesid', $row->notesid);
            $this->db->update('msnotes', $data);
            } else {
            show_error($this->email->print_debugger());
            }
          }
          else{
            if ($row->status != "Close") {
              if (date('Y-m-d H:i:s',strtotime($row->lastsend)) <= $current_date && $current_date <= date('Y-m-d H:i:s',strtotime('+5 minutes',strtotime($row->lastsend)))) {
                $message = "Dear Mr./Ms. " . $row->notesuser."<br><br>";
                $message .='
                <table cellpadding="5">
                <tr>
                  <th align="left">Date</th>
                  <th> :
                    <td align="left">'.date('d-M-Y H:i:s',strtotime($row->date)).'</td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Name</th>
                  <th> :
                    <td align="left">'.$row->usernama.'</td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Category</th>
                  <th> :
                    <td align="left">'.$row->category.'</td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Sub-Category</th>
                  <th> :
                    <td align="left">'.$row->subject.'</td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Subject</th>
                  <th> :
                    <td align="left">'.$row->others.'
                    </td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Room</th>
                  <th> :
                    <td align="left">'.$row->facilityname.'</td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Description</th>
                  <th> :
                    <td align="left">'.$row->description.'</td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Solution</th>
                  <th> :
                    <td align="left">'.$row->solution.'</td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Follow Up</th>
                  <th> :
                    <td align="left">'.date('d-M-Y H:i:s',strtotime($row->dateline)).'</td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Created By</th>
                  <th> :
                    <td align="left">'.$row->notesuser.'</td>
                  </th>
                </tr>
                <tr>
                  <th align="left">Status</th>
                  <th> :
                    <td align="left">'.$row->status.'</td>
                  </th>
                </tr>
                ';
                $message .='</table><br><br>';
                $message .= "System sending you this note as a reminder to Follow up your note above."."<br><br>";
                $message .= "<strong>This email is automatically sent by the system. Please do not reply this email. Thank you.</strong>"."<br><br>";
                $message .= "Best Regards";
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "smtp.gmail.com";
                $config['smtp_port'] = "587";
                $config['smtp_user'] = "lscalsut@gmail.com";
                $config['smtp_crypto'] = 'tls';
                $config['smtp_pass' ] = "lscalsut123"; 
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $this->email->initialize($config);
                $this->email->from('lscalsut@gmail.com','LSC Alam Sutera Binus University');
                // $this->email->to('asaputra@binus.edu');
                $this->email->to($row->notesemail);
                // $this->email->cc('lecturer_service@binus.edu');
                // $this->email->bcc('them@their-example.com');

                $this->email->subject("[No Reply] Reminder");
                $this->email->message($message);
                if ($this->email->send()) {
                print_r("Email Send");
                $data = array('lastsend' => date('Y-m-d H:i:s',strtotime('+1 day',strtotime($row->lastsend))) );
                $this->db->where('notesid', $row->notesid);
                    $this->db->update('msnotes', $data);
                } else {
                show_error($this->email->print_debugger());
                }


              }
            }
          }
        }
      }
    }

    $this->load->view('email');
  }

  public function sendemail($time=''){
    $time = $this->input->post('time');
    $data = $this->model->getdata();
        date_default_timezone_set('Asia/Jakarta');
        $current_date = date('Y-m-d H:i:s');
        print_r($current_date);
    foreach ($data as $row) {
      if ($time == $row->dateline) {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "smtp.gmail.com";
        $config['smtp_port'] = "587";
        $config['smtp_user'] = "lscalsut@gmail.com";
        $config['smtp_crypto'] = 'tls';
        $config['smtp_pass'] = "lscalsut123"; 
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $this->email->initialize($config);
        $this->email->from('lscalsut@gmail.com','LSC Alam Sutera Binus University');
        $this->email->to('asaputra@binus.edu');
        $this->email->cc('asaputra@binus.edu');
        // $this->email->bcc('them@their-example.com');

        $this->email->subject($row->category);
        $this->email->message('Testing the email class.');
        if ($this->email->send()) {
        $temptime = "Email Send";
        } else {
        $temptime = show_error($this->email->print_debugger());
        }
      }
      else{
        $temptime = $time;
      }
    }
  }
}

?>