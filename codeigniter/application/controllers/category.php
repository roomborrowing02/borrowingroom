<?php

class Category extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_category', 'model');
    }

    // menampilkan data ke viewkendaraan//
    public function index() {
        if ($this->session->userdata('username') != "") {
            $data['page'] = 25;
            $data['data'] = $this->model->get_category();
            $this->template->load('template', 'viewcategory', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }
    }

    // passing data insert dari view untuk ke model//
    public function insert(){
        $this->model->insert();
        redirect('category');
    }

    // passing data insert hasi update dari view untuk ke model//
    public function update(){
       $this->model->update();
        redirect('category');
    }

    //untuk fungsi button delete
    public function delete(){
        $this->model->delete();
        redirect('category');
    }
    
    public function showlist($subdata=''){
        $subdata = $this->input->post('subdata');
        $data = $this->model->showlist($subdata);

        echo json_encode($data);
    }

    public function getData(){
        echo json_encode( $this->model->get_category( $this->input->post('id') )->result()[0] );
    }

}
?>