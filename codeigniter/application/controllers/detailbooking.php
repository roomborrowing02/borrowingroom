
    <?php

    class Detailbooking extends CI_Controller {

         public function __construct()
    {
        parent::__construct();
        $this->load->model('model_detailbooking', 'model');
    }

        public function bookingperiode(){
        if ($this->session->userdata('username') != "") {
          $data['page'] = 12;
          $data['data'] = $this->model->cek_detailbookingperiode();
          $data['dataPickup'] = $this->model->get_datapickup();
          $data['dataDropoff'] = $this->model->get_datadropoff();
          $data['dataTerm'] = $this->model->get_term();
          $data['dataShift'] = $this->model->get_shift();
          $data['dataDay'] = $this->model->get_dataday();       
          $this->template->load('template', 'viewsdetailbookingperiode', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }
        
        }  


        public function deleteperiode(){
        $this->model->deleteperiode();
        redirect('index.php/detailbooking');
        }
        
  
        public function getDataPeriode(){
        echo json_encode( $this->model->cek_detailbookingperiode( $this->input->post('id') )->result()[0] );
        }
        
        //Booking Date// 
        public function index() {
          $data['page'] = 12;
          $data['data'] = $this->model->cek_detailbooking();
          $data['dataPickup'] = $this->model->get_pickup();
          $data['dataDropoff'] = $this->model->get_dropoff();
          $data['dataShift'] = $this->model->get_datashift();
          $data['dataDate'] = $this->model->get_date();
          $data['dataquery'] = $this->model->get_queryall();
          $this->template->load('template', 'viewsdetailbooking', $data);
        }

  
        public function delete($date='',$shift='',$nama='',$email='',$phone='',$pickupid='',$dropoffid='',$description='',$name='',$id='',$userid='',$totalorder=''){

          $this->model->delete();
          redirect('index.php/detailbooking');
        }


        public function getData(){
             echo json_encode( $this->model->cek_detailbooking( $this->input->post('id') )->result()[0] );
         } 
                                  
 }
 ?>