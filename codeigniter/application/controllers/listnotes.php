<?php

class Listnotes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_listnotes', 'model');
    }

    // menampilkan data ke viewkendaraan//
    public function index() {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 24;
        $data['data'] = $this->model->getnotes();
        $this->template->load('template', 'viewlistnotes', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

    }

    public function delete(){
        $this->model->delete();
        redirect('listnotes');
    }

}
?>