<?php

class Notes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_notes', 'model');
    }

    // menampilkan data ke viewkendaraan//
    public function index($list='') {
        if ($this->session->userdata('username') != "") {
        date_default_timezone_set('Asia/Jakarta');
        $current_date = date('M-d-Y == H:i:s');
        // date("M-d-Y H:i:s");
        $data['page'] = 23;
        $list = $this->input->post('id');
        $data['room'] = $this->model->room();
        $data['date'] = $current_date;
        $data['list'] = $this->model->get_list($list);
        $data['category'] = $this->model->category();
        $this->template->load('template', 'viewnotes', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

    }

    // passing data insert dari view untuk ke model//
    public function insert(){
        $this->model->insert();
        redirect('notes');
    }

    // ini untuk updatenotes dan view

    public function updatenotes($list=''){
        // $list = $this->input->post('id');
        $list = $this->uri->segment(3);
        $data['page'] = 23;
        $data['room'] = $this->model->room();
        $data['list'] = $this->model->get_list($list);
        $data['category'] = $this->model->category();
        $data['subject'] = $this->model->subject();
        $this->template->load('template', 'viewupdatenotes', $data);
    }

    public function view($list=''){
        $list = $this->uri->segment(3);
        $data['page'] = 23;
        $data['room'] = $this->model->room();
        $data['list'] = $this->model->get_list($list);
        $data['category'] = $this->model->category();
        $data['subject'] = $this->model->subject();
        $this->template->load('template', 'viewdetailnotes', $data);
    }

    public function update(){
        $this->model->update();
        redirect('listnotes');
    }

    // ini untuk ajax search autocomplete / select2 kode dosen / mahasiswa

    function search_user(){
        $this->load->model('model_notes');
        $nimdata = $_GET['nimdata'];
        $data_book = $this->model_notes->get_user($nimdata,'nim');
        echo json_encode($data_book);
    }

    // ini untuk ajax subject
    function getdata($notesid=''){
      $notesid = $this->input->post('notesid');
      $data = $this->model->getdata($notesid);
      $tempData = $data->result();

      echo json_encode($tempData);
    }

}
?>