<?php
class Role extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_role', 'model');
    }
    //untuk menampilkan data yang telah di insert ke viewscampus//
    public function index()
    {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 22;
        $data['data'] = $this->model->cek_role();
        $this->template->load('template', 'viewsrole', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

    }

    // untuk melkukan insert melalui button insert yang terdapat form insert//
    public function insert()
    {
        $data['page'] = 22;
        $this->model->insert();
        redirect('role');
    }

    // untuk melakukan update pada data yang suda ada melalui button update yang terdapat form update campus//  
    public function update()
    {
        $data['page'] = 22;
        $this->model->update();
        //echo '<script>alert("You Have Successfully delete this Record!");</script>';
        // redirect('role');
    }
    //untuk melakukan delete data yang sudah tidak terpakai//
    public function delete()
    {
        $data['page'] = 22;
        $this->model->delete();
        redirect('role');
    }

    public function showname($name = '')
    {
        $name = $this->input->post('name');
        $data = $this->model->showname($name);

        echo json_encode($data);
    }


    public function getData()
    {
        echo json_encode($this->model->cek_role($this->input->post('id'))->result()[0]);
    }

    // public function checkrole()
    //    {
    //     $this->load->model('Search_model');
    //     if($this->Search_model->getUsername($_POST['rolemaklo'])){
    //      echo '<label class="text-danger"><span><i class="fa fa-times" aria-hidden="true">
    //      </i> This rolename is already registered</span></label>';
    //     }
    //     else {
    //      echo '<label class="text-success"><span><i class="fa fa-check-circle-o" aria-hidden="true"></i> Rolename is available</span></label>';
    //     }
    //  }

    public function checkrole($rolename=''){
      $role = $this->input->post('rolename');
      // $this->load->model('Search_model');
      $data = $this->model->getrole($role);
      if (empty($data)) {
        echo json_encode("kosong");
      }
      else{
        echo json_encode($data);
      }

    }

}


