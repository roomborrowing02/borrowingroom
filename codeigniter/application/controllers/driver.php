<?php
class Driver extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_driver', 'model');
    }


  public function index() {
    if ($this->session->userdata('username') != "") {
      $data['page'] = 13;
      $data['data'] = $this->model->cek_driver();
      $this->template->load('template', 'viewsdriver', $data);
    }
    else{
      echo "<script>
      alert('Your session is end , please log in first');
      window.location.href='auth';
      </script>";
    }
   
  }

   public function insert($drivername='',$dob='',$address='',$phone=''){
     
        $this->model->insert();
       redirect('index.php/driver');
    }

     public function update($drivername='',$dob='',$address='',$phone=''){
       $this->model->update();
       redirect('index.php/driver');
}

    public function delete(){
        $this->model->delete();
        redirect('index.php/driver');
    }

    public function getData(){
         echo json_encode( $this->model->cek_driver( $this->input->post('id') )->result()[0] );
     }

}
?>