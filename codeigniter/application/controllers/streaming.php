 <?php
class Streaming extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_streaming', 'model');
        $this->load->helper(array('form', 'url'));

    }

    public function index() {
        $data['page'] = 19;
        $data['data'] = $this->model->get_data();
        $this->template->load('template', 'viewstreaming', $data);

    }

    public function fullscreen(){
        $streamingid = $this->uri->segment(3);
        $data['page'] = 19;
        $data['id'] = $this->model->fullscreen($streamingid);

        // $this->template->load('template', 'viewfullscreenstreaming', $data);    
        $this->load->view('viewfullscreenstreaming',$data);



    }

    public function insert(){
    	$content = $this->input->post('content');
    	// print_r($content);
    	// echo "<br>";
    	$description = $this->input->post('description');
    	// $hasil = getBetween($content,"v","&");
    	// $content = strstr($content,"v");
    	// $content = strstr($content,"&");
    	parse_str(parse_url($content, PHP_URL_QUERY), $content);
    	// print $content['v'];

    	// print_r($content);
    	// print_r($hasil);
    	$data = array(
    		'content'=>$content['v'],
    		'description'=>$description
    	);
    	$this->db->insert('streaming',$data);
    	redirect('streaming');
    }

    // public function getData(){
    //     echo json_encode( $this->model->get_data( $this->input->post('id') )->result()[0] );
    // }
}
?>