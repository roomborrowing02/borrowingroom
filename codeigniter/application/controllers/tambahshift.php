<?php
class Tambahshift extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_shift', 'model');
    }

	public function index() {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 5;
        $data['data'] = $this->model->get_shifts();
        $this->template->load('template', 'tambahshift', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

	}

    public function cekvalidasi($tempshift=''){
        $tempshift = $this->input->post('shift');
        $data = $this->model->insert($tempshift);
        echo json_encode($tempshift);
        // redirect('index.php/tambahshift');
    }
    
    public function update(){
        $this->model->update();
        redirect('index.php/tambahshift');

    }

    public function delete(){
        $this->model->delete();
        redirect('index.php/tambahshift');
    }

    public function getData(){
        echo json_encode( $this->model->get_shifts( $this->input->post('id') )->result()[0] );
    }
}
?>