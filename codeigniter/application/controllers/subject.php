<?php

class Subject extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_subject', 'model');
    }

    // menampilkan data ke viewkendaraan//
    public function index() {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 26;
        $data['data'] = $this->model->get_subject();
        $data['category'] = $this->model->get_category();
        $this->template->load('template', 'viewsubject', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

    }

    // passing data insert dari view untuk ke model//
    public function insert(){
        $this->model->insert();
        redirect('subject');

    }

    // passing data insert hasi update dari view untuk ke model//
    public function update(){
       $this->model->update();
        redirect('subject');
    }

    //untuk fungsi button delete
    public function delete(){
        $this->model->delete();
        redirect('subject');
    }
  
    public function getData(){
        echo json_encode( $this->model->get_subhject( $this->input->post('id') )->result()[0] );
    }

}
?>