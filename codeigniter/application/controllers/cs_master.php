
<?php
class Cs_master extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('cs_master_model', 'model');
    }
	public function index() {
        if ($this->session->userdata('username') != "") {
            $data['page'] = 9;
            $data['data'] = $this->model->cek_data();
            $data['dataTerm'] = $this->model->get_term_code();
            $data['dataVehicle'] = $this->model->get_vehicle_name();
            $data['dataDriver'] = $this->model->get_driver_name();
            $data['dataShift'] = $this->model->get_shift();
            $data['dataPickup'] = $this->model->get_pickup();
            $data['dataDropoff'] = $this->model->get_dropoff();
            $this->template->load('template', 'cs_master', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

	}
    public function insert($termcode='',$vehiclename='',$drivername='',$shift='',
        $pickup='',$dropoff='')
    {
        $this->model->insert();
        // print_r($temp);
        redirect('index.php/cs_master');
    }
    public function update($termcode='',$vehiclename='',$drivername='',$shift='',
        $pickup='',$dropoff='')
    {
        $this->model->update();
        // print_r($temp);
        redirect('index.php/cs_master');    }
    public function delete(){
        $this->model->delete();
        redirect('index.php/cs_master');
    }
    public function generate($termcode='',$vehicle='',$driver='',$day='',$shift='',
        $pickup='',$dropoff='',$shuttleid='',$temp=''){
        // $this->model->generate();
        $this->model->generate();
        redirect('index.php/cs_master');
        // print_r($temp);
    }
    public function getData()
    {
            echo json_encode( $this->model->cek_data( $this->input->post('id') )->result()[0] );
    }
     public function get_spec($pickupdata="",$dropoffdata){
        $pickupdata = $this->input->post('pickupid');
        $dropoffdata = $this->input->post('dropoffid');
        $data = $this->model->get_dataspec($pickupdata,$dropoffdata);
        $tempData = $data->result();
      echo json_encode($tempData);  
    }

    public function cekvalidasi(){
        $pickup = $this->input->post('pickupdata');
        $data = $this->model->cekvalidasi($pickup);
        // print_r($pickup);
        echo json_encode($data);
        // redirect('index.php/tambahshift');s
    }

    public function cekvalidasi2(){
        $pickup = $this->input->post('pickupdata');
        $data = $this->model->cekvalidasi2($pickup);
        // print_r($pickup);
        echo json_encode($data);
        // redirect('index.php/tambahshift');s
    }
}
?>