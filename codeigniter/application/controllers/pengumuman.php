 <?php
class Pengumuman extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pengumuman', 'model');
        $this->load->helper(array('form', 'url'));

    }

    public function index() {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 16;
        $data['data'] = $this->model->get_data();
        $data['date'] =  $tempdate = date('Y-m-d');
        // $data['status'] = $this->model->get_status();
        // header("Content-type: image/jpeg");
        $this->template->load('template', 'viewpengumuman', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }


    }

    public function fullscreen(){
        $data['data'] = $this->model->fullscreen();
        $this->load->view('viewFullscreenPengumuman',$data);
    }

    public function upload(){
        $data['page'] = 16;
        $this->template->load('template', 'viewupload',$data);
    }

       function get_content(){
        $this->model->fullscreen();
    }

    public function update(){
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
        $description = $this->input->post('description');
        $pengumumanid = $this->input->post('Pengumumanid');
        $rand = md5(uniqid(rand(), true));

         $this->load->helper(array('form', 'url'));
        if (empty($_FILES['file_name']['name']))
        {
             $data = array(
                'startdate' => $startdate,
                'enddate' => $enddate,
                'description' =>$description       
             );
            $this->db->where('pengumumanid',$pengumumanid);
            $this->db->update('mspengumuman',$data);
            redirect('pengumuman');
       
        }else{
        $config['upload_path']          = './assets/images/upload/';
        $config['allowed_types']        = '*';
        $config['file_name'] = $rand;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file_name');
        $fileData = $this->upload->data();
        $data = array(
                'content' => $fileData['file_name'],
                'startdate' => $startdate,
                'enddate' => $enddate,
                'description' =>$description       
             );
            $this->db->where('pengumumanid',$pengumumanid);
            $this->db->update('mspengumuman',$data);
            redirect('pengumuman');
        }

    }

    public function delete(){
        $this->model->delete();
        redirect('pengumuman');
    }
    public function insertupload(){
        $rand = md5(uniqid(rand(), true));
        $config = array();
        $config['upload_path'] =  './assets/images/upload/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG';
        $config['max_size'] = '256000';

        $this->load->library('upload', $config);

        $files = $_FILES;
        $cpy = count($_FILES['file']['name']);
        $tempname = $_FILES['file']['name'];
        for($i=0; $i<$cpy; $i++)
        {   
            if ($tempname == "gif" || $tempname == "jpg" || $tempname == "png" || $tempname == "jpeg") {
                $_FILES['file']['name']= $files['file']['name'][$i];
                $_FILES['file']['type']= $files['file']['type'][$i];
                $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
                $_FILES['file']['error']= $files['file']['error'][$i];
                $_FILES['file']['size']= $files['file']['size'][$i];    
                $config['file_name'] = $rand;
                $this->upload->initialize($config);
                $this->upload->do_upload('file');


            }else{
                $_FILES['file']['name']= $files['file']['name'][$i];
                $_FILES['file']['type']= $files['file']['type'][$i];
                $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
                $_FILES['file']['error']= $files['file']['error'][$i];
                $_FILES['file']['size']= $files['file']['size'][$i];    
                $config['file_name'] = $rand.".png";
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);
                $this->upload->do_upload('file');
            }
        }
            $fileData = $this->upload->data();
            echo json_encode($fileData);

    }
    public function form($params='[]'){
        $params = json_decode(urldecode($params), TRUE);
        $data['page'] = 16;
        $data['tempimg'] = $params;
        $this->template->load('template', 'viewformupload',$data);
    }

    public function formupload(){
        $user = $this->session->userdata('username');
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
        $description = $this->input->post('description');
        $tempgambar = $this->input->post('tempgambar');
        $count = count($description);
        for ($i=0; $i < $count; $i++) { 
                $data[] = array(
                    'content' => $tempgambar[$i],
                    'startdate' => $startdate[$i],
                    'enddate' => $enddate[$i],
                    'description' =>$description[$i],
                    'createdby' => $user
                );

        }
         $this->db->insert_batch('mspengumuman',$data);
        redirect('pengumuman');
    }

    public function remove()
    {
        $file = $this->input->post("file");
        $filename = $file['file_name'];
        if ($filename && file_exists('./assets/images/upload/' . "/" . $filename)) {
            unlink('./assets/images/upload/' . "/" . $filename);
        }
        echo json_encode($filename);
    }

    public function nextpage(){
        $temp = $this->input->post('idvar');
        echo $temp;
    }

    public function getData(){
        echo json_encode( $this->model->get_data( $this->input->post('id') )->result()[0] );
    }
}
?>