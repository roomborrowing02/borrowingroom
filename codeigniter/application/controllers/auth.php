<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

  public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

	public function index() {
		$this->load->view('login');
	}

	public function cek_login() {
		$username = $this->input->post('username', TRUE);
		$password = md5($this->input->post('password', TRUE));
		$this->load->model('modelUsers'); // load model_user
		$hasil = $this->modelUsers->cekuser($username,$password);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['userid'] = $sess->userid;
				$sess_data['username'] = $sess->username;
				$sess_data['rolename'] = $sess->rolename;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('rolename')=='admin' || $this->session->userdata('rolename')=='member') {
				
				redirect('dashboard/');
			}
			// elseif ($this->session->userdata('role')=='member') {
			// 	redirect('member');
			// }		
		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('role');
		session_destroy();
		redirect('/');
	}

	public function view(){
		$this->load->view('template');
	}

}

?>