<?php
class Cs_detail extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('cs_detail_model', 'model');
    }

    public function index() {
        if ($this->session->userdata('username') != "") {
            $data['page'] = 10;
            $data['data'] = $this->model->cek_data();
            $data['dataTerm'] = $this->model->get_term_code();
            $data['dataVehicle'] = $this->model->get_vehicle_name();
            $data['dataDriver'] = $this->model->get_driver_name();
            $data['dataShift'] = $this->model->get_shift();
            $data['dataPickup'] = $this->model->get_pickup();
            $data['dataDropoff'] = $this->model->get_dropoff();
            
            $this->template->load('template', 'cs_detail', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }
        
    }

    public function insert($termcode='',$vehiclename='',$drivername='',$day='',$date='',
        $shift='',$pickup='',$dropoff='')
    {
        $this->model->insert();
        redirect('index.php/cs_detail');
    }

    public function update($termcode='',$vehiclename='',$drivername='',$shift='',$pickup='',$dropoff='')
    {
        $this->model->update();
        redirect('index.php/cs_detail');
    }

    public function delete()
    {
        $this->model->delete();
        redirect('index.php/cs_detail');
    }
   public function cekvalidasi(){
    $pickup = $this->input->post('pickupdata');
    $data = $this->model->cekvalidasi($pickup);
    echo json_encode($data);
    // redirect('index.php/tambahshift');s
    }
    public function getData()
    {
        echo json_encode( $this->model->cek_data( $this->input->post('id') )->result()[0] );
    }

}
?>