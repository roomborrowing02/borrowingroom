<?php
class Booking extends CI_Controller {

  public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('model_trbooking', 'model');
    }
  
  public function index(){
    if ($this->session->userdata('username') != "") {
      $data['page'] = 11;
      $data['data'] = $this->model->cek_data();
      $data['dataPickup'] = $this->model->get_pickup();
      $data['dataTerm'] = $this->model->get_term();
      $this->template->load('template', 'viewsbooking', $data);
    }
    else{
      echo "<script>
      alert('Your session is end , please log in first');
      window.location.href='auth';
      </script>";
    }

  }
  public function test(){
    $data['page'] = 11;
    $data['data'] = $this->model->cek_data();
    $data['dataPickup'] = $this->model->get_pickup();
    $data['dataTerm'] = $this->model->get_term();
    $this->template->load('template', 'viewstesting', $data);
  }
  
  public function insert($id = '',$date = '',$shift='',$name='',$email='',$phone='',$termCode='',$pickup='', $dropoff='', $description='' , $term='' ,$doid='' ,$puid='',$sid='',$capacity='',$order1='',$order2='',$order3='',$order4='',$orde5r='',$order6='',$order7='',$order8='',$order9='',$order10='',$day='',$shuttleid=''){
        // $temp = $this->model->insert();
      $this->model->insert();
        // print_r($temp);
        redirect('index.php/booking');
  }

  public function periode(){
    $data['page'] = 17;
    $data['data'] = $this->model->cek_data();
    $data['dataPickup'] = $this->model->get_pickup();
    $data['dataTerm'] = $this->model->get_term();
    $this->template->load('template', 'viewsbookingperiode', $data);
  }

  public function insertperiode($id = '',$date = '',$shift='',$name='',$email='',$phone='',$termCode='',$pickup='', $dropoff='', $description='' , $term='' ,$doid='' ,$puid='',$sid='',$capacity='',$order1='',$order2='',$order3='',$order4='',$orde5r='',$order6='',$order7='',$order8='',$order9='',$order10='',$day=''){
        // $temp = $this->model->insert();
      $this->model->insertperiode();
        // print_r($temp);
        redirect('booking/periode');
  }
    
  public function getpickup(){
      $this->model->get_pickup();
  }

    function get_datauser($datauser=''){
      $datauser = $this->input->post('datauser');

      $data = $this->model->get_datauser($datauser);
      $tempData = $data->result();

      echo json_encode($tempData);

        
    }

    function get_data($frmData ='',$tempData='',$pickupdata=''){
      $pickupdata = $this->input->post('pickupdata');

      $data = $this->model->get_campus($pickupdata);
      $tempData = $data->result();

      echo json_encode($tempData);

        
    }

    function get_data2($dropoffdata ='',$tempData='',$pickupdata='',$tempdate=''){
      $pickupdata = $this->input->post('pickupdata');
      // $pickupdata = "4";
      $dropoffdata = $this->input->post('dropoffdata');
      // $dropoffdata = "1";
      $tempdate =  date("y-m-d");
      // echo $tempdate;
      $data = $this->model->get_data2($pickupdata,$dropoffdata,$tempdate);
      $tempData = $data->result();

      echo json_encode($tempData);
        
    }

    function get_data3($tanggal='',$dropoffdata ='',$tempData='',$pickupdata=''){
      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $tanggal = $this->input->post('tanggal');

      $data = $this->model->get_data3($pickupdata,$dropoffdata,$tanggal);
      $tempData = $data->result();

      echo json_encode($tempData);

        
    }

    function get_data4($tanggal='',$dropoffdata ='',$tempData='',$pickupdata='',$tempshift=''){
      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $tanggal = $this->input->post('tanggal');
      $tempshift = $this->input->post('tempshift');

      $data = $this->model->get_data4($pickupdata,$dropoffdata,$tanggal,$tempshift);
      $tempData = $data->result();

      echo json_encode($tempData);

        
    }

     function get_data5($vehicledata='',$pickupdata='',$dropoffdata='',$tanggal='',$tempshift=''){
      $vehicledata = $this->input->post('vehicledata');
      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $tanggal = $this->input->post('tanggal');
      $tempshift = $this->input->post('tempshift');

      $data = $this->model->get_data5($vehicledata,$pickupdata,$dropoffdata,$tanggal,$tempshift);
      
      $tempData = $data;

      echo json_encode($tempData);

        
    }

    function get_data6($vehicledata='',$pickupdata='',$dropoffdata='',$tanggal='',$tempshift=''){
      $vehicledata = $this->input->post('vehicledata');
      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $tanggal = $this->input->post('tanggal');
      $tempshift = $this->input->post('tempshift');

      $data = $this->model->get_data6($vehicledata,$pickupdata,$dropoffdata,$tanggal,$tempshift);

      $tempData = $data;

      echo json_encode($tempData);

    }

    function getdata1($frmData ='',$tempData='',$termdata=''){
      $termdata = $this->input->post('termdata');
      $data = $this->model->getdata1($termdata);
      $tempData = $data->result();

      echo json_encode($tempData);
    
    }

    function getdata2($frmData ='',$tempData='',$pickupdata='',$termdata=''){
      $pickupdata = $this->input->post('pickupdata');
      $termdata = $this->input->post('termdata');
      $data = $this->model->getdata2($pickupdata,$termdata);
      $tempData = $data->result();

      echo json_encode($tempData);
    
    }
    function getdata3($dropoffdata ='',$tempData='',$pickupdata='',$termdata=''){
      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $termdata = $this->input->post('termdata');
      $data = $this->model->getdata3($pickupdata,$dropoffdata,$termdata);
      $tempData = $data->result();

      echo json_encode($tempData);
        
    }

    function getdata4($hari='',$dropoffdata ='',$tempData='',$pickupdata='',$termdata=''){
      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $hari = $this->input->post('hari');
      $termdata = $this->input->post('termdata');

      $data = $this->model->getdata4($pickupdata,$dropoffdata,$hari,$termdata);
      $tempData = $data->result();

      echo json_encode($tempData);
  
    }

    function getdata5($hari='',$dropoffdata ='',$tempData='',$pickupdata='',$tempshift='',$termdata=''){
      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $hari = $this->input->post('hari');
      $tempshift = $this->input->post('tempshift');
      $termdata = $this->input->post('termdata');

      $data = $this->model->getdata5($pickupdata,$dropoffdata,$hari,$tempshift,$termdata);
      $tempData = $data->result();

      echo json_encode($tempData);

        
    }

    function getdata6($vehicledata='',$hari='',$dropoffdata ='',$tempData='',$pickupdata='',$tempshift='',$termdata=''){
      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $hari = $this->input->post('hari');
      $tempshift = $this->input->post('tempshift');
      $termdata = $this->input->post('termdata');
      $vehicledata = $this->input->post('vehicledata');
      
      $data = $this->model->getdata6($vehicledata,$hari,$dropoffdata,$pickupdata,$tempshift,$termdata);
      $tempData = array($data);

      echo json_encode($tempData);

        
    }

     function getdata7($vehicledata='',$hari='',$dropoffdata ='',$tempData='',$pickupdata='',$tempshift='',$termdata=''){
      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $hari = $this->input->post('hari');
      $tempshift = $this->input->post('tempshift');
      $termdata = $this->input->post('termdata');
      $vehicledata = $this->input->post('vehicledata');
      
      $data = $this->model->getdata7($vehicledata,$hari,$dropoffdata,$pickupdata,$tempshift,$termdata);
      $tempData = $data;

      echo json_encode($tempData);

        
    }

    public function listcapacity($vehicledata='',$hari='',$dropoffdata ='',$tempData='',$pickupdata='',$tempshift='',$termdata=''){

      $pickupdata = $this->input->post('pickupdata');
      $dropoffdata = $this->input->post('dropoffdata');
      $hari = $this->input->post('hari');
      $tempshift = $this->input->post('tempshift');
      $termdata = $this->input->post('termdata');
      $vehicledata = $this->input->post('vehicledata');

      $data = $this->model->listcapacity($vehicledata,$hari,$dropoffdata,$pickupdata,$tempshift,$termdata);
      $tempData = $data;

      echo json_encode($tempData);
    }

    public function getData(){
        echo json_encode( $this->model->cek_data( $this->input->post('id') )->result()[0] );
    }

    function get_birds(){
        $this->load->model('model_trbooking');
        if (isset($_GET['term'])){
          $q = strtolower($_GET['term']);
          $this->model_trbooking->get_bird($q);
        }
    }
}
?>