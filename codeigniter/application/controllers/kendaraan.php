<?php

class Kendaraan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_kendaraan', 'model');
    }

    public function index() {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 6;
        $data['data'] = $this->model->cek_kendaraan();
        $this->template->load('template', 'kendaraan', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

    }

    public function insert(){
        
        $this->model->insert();
        redirect('index.php/kendaraan');

    }

    public function update($vehiclename2='',$vehicleplate2='',$capacity2=''){

       $this->model->update();
        redirect('index.php/kendaraan');
    }

    public function delete(){
        $this->model->delete();
        redirect('index.php/kendaraan');
    }

        public function getData(){
        echo json_encode( $this->model->cek_kendaraan( $this->input->post('id') )->result()[0] );
    }

}
?>