<?php
class Itemcontrol extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_facilityitem', 'model');
    }

    public function index() {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 15;
        $data['data'] = $this->model->cek_item();
 //        $data['dataCampus'] = $this->model->get_campuscode();
        $this->template->load('template', 'viewsitem', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

     }

    public function insert($itemname='',$specification=''){
        $data['page'] = 15;
        $this->model->insert();
        redirect('index.php/itemcontrol');
    }

    public function update($itemname='',$specification=''){
        $data['page'] = 15;
        $this->model->update();
        redirect('index.php/itemcontrol');
    }

    public function delete(){
        $data['page'] = 15;
        $this->model->delete();
        redirect('index.php/itemcontrol');
    }
        public function getData(){
        echo json_encode( $this->model->cek_item( $this->input->post('id') )->result()[0] );
    }

}
?>