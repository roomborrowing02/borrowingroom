<?php
class Weblink extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_link', 'model');
    }
//untuk mengambil data dari database 
    public function index() {
        if ($this->session->userdata('username') != "") {
        $data['page'] = 21;
        $data['data'] = $this->model->cek_link();
        $this->template->load('template', 'viewlink', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

     }
//function untuk tombol insert
    public function insert($name='',$description='',$link=''){
        $data['page'] = 21;
        $this->model->insert();
        redirect('index.php/weblink');
    }
//function untuk tombol update
    public function update($name='',$description='',$link=''){
        $data['page'] = 21;
        $this->model->update();
        redirect('index.php/weblink');
    }
//function untuk tombol delete
    public function delete(){
        $data['page'] = 21;
        $this->model->delete();
        redirect('index.php/weblink');
    }
    public function activate()
    {
        $data['page'] = 21;
        $this->model->activate();

        redirect('index.php/weblink');
    }

    public function getData(){
        echo json_encode( $this->model->cek_item( $this->input->post('id') )->result()[0] );
    }

}
?>