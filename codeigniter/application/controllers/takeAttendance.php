<?php

class TakeAttendance extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_takeAttendance', 'model');
    }

    public function index($list='') {
        if ($this->session->userdata('username') != "") {
        date_default_timezone_set('Asia/Jakarta');
        $current_date = date('M-d-Y == H:i:s');
        $data['page'] = 27;
        $this->template->load('template', 'viewTakeAttendance', $data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

    }

// passing data insert dari view untuk ke model//
    public function actionTakeAttendance(){
        $data = $this->model->takeAttendance();
        //echo json_encode($data);
    }
}
?>