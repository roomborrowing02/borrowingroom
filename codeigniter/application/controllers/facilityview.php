<?php
class Facilityview extends CI_Controller {

    public function __construct()
    {

        parent::__construct();
        $this->load->model('model_facilityroom', 'model');
    }


    public function view($facilityid=''){
        if ($this->session->userdata('username') != "") {
            $facilityid = $this->uri->segment(3);
            $data['page'] = 18;
            $data['facility'] = $facilityid;
            $data['detail'] = $this->model->view($facilityid);
            $data['room'] = $this->model->get_room($facilityid);
            $data['dataItem'] = $this->model->get_itemname();
            $data['image'] = $this->model->image($facilityid);
            
            $this->template->load('template', 'detailfacility', $data);   
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }
    
    }

    public function descriptionview($content='',$id='',$facilityid=''){
        if ($this->session->userdata('username') != "") {
            $content = $_GET["content"];
            $facilityid = $_GET["id"];
            $data['page'] = 18;
            $params = json_decode(urldecode($content), TRUE);
            $data['room'] = $this->model->get_room($facilityid);
            $data['facility']= $facilityid;
            $data['tempimg'] = $params;
            // print_r($content);
            $this->template->load('template', 'viewdescription',$data);
        }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

    }

    public function uploadview($facilityid=''){
        $data['room'] = $this->model->get_room($facilityid);
        $data['facility']= $facilityid;
        $data['page'] = 18;
        $this->template->load('template', 'viewuploadfacility', $data);       
    }

    public function submitupload($facilityid=''){
        $faty = $this->input->post('faid');
        $description = $this->input->post('description');
        $tempgambar = $this->input->post('tempgambar');

        $data['page'] = 18;
        $data['facility']= $faty;
        $data['room'] = $this->model->get_room($facilityid);

        $count = count($description);
        for ($i=0; $i < $count; $i++) { 
                $temp[] = array(
                    'content' => $tempgambar[$i],
                    'facilityid' => $faty[$i],
                    'description' =>$description[$i],

                );
        }
         $this->db->insert_batch('trfacilityimage',$temp);
        redirect(base_url()."facilitycontrol/view/".$faty[0]);

    }

    public function get_spec($itemdata=''){
        $specification = $this->input->post('itemdata');
        $data = $this->model->get_dataspec($specification);
        $tempData = $data->result();

      echo json_encode($tempData);  
    }

    public function get_specUpdate($itemdata=''){
        $specification = $this->input->post('itemdata');
        $data = $this->model->get_dataspecupdate($specification);
        $tempData = $data->result();

      echo json_encode($tempData);  
    }

    public function insertuploaddetail(){
         $rand = md5(uniqid(rand(), true));
        $config = array();
        $config['upload_path'] =  './assets/images/facility/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG';
        $config['max_size'] = '256000';

        $this->load->library('upload', $config);

        $files = $_FILES;
        $cpy = count($_FILES['file']['name']);
        $tempname = $_FILES['file']['name'];
        for($i=0; $i<$cpy; $i++)
        {   
            if ($tempname == "gif" || $tempname == "jpg" || $tempname == "png" || $tempname == "jpeg") {
                $_FILES['file']['name']= $files['file']['name'][$i];
                $_FILES['file']['type']= $files['file']['type'][$i];
                $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
                $_FILES['file']['error']= $files['file']['error'][$i];
                $_FILES['file']['size']= $files['file']['size'][$i];    
                $config['file_name'] = $rand;
                $this->upload->initialize($config);
                $this->upload->do_upload('file');


            }else{
                $_FILES['file']['name']= $files['file']['name'][$i];
                $_FILES['file']['type']= $files['file']['type'][$i];
                $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
                $_FILES['file']['error']= $files['file']['error'][$i];
                $_FILES['file']['size']= $files['file']['size'][$i];    
                $config['file_name'] = $rand.".png";
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);
                $this->upload->do_upload('file');
            }
        }
            $fileData = $this->upload->data();
            echo json_encode($fileData);

    }
    
    public function remove()
    {
        $file = $this->input->post("file");
        $filename = $file['file_name'];
        if ($filename && file_exists('./assets/images/facility/' . "/" . $filename)) {
            unlink('./assets/images/facility/' . "/" . $filename);
        }
    }

    public function insertDetail($itemname='',$specification='',$qty='',$condition='')
    {
        $facilityid = $this->input->post('facility');
        $this->model->insertDetail();
        redirect(base_url()."facilitycontrol/view/".$facilityid);  
    }

    public function updateDetail($itemname='' ,$specification='',$qty='',$condition=''){
        $facilityid = $this->input->post('facility');
        $this->model->updateDetail();
         redirect(base_url()."facilitycontrol/view/".$facilityid);      
    }

     public function updateImg($facilityid='' ,$trfacilityimageid=''){
        $facilityid = $this->input->post('facility');
        $trfacilityimageid = $this->input->post('Trfacilityimageid');
        $description = $this->input->post('description');
        $rand = md5(uniqid(rand(), true));

         $this->load->helper(array('form', 'url'));
        if (empty($_FILES['file_name']['name']))
        {
             $data = array(
                'description' =>$description       
             );
            $this->db->where('trfacilityimageid',$trfacilityimageid);
            $this->db->update('trfacilityimage',$data);
            redirect(base_url()."facilitycontrol/view/".$facilityid);  
       
        }else{
        $config['upload_path']          = './assets/images/facility/';
        $config['allowed_types']        = '*';
        $config['file_name'] = $rand;
        // $facilityid = $this->input->post('facility');
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file_name');
        $fileData = $this->upload->data();
        $data = array(
                'content' => $fileData['file_name'],
                'description' =>$description       
             );
            $this->db->where('trfacilityimageid',$trfacilityimageid);
            $this->db->update('trfacilityimage',$data);
           redirect(base_url()."facilitycontrol/view/".$facilityid);  
        }
    }

    public function deleteDetail(){
        $facilityid = $this->input->post('facility');
        $this->model->deleteDetail();
        redirect(base_url()."facilitycontrol/view/".$facilityid);       
    }

     public function deleteImg(){
        $facilityid = $this->input->post('facility');
        $this->model->deleteImg();
         redirect(base_url()."facilitycontrol/view/".$facilityid);
    }

    public function get_item(){
        $this->load->model('model_facilityroom');

        if (isset($_GET['term'])){
          $q = strtolower($_GET['term']);
          echo json_encode($q);
          $this->model_facilityroom->get_itemdata($q);
        }
    }

    public function getData(){
        echo json_encode( $this->model->view( $this->input->post('id') )->result()[0] );
    }

}
?>