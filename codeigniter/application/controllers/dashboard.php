<?php
class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model('model_dashboard', 'model');
    }

	public function index() {
        if ($this->session->userdata('username') != "") {
	        $data['page'] = 0;
			$this->template->load('template', 'dashboard', $data);
	    }
        else{
          echo "<script>
          alert('Your session is end , please log in first');
          window.location.href='auth';
          </script>";
        }

	}
}
?>