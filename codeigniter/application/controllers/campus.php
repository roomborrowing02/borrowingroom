
<?php
class Campus extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_campus', 'model');
	}
	public function index() {
	    if ($this->session->userdata('username') != "") {
			$data['page'] = 8;
			$data['data'] = $this->model->cek_campus();
			$this->template->load('template', 'viewscampus', $data);
		}
	    else{
	      echo "<script>
	      alert('Your session is end , please log in first');
	      window.location.href='auth';
	      </script>";
	    }

	}

	
	public function insert($campuscode='',$campusname='',$campusaddress='',$campusemail='',$phoneoffice='',$detail=''){
		
		$this->model->insert();
        //echo '<script>alert("You Have Successfully delete this Record!");</script>';
		redirect('index.php/campus');
	}

	public function update($campuscode='',$campusname='',$campusaddress='',$campusemail='',$phoneoffice='',$detail=''){
		$this->model->update();
        //echo '<script>alert("You Have Successfully delete this Record!");</script>';
		redirect('index.php/campus');
	}
	public function delete(){
		$this->model->delete();
        // echo '<script>alert("You Have Successfully delete this Record!");</script>';
		redirect('index.php/campus');
	}
	
	public function getData(){
		echo json_encode( $this->model->cek_campus( $this->input->post('id') )->result()[0] );
	}

	
}
