<?php
class Admin extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model('model_admin', 'model');
    }

	public function index() {
		// // $data['username'] = $this->session->userdata('username');
		// // $page['page'] = 0;
		// // $this->template->load('template', 'indexadmin', $page);
		// $this->load->view('indexadmin',$data);
		$data['page'] = 0;
		$data['data'] = $this->model->cek_terms();
		$this->template->load('template', 'indexadmin', $data);
	}

	public function update(){
		$this->model->update();
        redirect('index.php/admin');
	}	

	public function getData(){
        echo json_encode( $this->model->cek_terms( $this->input->post('id') )->result()[0] );
    }


}
?>