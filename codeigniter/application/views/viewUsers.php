<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"  rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>"  rel="stylesheet">
<!-- bootstrap-datetimepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>"  rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style>
span.desc {
	color: red;
}
</style>

<div id="page-wrapper">
	<div class="row" style="min-height:850px">
		<div class="page-title">
			<div class="title_left">
				<h3>Manage Users <small></small></h3>
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(''); ?>">Home</a></li>
					<li class="active">Users</li>
				</ul>
			</div>
			<div class="title_right">
				<div class="left_col" role="main" >
					<a class="btn btn-sm btn-info pull-right"
					data-toggle="modal"
					data-for="insert"
					data-target="#modalInsertUsers" 
					>Insert
				</a>
			</div>
		</div>

		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-users"></i> List Users<small></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					<li><a class="close-link"><i class="fa fa-close"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<?php if ($data->num_rows() > 0) {?>
					<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" style="width:100%">
						<thead>
							<tr>
								<th>Nim</th>
								<th>Username</th>
								<th>Nama</th>
								<th>Email</th>
								<th>Role</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($data->result() as $row){ ?>
								<tr>
									<td><?php echo $row->nim; ?></td>
									<td><?php echo $row->username; ?></td>
									<td><?php echo $row->nama; ?></td>
									<td><?php echo $row->email; ?></td>
									<td><?php echo $row->rolename; ?></td>
									<td><a class="btn btn-xs btn-warning"
										data-for="update"
										data-toggle="modal"
										data-target="#modal<?php echo $row->userid; ?>"
										data-ajax="<?php echo base_url('admin/getData') ?>"
										data-id="<?php echo $row->userid; ?>"
										>Update</a>
									  <a class="btn btn-xs btn-danger" 
                                    data-toggle="modal" 
                                    data-target="#modalDel" 
                                    data-id="<?php echo $row->userid; ?>" 
                                    data-delete="<?php echo 'User';?>">Delete
                                	</a>
									</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					<?php } else { ?>
						<div class="well">There is no user created yet!</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Modal Insert -->
<div class="modal fade" name="forminsert" id="modalInsertUsers"  tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 class="modal-title" id="myModalLabel">Insert User</h3>
			</div>
			<form role="form" method="post" id="formInsert" class="form-horizontal form-label-left" action="<?php echo base_url('user/insert') ?>">
				<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-xs-3" >Nim</label>
							<div class="col-xs-8">
								<input name="nim" class="form-control" type="text" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-3" >Username</label>
							<div class="col-xs-8">
								<input name="username" class="form-control" type="text" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-3" >Nama</label>
							<div class="col-xs-8">
								<input name="nama" autocomplete="off"class="form-control" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-3" >Email</label>
							<div class="col-xs-8">
								<input name="email" autocomplete="off" class="form-control" type="text">
							</div>
						</div>
						<div class="item form-group">
					      <label class="control-label col-xs-3" for="role" >Role</label>
					      <div class="col-xs-8">
					        <select name="role" id="role" class=" form-control" tabindex="-1" autocomplete="off">
					        	<option value="">-- Select Role --</option>
					          <?php foreach($dataRole as $row){ ?>
					          <option value="<?php echo $row->roleid; ?>"><?php echo $row->rolename;?></option>';
					          <?php } ?>
					        </select> 
					      </div>       
					    </div>
						
					</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					   <input type="submit" class="btn btn-info " id="submitInsert" value="Save"> 
					<input type="hidden" name="Shiftid" id="shiftid" >
				</div>
			</form>
		</div>
	</div>
</div>


<!-- Modal Update -->
<?php
foreach($data->result_array() as $row):
	$userid=$row['userid'];
	$nim=$row['nim'];
	$username=$row['username'];
	$nama=$row['nama'];
	$email=$row['email'];
	$rolename=$row['rolename'];
	$roleid=$row['roleid'];
	?>
	<div class="modal fade" id="modal<?php echo $userid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 class="modal-title" id="myModalLabel">Edit User</h3>
				</div>
				<form role="form" method="post" class="form-horizontal form-label-left formUpdate" action="<?php echo base_url('user/update') ?>">
					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-xs-3" >Nim</label>
							<div class="col-xs-8">
								<input name="nim" class="form-control" type="text" value="<?php echo $nim ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-3" >Username</label>
							<div class="col-xs-8">
								<input name="username" class="form-control" type="text" value="<?php echo $username ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-3" >Nama</label>
							<div class="col-xs-8">
								<input name="nama" class="form-control" type="text" value="<?php echo $nama ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-3" >Email</label>
							<div class="col-xs-8">
								<input name="email" class="form-control" type="text" value="<?php echo $email ?>">
							</div>
						</div>
						<div class="item form-group">
					      <label class="control-label col-xs-3" for="role" >Role</label>
					      <div class="col-xs-8">
					        <select name="role" id="role" class=" form-control" tabindex="-1">
					          <?php foreach($dataRole as $row){ ?>
					          	<?php if ($row->roleid == $roleid): ?>
					          		<option value="<?php echo $row->roleid; ?>" selected><?php echo $row->rolename;?></option>';
					          			<?php else: ?>
					          			<option value="<?php echo $row->roleid; ?>"><?php echo $row->rolename;?></option>';

					          	<?php endif ?>
					          
					          <?php } ?>
					        </select> 
					      </div>       
					    </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					 <input type="submit" class="btn btn-info submitUpdate" value="Save">
						<input type="hidden" name="updateuser" value="<?php echo $userid ?>">
					</div>
				</form>
			</div>
		</div>
	</div>
<?php endforeach;?>

<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete User</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this User?
            </div>
            <form role="form" method="post" action="<?php echo base_url('index.php/user/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deleteuser">Delete</button>
                    <input type="hidden" name="userId" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap-datetimepicker -->    
<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>

<script>
	$(document).ready(function() {
		var handleDataTableButtons = function() {
			if ($("#datatable-responsive").length) {
				$("#datatable-responsive").dataTable({
					dom: "Bfrtip",
					order: [[ 1, "asc" ]],
					buttons: [
					{
						extend: "copy",
						className: "btn-sm"
					},
					{
						extend: "csv",
						className: "btn-sm"
					},
					{
						extend: "excel",
						className: "btn-sm"
					},
					{
						extend: "pdfHtml5",
						className: "btn-sm"
					},
					{
						extend: "print",
						className: "btn-sm"
					}
					],
					responsive: true
				});
			}
		};
		TableManageButtons = function() {
			"use strict";
			return {
				init: function() {
					handleDataTableButtons();
				}
			};
		}();

		TableManageButtons.init();
	});
</script>

	<!-- Validation Insert and Update-->
<script>

  $('#formInsert').validate({       
            errorElement: 'span',
            errorClass: 'desc',
          
              rules: {
                
               nim: {
              required: true,
           
          },
              username: {
              required: true,
              minlength:4,
          },
              nama: {
              required: true,
              minlength:5,
          },
              email: {
              required: true,
              minlength:5,
          },
          role: {
              required: true,
             
          },
               },
                submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
    });
          
  $('.formUpdate').each(function () {
    $(this).validate({
      errorElement: 'span',
      errorClass: 'desc',
      rules: {
         nim: {
              required: true,
             
          },
              username: {
              required: true,
              minlength:4,
          },
              nama: {
              required: true,
              minlength:5,
          },
              email: {
              required: true,
              minlength:5,
          },
          role: {
              required: true, 
          },
      },
      message: {
         nim: {
              required: true, 
          },
              username: {
              required: true,
          },
              nama: {
              required: true,
          },
              email: {
              required: true,
          },
          role: {
              required: true,
          },

      },
      submitHandler: function(form) {
          alert("Successfull");
        // console.log("asdasdas");
        form.submit();
        }
      });
  });

    $('#deleteuser').click(function() {
        /* Act on the event */
        alert("Sucessfull");
    });
</script>
