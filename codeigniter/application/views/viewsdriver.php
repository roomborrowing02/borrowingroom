<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"  rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>"  rel="stylesheet">
<!-- bootstrap-datetimepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>"  rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style>
  span.desc {
    color: red;
}
</style>

  <div id="page-wrapper">
        <div class="page-title">
        <div class="title_left">
           <h3>Manage Driver<small></small></h3>
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
            <li class="active">Driver</li>
        </ul>
        </div>
            <div class="title_right">
               <div class="left_col" role="main" >
             <a class="btn btn-sm btn-info pull-right"
                 data-toggle="modal"
                 data-for="insert"
                 data-target="#modal_insertdriver"
                 >Insert
              </a>
            </div>
          </div>
            </div>
            </div>

              <div class="col-sm-12 col-xl-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-id-card-o"></i> List Driver<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                   <?php if($data->num_rows() > 0){ ?>
          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
              <th>Driver Name</th>
              <th>DoB</th>
              <th>Address</th>
              <th>Phone</th>
              <th>Action</th>
            </tr>
        </thead>

        <tbody>
              <?php foreach($data->result() as $row){ ?>   
            <tr>
              <td><?php echo $row->drivername; ?></td>
              <td><?php echo date('M-d-Y', strtotime($row->dob)); ?></td>
              <td><?php echo $row->address; ?></td>
              <td><?php echo $row->phone; ?></td>
                 <td>
               <a class="btn btn-xs btn-warning"
                data-toggle="modal"
                data-for="update"
                data-target="#modalupdate<?php echo $row->driverid; ?>"
                data-href="<?php echo base_url('driver/update') ?>"
                data-ajax="<?php echo base_url('driver/getData') ?>"
                data-id="<?php echo $row->driverid; ?>">
                Update</a>
              <a class="btn btn-xs btn-danger" 
                data-toggle="modal" 
                data-target="#modalDel" 
                data-id="<?php echo $row->driverid; ?>" 
                data-delete="<?php echo 'Driver'; ?>">Delete</a>
             </td>
           </tr>
           <?php } ?>
      </tbody>
</table>
  <?php } ?>
      </div>
    </div>
  </div>
        <!-- /page content -->
  
<!-- Modal Insert -->
<div class="modal fade" id="modal_insertdriver"  tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">

  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h3 class="modal-title" id="myModalLabel">Insert Driver</h3>
    </div>
            <form role="form" method="post" id="driverInsert" class="form-horizontal form-label-left" action="<?php echo base_url('driver/insert') ?>">

                        <div class="modal-body">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="drivername">Drivername<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="drivername" placeholder="Must be filled" required="required" alphaExp="/^[a-zA-Z]+$" id="drivernameModal" autocomplete="off">
                        </div>
                      </div>

                   <div class="item form-group">
                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dob">DoB<span class="required">*</span>
                        </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class='input-group date' id='myDatepicker2'>
                            <input type='text' class="form-control" id="dobModal" name="dob" required="required"  autocomplete="off" />
                            <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div> 

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address<span class="required">*</span>
                      </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="address" placeholder="JL.      " required="required" type="text" id="addressModal"  autocomplete="off">
                      </div>
                      </div>
                      
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Phone<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="phone"placeholder="999-999999999" pattern="^\d{3}-\d{9}$" required="required" type="text" id="phoneModal" autocomplete="off" >
                        </div>
                      </div>
                    </div>  
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-info " id="submitInsert" value="Save">
          <input type="hidden" name="Driverid" id="driverid" >
        </div>
      </form>
    </div>
  </div>
</div>

 <!-- Modal update -->
 <?php
  foreach($data->result_array() as $row):
      $driverid=$row['driverid'];
      $drivername=$row['drivername'];
      $dob=$row['dob'];
      $address=$row['address'];
      $phone=$row['phone'];
?>
 <div class="modal fade" id="modalupdate<?php echo $driverid; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Manage Driver</h4>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left driverUpdate" action="<?php echo base_url('driver/update') ?>">

                <div class="modal-body">
                  
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="drivername">Drivername<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="drivername" placeholder="Must be filled" required="required" alphaExp="/^[a-zA-Z]+$"  value="<?php echo $drivername; ?>"  >
                        </div>
                      </div>
                   <div class="item form-group">
                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date">DoB<span class="required">*</span>
                        </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group" id="start">
                            <input class="form-control" placeholder="Start" name="dob" value="<?php echo date('M-d-Y',strtotime($dob)); ?>" />
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"  ></i></span>
                          </div>
                    </div>
                </div>
                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address<span class="required">*</span>
                      </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="address" placeholder="JL.      " required="required" type="text" value="<?php echo $address; ?>"  autocomplete="off">
                      </div>
                      </div>
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Phone<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="12" data-validate-words="2" name="phone" id="phoneModal"  placeholder="999-9999999" pattern="^\d{3}-\d{7}$" required="required" type="text"  autocomplete="off" value="<?php echo $phone; ?>" >
                        </div>
                    </div>

                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-info submitUpdate" value="Save">
                    <input type="hidden" name="driverId" value="<?php echo $driverid ?>">
                </div>
            </form>
                </div>
        </div>
    </div>
<?php endforeach;?>

<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Driver</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this Driver ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('driver/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deletedriver">Delete</button>
                    <input type="hidden" name="driverId" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap-datetimepicker -->    
<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>

<script>
    $(document).ready(function() {
        //$('#table_sort').DataTable();
        $('.select2_single').select2();
        var handleDataTableButtons = function() {
            if ($("#datatable-responsive").length) {
                $("#datatable-responsive").DataTable({
                    dom: "Bfrtip",
                    order: [[ 1, "asc" ]],
                    buttons: [
                        {

                         extend: "copy",
                        className: "btn-sm"

                        },
                        {
                          extend: "csv",
                          className: "btn-sm"
                        },
                         {
                           extend: "excel",
                          className: "btn-sm"
                         },
                         {

                           extend: "pdfHtml5",
                           className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };

        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        TableManageButtons.init();


    });
  </script>

  <script>
    $('#myDatepicker').datetimepicker();

    $('#myDatepicker2').datetimepicker({
        format: 'MMM-DD-YYYY'
    });
    
    $('#myDatepicker3').datetimepicker({
        format: 'hh:mm A'
    });
    
    $('#myDatepicker ').datetimepicker({
        ignoreReadonly: true,
        allowInputToggle: true
    });

    $('#datetimepicker6').datetimepicker();
    
    $('#datetimepicker7').datetimepicker({
        useCurrent: false
    });
    
    $("#datetimepicker6").on("dp.change", function(e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    
    $("#datetimepicker7").on("dp.change", function(e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
    $('#start, #end').datetimepicker({
            useCurrent: true,
            format: "MMM-DD-YYYY"
        });
</script>



<!-- Validation Insert and Update-->
<script>
     $('#driverInsert').validate({       
            errorElement: 'span',
            errorClass: 'desc',
          
              rules: {
                
            drivername: {
              required: true,
              minlength:3,
              
          },
          dob: {
              required: true,
              
          },
          address: {
              required: true,
              minlength:5,
            
          },
          phone: {
              required: true,
              minlength:9,
            
          },
          
               },
                submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
    });
          

      $('#deletedriver').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });

  $('.driverUpdate').each(function () {
    $(this).validate({
      errorElement: 'span',
      errorClass: 'desc',
      rules: {
          drivername: {
              minlength:3,
              maxlength:25,
          },
          dob: {
              required: true,
              
          },
          address: {
              required: true,
              minlength:5,
              maxlength: 50,
          },
          phone: {
              required: true,
              minlength:9,
              maxlength:13,
          },
          
      },
      message: {

          drivername: {
              required: true
          },
          dob: {
              required: true
          },
          address: {
              required: true
          },
          phone: {
              required: true
          },
      },
      submitHandler: function(form) {
          alert("Successfull");
        // console.log("asdasdas");
        form.submit();
        }
      });
  });
</script>


