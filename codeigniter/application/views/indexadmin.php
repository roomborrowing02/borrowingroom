<link href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>" rel="stylesheet">

<div id="page-wrapper" style="min-height: 768px;">
  <div class="row">
        <div class="col-lg-12">
            <h3>User List</h3>
    </div>
<?php if ($data->num_rows() > 0) {?>
<table id="table_sort" class="cell-border" style="width:100%">
      <thead>
            <tr>
                <th>ID</th>
                <th>Nim</th>
                <th>Username</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
      </thead>
      <tbody>
          <?php foreach($data->result() as $row){ ?>
           <tr>
             <td><?php echo $row->userid; ?></td>
             <td><?php echo $row->nim; ?></td>
             <td><?php echo $row->username; ?></td>
             <td><?php echo $row->nama; ?></td>
             <td><?php echo $row->email; ?></td>
             <td><?php echo $row->role; ?></td>
             <td><a class="btn btn-xs btn-warning"
                data-for="update"
                data-toggle="modal"
                data-target="#modal<?php echo $row->userid; ?>"
                data-ajax="<?php echo base_url('admin/getData') ?>"
                data-id="<?php echo $row->userid; ?>"
                >Update</a></td>
           </tr>
           <?php } ?>
      </tbody>
</table>
          <?php } else { ?>
      <div class="well">There is no user created yet!</div>
          <?php } ?>
      </div>
</div>

<!-- Modal -->
        <?php
        foreach($data->result_array() as $row):
            $userid=$row['userid'];
            $nim=$row['nim'];
            $username=$row['username'];
            $nama=$row['nama'];
            $email=$row['email'];
         ?>
<div class="modal fade" id="modal<?php echo $userid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Edit User</h3>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left" action="<?php echo base_url('admin/update') ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-xs-3" >NIM</label>
                        <div class="col-xs-8">
                            <input name="nim" class="form-control" type="text" value="<?php echo $nim ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Username</label>
                        <div class="col-xs-8">
                            <input name="username" class="form-control" type="text" value="<?php echo $username ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama</label>
                        <div class="col-xs-8">
                            <input name="nama" class="form-control" type="text" value="<?php echo $nama ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Email</label>
                        <div class="col-xs-8">
                            <input name="email" class="form-control" type="text" value="<?php echo $email ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Save</button>
                    <input type="hidden" name="updateuser" value="<?php echo $userid ?>">
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach;?>



<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>"></script>  

<script>
    $(document).ready(function() {
        $('#table_sort').DataTable();
    } );
</script>
