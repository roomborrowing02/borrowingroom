<link href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/css/select2.min.css'); ?>" rel="stylesheet">

 <style>
  span.desc {
    color: red;
}
</style> 

 <div id="page-wrapper">
  <div class="row" style="min-height: 768px;">
	<div class="page-title">
        <div class="title_left">
           <h3>Manage Room <small></small></h3>
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
            <li class="active">Facility Room</li>
          </ul>
        </div>
        <div class="title_right">
	        <div class="col-md-10 col-sm-10 col-xs-12 form-group pull-right top_search">
	          <a class="btn btn-sm btn-info pull-right"
	            data-toggle="modal"
	            data-for="insert"
	            data-target="#modal_insertFacility">Insert
	          </a>
	        </div>  
	    </div>
	</div>
    <div class="clearfix"></div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <p id="selectTriggerFilter"><label><b>Filter Facility:</b><br></label></p>
    </div>
		<div class="x_panel">
		    <div class="x_content">
		      <table id="table_sort" class="cell-border" style="width:100%">
		        <thead>
		          <tr>
		            <th>ID</th>
		            <th>Content</th>
		            <th>Description</th>
		            <th>Action</th>
		          </tr>
		        </thead>
		        <tbody>
             	<?php foreach($data->result() as $row){ ?>
		           <tr>
		             <td>S - <?php echo $row->streamingid; ?></td>
		             <td>
						<img src="http://img.youtube.com/vi/<?php echo $row->content; ?>/default.jpg"/>
					</td>
		             <td><?php echo $row->description; ?></td>
		             <td>
		              <a class="btn btn-xs btn-info" href="<?php echo base_url() ?>streaming/fullscreen/<?php echo $row->streamingid; ?>">View
		            </a>
		               <a class="btn btn-xs btn-warning">
		                  Update</a>
		               <a class="btn btn-xs btn-danger">Delete</a>
		             </td>
		           </tr>
           		<?php } ?>
		      </tbody>
		      </table>
		    </div>
		</div>
	</div>
</div>
<!-- <?php foreach($data->result() as $row){ ?>

<iframe width="600" height="300" src="http://www.youtube.com/embed/<?php echo $row->content; ?>" frameborder="0" allowfullscreen>
						</iframe>
           		<?php } ?> -->

<!-- Modal Insert Streaming -->
<div class="modal fade" id="modal_insertFacility" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 class="modal-title" id="myModalLabel">Insert Content</h3>
      </div>
    <form role="form" method="post" id="facilityInsert" class="form-horizontal form-label-left" action="<?php echo base_url('streaming/insert') ?>">
  <div class="modal-body">
    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="content" >Url
      <span class="required">*</span></label>
      <div class="col-md-6 col-sm-6 col-xs-12">
     <input name="content"  id="facilityname" class="form-control" required="required" type="text" autocomplete="off">
      </div>
    </div>
    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type" >Description
      <span class="required">*</span></label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input name="description"  id="description" class="form-control" required="required" 
        type="text" autocomplete="off">
      </div>
    </div>
  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info" id="submitInsert">Save</button>
        <input type="hidden" name="streamingId" id="InsertId" >
      </div>
    </form>
  </div>
  </div>
</div>

<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
<!-- <script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script> -->
<script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/nprogress/nprogress.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.js'); ?>"></script>
 <script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>

<script type="text/javascript">
  tinymce.init({
              selector: "textarea",
              plugins: [
                  "advlist autolink lists link image charmap print preview anchor",
                  "searchreplace visualblocks code fullscreen",
                  "insertdatetime media table contextmenu paste"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
          });
</script>
<script>
  $('#start, #end').datetimepicker({
            useCurrent: false,
            format: "YYYY-MM-DD"
        });
  $('#start').datetimepicker().on('dp.change', function (e) {
            var incrementDay = moment(new Date(e.date));
            incrementDay.add(1, 'days');
            $('#end').data('DateTimePicker').minDate(incrementDay);
            $(this).data("DateTimePicker").hide();
        });

        $('#end').datetimepicker().on('dp.change', function (e) {
            var decrementDay = moment(new Date(e.date));
            decrementDay.subtract(1, 'days');
            $('#start').data('DateTimePicker').maxDate(decrementDay);
             $(this).data("DateTimePicker").hide();
        });
      </script>

<script>
    $(document).ready(function() {
      $('#table_sort').DataTable({
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "deferRender": true,
        "initComplete": function() {
          var column = this.api().column(8);

          var values = [];
          column.data().each(function(d, j) {
            d.split(",").forEach(function(data) {
              data = data.trim();
              
              if (values.indexOf(data) === -1) {
                values.push(data);
              }
            });
          });

          $('<select class="select2_single form-control" style="width: 100%;" tabindex="-1" id="selectTriggerFilter"><option value="">-- Select All --</option></select>')
            .append(values.sort().map(function(o) {
              return '<option value="' + o + '">' + o + '</option>';
            }))
            .on('change', function() {
              column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
            })
            .appendTo('#selectTriggerFilter').select2();
        }
      });
    });
</script>