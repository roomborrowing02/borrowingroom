<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
<!--Selecy2 js-->
<!-- <link href="<?php echo base_url('assets/select2/test/dropdown/dropdownCss-tests.js'); ?>" rel="stylesheet"/>
 --><!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"  rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>"  rel="stylesheet">
<!-- bootstrap-datetimepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>"  rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style>
  span.desc {
    color: red;
}
</style>

<div class="mid_col" role="main">
<div id="page-wrapper">
  <div class="row" style="min-height: 800px">
    <div class="page-title">
      <div class="title_left">
        <h3>Manage Detail <small></small></h3>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
          <li class="active">Shuttle details</li>
        </ul>
      </div>
      <div class="title_right">
        <div class="col-md-10 col-sm-10 col-xs-12 form-group pull-right top_search">
          <a class="btn btn-sm btn-info pull-right"
          data-toggle="modal"
          data-for="insert"
          data-target="#modal_insertdetail"
          data-href="<?php echo base_url('cs_detail/insert') ?>">Insert
          </a>
        </div>
      </div>
    </div>

      <div class="clearfix"></div>
              <!--Dropdown filter di buat bertahap seperti booking -->
    <div class="col-md-4 col-sm-4 col-xs-12">
      <p id="dayfilter"><label><b>Filter Day:</b><br></label></p>
               <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
              <select class="select2_single form-control" tabindex="-1" id="dayFltr"><option value="">-- Select All --</option>
               </select>
    </div>

              
    <div class="col-md-4 col-sm-4 col-xs-12">
      <p id="shiftfilter"><label><b>Filter Shift:</b><br></label></p>
               <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
              <select class="select2_single form-control" tabindex="-1" id="shiftFltr"><option value="">-- Select All --</option>
               </select>
    </div>

      <div class="col-md-4 col-sm-4 col-xs-12">
        <p id="pickupfilter"><label><b>Filter Pickup:</b><br></label></p>
          <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
          <select class="select2_single form-control" tabindex="-1" id="pickupFltr"><option value="">-- Select All --</option>
          </select>
      </div>
    <br>
       <div class="col-md-4 col-sm-4 col-xs-12">
          <p id="dropofffilter"><label><b>Filter Dropoff:</b><br></label></p>
          <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
          <select class="select2_single form-control" tabindex="-1" id="dropoffFltr"><option value="">-- Select All --</option>
          </select>
          
      </div>

      <div class="col-md-4 col-sm-4 col-xs-12">
          <p id="vehiclefilter"><label><b>Filter Vehicle:</b><br></label></p>
              <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
            <select class="select2_single form-control" tabindex="-1" id="vehicleFltr"><option value="">-- Select All --</option>
            </select>
      </div>
          
    <div class="x_content">
    <div class="row">
    <div class="col-sm-12">

    <div class="x_panel">        
    <div class="x_title">
      <h2><i class="fa fa-bars"></i> All Datas <small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <?php if($data->num_rows() > 0){ ?>
          <table id="dataFilter" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Day</th>
                <th>Term</th>
                <th>Shift</th>
                <th>Pickup</th>
                <th>Dropoff</th>
                <th>Vehicle</th>
                <th>Driver</th>
                <th>Action</th>
                
            </thead>
           <tbody>
          <?php foreach($data->result() as $row){ ?>
             <tr>
                <td>TRD - <?php echo $row->trdetailshuttleid; ?></td>
                <td><?php echo $row->date; ?></td>
                <td><?php echo $row->day; ?></td>
                <td><?php echo $row->termcode; ?></td>
                <td><?php echo $row->shift; ?></td>
                <td><?php echo $row->pickupLoc; ?></td>
                <td><?php echo $row->dropoffLoc; ?></td>
                <td><?php echo $row->vehiclename; ?></td>
                <td><?php echo $row->drivername; ?></td>
                <td>
                <a class="btn btn-xs btn-warning"
                data-toggle="modal"
                data-target="#modal_update<?php echo $row->trdetailshuttleid; ?>"
                data-ajax="<?php echo base_url('cs_detail/getData') ?>"
                data-id="<?php echo $row->trdetailshuttleid; ?>">Update</a>
                <a class="btn btn-xs btn-danger" 
                data-toggle="modal" 
                data-target="#modalDel" 
                data-id="<?php echo $row->trdetailshuttleid; ?>" 
                data-delete="<?php echo $row->date." - ".$row->termcode; ?>">Delete</a>
                </td>
             </tr>
          <?php } ?>
           </tbody>
          </table>
          <?php } else { ?>
            <div class="well">There is no data created yet!</div>
          <?php } ?>
      </div>
    </div>
  </div>
</div>
</div>

<!-- Modal Insert -->
<div class="modal fade" id="modal_insertdetail" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Manage News</h4>
    </div>

    <form role="form" method="post" class="detailInsert" action="<?php echo base_url('cs_detail/insert') ?>" >
    <div class="modal-body">
    <div class="form-group">

    <div class="col-md-4 col-sm-4 col-xs-12">
    <label for="term">Term:</label>
    <select name="termcode" class="form-control" id="termcode">
     <option value=""></option>
    <?php foreach($dataTerm as $row){ ?>
    <option value="<?php echo $row->termid; ?>"><?php echo $row->termcode; ?></option>';
    <?php } ?>
    </select>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
    <label for="vehicle">Vehicle:</label>
    <select name="vehiclename" class="form-control" id="vehiclename">
      <option value=""></option>
    <?php foreach($dataVehicle as $row){ ?>  
    <option value="<?php echo $row->vehicleid; ?>"><?php echo $row->vehiclename; ?></option>';
    <?php } ?>
    </select>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
    <label for="term">Driver:</label>
    <select name="drivername" id="drivername" class="form-control">
    <option value=""></option>
    <?php foreach($dataDriver as $row){ ?>     
    <option value="<?php echo $row->driverid; ?>"><?php echo $row->drivername; ?></option>';
    <?php } ?>
    </select>
    </div>
    </div>

    <div class="form-group">
          <label>Date</label>
            <div class="input-group" id="start">
              <input class="form-control" name="date" />
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar" id="date"></i></span>
            </div>
          

    <label for="term">Day:</label>        
    <select name="day" class="select2_single form-control" tabindex="-1" id="day">
         <option value=""></option>
    <option value="Monday">Monday</option>
    <option value="Tuesday">Tuesday</option>
    <option value="Wednesday">Wednesday</option>
    <option value="Thursday">Thursday</option>
    <option value="Friday">Friday</option>
    <option value="Saturday">Saturday</option>
    </select> 

    <label for="term">Shift:</label>
    <select name="shift" class="form-control" id="shift">
         <option value=""></option>

    <?php foreach($dataShift as $row){ ?>
    <option value="<?php echo $row->shiftid; ?>"><?php echo $row->shift; ?></option>';
      <?php } ?>
    </select>
    </div>

    <div class="form-group">
    <label for="term">Pick up:</label>
    <select name="pickup" class="select2_single form-control" tabindex="-1" id="pickup">
         <option value=""></option>

    <?php foreach($dataPickup as $row){ ?>
    <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode; ?></option>';
    <?php } ?>
    </select>         

    <label for="term">Drop off:</label>
    <select name="dropoff" class="select2_single form-control" tabindex="-1" id="dropoff">
         <option value=""></option>
      
    <?php foreach($dataDropoff as $row){ ?>
    <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode; ?></option>';
      <?php } ?>
    </select>
    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <input type="submit" class="btn btn-info " id="submitInsert" value="Save">
      <input type="hidden" name="insertdetail" id="insertdetailid">
          
    </div>
    </div>
    </form>

    </div>
   </div>
  </div>
  
 <!-- Modal Update detail-->
<?php
    foreach($data->result_array() as $row):
        $trdetailshuttleid=$row['trdetailshuttleid'];
        $termcode2=$row['termcode'];
        $termid2=$row['termid'];
        $day=$row['day'];
        $date=$row['date'];
        $vehiclename=$row['vehicleid'];
        $drivername=$row['driverid'];
        $shift=$row['shiftid'];
        $campuscode1=$row['campuspickupid'];
        $campuscode=$row['campusdropoffid'];
     ?> 
<div class="modal fade" id="modal_update<?php echo $trdetailshuttleid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
         <h5 class="modal-title" id="exampleModalLabel">Update Detail</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <form role="form" method="post" class="form-horizontal form-label-left detailUpdate" action="<?php echo base_url('cs_detail/update') ?>">

        <div class="modal-body">
          <div class="form-group">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <label for="term">Term:</label>
              <select name="termcode" class="form-control">
                <?php foreach($dataTerm as $row){ ?>
                  <?php if ($row->termid == $termid2): ?>
                    <option value="<?php echo $row->termid; ?>" selected><?php echo $row->termcode; ?></option>';
                  <?php else: ?>
                    <option value="<?php echo $row->termid; ?>"><?php echo $row->termcode; ?></option>';
                  <?php endif; ?>
                <?php } ?>
              </select>
            </div>  
                 
            <div class="col-md-4 col-sm-4 col-xs-12">
              <label for="vehicle">Vehicle:</label>
                <select name="vehiclename" class="form-control">
                <?php foreach($dataVehicle as $row){ ?>
                  <?php if ($row->vehicleid == $vehiclename): ?>
                    <option value="<?php echo $row->vehicleid; ?>" selected><?php echo $row->vehiclename; ?>
                  </option>';
                  <?php else: ?>
                    <option value="<?php echo $row->vehicleid; ?>"><?php echo $row->vehiclename; ?></option>';
                  <?php endif; ?>
                <?php } ?>
              </select>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <label for="term">Driver:</label>
              <select name="drivername" class="form-control">
                <?php foreach($dataDriver as $row){ ?>
                  <?php if ($row->driverid == $drivername): ?>
                   <option value="<?php echo $row->driverid; ?>" selected><?php echo $row->drivername; ?>
                  </option>';
                  <?php else: ?>
                    <option value="<?php echo $row->driverid; ?>"><?php echo $row->drivername; ?></option>';
                  <?php endif; ?>
                <?php } ?>
              </select>
            </div>
        </div>

        <div class="form-group">
          <label>Date</label>
            <div class="input-group" id="start">
              <input class="form-control" name="date" value="<?php echo $date; ?>" />
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
          </div>

      <div class="form-group">
            
          <label for="day">Day:</label>        
          <select name="day" class="form-control">
              <?php if ("Monday" == $day): ?>
              <option value="Monday" selected>Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Tuesday" == $day): ?>
              <option value="Monday">Monday</option>;
              <option value="Tuesday"selected>Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Wednesday" == $day): ?>
              <option value="Monday">Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday"selected>Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Thursday" == $day): ?>
              <option value="Monday">Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday"selected>Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Friday" == $day): ?>
              <option value="Monday" >Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday"selected>Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Saturday" == $day): ?>
              <option value="Monday">Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday"selected>Saturday</option>;
            <?php endif; ?>
          </select>
      </div>

      <div class="form-group">
        <label for="term">Shift:</label>
          <select name="shift" class="form-control">
            <?php foreach($dataShift as $row){ ?>
              <?php if ($row->shiftid == $shift): ?>
                <option value="<?php echo $row->shiftid; ?>" selected><?php echo $row->shift; ?>
              </option>';
              <?php else: ?>
                <option value="<?php echo $row->shiftid; ?>"><?php echo $row->shift; ?></option>';
              <?php endif; ?>
            <?php } ?>
          </select>
      </div>

        <div class="form-group">
          <label for="term">Pick up:</label>
          <select name="pickup" class="select2_single form-control" tabindex="-1" >
            <?php foreach($dataPickup as $row){ ?>
              <?php if ($row->campusid == $campuscode1): ?>
                <option value="<?php echo $row->campusid; ?>" selected><?php echo $row->campuscode; ?>
              </option>';
              <?php else: ?>
                <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode; ?></option>';
              <?php endif; ?>
            <?php } ?>
          </select>
        </div>

        <div class="form-group">
          <label for="term">Drop off:</label>
            <select name="dropoff" class="select2_single form-control" tabindex="-1">
              <?php foreach($dataDropoff as $row){ ?>
                <?php if ($row->campusid == $campuscode): ?>
                  <option value="<?php echo $row->campusid; ?>" selected><?php echo $row->campuscode; ?>
                </option>';
                <?php else: ?>
                  <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode; ?></option>';
                <?php endif; ?>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-info submitUpdate" value="Save">
           <input type="hidden" name="updatedetail"  value="<?php echo $trdetailshuttleid; ?>">
        </div>
    </form>
      </div>
  </div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Detail</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this data ?
            </div>
        <form role="form" method="post" action="<?php echo base_url('cs_detail/delete'); ?>">
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="deletecsdetail">Delete</button>
                <input type="hidden" name="trdetailshuttleid" id="deleteId" value="<?php echo $trdetailshuttleid; ?>">
            </div>
        </form>
      </div>
   </div>
  </div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- Datetimepicker -->
<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!--Jquery Validation-->
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<!-- Select2 JS-->
 <script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>

<script>
  $(document).ready(function() {
    $('#start, #end').datetimepicker({
            useCurrent: true,
            format: "YYYY-MM-DD"
        });
  });
</script>

<script>   
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "lengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
      ],
      "deferRender": true,
      "initComplete": function() {
        var column = this.api().column(2);

        var values = [];
        column.data().each(function(d, j) {
          d.split(",").forEach(function(data) {
            data = data.trim();
            
            if (values.indexOf(data) === -1) {
              values.push(data);
            }
          });
        });

        $('<select class="select2_single form-control" tabindex="-1" id="selectTriggerFilter"><option value="">-- Select All --</option></select>')
          .append(values.sort().map(function(o) {
            return '<option value="' + o + '">' + o + '</option>';
          }))
          .on('change', function() {
            column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
          })
          .appendTo('#selectTriggerFilter').select2();
      }
    });
  });
</script>

<!-- Validation Insert and Update-->
<script>
  $('.detailInsert').validate({ // initialize the plugin         
    errorElement: 'span',
    errorClass: 'desc',
    rules: {
       day: {
         required: true,
       },
       date: {
           required: true,
       },
       termcode: {
           required: true,
       },
        shift: {
           required: true,
       },
        vehiclename: {
           required: true,
       },
        drivername: {
           required: true,
       },
       pickup:{
        required: true,
       },
       dropoff: {
        required: true
       },
     },
     submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
    });

 $('.detailUpdate').each(function () {
       $(this).validate({
         errorElement: 'span',
          errorClass: 'desc',
            rules: {  
                     day: {
                         required: true        
                     },

                     date: {
                         required: true        
                     },
                     termcodedss: {
                         required: true
                     },
                      shiftid: {
                         required: true     
                    },
                      vehicleid: {
                         required: true
                     },
                       driverid: {
                         required: true
                     },
                       pickupid: {
                         required: true
                     },
                       dropoffid: {
                         required: true
                     },  
            },
            message: {
                    day: {
                         required: true        
                     },

                     date: {
                         required: true        
                     },
                     termid: {
                         required: true
                     },
                      shiftid: {
                         required: true     
                    },
                      vehicleid: {
                         required: true
                     },
                       driverid: {
                         required: true
                     },
                       pickupid: {
                         required: true
                     },
                       dropoffid: {
                         required: true
                     },
            },
            submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
        });
      });
      
     $('#deletecsdetail').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });

</script>

<script >
  $(document).ready(function (){
    var table = $('#dataFilter').DataTable({
       dom: 'lrtip',
        initComplete: function () {


          // this.api().columns([0]).every( function () {
          //   var column = this;
          //   // console.log(column);
          //   var select = $("#idFltr"); 
          //   column.data().unique().sort().each( function ( d, j ) {
          //     select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
          //   } );
          // } );
          this.api().columns([2]).every( function () {
            var column = this;
            // console.log(column);
            var select = $("#dayFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
               this.api().columns([4]).every( function () {
            var column = this;
            // console.log(column);
            var select = $("#shiftFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
           this.api().columns([5]).every( function () {
            var column = this;
            // console.log(column);
            var select = $("#pickupFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
           this.api().columns([6]).every( function () {
            var column = this;
            // console.log(column);
            var select = $("#dropoffFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
           this.api().columns([7]).every( function () {
            var column = this;
            // console.log(column);
            var select = $("#vehicleFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
          $("#dayFltr,#shiftFltr,#pickupFltr,#dropoffFltr,#vehicleFltr,#idFltr")
       }
    });
    //  $('#idFltr').on('change', function(){
    //     var search = [];

    //   $.each($('#idFltr option:selected'), function(){
    //         search.push($(this).val());
    //   });

    //   search = search.join('|');
    //   table.column(0).search(search, true, false).draw();  
    // });
    $('#dayFltr').on('change', function(){
        var search = [];
      $.each($('#dayFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(2).search(search, true, false).draw();  
    });
    $('#shiftFltr').on('change', function(){
        var search = [];
      $.each($('#shiftFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(4).search(search, true, false).draw();
    });
       $('#pickupFltr').on('change', function(){
        var search = [];

      $.each($('#pickupFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(5).search(search, true, false).draw();  
    });
       $('#dropoffFltr').on('change', function(){
        var search = [];

      $.each($('#dropoffFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(6).search(search, true, false).draw();  
    });
   $('#vehicleFltr').on('change', function(){
        var search = [];

      $.each($('#vehicleFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(7).search(search, true, false).draw();  
    });
});
</script>

<script >
 $(document).ready(function () {
      // $('#shiftbutton').click(function() {
        /* Act on the event */
        $('#pickup').change(function(){
            $.ajax({
              type: 'POST',
              url: "<?php echo base_url(); ?>cs_master/cekvalidasi",
              dataType: "json",
              data: {
              pickupdata : $('#pickup').val()
              }, // <----send this way
              success: function(data) {
              // alert("Sucessfull");
               // console.log(data);
                var yourval = jQuery.parseJSON(JSON.stringify(data));
               var dropoffhtml="";

               dropoffhtml += "<option id =\"0\" value =\"\"></option>";
                  for (var i=0 ; i<yourval.length; i++)
                  {
                     dropoffhtml += "<option value =\""+yourval[i].campusid+"\" id =\""+ yourval[i].campusid +"\">"+yourval[i].campuscode+"</option>";
                  }
                
                $('#dropoff').html(dropoffhtml);  
              }
            });
            return false;
        });
      });
</script> 
