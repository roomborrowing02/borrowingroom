 <!-- Bootstrap -->
<!-- <link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?> rel="stylesheet"> -->
<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"  rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
<!--Selecy2 js-->
<link href="<?php echo base_url('assets/select2/test/dropdown/dropdownCss-tests.js'); ?>" rel="stylesheet"/>


 <style>
  span.desc {
    color: red;
}
</style> 

<div id="page-wrapper">
  <div class="row" style="min-height: 768px;">
<div class="page-title">
        <div class="title_left">
           <h3>Manage Room <small></small></h3>
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
            <li class="active">Facility Room</li>
          </ul>
        </div>
      <!-- Insert-->
      <div class="title_right"> <!-- Insert-->
        <div class="col-md-10 col-sm-10 col-xs-12 form-group pull-right top_search">
          <a class="btn btn-sm btn-info pull-right"
            data-toggle="modal"
            data-for="insert"
            data-target="#modal_insertFacility">Insert
          </a>
        </div>  
      </div>
</div>
    <div class="clearfix"></div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <p id="selectTriggerFilter"><label><b>Filter Facility:</b><br></label></p>
    </div>

  <div class="x_panel">
  <div class="x_title">
      <h2><i class="glyphicon glyphicon-list-alt"></i> Room List <small></small></h2>
      <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive " cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Facility Name</th>
            <th>Campus Code</th>
            <th>Capacity</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
             <?php foreach($data->result() as $row){ ?>
           <tr>
             <td><?php echo $row->facilityname; ?></td>
             <td><?php echo $row->campuscode; ?></td>
             <td><?php echo $row->capacity; ?></td>
             <td><?php echo $row->description; ?></td>
             <td>
              <a class="btn btn-xs btn-info" href="<?php echo base_url() ?>facilitycontrol/view/<?php echo $row->facilityid; ?>">View
            </a>
               <a class="btn btn-xs btn-warning"
                  data-for="update"
                  data-toggle="modal"
                  data-target="#modal_edit<?php echo $row->facilityid; ?>"
                  data-ajax="<?php echo base_url('facilitycontrol/get_campuscode') ?>"
                  data-id="<?php echo $row->facilityid; ?>">
                  Update</a>
               <a class="btn btn-xs btn-danger"
                  data-toggle="modal" 
                  data-target="#modalDel" 
                  data-id="<?php echo $row->facilityid; ?>" 
                  data-delete="<?php echo $row->facilityname; ?>">Delete</a>
             </td>
           </tr>
           <?php } ?>
      </tbody>
      </table>
    </div>
</div>
</div>
</div>
<!-- Modal Insert Facility room-->
<div class="modal fade" id="modal_insertFacility" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 class="modal-title" id="myModalLabel">Insert FacilityRoom</h3>
      </div>
    <form role="form" method="post" id="facilityInsert" class="form-horizontal form-label-left" action="<?php echo base_url('facilitycontrol/insert') ?>">
  <div class="modal-body">

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Facility Name</label>
      <div class="col-md-6 col-sm-6 col-xs-12">
     <input name="facilityname"  id="facilitynameModal" class="form-control" type="text" autocomplete="off">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campuscode" >Campus Code</label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <select name="campuscode" id="campuscodeinsertModal" class=" form-control" tabindex="-1">
          <?php foreach($dataCampus as $row){ ?>
          <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode;?></option>';
          <?php } ?>
        </select> 
      </div>       
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Capacity</label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input name="capacity" id="capacityModal" class="form-control" type="text"  autocomplete="off">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Description</label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input name="description"  id="description" class="form-control" type="text" autocomplete="off">
      </div>
    </div>
  </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-info " id="submitInsert" value="Save">
        <input type="hidden" name="facilityId" id="InsertId" >
      </div>

    </form>
  </div>
  </div>
</div>

<!-- Modal Update Facility room-->
<?php
        foreach($data->result_array() as $row):
            $facilityid=$row['facilityid'];
            $facilityname=$row['facilityname'];
            $campusid=$row['campusid'];
            $capacity=$row['capacity'];
            $description=$row['description'];
         ?>
<div class="modal fade" id="modal_edit<?php echo $facilityid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
  <h3 class="modal-title" id="myModalLabel">Edit Facility</h3>
  </div>
  <form role="form" method="post" class="form-horizontal form-label-left facilityUpdate" action="<?php echo base_url('facilitycontrol/update') ?>">

  <div class="modal-body">

  <div class="form-group">
    <label class="control-label col-xs-3" >Facility Name</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input name="facilityname" value="<?php echo $facilityname;?>" class="form-control" type="text" autocomplete="off">
    </div>
  </div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12 campuscode" for="campuscode">Campus Code</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
  <select name="campusid" class=" form-control" tabindex="-1"  value="<?php echo $campusid;?>">
  <?php foreach($dataCampus as $row){ ?>
  <?php if ($row->campusid == $campusid): ?>
  <option value="<?php echo $row->campusid; ?>" selected><?php echo $row->campuscode; ?>
  </option>';
  <?php else: ?>
  <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode; ?></option>';
  <?php endif; ?>
  <?php } ?>
  </select>  
  </div>
</div>

  <div class="item form-group">
    <label class="control-label col-xs-3" >Capacity</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input name="capacity" value="<?php echo $capacity;?>" class="form-control" type="text"  autocomplete="off">
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-xs-3" >Description</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input name="description" value="<?php echo $description;?>" class="form-control" type="text"  autocomplete="off">
    </div>
  </div>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <input type="submit" class="btn btn-info submitUpdate" value="Save">
    <input type="hidden" name="facilityId"  value="<?php echo $facilityid; ?>">
  </div>
  </form>
  </div>
  </div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Facility</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this data ?
            </div>
        <form role="form" method="post" action="<?php echo base_url('facilitycontrol/delete'); ?>">
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="deletefacilityroom">Delete</button>
                <input type="hidden" name="deletefacilityId" id="deleteId">
            </div>
        </form>
      </div>
   </div>
  </div>


<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!--Jqueryvalidate-->
 <script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script> 
 <!-- Select2 JS-->
 <script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>
 <script>   
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "lengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
      ],
      dom: "Bfrtip",
                    order: [[ 1, "asc" ]],
                    buttons: [
                         {
                             extend: "copy",
                             className: "btn-sm"
                         },
                         {
                             extend: "csv",
                             className: "btn-sm"
                         },
                         {
                             extend: "excel",
                             className: "btn-sm"
                         },
                         {
                             extend: "pdfHtml5",
                             className: "btn-sm"
                         },
                         {
                             extend: "print",
                             className: "btn-sm"
                         },
                    ],
                    responsive: true,
      "deferRender": true,
      "initComplete": function() {
        var column = this.api().column(3);

        var values = [];
        column.data().each(function(d, j) {
          d.split(",").forEach(function(data) {
            data = data.trim();
            
            if (values.indexOf(data) === -1) {
              values.push(data);
            }
          });
        });

        $('<select class="select2_single form-control" tabindex="-1" id="selectTriggerFilter"><option value="">-- Select All --</option></select>')
          .append(values.sort().map(function(o) {
            return '<option value="' + o + '">' + o + '</option>';
          }))
          .on('change', function() {
            column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
          })
          .appendTo('#selectTriggerFilter').select2();
      }
    });

  });
</script>

<!-- Validation Insert and Update-->
<script>
  $('#facilityInsert').validate({       
            errorElement: 'span',
            errorClass: 'desc',
          
              rules: {
                
              campuscode: {
                   required: true
               },
               facilityname: {
                   required: true,
                   minlength: 3
               },
               campusid: {
                   required: true, 
               },
               capacity: {
                   required: true,
                   minlength: 1
               },
                description: {
                   required: true,
                   minlength: 5
               },
               },
                submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
    });
                
                $('#deletefacilityroom').click(function() {
                /* Act on the event */
                alert("Sucessfull");
                });

     $('.facilityUpdate').each(function () {
       $(this).validate({
         errorElement: 'span',
          errorClass: 'desc',

           rules: {

            campuscode: {
            required: true
            },
              
            facilityname: {
            required: true,
            minlength: 3
            },
            campusid: {
            required: true
            },
            capacity: {
            required: true,
            minlength: 1
            },
            description: {
            required: true,
            minlength: 5
            },
            },

            message: {

            campuscode: {
            required: true
            },

            facilityname: {
            required: true
            },
            campusid:{
            required: true
            },
            capacity:{
            required: true
            },
            description: {
            required: true
            },
            },
            submitHandler: function(form) {
              alert("Successfull");
              console.log("asdasdas");
              form.submit();
            }
            });
            });
            $('.submitUpdate').click(function() {
            /* Act on the event */
            });
    
</script>

