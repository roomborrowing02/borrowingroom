 <link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/dropzone/dist/min/dropzone.min.css'); ?>" rel="stylesheet">

<link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">

<div class="row" style="min-height: 200px;">
  <div class="title_left">
    <h3>Master Pengumuman <small></small></h3>
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
        <li><a href="<?php echo base_url('pengumuman'); ?>">Pengumuman</a></li>
        <li class="active">Upload</li>
      </ul>     
  </div>
  <div class="x_content">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Multiple Upload Dropzone</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="my-dropzone" class="dropzone">
                    <div class="dz-message">
                      <h3>Drop files here</h3> or <strong>click</strong> to upload
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="left_col" role="main" >
          <a class="btn btn-sm btn-info pull-right buttonnext" 
          href="">Next <i class="fa fa-upload"></i>
          </a>
        </div>
	</div>
</div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/fastclick/lib/fastclick.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/dropzone/dist/min/dropzone.min.js'); ?>"></script>
<script>
  $(".buttonnext").hide();
</script>
<script>
    Dropzone.autoDiscover = false;
    Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "You can only upload one files.";
    var namaFile = Array();
    var jml = 0;
    var myDropzone = new Dropzone("#my-dropzone", {
      url: "<?php echo site_url("pengumuman/insertupload") ?>",
      acceptedFiles : ".jpg,.jpeg,.png,.gif",
      addRemoveLinks: true,
      paramName: "file[]",
      maxFiles: 10,
      init: function() {
        this.on("maxfilesexceeded", function(file){
            alert("You can only upload ten files");
        }),
        this.on("success", function(file, xhr){
             $(".buttonnext").show();
            var data = file.xhr.response;
            file.serverId = data;
              var tempdata = JSON.parse(data);
              namaFile[jml] = tempdata["file_name"];
              jml++;


        }),
        this.on("removedfile", function(file) {
          if (!file.serverId) { 
            return; 
          } 
          var coffee = JSON.parse(file.serverId);
          $.ajax({
          type: "post",
          url: "<?php echo site_url("pengumuman/remove") ?>",
          data: { file: coffee },
          dataType: 'html'
        });

          var index = namaFile.indexOf(coffee['file_name']);
          if (index > -1) {
             namaFile.splice(index, 1);
          }
        });
      },
    });
    $(".buttonnext").click(function() {

      function flatten(arr) {
          var flat = [];
          for (var i = 0; i < arr.length; i++) {
              flat = flat.concat(arr[i]);
          }
          return flat;
      }
      // console.log(namaFile);
      var temp = flatten(namaFile);
      // console.log(temp);
      window.location.href='<?php echo base_url(); ?>pengumuman/form/'+encodeURIComponent(JSON.stringify(namaFile));
    });
  </script>