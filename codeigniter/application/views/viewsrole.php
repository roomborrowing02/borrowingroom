<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">
<!-- bootstrap-datetimepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.css'); ?>" rel="stylesheet">

<style>
    span.desc {
	color: red;
}
</style>

<style>
    .verify
{
    margin-top: 4px;
    margin-left: 9px;
    position: absolute;
    width: 16px;
    height: 16px;
}
    
</style>


<div id="page-wrapper">
    <div class="row" style="min-height:900px">
        <div class="page-title">
            <div class="title_left">
                <h3>Manage Role <small></small></h3>
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('index.php/admin'); ?>">Home</a></li>
                    <li class="active">Role</li>
                </ul>
            </div>
            <div class="title_right">
                <div class="left_col" role="main">
                    <a class="btn btn-sm btn-info pull-right" data-toggle="modal" data-for="insert" data-target="#modal_insertrole">Insert</a>
                </div>
            </div>

            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-university"></i> List Role<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <?php if ($data->num_rows() > 0) { ?>
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Role Name</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data->result() as $row) { ?>
                            <tr>

                                <td>
                                    <?php echo $row->rolename; ?>
                                </td>
                                <td>
                                    <?php echo $row->description; ?>
                                </td>
                                <td>
                                    <button class="btn btn-xs btn-info" value="<?php echo $row->roleid; ?>" onclick='showlist(this)'>Show Member(s)</button>
                                <?php if ($row->rolename == "member" || $row->rolename == "admin"): ?>
                                <?php else: ?>
                                    <a class="btn btn-xs btn-warning" data-toggle="modal" data-for="update" data-target="#modalupdate<?php echo $row->roleid; ?>" data-href="<?php echo base_url('index.php/role/update') ?>" data-ajax="<?php echo base_url('index.php/role/getData') ?>" data-id="<?php echo $row->roleid; ?>">Update</a>

                                    <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modalDel" data-id="<?php echo $row->roleid; ?>" data-delete="<?php echo 'Role'; ?>">Delete</a>
                                <?php endif ?>

                                </td>
                            </tr>
                            <?php 
                        } ?>
                        </tbody>
                    </table>
                    <?php 
                } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Insert -->
<div class="modal fade" id="modal_insertrole" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Manage Role Name </h4>
            </div>

            <form role="form" method="post" id="roleInsert" class="form-horizontal form-label-left" name="firstform" action="<?php echo base_url('role/insert') ?>">

                <div class="modal-body">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rolename">Role Name<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="rolename" id="rolename" placeholder="Role Name Must be filled" required type="text" autocomplete="off"/>
                            <span id="role_verify" class="verify">
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="description" id="descriptionModal" placeholder=" Description Must be filled" required type="text" autocomplete="off"/>
                            <span id="description_verify" class="verify">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-info submitInsert" value="Save">
                        <input type="hidden" name="RoleId" id="roleid">
                        <p class="roleModal"></p>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- Modal update-->
<?php
foreach ($data->result_array() as $row) : 
    $roleid = $row['roleid'];
    $rolename = $row['rolename'];
    $description = $row['description'];
    ?>
<div class="modal fade" id="modalupdate<?php echo $roleid; ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Manage Role Name</h4>
            </div>

            <form role="form" method="POST" class="form-horizontal form-label-left roleUpdate" action="<?php echo base_url('role/update') ?>">

                <div class="modal-body">

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rolename">Role Name<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="rolename" placeholder="Role Name Must be filled" required type="text" value='<?php echo $rolename ?>' autocomplete="off"/>
                            <span id="role_verify" class="verify">
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 description" data-validate-length-range="6" data-validate-words="2" name="description" placeholder="Description Must be filled" required type="text" value="<?php echo $description; ?>" autocomplete="off">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-info submitUpdate" value="Save">
                    <input type="hidden" name="RoleId" value="<?php echo $roleid ?>">
                </div>
        </div>
        </form>
    </div>
</div>
</div>
<?php endforeach; ?>

<!-- Modal show member(s)-->
<div class="modal fade" id="listname" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLongTitle">List Name</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="name">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary closemodal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Role Name</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this rolename ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('index.php/role/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deleterole">Delete</button>
                    <input type="hidden" name="roleId" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap-datetimepicker -->
<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<!--Jquery Validation-->
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.js'); ?>"></script>

<script>
    $(document).ready(function() {
        $('#table_sort').DataTable();
        $('.select2_single').select2();
        var handleDataTableButtons = function() {
            if ($("#datatable-responsive").length) {
                $("#datatable-responsive").DataTable({
                    dom: "Bfrtip",
                    order: [
                        [1, "asc"]
                    ],
                    buttons: [{
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };
        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        TableManageButtons.init();
    });
</script>

<!-- Validation Insert and Update-->
<script>
    $('#rolename').keyup(checkrole);
    function checkrole(e){
        var role = $('#rolename').val();
        jQuery.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>role/checkrole',
                data: {rolename:role},
                success: function(data){
                var yourval = jQuery.parseJSON(JSON.stringify(data));
                    if(yourval == '"kosong"'){
                        $('.submitInsert').removeAttr('disabled');
                    }
                    else{ 
                        $('.submitInsert').attr('disabled', 'disabled');
                        e.preventDefault();
                        alert('Role has registered');
                    }
                }
            });
    }

    $('#roleInsert').validate({
        errorElement: 'span',
        errorClass: 'desc',
        rules: {
            rolename: {
                required: true,
                minlength: 2,
               
            },
            description: {
                required: true,
                minlength: 2,
            },
        },
        submitHandler: function(form) {
            alert("Successfull");
            form.submit();
        }
    });



    $('.roleUpdate').each(function() {
        $(this).validate({
            errorElement: 'span',
            errorClass: 'desc',
            rules: {
                
            rolename: {
                required: true,
                minlength: 2,
                
            },
            description: {
                required: true,
                minlength: 2,
               
            },
            },
            message: {
                rolename: {
                    required: true,
                },
                description: {
                    required: true,
                },
            },
            submitHandler: function(form) {
                alert("Successfull");
                // console.log("asdasdas");
                form.submit();
            }
        });
    });

    $('#deleterole').click(function() {
        /* Act on the event */
        alert("Sucessfull");
    });
</script>

<script>
    function showlist(e) {
        // alert(e.value);
        var tempdata = e.value;
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>role/showname",
            dataType: "json",
            data: {
                name: tempdata
            }, // <----send this way //
            success: function(data) {
                // console.log("Success",data);
                var yourval = jQuery.parseJSON(JSON.stringify(data));

                var table_header = "<table class='table table-striped table-bordered'><thead><tr><td>Name</td></tr></thead><tbody>";
                var table_footer = "</tbody></table>";
                var html = "";
                if (yourval === undefined || yourval.length == 0) {
                    html += "<td class='well'>There is no name created yet!</td>";
                } else {
                    yourval.forEach(function(element) {
                        html += "<tr><td>" + element.nama + "</td></tr>";
                    });
                }
                var all = table_header + html + table_footer;
                $('#name').html(all);
                $('#listname').modal('show');

            }
        });
    }
</script>



