<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">

<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">

<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">

<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">

<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" '); ?>" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css" '); ?>" rel="stylesheet">

<style>
  span.desc {
    color: red;
}
</style>


<div class="mid_col" role="main">
<div id="page-wrapper">
  <div class="row" style="min-height: 800px">
    <div class="page-title">
      <div class="title_left">
          <h3>Manage Master <small></small></h3>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
              <li class="active">Shuttle master</li>
            </ul>
      </div>
      <div class="title_right">
        <div class="col-md-2y8 col-sm-2 col-xs-2 form-group pull-right top_search">
              <a class="btn btn-sm btn-info pull-right"
                  data-toggle="modal"
                  data-for="insert"
                  data-target="#modal_masterInsert"
                  data-href="<?php echo base_url('cs_master/insert') ?>">Insert
              </a>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
      <div class="col-md-4 col-sm-4 col-xs-12">
        <p id="selectTriggerFilter"><label><b>Filter Term:</b><br></label>
        </p>
      </div>              
        <div class="row">
          <div class="x_panel">
            <div class="x_title">
              <h2><i class="fa fa-bars"></i> All Datas <small></small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
              <div class="clearfix"></div>
            </div> 
            <div class="x_content">
                <?php if($data->num_rows() > 0){ ?>
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" style="width:50; table-layout: fixed;">                      
                <thead>
                  <tr>
                    <!-- <th>No.</th> -->
                    <th>Day</th>
                    <th>Term</th>
                    <th>Shift</th>
                    <th>Pickup</th>
                    <th>Dropoff</th>
                    <th>Vehicle</th>
                    <th>Driver</th>
                    <th>Action</th>
                  </tr>
              </thead>
               <tbody>
                  <?php foreach($data->result() as $row){ ?>
                  <tr>
                      <!-- <td><?php echo $row->headershuttleid; ?></td> -->
                      <td><?php echo $row->day;?></td>
                      <td><?php echo $row->termcode; ?></td>
                      <td><?php echo $row->shift; ?></td>
                      <td><?php echo $row->pickupLoc; ?></td>
                      <td><?php echo $row->dropoffLoc; ?></td>
                      <td><?php echo $row->vehiclename; ?></td>
                      <td><?php echo $row->drivername; ?></td>
                      <td>
                      <a class="btn btn-xs btn-warning"
                          data-for="update"
                          data-toggle="modal"
                          data-target="#modal_update<?php echo $row->headershuttleid; ?>"
                          data-ajax="<?php echo base_url('cs_master/getData') ?>"
                          data-id="<?php echo $row->headershuttleid; ?>">Update</a>
                      <a class="btn btn-xs btn-danger" 
                          data-toggle="modal" 
                          data-target="#modaldelmaster" 
                          data-id="<?php echo $row->headershuttleid; ?>" 
                          data-delete="<?php echo $row->termcode." - ".$row->shift; ?>">Delete</a>
                       <a class="btn btn-xs btn-success"
                          data-toggle="modal"
                          data-target="#modal_generate<?php echo $row->headershuttleid; ?>"
                          data-ajax="<?php echo base_url('cs_master/getData') ?>"
                          data-id="<?php echo $row->headershuttleid; ?>">
                          Generate</a>
                      </td>
                </tr>
              <?php } ?>
              </tbody>
          </table>
            <?php } else { ?>
            <div class="well">There is no data created yet!</div>
            <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>   

<!-- Modal Insert -->
<div class="modal fade" id="modal_masterInsert" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Insert Master</h4>
            </div>
            <form role="form" method="post" id="masterInsert" class="form-horizontal form-label-left" action="<?php echo base_url('cs_master/insert') ?>">
                <div class="modal-body">

                  <div class="item form-group">
                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <label for="term">Term:</label>
                            <select name="termcode" id="termModal" class="form-control" id="term">
                              <option value=""></option>
                               <?php foreach($dataTerm as $row){ ?>
                               <option value="<?php echo $row->termid; ?>"><?php echo $row->termcode; ?> <?php echo $row->description; ?></option>';
                              <?php } ?>
                            </select>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <label for="vehicle">Vehicle:</label>
                          <select name="vehiclename" id="vehicle" class="form-control">
                            <option value=""></option>
                          <?php foreach($dataVehicle as $row){ ?>
                              <option value="<?php echo $row->vehicleid; ?>"><?php echo $row->vehiclename; ?></option>';
                                    <?php } ?>
                          </select>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                           <label for="term">Driver:</label>
                            <select name="drivername" id="drivernid" class="form-control">
                              <option value=""></option>
                             <?php foreach($dataDriver as $row){ ?>
                                <option value="<?php echo $row->driverid; ?>"><?php echo $row->drivername; ?></option>';
                                    <?php } ?>
                            </select>
                      </div>
                  </div>
                   <div class="item form-group">
                    <label for="term">Day:</label>        
                        <select name="day" id="day" class="select2_single form-control" tabindex="-1" id="day">
                             <option value=""></option>
                            <option value="Monday">Monday</option>
                            <option value="Tuesday">Tuesday</option>
                            <option value="Wednesday">Wednesday</option>
                            <option value="Thursday">Thursday</option>
                            <option value="Friday">Friday</option>
                            <option value="Saturday">Saturday</option>
                        </select>

                    <label for="term">Shift:</label>
                        <select name="shift" id="shift" class="form-control shift">
                              <option value=""></option>
                          <?php foreach($dataShift as $row){ ?>
                          <option value="<?php echo $row->shiftid; ?>"><?php echo $row->shift; ?></option>';
                          <?php } ?>
                        </select>
                    </div>
                  <div class="item form-group">
                        <label for="term">Pick up:</label>
                            <select name="pickup" class="select2_single form-control pickupinsert" tabindex="-1" >
                              <option value=""></option>
                            <?php foreach($dataPickup as $row){ ?>
                                  <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode; ?></option>';
                                  <?php } ?> 
                            </select>   

                        <label for="term">Drop off:</label>
                            <select name="dropoff" class="select2_single form-control dropoff" tabindex="-1" id="dropoff">
                              <option value=""></option>
                                <!-- <?php foreach($dataDropoff as $row){ ?>
                                  <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode; ?></option>';
                                        <?php } ?> -->
                            </select>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-info"  id="submitInsert" value="Save">
                        <input type="hidden" name="Masterid" id="masterid">
                  </div>
              </div>
            </form>
        </div>
    </div>
 </div>

<!-- Modal Update master-->
<?php
        foreach($data->result_array() as $row):
            $headershuttleid=$row['headershuttleid'];
            $termcode2=$row['termcode'];
            $termid2=$row['termid'];
            $day=$row['day'];
            $vehiclename=$row['vehicleid'];
            $drivername=$row['driverid'];
            $shift=$row['shiftid'];
            $campuscode1=$row['campuspickupid'];
            $campuscode=$row['campusdropoffid'];
?> 
<div class="modal fade" id="modal_update<?php echo $headershuttleid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update master</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form role="form" method="post"  class="form-horizontal form-label-left masterUpate" action="<?php echo base_url('cs_master/update') ?>">
        <div class="modal-body">

      <div class=" item form-group">
      <div class="col-md-4 col-sm-4 col-xs-12">      
      <label for="term">Term:</label>          
      <select name="termcode" class="form-control termcode">
        <?php foreach($dataTerm as $row){ ?>
        <?php if ($row->termid == $termid2): ?>
          <option value="<?php echo $row->termid; ?>" selected><?php echo $row->termcode; ?></option>';
        <?php else: ?>
          <option value="<?php echo $row->termid; ?>"><?php echo $row->termcode; ?></option>';
        <?php endif; ?>
        <?php } ?>
      </select>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12">
          <label for="vehicle">Vehicle:</label>
          <select name="vehiclename" class="form-control vehiclename" >
          <?php foreach($dataVehicle as $row){ ?>
            <?php if ($row->vehicleid == $vehiclename): ?>
        <option value="<?php echo $row->vehicleid; ?>" selected><?php echo $row->vehiclename; ?>
        </option>';
          <?php else: ?>
        <option value="<?php echo $row->vehicleid; ?>"><?php echo $row->vehiclename; ?></option>';
        <?php endif; ?>
          <?php } ?>
          </select>
      </div>

      <div class="col-md-4 col-sm-4 col-xs-12">
       <label for="term">Driver:</label>
        <select name="drivername" class="form-control drivername">
         <?php foreach($dataDriver as $row){ ?>
            <?php if ($row->driverid == $drivername): ?>
        <option value="<?php echo $row->driverid; ?>" selected><?php echo $row->drivername; ?>
        </option>';
        <?php else: ?>
        <option value="<?php echo $row->driverid; ?>"><?php echo $row->drivername; ?></option>';
            <?php endif; ?>
          <?php } ?>
        </select>
    </div>
  </div>

     <div class="item form-group">
      <label for="day">Day:</label>        
          <select name="day" class="form-control day">
              <?php if ("Monday" == $day): ?>
              <option value="Monday" selected>Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Tuesday" == $day): ?>
              <option value="Monday">Monday</option>;
              <option value="Tuesday"selected>Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Wednesday" == $day): ?>
              <option value="Monday">Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday"selected>Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Thursday" == $day): ?>
              <option value="Monday">Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday"selected>Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Friday" == $day): ?>
              <option value="Monday" >Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday"selected>Friday</option>;
              <option value="Saturday">Saturday</option>;

              <?php elseif ("Saturday" == $day): ?>
              <option value="Monday">Monday</option>;
              <option value="Tuesday">Tuesday</option>;
              <option value="Wednesday">Wednesday</option>;
              <option value="Thursday">Thursday</option>;
              <option value="Friday">Friday</option>;
              <option value="Saturday"selected>Saturday</option>;
            <?php endif; ?>
          </select>
                
      <label for="term">Shift:</label>
        <select name="shift" class="form-control shift" >
          <?php foreach($dataShift as $row){ ?>
      <?php if ($row->shiftid == $shift): ?>
      <option value="<?php echo $row->shiftid; ?>" selected><?php echo $row->shift; ?>
      </option>';
            <?php else: ?>
      <option value="<?php echo $row->shiftid; ?>"><?php echo $row->shift; ?></option>';
          <?php endif; ?>
            <?php } ?>
        </select>
      </div>

          <div class="item form-group">
      <label for="term">Pick up:</label>
      <select name="pickup"  class="select2_single form-control pickupupdate" tabindex="-1">
        <?php foreach($dataPickup as $row){ ?>
        <?php if ($row->campusid == $campuscode1): ?>
        <option value="<?php echo $row->campusid; ?>" selected><?php echo $row->campuscode; ?>
        </option>';
          <?php else: ?>
        <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode; ?></option>';
        <?php endif; ?>
          <?php } ?>
      </select>

        <label for="term">Drop off:</label>
        <select name="dropoff" class="select2_single form-control dropoffupdate" tabindex="-1" >
          <?php foreach($dataDropoff as $row){ ?>
          <?php if ($row->campusid == $campuscode): ?>
          <option value="<?php echo $row->campusid; ?>" selected><?php echo $row->campuscode; ?>
          </option>';
            <?php else: ?>
          <option value="<?php echo $row->campusid; ?>"><?php echo $row->campuscode; ?></option>';
          <?php endif; ?>
            <?php } ?>
        </select>
          </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-info submitUpdate" value="Save">
              <input type="hidden" name="updatemaster" value="<?php echo $headershuttleid; ?>">
      </div>
    </form>
    </div>
  </div>
  </div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<div class="modal fade" id="modaldelmaster" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Master</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this data ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('cs_master/delete'); ?>">
                <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deletecsmaster">Delete</button> 
                    <input type="hidden" name="deletemasterId" id="deleteId" value="<?php echo $headershuttleid ?>">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Generate Term-->
<?php
  foreach($data->result_array() as $row):
  $headershuttleid=$row['headershuttleid'];
  $termcode=$row['termid'];
  $day=$row['day'];
  $vehiclename=$row['vehicleid'];
  $drivername=$row['driverid'];
  $shift=$row['shiftid'];
  $pickup=$row['campuspickupid'];
  $dropoff=$row['campusdropoffid'];
?>
<div class="modal fade" id="modal_generate<?php echo $headershuttleid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h3 class="modal-title" id="myModalLabel">Generate Data Master</h3>
  </div>
  <form role="form" method="post" class="form-horizontal form-label-left" action="<?php echo base_url('cs_master/generate') ?>">
      <div class="modal-body">

          <h2 class="modal-title">Are you sure ?</h3>
          <div class="form-group">
          </div>
  <div class="form-group">
    <div class="col-md-4 col-sm-4 col-xs-12">
       <input type="hidden" class="form-control" name="termcode"  value="<?php echo $termcode ?>" />
       <input type="hidden" class="form-control" name="shuttleid" value="<?php echo $headershuttleid ?>" />
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
         <input type="hidden" class="form-control" name="vehicle" value="<?php echo $vehiclename ?>" />
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
         <input type="hidden" class="form-control" name="driver" value="<?php echo $drivername ?>" />
    </div>
  </div>

  <div class="form-group">  
       <input type="hidden" class="form-control" name="day"  value="<?php echo $day ?>" />

     <input type="hidden" class="form-control" name="shift"  value="<?php echo $shift ?>" />
  </div>

  <div class="form-group">
    <input type="hidden" class="form-control" name="pickup"  value="<?php echo $pickup ?>" />

    <input type="hidden" class="form-control" name="dropoff"  value="<?php echo $dropoff ?>" />
  </div>

      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-info">Save</button>
          <input type="hidden" name="shuttleid" value="<?php echo $headershuttleid; ?>">
      </div>
  </form>
  </div>
  </div>
</div>
<?php endforeach;?>


<!-- jQuery -->
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
 <script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap-datetimepicker -->    
<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<!--Jquery Validation-->
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<!-- Select2 JS-->
 <script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>

<script>
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "lengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
      ],
      "deferRender": true,
      "initComplete": function() {
        var column = this.api().column(1);

        var values = [];
        column.data().each(function(d, j) {
          d.split(",").forEach(function(data) {
            data = data.trim();
            
            if (values.indexOf(data) === -1) {
              values.push(data);
            }
          });
        });

        $('<select class="select2_single form-control" tabindex="-1" id="selectTriggerFilter"><option value="">-- Select All --</option></select>')
          .append(values.sort().map(function(o) {
            return '<option value="' + o + '">' + o + '</option>';
          }))
          .on('change', function() {
            column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
          })
          .appendTo('#selectTriggerFilter').select2();
      }
    });
  });
</script>

      <!-- Validation Insert and Update-->
<script>
  $('#masterInsert').validate({ // initialize the plugin         
          errorElement: 'span',
          errorClass: 'desc',
        
        rules: {
                  
                     
                     termcode: {
                         required: true
                     },
                      shift: {
                         required: true     
                    },
                      vehiclename: {
                         required: true
                     },
                       drivername: {
                         required: true
                     },

                       day: {
                         required: true        
                     },

                       pickup: {
                         required: true
                     },
                       dropoff: {
                         required: true
                     },
                     },
 
     submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
    });

 $('.masterUpdate').each(function () {
       $(this).validate({
         errorElement: 'span',
          errorClass: 'desc',

                    rules: {
                  
                     termcode: {
                         required: true
                     },
                      shift: {
                         required: true     
                    },
                      vehiclename: {
                         required: true
                     },
                       drivername: {
                         required: true
                     },
                       day: {
                         required: true        
                     },
                       pickup: {
                         required: true
                     },
                       dropoff: {
                         required: true
                     },
                    
                  },
                  message: {

                     termcode: {
                         required: true
                     },
                      shift: {
                         required: true     
                    },
                      vehiclename: {
                         required: true
                     },
                       drivername: {
                         required: true
                     },

                     day: {
                         required: true        
                     },
                       pickup: {
                         required: true
                     },
                       dropoff: {
                         required: true
                     },
            },
            submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
        });
      });
     $('.submitUpdate').click(function() {
     /* Act on the event */
       alert("Sucessfull");
     });


      $('#deletecsmaster').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });

   $('#genereate').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });

</script>

<script >
 $(document).ready(function () {
      // $('#shiftbutton').click(function() {
        /* Act on the event */
        $('.pickupinsert').change(function(){
            $.ajax({
              type: 'POST',
              url: "<?php echo base_url(); ?>cs_master/cekvalidasi",
              dataType: "json",
              data: {
              pickupdata : $('.pickupinsert').val()
              }, // <----send this way
              success: function(data) {
              // alert("Sucessfull");
               // console.log(data);
                var yourval = jQuery.parseJSON(JSON.stringify(data));
               var dropoffhtml="";

               dropoffhtml += "<option id =\"0\" value =\"\"></option>";
                  for (var i=0 ; i<yourval.length; i++)
                  {
                     dropoffhtml += "<option value =\""+yourval[i].campusid+"\" id =\""+ yourval[i].campusid +"\">"+yourval[i].campuscode+"</option>";
                  }
                
                $('#dropoff').html(dropoffhtml);  
              }
            });
            return false;
        });

        $('.pickupupdate').change(function(){
            $.ajax({
              type: 'POST',
              url: "<?php echo base_url(); ?>cs_master/cekvalidasi2",
              dataType: "json",
              data: {
              pickupdata : $('.pickupupdate').val()
              }, // <----send this way
              success: function(data) {
              // alert("Sucessfull");
               // console.log(data);
                var yourval = jQuery.parseJSON(JSON.stringify(data));
               var dropoffhtml="";

               dropoffhtml += "<option id =\"0\" value =\"\"></option>";
                  for (var i=0 ; i<yourval.length; i++)
                  {
                     dropoffhtml += "<option value =\""+yourval[i].campusid+"\" id =\""+ yourval[i].campusid +"\">"+yourval[i].campuscode+"</option>";
                  }
                
                $('.dropoffupdate').html(dropoffhtml);  
              }
            });
            return false;
        });
      });

</script> 
