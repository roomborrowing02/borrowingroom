  <!-- Bootstrap -->
<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
 <!-- Font Awesome -->
 <link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css'); ?>" rel="stylesheet">
 <!-- Custom Theme Style -->
 <link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>"  rel="stylesheet">
<!-- Switchery -->
<link href="<?php echo base_url('assets/jquery-ui-1.11.4.custom/jquery-ui.theme.css'); ?>" rel="stylesheet"/>

<form action="" method="post" id="formbooking">
	<div>
		<label for="">Name</label>
		<input type="text" name="name" id="nameform">
	</div>
	<div>
		<label for="">Email</label>
		<input type="text" name="email" id="emailform">
	</div>
	<div>
		<label for="">Password</label>
		<input type="password" name="password" id="passwordform">
	</div>
	<input class="btn btn-sm btn-info pull-right" name='submit' id="submitform" value="Submit">
</form>

<!-- Modal Update/Insert -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
      </div>
      <form class="form-horizontal form-label-left"  method="post" id="formmodal">
        <div>
			<label for="">Name</label>
			<input type="text" name="name" id="namemodal">
		</div>
		<div>
			<label for="">Email</label>
			<input type="text" name="email" id="emailmodal">
		</div>
		<div>
			<label for="">Password</label>
			<input type="password" name="password" id="passwordmodal">
		</div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" id="Save" style="bottom: -40px; right: 10px;">Book</button>
          <input type="hidden" name="Bookingid" id="bookingid" >
        </div>
      </form>
    </div>
  </div>
</div>
<!-- jQuery -->
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/jquery-ui-1.11.4.custom/external/jquery/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/jquery-ui-1.11.4.custom/jquery-ui.js'); ?>"></script>

<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>

<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>

<script>
	
</script>
<script>
	 $(function(){
    $("#formbooking").validate(
      {
        errorElement: 'span',
        errorClass: 'desc',
        rules: 
        {
          name: 
          {
            required: true
          },
          email:
          {
          	required: true
          },
          password:
          {
          	required: true
          }
        },

      });
    	$('#submitform').click(function(){
			if($('#formbooking').valid()){
	          	$('#modal').modal('show');
	          	$('#namemodal').val($('#nameform').val());
		    	$('#emailmodal').val($('#emailform').val());
		    	$('#passwordmodal').val($('#passwordform').val());
	      }	    
  		});
    });
</script>