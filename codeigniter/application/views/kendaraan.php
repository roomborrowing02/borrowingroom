<link href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>" rel="stylesheet">

<style>
  span.desc {
    color: red;
}
</style>


<div id="page-wrapper">
  <div class="row" style="min-height: 768px;">
        <div class="page-title">
          <div class="title_left">
           <h3>Manage Vehicle <small></small></h3>
                  <ul class="breadcrumb">
            <li><a href="<?php echo base_url('index.php/admin'); ?>">Home</a></li>
            <li class="active">Vehicle</li>
        </ul>
          </div>
          <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <a class="btn btn-sm btn-info pull-right"
               data-toggle="modal"
               data-for="insert"
               data-target="#modal_insertKendaraan">Insert
            </a>
        </div>
      </div>
    </div>
    <?php if($data->num_rows() > 0){ ?>
<table id="table_sort" class="cell-border" style="width:100%">
      <thead>
            <tr>
                <th>VehicleName</th>
                <th>VehiclePlate</th>
                <th>Capacity</th>
               <!--  <th>Foto Vehicle</th> -->
                <th>Action</th>
            </tr>
      </thead>
     <tbody>
             <?php foreach($data->result() as $row){ ?>
           <tr>
             <td><?php echo $row->vehiclename; ?></td>
             <td><?php echo $row->vehicleplate; ?></td>
             <td><?php echo $row->capacity; ?></td>
           <!--   <td></td> -->
             <td>
               <a class="btn btn-xs btn-warning"
                data-toggle="modal"
                data-for="update"
                data-target="#modalupdate<?php echo $row->vehicleid; ?>"
                data-ajax="<?php echo base_url('index.php/kendaraan/getData') ?>"
                data-id="<?php echo $row->vehicleid; ?>">
                Update</a>
              <a class="btn btn-xs btn-danger" 
                data-toggle="modal" 
                data-target="#modalDel" 
                data-id="<?php echo $row->vehicleid; ?>" 
                data-delete="<?php echo $row->vehiclename; ?>"">Delete</a>
                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } else { ?>
            <div class="well">There is no Vehicle Data! </div>
            <?php } ?>
        </div>

    </div>
</div>

<!-- Modal Insert -->
<div class="modal fade" id="modal_insertKendaraan"  tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">

  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h4 class="modal-title" id="myModalLabel">Register Vehicle</h4>
    </div>
      <form role="form" method="post" id="formInsert" class="form-horizontal form-label-left" action="<?php echo base_url('kendaraan/insert') ?>">
  <div class="modal-body">

    <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Vehicle Name <span class="required">*</span>
    </label>
     <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="vehiclename" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="vehiclename" placeholder="ex: Bus/Elf" required="required" type="text" autocomplete="off" >
       </div>
    </div>

    <div class="item form-group">
     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Vehicle Plate <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="vehicleplate" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="vehicleplate" placeholder="ex: B 1 NUS" required="required" type="text" autocomplete="off">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Capacity <span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="capacity" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="capacity" placeholder="ex: 01/50" required="required" type="text" autocomplete="off">
      </div>
    </div>
  </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <!--  <button type="submit" class="btn btn-info" id="submitInsert">Save</button> -->
          <input type="submit" class="btn btn-info " id="submitInsert" value="Save">
          <input type="hidden" name="Vehicleid" id="vehicleid" >
        </div>

      </form>
    </div>
  </div>
</div>


<!-- Modal Update-->
        <?php
        foreach($data->result_array() as $row):
            $vehicleid=$row['vehicleid'];
            $vehiclename=$row['vehiclename'];
            $vehicleplate=$row['vehicleplate'];
            $capacity=$row['capacity'];
         ?>
<div class="modal fade" id="modalupdate<?php echo $vehicleid; ?>" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Vehicle</h4>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left vehicleUpdate" action="<?php echo base_url('kendaraan/update') ?>">
                <div class="modal-body">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Vehicle Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="vehiclename" placeholder="ex: Bus/Elf" required="required" type="text" value="<?php echo $vehiclename; ?>" autocomplete="off">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Vehicle Plate <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="vehicleplate" placeholder="ex: B 1 NUS" required="required" type="text" value="<?php echo $vehicleplate; ?>" autocomplete="off" >
                        </div>
                      </div>

                               <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Capacity <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="capacity" placeholder="ex: 01/50" required="required" type="text" value="<?php echo $capacity;?>"  autocomplete="off">
                        </div>
                      </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                   <!--  <button type="submit" class="btn btn-info submitUpdate">Save</button> -->
                      <input type="submit" class="btn btn-info updateButton" value="Save">
                    <input type="hidden" name="vehicleId" value="<?php echo $vehicleid ?>">
                </div>
            </form>
        </div>
    </div>
  </div>
<?php endforeach;?>


<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Vehicle</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this term ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('index.php/kendaraan/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deletevehicle">Delete</button>
                    <input type="hidden" name="vehicleId" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/jquery.dataTables.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $('#table_sort').DataTable();
    } );
</script>


<script>
  $('#formInsert').each(function () {
    $(this).validate({
      errorElement: 'span',
      errorClass: 'desc',
      rules: {
          vehiclename: {
              required: true,
               minlength: 3,
              

          },
           vehicleplate: {
              required: true,
              minlength: 5,
              maxlength: 10 ,

              
          },
           capacity: {
              required: true,

          },
      },
      message: {
          vehiclename: {
              required: true
          },
           vehicleplate: {
              required: true
          },
           capacity: {
              required: true
          },
      },
      submitHandler: function(form) {
          alert("Successfull");
        // console.log("asdasdas");
        form.submit();
        }
      });
  });

  $('.vehicleUpdate').each(function () {
    $(this).validate({
      errorElement: 'span',
      errorClass: 'desc',
      rules: {
          vehiclename: {
              required: true,
               minlength: 3,
          
          },
           vehicleplate: {
              required: true,
              minlength: 5,
              maxlength: 10 ,

              
          },
           capacity: {
              required: true,

          },
      },
      message: {
          vehiclename: {
              required: true
          },
           vehicleplate: {
              required: true
          },
           capacity: {
              required: true
          },
      },
      submitHandler: function(form) {
          alert("Successfull");
        // console.log("asdasdas");
        form.submit();
        }
      });
  });

      $('#deletevehicle').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });

</script> 

