

<html>

    <head>
		<title>Lecturer's Contacts Form</title>
		<link rel="shortcut icon" href="<?php echo base_url('assets/images/general/icon.ico'); ?>">

        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo base_url('assets/vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
        <!-- Animate.css -->
        <link href="<?php echo base_url('assets/vendors/animate.css/animate.min.css'); ?>" rel="stylesheet">
        <!-- iCheck -->
        <link href="<?php echo base_url('assets/vendors/iCheck/skins/flat/green.css'); ?>" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="<?php echo base_url('assets/css/custom.min.css'); ?>" rel="stylesheet">
        <style type="text/css">
            
@charset "utf-8";
/* CSS Document */

a, a:visited{
    text-decoration:none;
    color: #777;
}

body{
    color: #777;
    font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif; 
    font-size:19px;
    background-color: white;
}
.title{
    margin: 0 auto;
    text-align: center;
    color: #0098D7;
    margin-top: 40px;
}
    .title h1{
        font-size: 50px;
    }
#wrapper
{
    width:540px;
    margin:auto;
    text-align:center;
    font-weight: bold;
}
#roomPanel{
    text-align:center;
    width:100%;
    margin:20px 0 10px 0;
}
    #roomPanel select{
        width: 250px;
        height: 35px;
        color: #777;
        font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;
        font-weight: bold;
        font-size: 18px;
        border-radius: 5px;
        background-color: #f6f6f6;
        text-indent: 10px;
    }
#leftPanel{
    float:left;
    width:96%;
    padding:2%;
    border:6px double #0098D7;
    border-radius:20px;
    background-color: #D9D9D9;
    margin-top: 20px;
}
    #leftPanel input[type=checkbox]{
        transform: scale(1.5);
        -webkit-transoform: scale(1.5);
    }
    #leftPanel button{
        width:100%;
        color: white;
        border:2px solid #0098D7;
        border-radius:10px;
        height:30px;
        margin-bottom:1%;
        font-size:15px;
        font-weight:bold;
        background-color: #777;
    }
#rightPanel{
    float:left;
    width:100%;
    padding:10px 0 10px 0;
}
    #rightPanel button{
        margin-top:10px;
        color: #0098D7;
        transform: scale(1.5);
        -webkit-transoform: scale(1.5);
        font-family:"Comic Sans MS", cursive, sans-serif;
    }
#leftPanel div{
    float:left;
    width:33.33%;
}
#leftPanel1 label, #leftPanel2 label, #leftPanel3 label{
    margin:5px 5px 5px 10px;
    width:100%;
    float:left;
    text-align:justify;
}

/*Tagline Binus*/
.tagline{
    background-color: #0098D7;
    width: 53px;
    height: 266px;
    position: fixed;
    top:0;
    left: 20px;
    z-index: 1;
}
    #tagline-tale{
        margin: 200px 0 50px 0;
        display: block;
        text-indent: -200px;
        overflow: hidden;
        background: url('<?php echo base_url('assets/images/general/tagline.png'); ?>') 0 0 no-repeat;
        width: 53px;
        height: 203px;
    }
    #binus-tagline{
        font-size: 11px;
        font-weight: normal;
        font-family: "Interstate",Helvetica,Arial,sans-serif;
        color: #414042;
        line-height: 1.7em;
    }
.logo-lab{
    position: fixed;
    margin-left: 65px;
    width: 250px;
    z-index: 0;
    top:10px;
    left: 15px;
}
    .logo-lab img{
        width: 150px;
        height: auto;
    }
    .form-control{
        font-size: 50px;
        width: 700px !important;
        height: 100px !important;
    }
    .login_wrapper{
        max-width: 700px;
        margin-top: 0%;
    }
    #wrapper{
        width: auto !important;
    }
    input { 
        text-align: center; 
    }
    .cbWA{
        font-size: 40px;
        width: 100px !important;
        height: 25px !important;
    }
    #lecturerName{
        color: #0098D7;
        font-size: 40px;
    }

        #lecturerName .err{
            color: red;
        }
        #lecturerName .suc{
            color: #0098D7;
        }


    .footer{
        font-size: 10px !important;
    }

/*END - Tagline Binus*/
        </style>
        
            </head>

	<body>
		<!--Tagline binus-->

		<div class="tagline">
			<span id="tagline-tale"></span>
			<span id="binus-tagline">People Innovation Excellence</span>
		</div>

		<div class="logo-lab">
			<img src="<?php echo base_url('assets/images/general/binus-logo.png'); ?>"/>
		</div>
		<!--END - Tagline binus-->

		<div class="title">
			<h1>Update Your Phone Number Here!</h2>
		</div>

        	
    	<div id="wrapper">
        	<div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form>

              <div id="lecturerName">
                <p class="err">&nbsp;</p>
              </div>
              <div>
                <input type="text" class="form-control" id="txtLecturerCode" placeholder="Lecturer Code" required="" autocomplete="off" autofocus="1" data-inputmask="'mask' : 'D9999'" onkeyup="search(this)"/>
              </div>

              <div>
                <input type="text" id="txtPhoneNumber" class="form-control" placeholder="Phone Number" required="" autocomplete="off"  autofocus="2" data-inputmask="'mask' : '+62 999-9999-9999'" onkeyup="collect(this)"/>
              </div>
              <div>
                    <label>
                        <input type="checkbox" class="flat" checked="checked"> <br> This is also my Whatsapp number <i class="fa fa-2x fa-whatsapp"></i>
                    </label>
                </div>
              </div>
              <!-- <div class="clearfix"></div>
              <div>
                <button type="button" class="btn btn-success btn-lg">Press Enter to Submit</button>
              </div> -->

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />
<!-- 
                <div class="footer">
                  <p>©2018 All Rights Reserved<br>Lecturer Service Center. Bina Nusantara University</p>
                </div> -->
              </div>
            </form>
          </section>
        </div>
      </div>
        </div>
            
	</body>
</html>


<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- jquery.inputmask -->
<script src="<?php echo base_url('assets/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/vendors/iCheck/icheck.min.js'); ?>"></script>

<script type="text/javascript">
    
        /* INPUT MASK */
        
        function init_InputMask() {
            
            if( typeof ($.fn.inputmask) === 'undefined'){ return; }
            console.log('init_InputMask');
            
                $(":input").inputmask();
                
        };

        $(document).ready(function() {

        init_InputMask();
        });

</script>

<script>
   //       function test(){
            //  alert("Test");
            // }
            
            $(function(){
                $(document).keydown(function(objEvent){
                    if(objEvent.keyCode == 13){
                        alert("Submitted");
                    }
                    // if(objEvent.ctrlKey){
                    //  if(objEvent.keyCode == 65)
                    //      selectAllAction();
                    //  else if(objEvent.keyCode == 68)
                    //      deSelectAllAction();
                    //  else if(objEvent.keyCode == 73)
                    //      inverseAllAction();
                    //  return false;
                    // }
                })
            });

            function search(ele) {
                var lecturerCode = ele.value;
                const userKeyRegExp = /D[0-9]{4}/i;
                const valid = userKeyRegExp.test(lecturerCode);
                console.log(lecturerCode);

                if(valid){
                    $.ajax({
                        type:"post",
                        url: "<?php echo base_url(); ?>lecturer/getLecturerName",
                        data:{ lecturerCode:lecturerCode},
                            success:function(response){
                                response = JSON.parse(response);
                                console.log(response);
                                if(response.message != "ERROR"){
                                    console.log(response.nmKTP);
                                    $("#lecturerName").html("<p class=\"suc\">"+response.nmKTP+"</p>");
                                }else{
                                    $("#lecturerName").html("<p class=\"err\">Invalid ID</p>");
                                }
                            },
                            error: function(){
                                alert("Invalid!");
                            }
                    });
                    if(event.key === 'Enter') {
                        $("#txtPhoneNumber").focus();
                    }
                }else{
                    $("#lecturerName").html("<p class=\"err\">Input your valid lecturer's code</p>");
                }

                // if(event.key === 'Enter') {
                //     alert(ele.value);        
                // }
            }

            function collect(param){
                var lecturerPhone = param.value;
                console.log(lecturerPhone);
                if(event.key === 'Enter') {
                        $("#txtPhoneNumber").focus();
                    }
            }
            // iCheck
$(document).ready(function() {
    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
});
// /iCheck
        </script>