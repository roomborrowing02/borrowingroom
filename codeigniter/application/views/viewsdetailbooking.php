<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"  rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>"  rel="stylesheet">
 <!-- bootstrap-datetimepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>"  rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/jquery-ui-1.11.4.custom/jquery-ui.theme.css'); ?>" rel="stylesheet"/>

<style>
  span.desc {
    color: red;
}

</style>
    <div id="page-wrapper">
    <div class="row" style="min-height: 768px">
       <div class="page-title">
        <div class="title_left">
           <h3>Manage Detail Booking Date<small></small></h3>
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
            <li class="active">Detail Booking Date</li>
        </ul>
          </div>
          <div class="title_right">
      </div>
          </div>
            <div class="clearfix"></div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <p id="selectTriggerFilter"><label><b>Filter Booking Date:</b><br></label></p>
    </div>
                <div class="x_panel">
                <div class="x_title">
                    <h2><i class="glyphicon glyphicon-list-alt"></i> All Passenger Booking Date <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                    <thead>

                        <tr>
                          <th>Date</th>
                          <th>Shift</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>  
                          <th>Pickup</th>
                          <th>Dropoff</th>
                          <th>OrderPassenger</th>
                          <th>Description</th>
                          <th>Booking By</th>
                          <th>Action</th>
                       
                        </tr>
                    </thead>
                    <tbody>
              <?php foreach($dataquery->result() as $row){ ?>
              <?php $totalid = str_replace(array('P','T'), '',$row->id); ?>
            <tr>
              <td><?php echo date('M-d-Y',strtotime($row->date));?></td>
              <td><?php echo $row->shift; ?></td>           
              <td><?php echo $row->nama; ?></td>
              <td><?php echo $row->email;?></td> 
              <td><?php echo $row->phone; ?></td>   
              <td><?php echo $row->pickupLoc; ?></td>
              <td><?php echo $row->dropoffLoc; ?></td>
              <td><?php echo $row->name; ?></td>
              <td><?php echo $row->description; ?></td>
              <td><?php echo $row->bookingby; ?></td>
              <!-- <td><?php echo $row->pid; ?></td> -->
              <td>
              <a class="btn btn-xs btn-danger" 
                data-toggle="modal" 
                data-target="#modalDel<?php echo $row->id ?><?php echo $row->date ?>" 
                data-delete="<?php echo 'trbooking';?>">Delete</a></td>
              </tr>
                 <?php } ?>
            </tbody>
        </table>
             </div>
             </div> 
          </div>
        </div>

<!-- Modal Delete -->
<?php
        foreach($dataquery->result_array() as $row):
            $id=$row['id'];
            $pid=$row['pid'];
            $date=$row['date'];
            $shiftid=$row['shiftid'];
            $totalorder=$row['totalorder'];
            $userid=$row['userid'];
            $nama=$row['nama'];
            $name=$row['name'];
            $email=$row['email'];
            $phone=$row['phone']; 
            $campusid=$row['pickupid'];
            $campusid1=$row['dropoffid'];
            $totalorder=$row['totalorder'];
            $description=$row['description'];
         ?>
<div class="modal fade" id="modalDel<?php echo $id ?><?php echo $date ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"> Are you sure want to delete this data DetailBooking?</h4>
            </div>
              <br>
            <form role="form" method="post" class="form-horizontal form-label-left" action="<?php echo base_url('detailbooking/delete'); ?>">
              <div class="col-md-12">
               <div class="item form-group">
                     <label class="control-label col-md-3 col-sm-3 col-xs-12 datebooking" for="date">Date<span class="required">*</span>
                        </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="text" name="date" value="<?php echo $date ?>" readonly="readonly">
                    </div>
                </div>
               <div class="item form-group">
                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="shift">Shift<span class="required">*</span>
                        </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                     <input type="text" name="shiftid" value="<?php echo $shiftid ?>" readonly="readonly">
                    </div>
                </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="nama" value="<?php echo $nama ?>" readonly="readonly">
                      </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">E-Mail<span class="required">*</span>
                        </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text"name="email" value="<?php echo $email ?>" readonly="readonly">
                      </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Phone<span class="required">*</span>
                        </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="phone" value="<?php echo $phone ?>"readonly="readonly">
                      </div>
                      
                    </div>
                   <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-3">Pickup</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                     <input type="text" name="pickupid" value="<?php echo $campusid?>"readonly="readonly">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-3">Dropoff</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="dropoffid" value="<?php echo $campusid1 ?>"readonly="readonly">
                </div>
              </div>
                     <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ordderpassenger">OrderPassenger<span class="required">*</span>
                          </label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="name" value="<?php echo $name ?>"readonly="readonly">
                      </div>
              </div>
                    </div>
                     <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description<span class="required">*</span>
                          </label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="text" name="description" value="<?php echo $description ?>"readonly="readonly">
                      </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <input type="hidden" name="userid" value="<?php echo $userid; ?>">
                    <input type="hidden" name="totalorder" value="<?php echo $totalorder; ?>">
                    <input type="hidden" name="pid" value="<?php echo $pid; ?>">
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-danger" id="deletedate">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="hidden" name="trbookingId" class="deleteId">
                    </div>
                    </div>
            </form>
        </div>
    </div>
</div>

<?php endforeach;?>     
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/jquery-ui-1.11.4.custom/external/jquery/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/jquery-ui-1.11.4.custom/jquery-ui.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
 <script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap-datetimepicker -->    
<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<!--Jquery Validation-->
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.js'); ?>"></script>

  <script>   
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "lengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
      ],
      dom: "Bfrtip",
                    order: [[ 1, "asc" ]],
                    buttons: [
                         {
                             extend: "copy",
                             className: "btn-sm"
                         },
                         {
                             extend: "csv",
                             className: "btn-sm"
                         },
                         {
                             extend: "excel",
                             className: "btn-sm"
                         },
                         {
                             extend: "pdfHtml5",
                             className: "btn-sm"
                         },
                         {
                             extend: "print",
                             className: "btn-sm"
                         },
                    ],
                    responsive: true,
      "deferRender": true,
      "initComplete": function() {
        var column = this.api().column(0);

        var values = [];
        column.data().each(function(d, j) {
          d.split(",").forEach(function(data) {
            data = data.trim();
            
            if (values.indexOf(data) === -1) {
              values.push(data);
            }
          });
        });

        $('<select class="select2_single form-control" tabindex="-1" id="selectTriggerFilter"><option value="">-- Select All --</option></select>')
          .append(values.sort().map(function(o) {
            return '<option value="' + o + '">' + o + '</option>';
          }))
          .on('change', function() {
            column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
          })
          .appendTo('#selectTriggerFilter').select2();
      }
    });
  });
</script>

 <script>
  $('#myDatepicker').datetimepicker();
  $('#myDatepicker2').datetimepicker({
    format: 'YYYY-MM-DD',
  });
  $('#myDatepicker3').datetimepicker({
      format: 'hh:mm A'
  });
  $('#myDatepicker4').datetimepicker({
      ignoreReadonly: true,
      allowInputToggle: true
  });
  $('#datetimepicker6').datetimepicker();
  $('#datetimepicker7').datetimepicker({
      useCurrent: true
  });
  $("#datetimepicker6").on("dp.change", function(e) {
      $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
  });
  $("#datetimepicker7").on("dp.change", function(e) {
      $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
  });
  $('#start, #end').datetimepicker({
            useCurrent: true
        });
</script>

<!-- Validation Insert and Update-->
<script>
      $('#deletedate').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });
</script>
