<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="/">


	<title>E-Shuttle</title>
	<link rel="icon" href="<?php echo base_url('assets/images/icons/binusicon.png'); ?>" type="image/png">

	<!-- Bootstrap -->
	<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">

	<!-- <link href="<?php echo base_url('assets/css/bootstrap-material-design.min.css'); ?>" rel="stylesheet"> -->
	<link href="<?php echo base_url('assets/css/bootstrap-treeview.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/ripples.min.css'); ?>" rel="stylesheet">

	<link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">   
	<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/ts.css'); ?>" rel="stylesheet"> <!-- ts -->

	<!-- Font Awesome -->
	<link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="<?php echo base_url('assets/css/custom.min.css'); ?>" rel="stylesheet">
</head>
<body class="nav-md" ng-app="app">

	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a class="site_title">
							<img src="<?php echo base_url('assets/images/binuslogo.png'); ?>" class="w3-circle" alt="Norway" style="
							position:absolute; display: block; max-width: 33%; height: auto;"></a>
						</div>

						<div class="clearfix"></div>

						<!-- menu profile quick info -->
						<div class="profile">
							<div class="profile_pic">
								<img src="<?php echo base_url('assets/images/default-photo.png'); ?>" alt="..." class="img-circle profile_img">
							</div>
							<div class="profile_info">
								<span>Welcome,</span>
								<h2><?php echo strtoupper($this->session->userdata('username')); ?></h2>
							</div>
						</div>
						<!-- /menu profile quick info -->

						<br />

						<!-- sidebar menu -->
						<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
							<div class="menu_section">
								<h3>Menu</h3>
								<ul class="nav side-menu">

									<?php if($this->session->userdata('rolename') == 'admin'){ ?>

									<li <?php if($page == 0) echo 'class="active"'; ?>>
										<a href="<?php echo base_url('dashboard'); ?>"> <i class="fa fa-dashboard"></i> Dashboard </a>
									</li>

									<li>
										<a><i class="fa fa-bullseye"></i> Master <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">

											<li <?php if($page == 2) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('user'); ?>"> <i class="fa fa-users"></i> User List </a>
											</li>
											<li <?php if($page == 4) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('s_term'); ?>"> <i class="fa fa-calendar"></i> Manage Term / Calendar </a>
											</li>

											<li <?php if($page == 5) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('tambahshift'); ?>"> <i class="glyphicon glyphicon-time"></i> Add Shift / Time</a>
											</li>
											<li <?php if($page == 6) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('kendaraan'); ?>"> <i class="fa fa-car"></i>Vehicle</a>
											</li>

											<li <?php if($page == 7) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('driver'); ?>"> <i class="fa fa-id-card-o"></i>Driver</a>
											</li>

											<li <?php if($page == 8) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('campus'); ?>"> <i class="fa fa-university"></i>Campus </a>
											</li>

											<li <?php if($page == 18) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('facilitycontrol'); ?>"> <i class="fa fa-building"></i>Facility Room </a>
											</li>
											<li <?php if($page == 15) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('itemcontrol'); ?>"> <i class="fa fa-keyboard-o"></i>Facility Item </a>
											</li>
											<li <?php if($page == 22) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('role'); ?>"> <i class="fa fa-user"></i> Role </a>
											</li>
											<li <?php if($page == 21) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('weblink'); ?>"> <i class="fa fa-globe glyphicon-user"></i> Weblink </a>
											</li>
										</ul>
									</li>


									<li> 
										<a><i class="fa fa-bullseye"></i> Schedule Configuration <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li <?php if($page == 9) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('cs_master'); ?>"> <i class="fa fa-newspaper-o"></i>
													Configuration Master
												</a>
											</li>
											<li <?php if($page == 10) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('cs_detail'); ?>"> <i class="fa fa-newspaper-o"></i> 
													Configuration Detail 
												</a>
											</li>
										</ul>
									</li>
									<?php } ?>

									<li> 
										<a><i class="fa fa-bus"></i> Booking <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li <?php if($page == 11) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('booking'); ?>"> <i class="fa fa-bus"></i>Booking Date</a>
											</li>
											<li <?php if($page == 17) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('booking/periode'); ?>"> <i class="fa fa-bus"></i>Booking Periode</a>
											</li>
										</ul>
									</li>



									<li> 
										<a><i class="fa fa-print"></i> Detail Booking <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu"> 


											<li <?php if($page == 12) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('detailbooking/bookingperiode'); ?>"> <i class="fa fa-print"></i>Detail Boooking Periode</a>
											</li>


											<li <?php if($page == 12) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('detailbooking'); ?>"> <i class="fa fa-print"></i>Detail Boooking Date</a>
											</li>

										</ul>
									</li>

									<li>
										<a><i class="fa fa-newspaper-o"></i> Notes <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li <?php if($page == 23) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('notes'); ?>"> <i class="fa fa-newspaper-o"></i>
													Notes Report
												</a>
											</li>
											<li <?php if($page == 24) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('listnotes'); ?>"> <i class="fa fa-newspaper-o"></i> 
													List Notes
												</a>
											</li>
											<li <?php if($page == 25) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('category'); ?>"> <i class="fa fa-newspaper-o"></i> 
													Category
												</a>
											</li>
											<li <?php if($page == 26) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('subject'); ?>"> <i class="fa fa-newspaper-o"></i> 
													Sub-Category
												</a>
											</li>
										</ul>
									</li>
									<li>
										<a><i class="fa fa-newspaper-o"></i> Internship <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li <?php if($page == 27) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('takeAttendance'); ?>"> <i class="fa fa-newspaper-o"></i>
													Take Attendance
												</a>
											</li>
											<li <?php if($page == 28) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('attendance'); ?>"> <i class="fa fa-newspaper-o"></i> 
													Attendance
												</a>
											</li>
										</ul>
									</li>
									<li <?php if($page == 16) echo 'class="active"'; ?>>
										<a href="<?php echo base_url('pengumuman'); ?>"> <i class="fa fa-file-picture-o"></i>Pengumuman</a>
									</li>




									<?php if($this->session->userdata('role') == 'member'){ ?>
									<li <?php if($page == 1) echo 'class="active"'; ?>>
										<a href="<?php echo base_url('member'); ?>"> <i class="fa fa-home"></i> Home </a>
									</li>
									<li <?php if($page == 14) echo 'class="active"'; ?>>
										<a href="<?php echo base_url('lecturer'); ?>"> <i class="fa fa-user"></i> Lecturer </a>
									</li>
									<li>
										<li <?php if($page == 13) echo 'class="active"'; ?>>
											<a href="<?php echo base_url('booking'); ?>"> <i class="fa fa-bus"></i>Booking<span class="label label-success pull-right">Coming Soon</span></a>
										</li>
									</li>

									<?php } ?>

								</div>
							</div>

							<!-- /sidebar menu -->
						</div>
					</div>

					<!-- top navigation -->
					<div class="top_nav">
						<div class="nav_menu">
							<nav>
								<div class="nav toggle">
									<a id="menu_toggle"><i class="fa fa-bars"></i></a>
								</div>
								<div class="nav toggle">
									<span><img height="20px" src="<?php echo base_url('assets/images/binus-logo.png'); ?>"></span> 
								</div>
								<ul class="nav navbar-nav navbar-right">
									<li class="">
										<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<img src="<?php echo base_url('assets/images/default-photo.png'); ?>" alt=""><?php echo strtoupper($this->session->userdata('username')); ?>
											<span class=" fa fa-angle-down"></span>
										</a>
										<ul class="dropdown-menu dropdown-usermenu pull-right">
											<li>
												<a data-toggle="modal" data-target="#modalPass">
													<i class="fa fa-key pull-right"></i> Change Local Password
												</a>
											</li>
											<li>
												<a href="<?php echo base_url("auth/logout"); ?>">
													<i class="fa fa-sign-out pull-right"></i> Log Out
												</a>
											</li>
										</ul>
									</li>

									<li role="presentation" class="dropdown" ng-controller="messageController" ng-cloak>
										<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
											<i class="fa fa-envelope-o"></i>
											<span class="badge bg-green">{{latestMessageLen}}</span>
										</a>
										<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
											<li class="message-preview" id="has-no-message">
												<div class="media">
													<div class="media-body">
														<span class="media-heading">
															<p class="text-muted">
																Recently, there are no message reported..
															</p>
														</span>
													</div>
												</div>
											</li>

											<li id="has-message" ng-repeat="row in latestMessage">
												<a href="<?php echo base_url('message'); ?>">
													<span>
														<span>{{row.room_name}}</span>
														<span class="time">{{row.inserted_time}}</span>
													</span>
													<span class="message">
														<table>
															<tr>
																<th>Problem</th>
																<td>:</td>
																<td>{{row.problem_name}}</td>
															</tr>
															<tr>
																<th>Problem Type</th>
																<td>:</td>
																<td>{{row.problem_type}}</td>
															</tr>
															<tr>
																<th>Comment</th>
																<td>:</td>
																<td>{{row.comment | limitTo: 25}}...</td>
															</tr>
														</table>
													</span>
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</nav>
						</div>
					</div>
					<!-- /top navigation -->

					<!-- page content -->
					<div class="right_col" role="main">
						<?php echo $contents; ?>
					</div>

					<!-- footer content -->
					<footer>
						<div class="pull-right">
							Lecture Service Center
						</div>
						<div class="clearfix"></div>
					</footer>
					<!-- /footer content -->
				</div>
			</div>

			<div class="modal fade" id="modalPass" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Change Local Password</h4>
						</div>
						<form role="form" method="post" action="<?php echo base_url('auth/changePass'); ?>">
							<div class="modal-body">
								<div class="form-group label-floating">
									<label for="pass" class="control-label">Password</label>
									<input class="form-control" type="password" name="pass" id="pass" required>
									<span class="help-block">Input new password</span>
								</div>
								<div class="form-group label-floating">
									<label for="confPass" class="control-label">Confirm Password</label>
									<input class="form-control" type="password" id="confPass" required>
									<span class="help-block">Verify you new password</span>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-success" id="btn-update" disabled>Update</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<audio id="notif">
				<source src="<?php echo base_url('assets/music/notify.mp3'); ?>" type="audio/mpeg">
				</audio>
			</body>
			</html>


			<script type="text/javascript" src="<?php echo base_url('assets/js/material.min.js'); ?>"></script>
			<script type="text/javascript" src="<?php echo base_url('assets/js/ripples.min.js'); ?>"></script>
			<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-treeview.min.js'); ?>"></script>
			<script type="text/javascript" src="<?php echo base_url('assets/js/socket.io-client/socket.io.js'); ?>"></script>
			<!-- <script type="text/javascript" src="<?php echo base_url('assets/js/chat.js'); ?>"></script> -->
			<script type="text/javascript" src="<?php echo base_url('assets/js/angular.min.js'); ?>"></script>


			<!-- Custom Theme Scripts -->
			<script src="<?php echo base_url('assets/build/js/custom.min.js'); ?>"></script>

			<!-- ts -->
			<script type="text/javascript">
				var baseurl = '<?php echo base_url("") ?>';
			</script>
			<script type="text/javascript" src="<?php echo base_url('assets/js/angular-route.min.js'); ?>"></script>
			<script type="text/javascript" src="<?php echo base_url('assets/js/angular-sanitize.min.js'); ?>"></script>
			<script type="text/javascript" src="<?php echo base_url('assets/js/ng-content-editable.min.js'); ?>"></script>
			<script type="text/javascript" src="<?php echo base_url('assets/js/ts.js'); ?>"></script>
			<!-- ts -->
			<script type="text/javascript">


				$(document).ready(function() {
					$.material.init();
	//$.chat.init('<?php echo $this->session->userdata("initial"); ?>');

	$('#confPass').keyup(function (e){
		if( $('#confPass').val() == $('#pass').val() ) $('#btn-update').removeAttr('disabled');
		else $('#btn-update').attr('disabled', 'disabled');
	});

	/// TS15-1
	/// Angular patch for unsubmited form through submit button.
	$("form button[type='submit']").on('click',function(){
		$( $(this).parent() .closest('form') ).submit() ;
	// alert("Success");
});
	///

});
</script>