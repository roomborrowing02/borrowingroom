  <!-- Bootstrap -->
<!-- <link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?> rel="stylesheet"> -->
<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"  rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); 
?>" rel="stylesheet">
<!--Selecy2 js-->
<link href="<?php echo base_url('assets/select2/test/dropdown/dropdownCss-tests.js'); ?>" rel="stylesheet"/>

 <style>
  span.desc {
    color: red;
}
</style> 

<div id="page-wrapper">
  <div class="row" style="min-height: 768px;">
  <div class="page-title">
        <div class="title_left">
           <h3>Manage Facility Item <small></small></h3>
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url('index.php/admin'); ?>">Home</a></li>
            <li class="active">Facility Item</li>
          </ul>
        </div>
 

      <div class="title_right"> <!-- Insert-->
        <div class="col-md-10 col-sm-10 col-xs-12 form-group pull-right top_search">
          <a class="btn btn-sm btn-info pull-right"
            data-toggle="modal"
            data-for="insert"
            data-target="#modal_insertItem">Insert
          </a>
        </div>  
      </div>
    </div>
        <div class="clearfix"></div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <p id="selectTriggerFilter"><label><b>Filter Item:</b><br></label></p>
    </div>

<div class="x_panel">
  <div class="x_title">
      <h2><i class="glyphicon glyphicon-list-alt"></i> Item List <small></small></h2>
      <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Item Name</th>
            <th>Specification</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
             <?php foreach($data->result() as $row){ ?>
           <tr>
             <td><?php echo $row->itemname; ?></td>
             <td><?php echo $row->specification; ?></td>
             <td>
               <a class="btn btn-xs btn-warning"
                  data-for="update"
                  data-toggle="modal"
                  data-target="#modal_edit<?php echo $row->itemid; ?>"
                  data-id="<?php echo $row->itemid; ?>">
                  Update</a>
               <a class="btn btn-xs btn-danger"
                  data-toggle="modal" 
                  data-target="#modalDel" 
                  data-id="<?php echo $row->itemid; ?>"
                  data-delete="<?php echo $row->itemname; ?>" 
                  >Delete</a>
             </td>
           </tr>
           <?php } ?>
      </tbody>
      </table>
    </div>
</div>
</div>
</div>

<!-- Modal Insert Facility item-->
<div class="modal fade" id="modal_insertItem"  tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">

  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h3 class="modal-title" id="myModalLabel">Insert Item</h3>
    </div>
      <form role="form" method="post" id="itemInsert" class="form-horizontal form-label-left" action="<?php echo base_url('index.php/itemcontrol/insert') ?>">
        <div class="modal-body">


                 <div class="item form-group" >
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="itemname">Item Name<span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="itemname" id="itemnameModal" placeholder="Must be filled" required type="text"  autocomplete="off">
                      </div>
                      </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="specification">Specification<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="specification" id="specificationModal"  placeholder="Must be filled" required="required"  type="text"  autocomplete="off">
                      </div>
                    </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <input type="submit" class="btn btn-info " id="submitInsert" value="Save">
          <input type="hidden" name="InsertItem" id="InsertId" >
        </div>

      </form>
    </div>
  </div>
</div>

<!-- Modal Update Facility item-->
<?php
        foreach($data->result_array() as $row):
            $itemid=$row['itemid'];
            $itemname=$row['itemname'];
            $specification=$row['specification'];
         ?>
<div class="modal fade" id="modal_edit<?php echo $itemid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 class="modal-title" id="myModalLabel">Edit Item</h3>
  </div>
  <form role="form" method="post"  class="form-horizontal form-label-left itemUpdate" action="<?php echo base_url('itemcontrol/update') ?>">

    <div class="modal-body">
  
         <div class="item form-group" >
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="itemname">Item Name<span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input class="form-control col-md-7 col-xs-12 itemname" data-validate-length-range="6" data-validate-words="2" name="itemname"  placeholder="Must be filled" required="required" type="text"  autocomplete="off" value="<?php echo $itemname;?>"  >
                      </div>
                      </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" 
                      for="specification">Specification<span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input class="form-control col-md-7 col-xs-12 specification" data-validate-length-range="6" data-validate-words="2" name="specification" placeholder="Must be filled" required="required"  type="text"  autocomplete="off"  value="<?php echo $specification;?>" >
                      </div>
                    </div>
    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info submitUpdate" value="Save">
      <input type="hidden" name="UpdateItem"  value="<?php echo $itemid ?>" >
    </div>

  </form>
  </div>
  </div>
</div>
<?php endforeach;?>

<!-- Modal Delete item -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"> Delete Facility Item</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this data ?
            </div>
        <form role="form" method="post" action="<?php echo base_url('index.php/itemcontrol/delete'); ?>">
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="deleteitem">Delete</button>
                <input type="hidden" name="itemid" id="deleteId">
            </div>
        </form>
      </div>
   </div>
  </div>


<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!--Jqueryvalidate-->
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
 <!-- Select2 JS-->
 <script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>

<script>   
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "lengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
      ],
      dom: "Bfrtip",
                    order: [[ 1, "asc" ]],
                    buttons: [
                         {
                             extend: "copy",
                             className: "btn-sm"
                         },
                         {
                             extend: "csv",
                             className: "btn-sm"
                         },
                         {
                             extend: "excel",
                             className: "btn-sm"
                         },
                         {
                             extend: "pdfHtml5",
                             className: "btn-sm"
                         },
                         {
                             extend: "print",
                             className: "btn-sm"
                         },
                    ],
                    responsive: true,
      "deferRender": true,
      "initComplete": function() {
        var column = this.api().column(0);

        var values = [];
        column.data().each(function(d, j) {
          d.split(",").forEach(function(data) {
            data = data.trim();
            
            if (values.indexOf(data) === -1) {
              values.push(data);
            }
          });
        });

        $('<select class="select2_single form-control" tabindex="-1" id="selectTriggerFilter"><option value="">-- Select All --</option></select>')
          .append(values.sort().map(function(o) {
            return '<option value="' + o + '">' + o + '</option>';
          }))
          .on('change', function() {
            column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
          })
          .appendTo('#selectTriggerFilter').select2();
      }
    });

  });
</script>

<!-- Validation Insert and Update-->
<script>
    $('#itemInsert').validate({       
            errorElement: 'span',
            errorClass: 'desc',
          
              rules: {
                
                itemname: {
              required: true,
              minlength:2,
             
          },
          specification: {
              required: true,
              minlength:2,
              
          },
               },
                submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
    });
        
  $('.itemUpdate').each(function () {
    $(this).validate({
      errorElement: 'span',
      errorClass: 'desc',
      rules: {
          itemname: {
              required: true,
             minlength:2,
              maxlength:15,
          },
          specification: {
              required: true,
              minlength:2,
              maxlength:10,
          },
      },
      message: {
          itemname: {
              required: true,
          },
           specification: {
              required: true,
          },
      },
      submitHandler: function(form) {
          alert("Successfull");
        // console.log("asdasdas");
        form.submit();
        }
      });
  });
 
       $('#deleteitem').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });
</script>