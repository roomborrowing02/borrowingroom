  <!-- Bootstrap -->
<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
 <!-- Font Awesome -->
 <link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css'); ?>" rel="stylesheet">
 <!-- Custom Theme Style -->
 <link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>"  rel="stylesheet">
<!-- Switchery -->
<link href="<?php echo base_url('assets/jquery-ui-1.11.4.custom/jquery-ui.theme.css'); ?>" rel="stylesheet"/>

<style>
  .ui-state-holiday .ui-state-default{
      color: white;
      background-color: black;
  }
  span.desc {
      color: red;
  } 
</style>

<!-- form input mask -->
<div class="clearfix"></div>
<div class="col-sm-12 col-xl-12 col-xs-12">
  <div class="x_panel"> 
    <div class="x_title">
      <h2>
        <i class="fa fa-bus"></i> Booking Shuttle Periode<small></small>
      </h2>
      <ul class="nav navbar-right panel_toolbox">
        <li>
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
      <div class="x_content">
        <form class="form-horizontal form-label-left" method="post" id="formbooking" name="firstform" onsubmit='return formValidation()'>
          <div class="col-md-6"><!-- Untuk Bagi Colums-->
                      <div class="form-group termdiv">
                <label class="control-label col-md-3 col-sm-3 col-xs-3">Term</label>
                <div class="col-md-9 col-sm-9 col-xs-9">
                  <select name="termcode" id="termForm" class="form-control">
                    <option id="0" value="">-- Please Select the term --</option>
                    <?php foreach($dataTerm as $row){ ?>
                    <option id="<?php echo $row->termid; ?>" value="<?php echo $row->termcode; ?>"><?php echo $row->termcode;?> <?php echo $row->description;?></option>';
                    <?php } ?>
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-3">Pickup</label>
                <div class="col-md-9 col-sm-9 col-xs-9">
                  <select name="pickup" id="pickupForm" class="form-control" disabled="disabled">
                    <option id="" value=""></option>
                    <?php foreach($dataPickup as $row){ ?>
                    <option id="<?php echo $row->pickupid; ?>" value="<?php echo $row->pickupLoc; ?>"><?php echo $row->pickupLoc; ?></option>';
                    <?php } ?>
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-3">Dropoff</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <select name="dropoff" id="dropoffForm" class="form-control" disabled="disabled">
                </select>
              </div>
            </div>
            <div class="form-group">
            <div id="notif" style="color: red;">
            </div>
            </div>
            <div class="form-group" id="formday">
              <label class="control-label col-md-3 col-sm-3 col-xs-3">Day</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
              <select name="day" class="form-control dayForm" disabled="">
              </select>
            </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-3">Shift</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                  <select name="shift" id="shiftForm" class="form-control" disabled="disabled">
                </select>
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-3">Vehicle</label>
                <div class="col-md-9 col-sm-9 col-xs-9">
                  <select name="vehicle" id="vehicleForm" class="form-control" disabled="disabled">
                  </select>
                </div>
            </div>
            
          </div><!--Tutup md 6-->
          <div class="col-md-6"><!-- Untuk Bagi Colums-->
            <div class="form-group">

                <label class="control-label col-md-3 col-sm-3 col-xs-3">Capacity</label>
                <div class="col-md-9 col-sm-9 col-xs-9">
                  <input type="text" class="form-control" name="capacity" id="capacityForm" autocomplete="off" disabled="disabled">
                  <input type="button" class="btn btn-success" id="formcheck" value="View Capacity">
                </div>
              </div>
            <div class="form-group">
              <div id="notif2" style="color: red;">
                
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-3">Name</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" data-inputmask="'mask': ''" placeholder="Search Name here"  name="name" id="nameForm" autocomplete="off" disabled="disabled">
              <span class="fa fa-id-card-o form-control-feedback right" aria-hidden="true"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-3">E-Mail</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <input type="email" class="form-control" placeholder="@gmail.com" id="emailForm" name="email" autocomplete="off" disabled="disabled">
                <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-3">Phone</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <input type="text" class="form-control" data-inputmask="'mask' : '(999) 999-999999'" id="phoneForm" name="phone" autocomplete="off" disabled="disabled">
                <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-3">Order</label>
                <div class="col-md-9 col-sm-9 col-xs-9">
                  <select class="select2_single form-control" tabindex="-1" id="orderSelect" name="order" onchange="showfield(this.options[this.selectedIndex].value)" disabled="disabled">
                    <option value="0" selected="selected" disabled></option>
                  </select>
                    <div id="div1"></div>
                    <div id="div2"></div>
                    <div id="div3"></div>
                    <div id="div4"></div>
                    <div id="div5"></div>
                    <div id="div6"></div>
                    <div id="div7"></div>
                    <div id="div8"></div>
                    <div id="div9"></div>
                    <div id="div10"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-3">Description</label>
                <div class="col-md-9 col-sm-9 col-xs-9">
                  <input type="text" class="form-control" data-inputmask="'mask': ''" placeholder="Description Must be Filled"  name="description" id="descriptionForm" autocomplete="off" disabled="disabled">
                  <span class="fa fa-id-card-o form-control-feedback right" aria-hidden="true"></span>
                  <input type="hidden" name="shuttleid" id="shuttleidform">

                </div>
              </div>
            </div><!--Tutup md 6-->
          <div class="ln_solid"></div>
          <br>
          <div class="title_right">
            <div class="left_col" role="main" >
              <!-- <a class="btn btn-sm btn-info pull-right">Submit
              </a> -->
              <input class="btn btn-sm btn-info pull-right" name='submit' id="testModal123" value="Submit">
            </div>
          </div>
        </form>
      </div>
  </div>
</div>

<!-- Modal Update/Insert -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
      </div>
      <form class="form-horizontal form-label-left"  method="post" id="formbooking2" action="<?php echo base_url('booking/insertperiode') ?>">
        <div class="col-md-6"><!-- Untuk Bagi Colums-->
          <div class="item form-group modalterm">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Term</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="hidden" name="teco" id="termidModal">
              <input type="text" class="form-control" data-inputmask="'mask': ''" name="termcode" id="termModal" readonly="readonly">
              <span class="fa fa-university form-control-feedback right" aria-hidden="true"></span>
            </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Pickup</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="hidden" name="puid" id="pickupidModal">
              <input type="text" class="form-control" data-inputmask="'mask': ''" name="pickup" id="pickupModal" readonly="readonly">
              <span class="fa fa-university form-control-feedback right" aria-hidden="true"></span>
            </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Dropoff</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="hidden" name="doid" id="dropoffidModal">
              <input type="text" class="form-control" data-inputmask="'mask': ''" name="dropoff" id="dropoffModal" readonly="readonly">
              <span class="fa fa-university form-control-feedback right" aria-hidden="true"></span>
            </div>
          </div>
          <div class="item form-group modalday">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Day</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" data-inputmask="'mask': ''" name="day" id="dayForm" readonly="readonly">
              <span class="fa fa-calendar-o form-control-feedback right" aria-hidden="true"></span>
            </div>
        </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Shift</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="hidden" name="sid" value="shiftid" id="shiftidModal">
              <input type="text" class="form-control" data-inputmask="'mask': ''" name="shift" id="shiftModal" readonly="readonly" >
              <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
            </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Vehicle</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="hidden" name="vid" value="vehicleid" id="vehicleidModal">
              <input type="text" class="form-control" data-inputmask="'mask': ''"  name="vehicle" id="vehicleModal" readonly="readonly">
              <span class="fa fa-automobile form-control-feedback right" aria-hidden="true"></span>
            </div>
          </div>
        
        </div><!--Tutup md 6-->
        <div class="col-md-6"><!-- Untuk Bagi Colums-->
          <div class="item form-group">
              <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Capacity</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" data-inputmask="'mask': ''"  name="capacity" id="capacityModal" readonly="readonly">
              <span class="fa fa-id-card-o form-control-feedback right" aria-hidden="true"></span>
            </div>
          </div>
            <label class="control-label col-md-3 col-sm-3 col-xs-3" id="">Order</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" data-inputmask="'mask': ''"  name="order" id="orderModal" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order1" id="orderModal1" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order2" id="orderModal2" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order3" id="orderModal3" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order4" id="orderModal4" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order5" id="orderModal5" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order6" id="orderModal6" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order7" id="orderModal7" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order8" id="orderModal8" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order9" id="orderModal9" readonly="readonly">
              <input type="hidden" class="form-control" data-inputmask="'mask': ''"  name="order10" id="orderModal10" readonly="readonly">
              <span class="fa fa-group form-control-feedback right" aria-hidden="true"></span>
            </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Name</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" data-inputmask="'mask': ''"  name="name" id="nameModal" readonly="readonly">
              <span class="fa fa-id-card-o form-control-feedback right" aria-hidden="true"></span>
            </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">E-Mail</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" data-inputmask="'mask': ''"  name="email" id="emailModal" readonly="readonly">
              <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
            </div>
          </div>
         <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Phone</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" data-inputmask="'mask': ''" name="phone" id="phoneModal" readonly="readonly">
              <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
            </div>
        </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3">Description</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" data-inputmask="'mask': ''"  name="description" id="descriptionModal" readonly="readonly">
              <span class="fa fa-id-card-o form-control-feedback right" aria-hidden="true"></span>
              <input type="hidden" name="shuttleid" id="shuttleidmodal">

            </div>
          </div>
        </div><!--Tutup md 6-->
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" id="Save" style="bottom: -40px; right: 10px;">Book</button>
          <input type="hidden" name="Bookingid" id="bookingid" >
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="allcapacity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLongTitle">List Capacity</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="pota">
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary closemodal" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/jquery-ui-1.11.4.custom/external/jquery/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/jquery-ui-1.11.4.custom/jquery-ui.js'); ?>"></script>

<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<!-- Switchery -->
<script src="<?php echo base_url('assets/vendors/switchery/dist/switchery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>


<script type="text/javascript">
  function showfield(name){
    if(name== '1'){
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger';
      document.getElementById('div2').innerHTML='';
      document.getElementById('div3').innerHTML='';
      document.getElementById('div4').innerHTML='';
      document.getElementById('div5').innerHTML='';
      document.getElementById('div6').innerHTML='';
      document.getElementById('div7').innerHTML='';
      document.getElementById('div8').innerHTML='';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
    }
    else if (name=='2') {
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger ?';
      document.getElementById('div2').innerHTML='Name: <input type="text" name="order" id="orderInput2"/>';
      document.getElementById('div3').innerHTML='';
      document.getElementById('div4').innerHTML='';
      document.getElementById('div5').innerHTML='';
      document.getElementById('div6').innerHTML='';
      document.getElementById('div7').innerHTML='';
      document.getElementById('div8').innerHTML='';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
    }
    else if (name=='3') {
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger ?';
      document.getElementById('div2').innerHTML='Name: <input type="text" name="order" id="orderInput2"/>';
      document.getElementById('div3').innerHTML='Name: <input type="text" name="order" id="orderInput3"/>';
      document.getElementById('div4').innerHTML='';
      document.getElementById('div5').innerHTML='';
      document.getElementById('div6').innerHTML='';
      document.getElementById('div7').innerHTML='';
      document.getElementById('div8').innerHTML='';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
    }
    else if (name=='4') {
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger ?';    
      document.getElementById('div2').innerHTML='Name: <input type="text" name="order" id="orderInput2"/>';
      document.getElementById('div3').innerHTML='Name: <input type="text" name="order" id="orderInput3"/>';
      document.getElementById('div4').innerHTML='Name: <input type="text" name="order" id="orderInput4"/>';
      document.getElementById('div5').innerHTML='';
      document.getElementById('div6').innerHTML='';
      document.getElementById('div7').innerHTML='';
      document.getElementById('div8').innerHTML='';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
    }  
    else if (name=='5') {
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger ?';
      document.getElementById('div2').innerHTML='Name: <input type="text" name="order" id="orderInput2"/>';
      document.getElementById('div3').innerHTML='Name: <input type="text" name="order" id="orderInput3"/>';
      document.getElementById('div4').innerHTML='Name: <input type="text" name="order" id="orderInput4"/>';
      document.getElementById('div5').innerHTML='Name: <input type="text" name="order" id="orderInput5"/>';
      document.getElementById('div6').innerHTML='';
      document.getElementById('div7').innerHTML='';
      document.getElementById('div8').innerHTML='';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
    }  
    else if (name=='6') {
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger ?';
      document.getElementById('div2').innerHTML='Name: <input type="text" name="order" id="orderInput2"/>';
      document.getElementById('div3').innerHTML='Name: <input type="text" name="order" id="orderInput3"/>';
      document.getElementById('div4').innerHTML='Name: <input type="text" name="order" id="orderInput4"/>';
      document.getElementById('div5').innerHTML='Name: <input type="text" name="order" id="orderInput5"/>';
      document.getElementById('div6').innerHTML='Name: <input type="text" name="order" id="orderInput6"/>';
      document.getElementById('div7').innerHTML='';
      document.getElementById('div8').innerHTML='';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
    }  
    else if (name=='7') {
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger ?';
      document.getElementById('div2').innerHTML='Name: <input type="text" name="order" id="orderInput2"/>';
      document.getElementById('div3').innerHTML='Name: <input type="text" name="order" id="orderInput3"/>';
      document.getElementById('div4').innerHTML='Name: <input type="text" name="order" id="orderInput4"/>';
      document.getElementById('div5').innerHTML='Name: <input type="text" name="order" id="orderInput5"/>';
      document.getElementById('div6').innerHTML='Name: <input type="text" name="order" id="orderInput6"/>';
      document.getElementById('div7').innerHTML='Name: <input type="text" name="order" id="orderInput7"/>';
      document.getElementById('div8').innerHTML='';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
    }  
    else if (name=='8') {
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger ?';
      document.getElementById('div2').innerHTML='Name: <input type="text" name="order" id="orderInput2"/>';
      document.getElementById('div3').innerHTML='Name: <input type="text" name="order" id="orderInput3"/>';
      document.getElementById('div4').innerHTML='Name: <input type="text" name="order" id="orderInput4"/>';
      document.getElementById('div5').innerHTML='Name: <input type="text" name="order" id="orderInput5"/>';
      document.getElementById('div6').innerHTML='Name: <input type="text" name="order" id="orderInput6"/>';
      document.getElementById('div7').innerHTML='Name: <input type="text" name="order" id="orderInput7"/>';
      document.getElementById('div8').innerHTML='Name: <input type="text" name="order" id="orderInput8"/>';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
    }  
    else if (name=='9') {
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger ?';
      document.getElementById('div2').innerHTML='Name: <input type="text" name="order" id="orderInput2"/>';
      document.getElementById('div3').innerHTML='Name: <input type="text" name="order" id="orderInput3"/>';
      document.getElementById('div4').innerHTML='Name: <input type="text" name="order" id="orderInput4"/>';
      document.getElementById('div5').innerHTML='Name: <input type="text" name="order" id="orderInput5"/>';
      document.getElementById('div6').innerHTML='Name: <input type="text" name="order" id="orderInput6"/>';
      document.getElementById('div7').innerHTML='Name: <input type="text" name="order" id="orderInput7"/>';
      document.getElementById('div8').innerHTML='Name: <input type="text" name="order" id="orderInput8"/>';
      document.getElementById('div9').innerHTML='Name: <input type="text" name="order" id="orderInput9"/>';
      document.getElementById('div10').innerHTML='';
    }
    else if (name=='10') {
      document.getElementById('div1').innerHTML='Name: <input type="text" name="order" id="orderInput1"/> <input type="checkbox" id="ordercheck" value="asd"/>I"m also passenger ?';
      document.getElementById('div2').innerHTML='Name: <input type="text" name="order" id="orderInput2"/>';
      document.getElementById('div3').innerHTML='Name: <input type="text" name="order" id="orderInput3"/>';
      document.getElementById('div4').innerHTML='Name: <input type="text" name="order" id="orderInput4"/>';
      document.getElementById('div5').innerHTML='Name: <input type="text" name="order" id="orderInput5"/>';
      document.getElementById('div6').innerHTML='Name: <input type="text" name="order" id="orderInput6"/>';
      document.getElementById('div7').innerHTML='Name: <input type="text" name="order" id="orderInput7"/>';
      document.getElementById('div8').innerHTML='Name: <input type="text" name="order" id="orderInput8"/>';
      document.getElementById('div9').innerHTML='Name: <input type="text" name="order" id="orderInput9"/>';
      document.getElementById('div10').innerHTML='Name: <input type="text" name="order" id="orderInput10"/>';
    }  
    else{
      document.getElementById('div1').innerHTML='';
      document.getElementById('div2').innerHTML='';
      document.getElementById('div3').innerHTML='';
      document.getElementById('div4').innerHTML='';
      document.getElementById('div5').innerHTML='';
      document.getElementById('div6').innerHTML='';
      document.getElementById('div7').innerHTML='';
      document.getElementById('div8').innerHTML='';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
    }
    var test ='';
  $('#ordercheck').click(function () {    
      if($('#ordercheck').is(':checked')){
      $('#orderInput1').val($('#nameForm').val());
      $('#orderInput1').prop('disabled',true);

      }else{
        $('#orderInput1').val('');
         $('#orderInput1').prop('disabled',false);
      }
  });
  }
  $('#orderSelect').click(function() {
    var a = $('#capacityForm').val();
    // alert(a);
    if (a == '1') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").hide();
       $("#orderSelect option[value='3']").hide();
       $("#orderSelect option[value='4']").hide();
       $("#orderSelect option[value='5']").hide();
       $("#orderSelect option[value='6']").hide();
       $("#orderSelect option[value='7']").hide();
       $("#orderSelect option[value='8']").hide();
       $("#orderSelect option[value='9']").hide();
       $("#orderSelect option[value='10']").hide();
    }
    if (a == '2') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").show();
       $("#orderSelect option[value='3']").hide();
       $("#orderSelect option[value='4']").hide();
       $("#orderSelect option[value='5']").hide();
       $("#orderSelect option[value='6']").hide();
       $("#orderSelect option[value='7']").hide();
       $("#orderSelect option[value='8']").hide();
       $("#orderSelect option[value='9']").hide();
       $("#orderSelect option[value='10']").hide();
    }
    if (a == '3') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").show();
       $("#orderSelect option[value='3']").show();
       $("#orderSelect option[value='4']").hide();
       $("#orderSelect option[value='5']").hide();
       $("#orderSelect option[value='6']").hide();
       $("#orderSelect option[value='7']").hide();
       $("#orderSelect option[value='8']").hide();
       $("#orderSelect option[value='9']").hide();
       $("#orderSelect option[value='10']").hide();
    }
    if (a == '4') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").show();
       $("#orderSelect option[value='3']").show();
       $("#orderSelect option[value='4']").show();
       $("#orderSelect option[value='5']").hide();
       $("#orderSelect option[value='6']").hide();
       $("#orderSelect option[value='7']").hide();
       $("#orderSelect option[value='8']").hide();
       $("#orderSelect option[value='9']").hide();
       $("#orderSelect option[value='10']").hide();
    }
    if (a == '5') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").show();
       $("#orderSelect option[value='3']").show();
       $("#orderSelect option[value='4']").show();
       $("#orderSelect option[value='5']").show();
       $("#orderSelect option[value='6']").hide();
       $("#orderSelect option[value='7']").hide();
       $("#orderSelect option[value='8']").hide();
       $("#orderSelect option[value='9']").hide();
       $("#orderSelect option[value='10']").hide();
    }
    if (a == '6') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").show();
       $("#orderSelect option[value='3']").show();
       $("#orderSelect option[value='4']").show();
       $("#orderSelect option[value='5']").show();
       $("#orderSelect option[value='6']").show();
       $("#orderSelect option[value='7']").hide();
       $("#orderSelect option[value='8']").hide();
       $("#orderSelect option[value='9']").hide();
       $("#orderSelect option[value='10']").hide();
    }
    if (a == '7') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").show();
       $("#orderSelect option[value='3']").show();
       $("#orderSelect option[value='4']").show();
       $("#orderSelect option[value='5']").show();
       $("#orderSelect option[value='6']").show();
       $("#orderSelect option[value='7']").show();
       $("#orderSelect option[value='8']").hide();
       $("#orderSelect option[value='9']").hide();
       $("#orderSelect option[value='10']").hide();
    }
    if (a == '8') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").show();
       $("#orderSelect option[value='3']").show();
       $("#orderSelect option[value='4']").show();
       $("#orderSelect option[value='5']").show();
       $("#orderSelect option[value='6']").show();
       $("#orderSelect option[value='7']").show();
       $("#orderSelect option[value='8']").show();
       $("#orderSelect option[value='9']").hide();
       $("#orderSelect option[value='10']").hide();
    }
    if (a == '9') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").show();
       $("#orderSelect option[value='3']").show();
       $("#orderSelect option[value='4']").show();
       $("#orderSelect option[value='5']").show();
       $("#orderSelect option[value='6']").show();
       $("#orderSelect option[value='7']").show();
       $("#orderSelect option[value='8']").show();
       $("#orderSelect option[value='9']").show();
       $("#orderSelect option[value='10']").hide();
    }
    if (a == '10') {
       $("#orderSelect option[value='1']").show();
       $("#orderSelect option[value='2']").show();
       $("#orderSelect option[value='3']").show();
       $("#orderSelect option[value='4']").show();
       $("#orderSelect option[value='5']").show();
       $("#orderSelect option[value='6']").show();
       $("#orderSelect option[value='7']").show();
       $("#orderSelect option[value='8']").show();
       $("#orderSelect option[value='9']").show();
       $("#orderSelect option[value='10']").show();
    }
  })
</script> 

<script>
  //autofilltextfield//
  $(function(){
  $("#nameForm").autocomplete({
    source: "<?php echo base_url();?>booking/get_birds", 
    // path to the get_birds method
    select: function(event,ui){   //do something
    $('#emailForm').val(ui.item.value2);
    $('#phoneForm').val(ui.item.value3);
    }
  });
});
</script>

<script>
  $(document).ready(function() {
    $("#formcheck").hide();
   $("#formcheck").click(function() {
        $.ajax({
          type: 'POST',
          url: "<?php echo base_url(); ?>booking/listcapacity",
          dataType: "json",
          data: {
            pickupdata : $('#pickupForm option:selected').attr('id'),
            dropoffdata : $('#dropoffForm option:selected').attr('id'),
            hari : $('.dayForm').val(),
            tempshift : $('#shiftForm option:selected').attr('id'),
            termdata : $('#termForm option:selected').attr('id'),
            vehicledata : $('#vehicleForm option:selected').attr('id')
          }, // <----send this way
            success: function(data) {
                // console.log("Success",data);
                var yourval = jQuery.parseJSON(JSON.stringify(data));
                
                var table_header = "<table class='table table-striped table-bordered'><thead><tr><td>Date</td><td>Driver</td><td>Vehicle name</td><td>Capacity</td></tr></thead><tbody>";
                   var table_footer = "</tbody></table>";
                var html ="";

               yourval.forEach(function(element) {
              html += "<tr><td>"+element.day+" ("+element.date+")"+"</td><td>"+element.drivername+"</td><td>"+element.vehiclename+" ("+element.vehicleplate+")"+"</td><td>"+element.capacity+"</td></tr>";
            });
                var all = table_header +html+ table_footer;

                $('#pota').html(all);
                $('#allcapacity').modal('show');

            }
        });
      
   });
});
</script>
<!-- Validation -->

<!--Booking-->

<script>
  $('#termForm').change(function(){
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>booking/getdata1",
        dataType: "json",
        data: {
        termdata : $('#termForm option:selected').attr('id')
        }, // <----send this way
          success: function(data) {
              // console.log("Success",data);
              var yourval = jQuery.parseJSON(JSON.stringify(data));
              var pickuphtml="";
              
              if ($('#termForm option:selected').attr('id') == 0) {
                pickuphtml += "<option id =\"\" value =\"\"></option>";
                $('#pickupForm').val('');
                $('#dropoffForm').val('');
                $('.dayForm').val('');
                $('#shiftForm').val('');
                $('#vehicleForm').val('');
                $('#capacityForm').val('');
                $('#nameForm').val('');
                $('#emailForm').val('');
                $('#phoneForm').val('');
                $('#descriptionForm').val('');
                $("#formcheck").hide();
                $('#nameForm').prop('disabled',true);
                $('#emailForm').prop('disabled',true);
                $('#phoneForm').prop('disabled',true);
                $('#orderSelect').prop('disabled',true);
                $('#descriptionForm').prop('disabled',true);
                $('#vehicleForm').prop('disabled',true);
                $('#shiftForm').prop('disabled',true);
                $('.dayForm').prop('disabled',true);
                $('#dropoffForm').prop('disabled',true);
                $('#pickupForm').prop('disabled',true);
                $("#orderSelect option").remove();
                document.getElementById('div1').innerHTML='';
                document.getElementById('div2').innerHTML='';
                document.getElementById('div3').innerHTML='';
                document.getElementById('div4').innerHTML='';
                document.getElementById('div5').innerHTML='';
                document.getElementById('div6').innerHTML='';
                document.getElementById('div7').innerHTML='';
                document.getElementById('div8').innerHTML='';
                document.getElementById('div9').innerHTML='';
                document.getElementById('div10').innerHTML='';
                document.getElementById("notif2").innerHTML ="";

              }else{
                pickuphtml += "<option id =\"\" value =\"\">-- Please select the pickup --</option>";
               for (var i=0 ; i<yourval.length; i++)
               {
                  pickuphtml += "<option id =\""+ yourval[i].campusid +"\">"+yourval[i].campuscode+"</option>";
               }
              }
              $('#pickupForm').html(pickuphtml);  
          }

      });
      $('#pickupForm').prop('disabled',false);

  })

    $('#pickupForm').change(function(){
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>booking/getdata2",
        dataType: "json",
        data: {
        pickupdata : $('#pickupForm option:selected').attr('id'),
        termdata : $('#termForm option:selected').attr('id')
        }, // <----send this way
          success: function(data) {
              // console.log("Success",data);
              var yourval = jQuery.parseJSON(JSON.stringify(data));
              
              var dropoffhtml="";
              if ($('#pickupForm option:selected').attr('id') == 0) {
                dropoffhtml += "<option id =\"\" value =\"\"></option>";
               $('#dropoffForm').prop('disabled',true);
               $('#dropoffForm').val('');
               $('.dayForm').val('');
              $('#shiftForm').val('');
              $('#vehicleForm').val('');
              $('#capacityForm').val('');
              $('#nameForm').val('');
              $('#emailForm').val('');
              $('#phoneForm').val('');
              $('#descriptionForm').val('');
              $("#formcheck").hide();
              $('#nameForm').prop('disabled',true);
              $('#emailForm').prop('disabled',true);
              $('#phoneForm').prop('disabled',true);
              $('#orderSelect').prop('disabled',true);
              $('#descriptionForm').prop('disabled',true);
              $('#vehicleForm').prop('disabled',true);
              $('#shiftForm').prop('disabled',true);
              $('.dayForm').prop('disabled',true);
              $('#dropoffForm').prop('disabled',true);
              $("#orderSelect option").remove();
              document.getElementById('div1').innerHTML='';
              document.getElementById('div2').innerHTML='';
              document.getElementById('div3').innerHTML='';
              document.getElementById('div4').innerHTML='';
              document.getElementById('div5').innerHTML='';
              document.getElementById('div6').innerHTML='';
              document.getElementById('div7').innerHTML='';
              document.getElementById('div8').innerHTML='';
              document.getElementById('div9').innerHTML='';
              document.getElementById('div10').innerHTML='';
              document.getElementById("notif2").innerHTML ="";

              }else{
                dropoffhtml += "<option id =\"0\" value =\"\">-- Please select the dropoff --</option>";
              
               for (var i=0 ; i<yourval.length; i++)
               {
                  dropoffhtml += "<option id =\""+ yourval[i].campusid +"\">"+yourval[i].campuscode+"</option>";
               }
             }
              $('#dropoffForm').html(dropoffhtml);  
          }

      });
      $('#dropoffForm').prop('disabled',false);

  })
  $('#dropoffForm').change(function(){
    $.ajax({
     type: "post",
     url: "<?php echo base_url(); ?>booking/getdata3",
     dataType: "json",
     data: {
      pickupdata : $('#pickupForm option:selected').attr('id'),
      dropoffdata : $('#dropoffForm option:selected').attr('id'),
      termdata : $('#termForm option:selected').attr('id')
      },
     success: function (data){
      var dayhtml="";
      if ($('#dropoffForm option:selected').attr('id') == 0) {
          dayhtml += "<option id =\"\" value =\"\"></option>";
          $('.dayForm').val('');
          $('#shiftForm').val('');
          $('#vehicleForm').val('');
          $('#capacityForm').val('');
          $('#nameForm').val('');
          $('#emailForm').val('');
          $('#phoneForm').val('');
          $('#descriptionForm').val('');
          $("#formcheck").hide();
          $('#nameForm').prop('disabled',true);
          $('#emailForm').prop('disabled',true);
          $('#phoneForm').prop('disabled',true);
          $('#orderSelect').prop('disabled',true);
          $('#descriptionForm').prop('disabled',true);
          $('#vehicleForm').prop('disabled',true);
          $('#shiftForm').prop('disabled',true);
          $('.dayForm').prop('disabled',true);
          $("#orderSelect option").remove();
          document.getElementById('div1').innerHTML='';
          document.getElementById('div2').innerHTML='';
          document.getElementById('div3').innerHTML='';
          document.getElementById('div4').innerHTML='';
          document.getElementById('div5').innerHTML='';
          document.getElementById('div6').innerHTML='';
          document.getElementById('div7').innerHTML='';
          document.getElementById('div8').innerHTML='';
          document.getElementById('div9').innerHTML='';
          document.getElementById('div10').innerHTML='';
          document.getElementById("notif2").innerHTML ="";
        }else{
          if(data.length == 0){
            // console.log("kosong");
            document.getElementById("notif").innerHTML = "No Schedule in this month";
            $('.dayForm').prop('disabled',true);
            $('#shiftForm').prop('disabled',true);
            $('#capacityForm').prop('disabled',true);
            $('#vehicleForm').prop('disabled',true);
            $('#nameForm').prop('disabled',true);
            $('#emailForm').prop('disabled',true);
            $('#phoneForm').prop('disabled',true);
            $('#orderSelect').prop('disabled',true);
            $('#descriptionForm').prop('disabled',true);
            $('.dayForm').val(""); $('#shiftForm').val("");
            $('#vehicleForm').val(""); $('#capacityForm').val("");
          }else{
            document.getElementById("notif").innerHTML ="";
            var yourval = jQuery.parseJSON(JSON.stringify(data));
            dayhtml += "<option id =\"0\" value =\"\">-- Please select the day --</option>";
            
             for (var i=0 ; i<yourval.length; i++)
             {
                dayhtml += "<option value =\""+ yourval[i].day +"\">"+yourval[i].day+"</option>";
             }
          }  
        }
      $('.dayForm').html(dayhtml);
     }  
    })
    $('.dayForm').prop('disabled',false);  
  })

  $('.dayForm').click(function(){
    $.ajax({
     type: "post",
     url: "<?php echo base_url(); ?>booking/getdata4",
     dataType: "json",
     data: {  
        pickupdata : $('#pickupForm option:selected').attr('id'),
        dropoffdata : $('#dropoffForm option:selected').attr('id'),
        termdata : $('#termForm option:selected').attr('id'),
        hari : $('.dayForm').val()
      },
      success: function (data){
        var yourval = jQuery.parseJSON(JSON.stringify(data));
        var shifthtml="";
        if ($('.dayForm option:selected').attr('id') == 0) {
          shifthtml += "<option id =\"\" value =\"\"></option>";
          $('#shiftForm').val('');
          $('#vehicleForm').val('');
          $('#capacityForm').val('');
          $('#nameForm').val('');
          $('#emailForm').val('');
          $('#phoneForm').val('');
          $('#descriptionForm').val('');
          $("#formcheck").hide();
          $('#nameForm').prop('disabled',true);
          $('#emailForm').prop('disabled',true);
          $('#phoneForm').prop('disabled',true);
          $('#orderSelect').prop('disabled',true);
          $('#descriptionForm').prop('disabled',true);
          $('#vehicleForm').prop('disabled',true);
          $('#shiftForm').prop('disabled',true);
          $("#orderSelect option").remove();
          document.getElementById('div1').innerHTML='';
          document.getElementById('div2').innerHTML='';
          document.getElementById('div3').innerHTML='';
          document.getElementById('div4').innerHTML='';
          document.getElementById('div5').innerHTML='';
          document.getElementById('div6').innerHTML='';
          document.getElementById('div7').innerHTML='';
          document.getElementById('div8').innerHTML='';
          document.getElementById('div9').innerHTML='';
          document.getElementById('div10').innerHTML='';
          document.getElementById("notif2").innerHTML ="";
        }else{
            shifthtml += "<option id =\"0\" value =\"\">-- Please select the shift --</option>";

           for (var i=0 ; i<yourval.length; i++)
           {
              shifthtml += "<option id =\""+ yourval[i].shiftid +"\">"+yourval[i].shift+"</option>";
           }
         }
          $('#shiftForm').html(shifthtml);
        }
    })
      $('#shiftForm').prop('disabled',false);

  })

  $('#shiftForm').change(function(){
    $.ajax({
     type: "post",
     url: "<?php echo base_url(); ?>booking/getdata5",
     dataType: "json",
     data: {
        pickupdata : $('#pickupForm option:selected').attr('id'),
        dropoffdata : $('#dropoffForm option:selected').attr('id'),
        hari : $('.dayForm').val(),
        tempshift : $('#shiftForm option:selected').attr('id'),
        termdata : $('#termForm option:selected').attr('id')
      },
        success: function (data){
        var yourval = jQuery.parseJSON(JSON.stringify(data));                
        var vehiclehtml="";
        var capacityhtml="";
       if ($('#shiftForm option:selected').attr('id') == 0) {
          vehiclehtml += "<option id =\"\" value =\"\"></option>";
          $('#vehicleForm').val('');
          $('#capacityForm').val('');
          $('#nameForm').val('');
          $('#emailForm').val('');
          $('#phoneForm').val('');
          $('#descriptionForm').val('');
          $("#formcheck").hide();
          $('#nameForm').prop('disabled',true);
          $('#emailForm').prop('disabled',true);
          $('#phoneForm').prop('disabled',true);
          $('#orderSelect').prop('disabled',true);
          $('#descriptionForm').prop('disabled',true);
          $('#vehicleForm').prop('disabled',true);
          $("#orderSelect option").remove();
          document.getElementById('div1').innerHTML='';
          document.getElementById('div2').innerHTML='';
          document.getElementById('div3').innerHTML='';
          document.getElementById('div4').innerHTML='';
          document.getElementById('div5').innerHTML='';
          document.getElementById('div6').innerHTML='';
          document.getElementById('div7').innerHTML='';
          document.getElementById('div8').innerHTML='';
          document.getElementById('div9').innerHTML='';
          document.getElementById('div10').innerHTML='';
          document.getElementById("notif2").innerHTML ="";
        }else{
            vehiclehtml += "<option id =\"0\" value =\"\">-- Please select the vehicle --</option>";
           for (var i=0 ; i<yourval.length; i++)
           {
              vehiclehtml += "<option id =\""+ yourval[i].vehicleid +"\">"+yourval[i].vehiclename+"   "+yourval[i].vehicleplate+"</option>";
           }
        }
          $('#vehicleForm').html(vehiclehtml);

      }
    })
    $('#vehicleForm').prop('disabled',false);
    $('#nameForm').prop('disabled',true);
    $('#emailForm').prop('disabled',true);
    $('#phoneForm').prop('disabled',true);
    $('#orderSelect').prop('disabled',true);
    $('#descriptionForm').prop('disabled',true);
  })

  $('#vehicleForm').change(function(e){
      e.preventDefault();
      $("#orderSelect option").remove();
      $('#orderSelect').append($("<option value='0'>-- Please choose the order --</option>"));
    if ($('#vehicleForm option:selected').attr('id') == 0) {
      $('#capacityForm').val('');
      $('#nameForm').val('');
      $('#emailForm').val('');
      $('#phoneForm').val('');
      $('#descriptionForm').val('');
      $("#formcheck").hide();
      $('#nameForm').prop('disabled',true);
      $('#emailForm').prop('disabled',true);
      $('#phoneForm').prop('disabled',true);
      $('#orderSelect').prop('disabled',true);
      $('#descriptionForm').prop('disabled',true);
      document.getElementById('div1').innerHTML='';
      document.getElementById('div2').innerHTML='';
      document.getElementById('div3').innerHTML='';
      document.getElementById('div4').innerHTML='';
      document.getElementById('div5').innerHTML='';
      document.getElementById('div6').innerHTML='';
      document.getElementById('div7').innerHTML='';
      document.getElementById('div8').innerHTML='';
      document.getElementById('div9').innerHTML='';
      document.getElementById('div10').innerHTML='';
      document.getElementById("notif2").innerHTML ="";
    }else{
      $.ajax({
       type: "post",
       url: "<?php echo base_url(); ?>booking/getdata6",
       dataType: "json",
       data: {
          pickupdata : $('#pickupForm option:selected').attr('id'),
          dropoffdata : $('#dropoffForm option:selected').attr('id'),
          hari : $('.dayForm').val(),
          tempshift : $('#shiftForm option:selected').attr('id'),
          termdata : $('#termForm option:selected').attr('id'),
          vehicledata : $('#vehicleForm option:selected').attr('id')
        },
        success: function (data){
          var yourval = jQuery.parseJSON(JSON.stringify(data));
          var valdata = yourval[0] < 10 ? yourval[0] : 10;
          for (var a = 1 ; a<valdata+1; a++) {
            var b = $("<option value='"+a+"'>"+a+"</option>");
            $('#orderSelect').append(b);
          }

          var capa ="";
           for (var i=0 ; i<yourval.length; i++)
           {
              capa = yourval[i];
           }
            if(capa == 0){
              document.getElementById("notif2").innerHTML = "Full Capacity";
              $("#formcheck").show();
              $('#nameForm').prop('disabled',true);
              $('#emailForm').prop('disabled',true);
              $('#phoneForm').prop('disabled',true);
              $('#orderSelect').prop('disabled',true);
              $('#descriptionForm').prop('disabled',true);
            }else{
              document.getElementById("notif2").innerHTML ="";

              <?php if($this->session->userdata('role') == 'admin'){ ?>
              $('#nameForm').prop('disabled',false);
              $('#emailForm').prop('disabled',false);
              $('#phoneForm').prop('disabled',false);
              $('#orderSelect').prop('disabled',false);
              $('#descriptionForm').prop('disabled',false);
              $("#formcheck").show();

              <?php } elseif ($this->session->userdata('role') == 'member') { ?>
              $('#orderSelect').prop('disabled',false);
              $('#descriptionForm').prop('disabled',false);
              $("#formcheck").show();
                $.ajax({
                 type: "post",
                 url: "<?php echo base_url(); ?>booking/get_datauser",
                 dataType: "json",
                 data: {
                    datauser : <?php echo $this->session->userdata('userid');?>
                  },
                  success: function (data){
                    var yourval = jQuery.parseJSON(JSON.stringify(data));
                     for (var i=0 ; i<yourval.length; i++)
                     {
                      var n = yourval[i].nama;
                      var e = yourval[i].email;
                      var p = yourval[i].phone;
                     }
                      $('#nameForm').val(n);
                      $('#emailForm').val(e);
                      $('#phoneForm').val(p);
                  }
                })
              <?php } ?>
            }
            $('#capacityForm').val(capa);
        }
      })
    }
      
  })  
</script>

<script>
  function formValidation(){
    var term = document.forms["firstform"]["termcode"].value;
    var pickup = document.forms["firstform"]["pickup"].value;
    var dropoff = document.forms["firstform"]["dropoff"].value;
    var shift = document.forms["firstform"]["shift"].value;
    var vehicle = document.forms["firstform"]["vehicle"].value;
    var capacity = document.forms["firstform"]["capacity"].value;
    var name = document.forms["firstform"]["name"].value;
    var email = document.forms["firstform"]["email"].value;
    var phone = document.forms["firstform"]["phone"].value;
    var order = document.forms["firstform"]["order"].value;
    var description = document.forms["firstform"]["description"].value;
    var order = $('#orderSelect option:selected').val();
      if (term == "") {
          alert("Term must be filled out");
          return false;
      }
      if (pickup == "") {
          alert("Pickup must be filled out");
          return false;
      }
      if (dropoff == "") {
          alert("Dropoff must be filled out");
          return false;
      }
      if (shift == "") {
          alert("Shift must be filled out");
          return false;
      }
      if (vehicle == "") {
          alert("Vehicle must be filled out");
          return false;
      }
      if (capacity == "" || capacity == "0") {
          alert("Full Capacity");
          return false;
      }
      if (name == "") {
          alert("Name must be filled out");
          return false;
      }
      if (email == "") {
          alert("Email must be filled out");
          return false;
      }
      if (phone == "") {
          alert("Phone must be filled out");
          return false;
      }
      if (order == "" ||order == "0") {
          alert("Order must be filled out");
          return false;
      }
      if (description == "") {
          alert("Description must be filled out");
          return false;
      }
      return true;
  }
</script>

<!-- <script>
  //untuk passing data ke modal//
  $('#testModal123').click(function(){
    $('#modal').modal('show');
    $('#orderModal').val($('#orderSelect').val());
    $('#orderModal1').val($('#orderInput1').val());
    $('#orderModal2').val($('#orderInput2').val());
    $('#orderModal3').val($('#orderInput3').val());
    $('#orderModal4').val($('#orderInput4').val());
    $('#orderModal5').val($('#orderInput5').val());
    $('#orderModal6').val($('#orderInput6').val());
    $('#orderModal7').val($('#orderInput7').val());
    $('#orderModal8').val($('#orderInput8').val());
    $('#orderModal9').val($('#orderInput9').val());
    $('#orderModal10').val($('#orderInput10').val());

    $('#dateModal').val($('#dateForm123').val());
    $('#dayForm').val($('.dayForm').val());
    $('#shiftModal').val($('#shiftForm').val());
    $('#shiftidModal').val($('#shiftFormid').val());
    $('#nameModal').val($('#nameForm').val());
    $('#emailModal').val($('#emailForm').val());
    $('#phoneModal').val($('#phoneForm').val());
    $('#descriptionModal').val($('#descriptionForm').val());
    $('#pickupModal').val($('#pickupForm').val());
    $('#dropoffModal').val($('#dropoffForm').val());
    $('#termModal').val($('#termForm').val());
    $('#vehicleModal').val($('#vehicleForm').val());
    $('#capacityModal').val($('#capacityForm').val());
    $("button").click(function(){
      $('#pickupidModal').val($('#pickupForm option:selected').attr("id"));
      $('#dropoffidModal').val($('#dropoffForm option:selected').attr("id"));
      $('#shiftidModal').val($('#shiftForm option:selected').attr("id"));
      $('#vehicleidModal').val($('#vehicleForm option:selected').attr("id"));
      $('#termidModal').val($('#termForm option:selected').attr("id"));
   });

  });
</script> -->
<script>

    $('#testModal123').click(function(e) {
      /* Act on the event */
      e.preventDefault();
      if (formValidation()) {

        $.ajax({
          type: 'POST',
          url: "<?php echo base_url(); ?>booking/getdata7",
          dataType: "json",
          data: {
          pickupdata : $('#pickupForm option:selected').attr('id'),
          dropoffdata : $('#dropoffForm option:selected').attr('id'),
          hari : $('.dayForm').val(),
          tempshift : $('#shiftForm option:selected').attr('id'),
          termdata : $('#termForm option:selected').attr('id'),
          vehicledata : $('#vehicleForm option:selected').attr('id')
          }, // <----send this way
            success: function(data) {
                var yourval = jQuery.parseJSON(JSON.stringify(data));
            var shuttlehtml="";
                
                 for (var i=0 ; i<yourval.length; i++)
                 {
                    var shuttlehtml = yourval[i].headershuttleid;
                 }
                $('#shuttleidform').val(shuttlehtml);
            }
            });

          $('#modal').modal('show');
           $('#orderModal').val($('#orderSelect').val());
          $('#orderModal1').val($('#orderInput1').val());
          $('#orderModal2').val($('#orderInput2').val());
          $('#orderModal3').val($('#orderInput3').val());
          $('#orderModal4').val($('#orderInput4').val());
          $('#orderModal5').val($('#orderInput5').val());
          $('#orderModal6').val($('#orderInput6').val());
          $('#orderModal7').val($('#orderInput7').val());
          $('#orderModal8').val($('#orderInput8').val());
          $('#orderModal9').val($('#orderInput9').val());
          $('#orderModal10').val($('#orderInput10').val());

          $('#dateModal').val($('#dateForm123').val());
          $('#dayForm').val($('.dayForm').val());
          $('#shiftModal').val($('#shiftForm').val());
          $('#shiftidModal').val($('#shiftFormid').val());
          $('#nameModal').val($('#nameForm').val());
          $('#emailModal').val($('#emailForm').val());
          $('#phoneModal').val($('#phoneForm').val());
          $('#descriptionModal').val($('#descriptionForm').val());
          $('#pickupModal').val($('#pickupForm').val());
          $('#dropoffModal').val($('#dropoffForm').val());
          $('#termModal').val($('#termForm').val());
          $('#vehicleModal').val($('#vehicleForm').val());
          $('#capacityModal').val($('#capacityForm').val());
          $("button").click(function(){
            $('#pickupidModal').val($('#pickupForm option:selected').attr("id"));
            $('#dropoffidModal').val($('#dropoffForm option:selected').attr("id"));
            $('#shiftidModal').val($('#shiftForm option:selected').attr("id"));
            $('#vehicleidModal').val($('#vehicleForm option:selected').attr("id"));
            $('#termidModal').val($('#termForm option:selected').attr("id"));
            $('#shuttleidmodal').val($('#shuttleidform').val());
         });
        }
    });

</script> 