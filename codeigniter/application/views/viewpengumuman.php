<link href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/css/select2.min.css'); ?>" rel="stylesheet">


<style>
  span.desc {
    color: red;
}
</style>

<div id="page-wrapper">
  <div class="row" style="min-height: 768px">
    <div class="page-title">
      <div class="title_left">
        <h3>Master Pengumuman <small></small></h3>
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
            <li class="active">Pengumuman</li>
          </ul>
          <p id="selectTriggerFilter"><label><b>Filter Status:</b></label><br></p>         
      </div>
      <div class="title_right">
        <div class="left_col" role="main" >
          <a class="btn btn-sm btn-info pull-right" 
          href="<?php echo base_url('pengumuman/upload'); ?>">Upload <i class="fa fa-upload"></i>
          </a>
          <a class="pull-right">&nbsp;&nbsp;&nbsp;</a>
          <a href="<?php echo base_url('pengumuman/fullScreen'); ?>" class="btn btn-sm btn-info pull-right" target="_blank"><i class="fa fa-arrows-alt"></i> FullScreen</a> 
        </div>
      </div>
      <?php if($data->num_rows() > 0){ ?>
        <table id="table_sort" class="cell-border" style="width:100%">
          <thead>
            <tr>
              <th>ID</th>
              <th>Images</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Description</th>
              <th>Last Update</th>
              <th>Last Insert</th>
              <th>Created By</th>
              <th>Status</th>
              <th>Act</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($data->result() as $row){ ?>
            <tr>
              <td>ANN - <?php echo $row->pengumumanid ?></td>
              <td>
                  <a href="#">
                <img style="width: 100px; height: 75px;" id="<?php echo $row->pengumumanid; ?>" data-toggle="modal" data-target="#myModal" src="<?php echo base_url()?>assets/images/upload/<?php echo $row->content;?>" alt='Text dollar code part one.' onerror="this.src='<?php echo base_url()?>assets/images/error/noimages.jpg';"/>
                 </a>
                
              </td>
              <!-- <td>a</td> -->
              <td><?php echo date('d-M-Y',strtotime($row->startdate)); ?></td>
              <td><?php echo date('d-M-Y',strtotime($row->enddate)); ?></td>
              <td><?php echo $row->description; ?></td>
              <td><?php echo date('d-M-Y h:i A',strtotime($row->lastupdate)); ?></td>
              <td><?php echo date('d-M-Y h:i A',strtotime($row->lastinsert)); ?></td>
              <td><?php echo $row->createdby; ?></td>
              <?php if ($row->startdate <= $date && $date <= $row->enddate): ?>
                  <td>Active</td>
                <?php elseif($date >= $row->enddate): ?>
                  <td>Expired</td>
                <?php elseif($date <= $row->startdate && $date <= $row->enddate): ?>
                 <td>On Future</td>
              <?php endif; ?>
              <td>
                <a class="btn btn-xs btn-warning"
                data-for="update"
                data-toggle="modal"
                data-target="#modal_edit<?php echo $row->pengumumanid; ?>"
                data-ajax="<?php echo base_url('pengumuman/getData') ?>"
                data-id="<?php echo $row->pengumumanid; ?>"
                >Update</a>
              <a class="btn btn-xs btn-danger" 
                data-toggle="modal" 
                data-target="#modalDel<?php echo $row->pengumumanid; ?>" 
                data-id="<?php echo $row->pengumumanid; ?>">Delete</a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    <?php } else { ?>
            <div class="well">There is no Images created yet!</div>
      <?php } ?>
  </div>
</div>


<!-- Modal thumbnail -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <img style="width: 100%; height: 100%;" class="showimage img-responsive" src="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update-->
<?php
foreach($data->result_array() as $row):
    $pengumumanid=$row['pengumumanid'];
    $startdate=$row['startdate'];
    $enddate=$row['enddate'];
    $description=$row['description'];
    $content=$row['content'];
 ?>   
<div class="modal fade bd-example-modal-lg" id="modal_edit<?php echo $pengumumanid;?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Manage Pengumuman</h4>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left formupdate" enctype="multipart/form-data" action="<?php echo base_url('pengumuman/update') ?>" runat="server" onsubmit='return formValidation()' name="firstform">
                <div class="modal-body">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="startdate">Start Date
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <div class="input-group" id="start">
                            <input class="form-control" placeholder="Start" name="startdate" value="<?php echo $startdate ?>" />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enddate">End Date 
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="input-group" id="end">
                            <input class="form-control" placeholder="End" name="enddate" value="<?php echo $enddate ?>" />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Select File</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type='file' class="inputFile" name="file_name" />
                            <img class="imagepreview" src="<?php echo base_url()?>assets/images/upload/<?php echo $content ?>" style="width: 300px;" alt="your image" />
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <textarea name="description" class="some-textarea validate[required]" cols="30" rows="10"><?php echo $description ?></textarea>
                        </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                     <input type="hidden" name="Pengumumanid" value="<?php echo $pengumumanid ?>">
                     <input type="submit" class="btm btn-primary pull-right buttonupdate">
                   </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<?php
foreach($data->result_array() as $row):
    $pengumumanid=$row['pengumumanid'];
    $content=$row['content'];
    $description=$row['description'];
 ?>   
<div class="modal fade" id="modalDel<?php echo $pengumumanid; ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Pengumuman</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this ? <?php echo $description; ?>
                <img class="imagepreview" src="<?php echo base_url()?>assets/images/upload/<?php echo $content ?>" style="width: 100%; height: 100%;" alt="your image" />
            </div>
            <form role="form" method="post" action="<?php echo base_url('pengumuman/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger deletepengumuman">Delete</button>
                    <input type="hidden" name="DeleteId" value="<?php echo $pengumumanid ?>">
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>

<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
<!-- <script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script> -->
<script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/nprogress/nprogress.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.js'); ?>"></script>
 <script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>

<script type="text/javascript">
  tinymce.init({
              selector: "textarea",
              plugins: [
                  "advlist autolink lists link image charmap print preview anchor",
                  "searchreplace visualblocks code fullscreen",
                  "insertdatetime media table contextmenu paste"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
          });
</script>
<script>
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
              $('.imagepreview').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
      }
  }
  $(".inputFile").change(function () {
      readURL(this);
  });
</script>
<script>
  $('#start, #end').datetimepicker({
            useCurrent: false,
            format: "YYYY-MM-DD"
        });
  $('#start').datetimepicker().on('dp.change', function (e) {
            var incrementDay = moment(new Date(e.date));
            incrementDay.add(1, 'days');
            $('#end').data('DateTimePicker').minDate(incrementDay);
            $(this).data("DateTimePicker").hide();
        });

        $('#end').datetimepicker().on('dp.change', function (e) {
            var decrementDay = moment(new Date(e.date));
            decrementDay.subtract(1, 'days');
            $('#start').data('DateTimePicker').maxDate(decrementDay);
             $(this).data("DateTimePicker").hide();
        });
      </script>
<script>
  $(document).ready(function () {
      $('img').on('click', function () {
          var image = $(this).attr('src');
          //alert(image);
          $('#myModal').on('show.bs.modal', function () {
              $(".showimage").attr("src", image);
          });
      });
  });
</script>

<script>
    $(document).ready(function() {
      $('#table_sort').DataTable({
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "deferRender": true,
        "initComplete": function() {
          var column = this.api().column(8);

          var values = [];
          column.data().each(function(d, j) {
            d.split(",").forEach(function(data) {
              data = data.trim();
              
              if (values.indexOf(data) === -1) {
                values.push(data);
              }
            });
          });

          $('<select class="select2_single form-control" style="width: 100%;" tabindex="-1" id="selectTriggerFilter"><option value="">-- Select All --</option></select>')
            .append(values.sort().map(function(o) {
              return '<option value="' + o + '">' + o + '</option>';
            }))
            .on('change', function() {
              column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
            })
            .appendTo('#selectTriggerFilter').select2();
        }
      });
    });
</script>

<script>
  function formValidation(){
    var startdate = document.getElementsByName("startdate");
    var enddate = document.getElementsByName("enddate");

    var isValid = true;
    for (var i = 0; i < startdate.length; i++) {
      if (startdate[i].value == "") {
        alert("Start Date Must be filled out");
        // return false;
        isValid = false;

      }
    }

    for (var i = 0; i < enddate.length; i++) {
      if (enddate[i].value == "") {
        alert("End Date Must be filled out");
        // return false;
        isValid = false;

      }
    }
    
      var editorContent = tinyMCE.activeEditor.getContent();   
      if (editorContent == '' || editorContent == null) {
        alert("Description Must be filled out");
        isValid = false;
      } 


    if (!isValid) {
      alert('Submit Fail');
      return false;
    }
    alert('Data Updated');
    return isValid;
  }


 $('.deletepengumuman').click(function() {
   /* Act on the event */
   alert("Successfull");
 });
</script>