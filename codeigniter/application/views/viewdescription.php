<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>" rel="stylesheet">

<style>
  span.desc {
    color: red;
}
</style>


<div id="page-wrapper">
  <div class="row" style="min-height: 768px">
    <div class="page-title">
      <div class="title_left">
        <h3>Detail Facility <small></small></h3>
          <ul class="breadcrumb">
	        <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
	        <li><a href="<?php echo base_url('facilitycontrol'); ?>">Facility Room</a></li>
	        <li class="active">Upload</li>
	      </ul>    	    
      </div>
      <div class="title_right">
      </div>	
		<div class="clearfix"></div>
		<div class="col-sm-12 col-xl-12 col-xs-12">
			<div class="x_panel"> 
				<!-- <div class="x_content"> -->


            <form class="form-horizontal form-label-left" action="<?php echo base_url('facilitycontrol/submitupload') ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8" id="submitupload" name="firstform" onsubmit="return formValidation();">          
              <?php foreach ($tempimg as $img): ?>
            
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Image">Image <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <a href="">
                    <img style="width: 150px; height: 150px;" data-toggle="modal" data-target="#myModal" src="<?php echo base_url()?>assets/images/facility/<?php echo str_replace(' ','', $img);?>" alt='Text dollar code part one.' />
                    <input type="hidden" class="form-control" name="tempgambar[]" value="<?php echo str_replace(' ','', $img); ?>">
                  </a>
              </div>
            </div>
             
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Description<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea name="description[]" class="some-textarea" cols="10" rows="10"></textarea>
                </div>
                   <?php foreach($room->result() as $row){ ?>
                      <input type="hidden" name="faid[]" value="<?php echo $row->facilityid;?>">
                    <?php } ?>
              </div>
                        <?php endforeach ?>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <input type="submit" class="btm btn-primary pull-right" id="buttonupload" value="Upload">
              <input type="hidden" name="facility[]" value="<?php echo $row->facilityid;?>">
              </div>
            </div>
          </form>
            <div class="ln_solid"></div>

				</div>
			</div>
		</div>
	</div>
  </div>

<!-- Modal thumbnail -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img style="width: 100%; height: 100%;" class="showimage img-responsive" src="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/fastclick/lib/fastclick.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/nprogress/nprogress.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.js'); ?>"></script>

<!-- script untuk description -->
<script type="text/javascript">
  tinymce.init({
              selector: "textarea",
              plugins: [
                  "advlist autolink lists link image charmap print preview anchor",
                  "searchreplace visualblocks code fullscreen",
                  "insertdatetime media table contextmenu paste"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
          });
</script>

<script>
  $(document).ready(function () {
      $('img').on('click', function () {
          var image = $(this).attr('src');
          //alert(image);
          $('#myModal').on('show.bs.modal', function () {
              $(".showimage").attr("src", image);
          });
      });
  });
</script>

<script>
  function formValidation(){
    var isValid = true;
 
    for (var i = 0; i < tinymce.editors.length; i++) {
      var content = tinymce.editors[i].getContent();
      if (content == "" || content == null) {
        alert("Description Must be filled out");
        isValid = false;
      } 
    }


    if (!isValid) {
      alert('Submit Fail');
      return false;
    }
    alert('Data Inserted');
    return isValid;
  }
</script>