<link href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.css'); ?>" rel="stylesheet">
<style>
    span.desc {
    color: red;
}
</style>


<div id="page-wrapper">
    <div class="row" style="min-height: 768px">
        <div class="page-title">
            <div class="title_left">
                <h3>Manage Category <small></small></h3>
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
                    <li class="active">Category</li>
                </ul>
            </div>
            <div class="title_right">
                <div class="left_col" role="main">
                    <a class="btn btn-sm btn-info pull-right" data-toggle="modal" data-for="insert" data-target="#modal_insertshift">Insert
                    </a>
                </div>
            </div>
        </div>
        <div class="x_panel">
            <div class="x_content">
                <?php if ($data->num_rows() > 0) { ?>
                <table id="table_sort" class="cell-border" style="width:100%">
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th>Act</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data->result() as $row) { ?>
                        <tr>
                            <td>
                                <?php echo $row->category; ?>
                            </td>
                            <td>
                                <button class="btn btn-xs btn-info" value="<?php echo $row->categoryid; ?>" onclick='showlist(this)'>Show Sub-Category</button>
                                <a class="btn btn-xs btn-warning" data-for="update" data-toggle="modal" data-target="#modal_edit<?php echo $row->categoryid; ?>" data-ajax="<?php echo base_url('category/getData') ?>" data-id="<?php echo $row->categoryid; ?>">Update</a>||
                                <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modalDel" data-id="<?php echo $row->categoryid; ?>" data-delete="<?php echo $row->category; ?>">Delete</a>
                            </td>
                        </tr>
                        <?php 
                      } ?>
                    </tbody>
                </table>
                <?php 
              } else { ?>
                <div class="well">There is no Category created yet!</div>
                <?php 
              } ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Insert -->
<div class="modal fade" id="modal_insertshift" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Insert Category</h3>
            </div>
            <form role="form" method="post" id="forminsert" class="form-horizontal form-label-left" action="<?php echo base_url('category/insert') ?>">
                <div class="modal-body">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" name="category" required="required" type="text" autocomplete="off" id="categoryModal" name="category">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-info " id="submitInsert" value="Save">
                    <input type="hidden" name="insert" id="insertbutton">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Update -->
<?php
foreach ($data->result_array() as $row): $categoryid = $row['categoryid'];
  $category = $row['category'];
  ?>
<div class="modal fade" id="modal_edit<?php echo $categoryid; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Edit Category</h3>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left categoryUpdate" action="<?php echo base_url('category/update') ?>">
                <div class="modal-body">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" name="category" required="required" type="text" autocomplete="off" value="<?php echo $category ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <!--     <button type="submit" class="btn btn-info updatebutton" value="save">Save</button> -->
                    <input type="submit" class="btn btn-info updatebutton" value="Save">
                    <input type="hidden" name="CategoryId" value="<?php echo $categoryid; ?>">
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach; ?>

<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Category</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this Category ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('category/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deletecategory">Delete</button>
                    <input type="hidden" name="delete" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal show sub-category -->
<div class="modal fade" id="listsub" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLongTitle">List Sub-Category</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="pota">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary closemodal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>

<script>
    $(document).ready(function() {
        $('#table_sort').DataTable();
    });
</script>

<script>
    function showlist(e) {
        // alert(e.value);
        var tempdata = e.value;

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>category/showlist",
            dataType: "json",
            data: {
                subdata: tempdata
            }, // <----send this way
            success: function(data) {
                // console.log("Success",data);
                var yourval = jQuery.parseJSON(JSON.stringify(data));

                var table_header = "<table class='table table-striped table-bordered'><thead><tr><td>Sub-Category</td></tr></thead><tbody>";
                var table_footer = "</tbody></table>";
                var html = "";
                if (yourval === undefined || yourval.length == 0) {
                    html += "<td class='well'>There is no Sub-Category created yet!</td>";
                } else {
                    yourval.forEach(function(element) {
                        html += "<tr><td>" + element.subject + "</td></tr>";
                    });
                }

                var all = table_header + html + table_footer;

                $('#pota').html(all);
                $('#listsub').modal('show');

            }
        });
    }
</script>

<!-- Validation Insert and Update-->
<script>
    $('#forminsert').validate({
        errorElement: 'span',
        errorClass: 'desc',

        rules: {

            category: {
                required: true,
                minlength: 5,
            },
        },
        submitHandler: function(form) {
            alert("Successfull");
            // console.log("asdasdas");
            form.submit();
        }
    });

    $('.categoryUpdate').each(function() {
        $(this).validate({
            errorElement: 'span',
            errorClass: 'desc',
            rules: {
                category: {
                    required: true,
                    minlength: 5,
                },
            },
            message: {
                category: {
                    required: true
                },
            },
            submitHandler: function(form) {
                alert("Successfull");
                // console.log("asdasdas");
                form.submit();
            }
        });
    });

    $('#deletecategory').click(function() {
        /* Act on the event */
        alert("Sucessfull");
    });
</script> 