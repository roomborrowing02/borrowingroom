<link href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.buttons.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.nonblock.css'); ?>" rel="stylesheet">
<!--Selecy2 js-->
<link href="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style>
  span.desc {
    color: red;
}
</style>


<div class="mid_col" role="main">
  <div class="">
    <div class="page-title">
    </div>
    <div class="clearfix"></div>
    <div class="">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Term</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Term Detail</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Lecture Week Session</a>
                </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                  <div id="page-wrapper">
                  <div class="row">
                      <div class="title_right">
                        <div class="left_col" role="main" >
                            <a class="btn btn-sm btn-info pull-right"
                               data-toggle="modal"
                               data-for="insert"
                               data-target="#modalinsert"
                               >Insert
                            </a>
                        </div>
                      </div>
                      <?php if($sterm->num_rows() > 0){ ?>
                  <table id="table_sort" class="cell-border" style="width:100%">
                        <thead>
                              <tr>
                                  <th>Term</th>
                                  <th>Start</th>
                                  <th>End</th>
                                  <th>Description</th>
                                  <th>Act</th>
                              </tr>
                        </thead>
                        <tbody>
                           <?php foreach($sterm->result() as $row){ ?>
                             <tr>
                               <td><?php echo $row->termcode; ?></td>
                               <td><?php echo $row->startdate; ?></td>
                               <td><?php echo $row->enddate; ?></td>
                               <td><?php echo $row->description; ?></td>
                               <td>
                                    <a class="btn btn-xs btn-warning"
                                    data-toggle="modal"
                                    data-for="update"
                                    data-target="#modalupdate<?php echo $row->termid;?>"
                                    data-ajax="<?php echo base_url('s_term/getData') ?>"
                                    data-id="<?php echo $row->termid; ?>">
                                    Update</a>
                                   <a class="btn btn-xs btn-danger" 
                                    data-toggle="modal" 
                                    data-target="#modalDel" 
                                    data-id="<?php echo $row->termid; ?>" 
                                    data-delete="<?php echo $row->termcode." - ".$row->description; ?>">Delete</a>
                                    <a class="btn btn-xs btn-success"
                                    data-toggle="modal"
                                    data-target="#modal_generate<?php echo $row->termid; ?>"
                                    data-ajax="<?php echo base_url('s_term/getData') ?>"
                                    data-id="<?php echo $row->termid; ?>">
                                    Generate</a>
                               </td>
                             </tr>
                               <?php } ?>
                        </tbody>
                  </table>
                   <?php } else { ?>
                    <div class="well">There is no term created yet!</div>
                    <?php } ?>
                 </div>
                  </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                  <div id="page-wrapper">
                   <div class="row">
                         <div class="title_right">
                        <div class="left_col" role="main" >
                            <a class="btn btn-sm btn-info pull-right"
                               data-toggle="modal"
                               data-for="insert"
                               data-target="#modal_insertDetail"
                               data-href="<?php echo base_url('s_term/insertDetail') ?>">Insert
                            </a>
                        </div>
                      </div>
                  <div class="container-fluid">
                    <?php if($sdetail->num_rows() > 0){ ?>
                      <table id="tableTrigger" class="cell-border">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                    <p id="selectTriggerFilter"><label><b>Filter Term:</b></label><br></p>
                      </div>
                        <thead>
                          <tr>
                            <th>Term</th>
                            <th>Including <br>Schedule</th>
                            <th>Description </th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($sdetail->result() as $row){ ?>
                          <tr>
                            <td><?php echo $row->termcode; ?></td>
                            <td><?php echo $row->includeSchedule; ?></td>
                            <td><?php echo $row->descTermDetail; ?></td>
                            <td><?php echo $row->startdetail; ?></td>
                            <td><?php echo $row->enddetail; ?></td>
                            <td>
                                <a class="btn btn-xs btn-warning"
                                    data-for="update"
                                    data-toggle="modal"
                                    data-target="#modal_edit<?php echo $row->termdetailid; ?>"
                                    data-ajax="<?php echo base_url('s_term/getDatadetail') ?>"
                                    data-id="<?php echo $row->termdetailid; ?>">
                                    Update</a>
                                <a class="btn btn-xs btn-danger" 
                                    data-toggle="modal" 
                                    data-target="#modaldeldetail" 
                                    data-delete="<?php echo $row->termcode." - ".$row->descTermDetail; ?>">
                                  Delete</a>
                            </td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                      <?php } else { ?>
                    <div class="well">There is no term created yet!</div>
                    <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
              
                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                  <div class="row">
                         <div class="title_right">
                        <div class="left_col" role="main" >
                            <a class="btn btn-sm btn-info pull-right"
                               data-toggle="modal"
                               data-for="insert"
                               data-target="#modalinsertlectureweek"
                               data-href="<?php echo base_url('s_term/insertlectureweek') ?>">Insert
                            </a>
                        </div>
                      </div>
                      <?php if($lweek->num_rows() > 0){ ?>
                  <table id="table_sorttt" class="cell-border" style="width:100%">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <p id="selectFilter"><label><b>Filter Term:</b></label><br></p>
                      </div>
                      <thead>
                            <tr>
                                <th>Code</th>
                                <th>Week<br>Session</th>
                                <th>Day</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Act</th>
                            </tr>
                      </thead>
                      <tbody>
                        <?php foreach($lweek->result() as $row){ ?>
                           <tr>
                             <td><?php echo $row->termcode; ?></td>
                             <td><?php echo $row->weeksession; ?></td>
                             <td><?php echo $row->day; ?></td>
                             <td>
                             <?php echo $row->startdateLWS; ?>
                             <i class="glyphicon glyphicon-calendar""></i>
                           </td>
                             <td>
                             <?php echo $row->enddateLWS; ?>
                              <i class="glyphicon glyphicon-calendar""></i>
                           </td>
                             <td>
                                <a class="btn btn-xs btn-warning"
                                    data-for="update"
                                    data-toggle="modal"
                                    data-target="#modal_update<?php echo $row->lectureweekid; ?>"
                                    data-ajax="<?php echo base_url('s_term/getDatalweek') ?>"
                                    data-id="<?php echo $row->lectureweekid; ?>">
                                    Update</a>
                                 <a class="btn btn-xs btn-danger" 
                                    data-toggle="modal" 
                                    data-target="#modaldellecture" 
                                    data-id="<?php echo $row->lectureweekid; ?>" 
                                    data-delete="<?php echo $row->termcode." - ".$row->weeksession; ?>">
                                  Delete</a>
                             </td>
                           </tr>
                           <?php } ?>
                      </tbody>
                </table>
                <?php } else { ?>
                    <div class="well">There is no term created yet!</div>
                    <?php } ?>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- Modal Insert Term  -->
<div class="modal fade" id="modalinsert" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Manage Term</h4>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left forminsert" action="<?php echo base_url('s_term/insert') ?>">
                <div class="modal-body">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Term (ex: 1710/1720) <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12 termcode" data-validate-length-range="6" data-validate-words="2" name="termcode" placeholder="1710/1720" required="required" type="text" autocomplete="off">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Start Date <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <div class="input-group" id="start">
                            <input class="form-control startdate" placeholder="Start" name="startdate" required autocomplete="off" />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">End Date 
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="input-group" id="end">
                            <input class="form-control enddate" placeholder="End" name="enddate" required autocomplete="off"/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description 
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="description" class="form-control col-md-7 col-xs-12 description" name="description" required="required" type="text" autocomplete="off">
                        </div>
                      </div>
                  </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-info insertterm" value="Save">
                    <input type="hidden" name="Termid" id="termid">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Update Term  -->
        <?php
        foreach($sterm->result_array() as $row):
            $termid=$row['termid'];
            $termcode=$row['termcode'];
            $startdate=$row['startdate'];
            $enddate=$row['enddate'];
            $description=$row['description'];
         ?>
<div class="modal fade" id="modalupdate<?php echo $termid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Edit Term</h3>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left formupdate" action="<?php echo base_url('s_term/update') ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Termcode</label>
                        <div class="col-xs-8">
                          <input name="termcode" value="<?php echo $termcode;?>" class="form-control uptermcode" type="text" autocomplete="off">
                        </div>
                        </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Startdate</label>
                        <div class="col-xs-8">
                          <div class="input-group" id="start">
                            <input class="form-control" placeholder="Start" name="startdate" value="<?php echo $startdate ?>" autocomplete="off"/>
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Enddate</label>
                        <div class="col-xs-8">
                          <div class="input-group" id="end">
                            <input class="form-control" placeholder="End" name="enddate" value="<?php echo $enddate ?>" autocomplete="off"/>
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Description</label>
                        <div class="col-xs-8">
                            <input name="description" value="<?php echo $description;?>" class="form-control updescription" type="text" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <!--   <button type="submit" class="btn btn-info">Save</button> -->
                      <input type="submit" class="btn btn-info submitUpdate" value="Save">
                    <input type="hidden" name="Termid" value="<?php echo $termid; ?>">
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach;?>

<!-- Modal Delete Term-->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Term</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this term ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('s_term/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger deleteterm">Delete</button>
                    <input type="hidden" name="Termid" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Generate Term-->
<?php
        foreach($sterm->result_array() as $row):
            $termid=$row['termid'];
            $termcode=$row['termcode'];
            $startdate=$row['startdate'];
            $enddate=$row['enddate'];
        ?>
<div class="modal fade" id="modal_generate<?php echo $termid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Generate Term</h3>
            </div>
            <form role="form" method="post"  class="form-horizontal form-label-left" action="<?php echo base_url('s_term/generate') ?>">
                <div class="modal-body">
                  <h2 class="modal-title">Are you sure ?</h3>
                    <div class="form-group">
                        <div class="col-xs-8">
                          <input type="hidden" class="form-control" name="termid" value="<?php echo $termid ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8">
                          <input type="hidden" class="form-control" name="termcode" value="<?php echo $termcode ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8">
                          <div class="input-group" id="start">
                            <input type="hidden" class="form-control" placeholder="Start" name="startdate" value="<?php echo $startdate ?>" />
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8">
                          <div class="input-group" id="end">
                            <input type="hidden" class="form-control" placeholder="End" name="enddate" value="<?php echo $enddate ?>" />
                          </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Save</button>
                    <input type="hidden" name="generaterm" value="<?php echo $termid; ?>">
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach;?>

<!-- Modal Insert Termdetail-->
<div class="modal fade" id="modal_insertDetail" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3 class="modal-title" id="myModalLabel">Insert Termdetail</h3>
      </div>
      <form role="form" method="post" id="forminsertdetail" class="form-horizontal form-label-left" action="<?php echo base_url('s_term/insertDetail') ?>">
        <div class="modal-body">
          <div class="form-group">
               <label class="control-label col-md-3 col-sm-3 col-xs-12" >Termcode</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="termcode" class="form-control" id="termcodedetail">
                     <?php foreach($g_termcode->result() as $row){ ?>
                        <option value="<?php echo $row->termid; ?>"><?php echo $row->termcode; ?> - <?php echo $row->description; ?></option>';
                      <?php } ?>
                    </select>
                  </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Start Date <span class="required">*</span>
            </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="input-group" id="start">
                  <input class="form-control" placeholder="Start" name="startdetail" id="startdetail" required autocomplete="off"/>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
              </div>
          </div>
          <div class="item form-group">
           <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">End Date 
              <span class="required">*</span>
            </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="input-group" id="end">
                    <input class="form-control" placeholder="End" name="enddetail" id="enddetail" required autocomplete="off"/>
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                  </div>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >Description</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name="description" id="descriptiondetail" class="form-control" type="text" autocomplete="off">
                </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >Include Schedule</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="includeSchedule" id="includeSchedule" class="form-control">
                        <option value="" active></option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                </div>
          </div>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="submit" class="btn btn-info" id="insertdetail">Save</button> -->
              <input type="submit" class="btn btn-info" id="insertdetail" value="Save">
              <input type="hidden" name="InsertDetail" id="termdetailid" >
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Update Termdetail-->
        <?php
        foreach($sdetail->result_array() as $row):
            $termdetailid=$row['termdetailid'];
            $termid2=$row['termid'];
            $startdetail=$row['startdetail'];
            $enddetail=$row['enddetail'];
            $description=$row['descTermDetail'];
            $includeSchedule2=$row['includeSchedule'];
         ?>
<div class="modal fade" id="modal_edit<?php echo $termdetailid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Edit Termdetail</h3>
            </div>
            <form role="form" method="post"  class="form-horizontal form-label-left formupdatedetail" action="<?php echo base_url('s_term/updatedetail') ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Termcode</label>
                        <div class="col-xs-8">
                          <select name="termcode" class="form-control">
                              <?php foreach($g_termcode->result() as $row){ ?>
                              <?php if ($row->termid == $termid2): ?>
                                <option value="<?php echo $row->termid; ?>" selected><?php echo $row->termcode; ?> - <?php echo $row->description; ?></option>';
                              <?php else: ?>
                                <option value="<?php echo $row->termid; ?>"><?php echo $row->termcode; ?> - <?php echo $row->description; ?></option>';
                            <?php endif; ?>
                              <?php } ?>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Startdate</label>
                        <div class="col-xs-8">
                          <div class="input-group" id="start">
                            <input class="form-control" placeholder="Start" name="startdetail" value="<?php echo $startdetail ?>" autocomplete="off"/>
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Enddate</label>
                        <div class="col-xs-8">
                          <div class="input-group" id="end">
                            <input class="form-control" placeholder="End" name="enddetail" value="<?php echo $enddetail ?>" autocomplete="off"/>
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Description</label>
                        <div class="col-xs-8">
                            <input name="description" value="<?php echo $description;?>" class="form-control" type="text" autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Include Schedule</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="includeSchedule" class="form-control">
                                <?php if ("Yes" == $includeSchedule2): ?>
                                  <option value="Yes" selected>Yes</option>
                                  <option value="No">No</option>
                                <?php elseif("No" == $includeSchedule2): ?>
                                <option value="No" selected>No</option>
                                  <option value="Yes">Yes</option>
                                <?php endif ?>
                              </select>
                          </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <!-- <button type="submit" class="btn btn-info">Save</button> -->
                      <input type="submit" class="btn btn-info updatedetail" value="Save">
                    <input type="hidden" name="Termdetailid" value="<?php echo $termdetailid; ?>">
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach;?>

<!-- Modal Delete Termdetail-->
<div class="modal fade" id="modaldeldetail" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Term</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this term ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('s_term/deletedetail'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger deletedetail">Delete</button>
                    <input type="hidden" name="deletedeldetail" id="deletedetailId" value="<?php echo $termdetailid; ?>">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Insert Lecture Week-->
<div class="modal fade" id="modalinsertlectureweek" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3 class="modal-title" id="myModalLabel">Insert Lecture Week Session</h3>
      </div>
      <form role="form" method="post" id="forminsertlecture" class="form-horizontal form-label-left" action="<?php echo base_url('s_term/insertlectureweek') ?>">
        <div class="modal-body">
          <div class="form-group">
               <label class="control-label col-md-3 col-sm-3 col-xs-12" >Termcode</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="termcode" class="form-control termcodelecture">
                     <?php foreach($g_termcode->result() as $row){ ?>
                        <option value="<?php echo $row->termid; ?>"><?php echo $row->termcode; ?> - <?php echo $row->description ?></option>';
                      <?php } ?>
                    </select>
                  </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >WeekSession</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name="weeksession"  class="form-control weeksession" type="text" autocomplete="off">
                </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="term">Day:</label> 
            <div class="col-md-6 col-sm-6 col-xs-12">       
              <select name="day" class="form-control day" tabindex="-1" id="day">
                  <option selected="selected"></option>
                  <option value="Monday">Monday</option>
                  <option value="Tuesday">Tuesday</option>
                  <option value="Wednesday">Wednesday</option>
                  <option value="Thursday">Thursday</option>
                  <option value="Friday">Friday</option>
                  <option value="Saturday">Saturday</option>
              </select>
            </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Start Date <span class="required">*</span>
            </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="input-group" id="start">
                  <input class="form-control startdateLWS" placeholder="Start" name="startdateLWS" required autocomplete="off"/>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
              </div>
          </div>
          <div class="item form-group">
           <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">End Date 
              <span class="required">*</span>
            </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="input-group" id="end">
                    <input class="form-control enddateLWS" placeholder="End" name="enddateLWS" required autocomplete="off"/>
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                  </div>
              </div>
          </div>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="submit" class="btn btn-info insertlecture">Save</button> -->
              <input type="submit" class="btn btn-info" id="insertlecture" value="Save">
              <input type="hidden" name="InsertLecture" id="lectureweekid" >
        </div>
      </form>
            </div>
            </div>
        </div>

<!-- Modal Update Lecture Week -->
 <?php
        foreach($lweek->result_array() as $row):
            $lectureweekid=$row['lectureweekid'];
            $termid2=$row['termid'];
            $startdate=$row['startdateLWS'];
            $enddate=$row['enddateLWS'];
            $weeksession=$row['weeksession'];
         ?>
<div class="modal fade" id="modal_update<?php echo $lectureweekid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Edit Lecture Week</h3>
            </div>
            <form role="form" method="post"  class="form-horizontal form-label-left formupdatelecture" action="<?php echo base_url('s_term/updatelecture') ?>">
                <div class="modal-body">
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Termcode</label>
                        <div class="col-xs-8">
                          <select name="termcode" class="form-control">
                              <?php foreach($g_termcode->result() as $row){ ?>
                              <?php if ($row->termid == $termid2): ?>
                                <option value="<?php echo $row->termid; ?>" selected><?php echo $row->termcode; ?> - <?php echo $row->description; ?></option>';
                              <?php else: ?>
                                <option value="<?php echo $row->termid; ?>"><?php echo $row->termcode; ?> - <?php echo $row->description; ?></option>';
                            <?php endif; ?>
                              <?php } ?>
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Weeksession</label>
                        <div class="col-xs-8">
                            <input name="weeksession" value="<?php echo $weeksession;?>" class="form-control" type="text" autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Startdate</label>
                        <div class="col-xs-8">
                          <div class="input-group" id="start">
                            <input class="form-control" placeholder="Start" name="startdateLWS" value="<?php echo $startdate ?>" autocomplete="off"/>
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Enddate</label>
                        <div class="col-xs-8">
                          <div class="input-group" id="end">
                            <input class="form-control" placeholder="End" name="enddateLWS" value="<?php echo $enddate ?>" autocomplete="off"/>
                              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <!-- <button type="submit" class="btn btn-info">Save</button> -->
                      <input type="submit" class="btn btn-info lectureupdate" value="Save">
                    <input type="hidden" name="Lectureweekid" value="<?php echo $lectureweekid; ?>">
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach ?>

<!-- Modal Delete Lecture Week-->
<div class="modal fade" id="modaldellecture" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Term</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this term ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('s_term/deletelecture'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger deletelecture">Delete</button>
                    <input type="hidden" name="deletelectureweek" id="deletelectureweek" value="<?php echo $lectureweekid ?>">
                </div>
            </form>
        </div>
    </div>
</div>


<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>"></script>  
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
 <script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>


<script>
  $(document).ready(function() {
      var startDate;
        $('#table_sort').DataTable();
        $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).    tab('show')
        })
        $('#start, #end').datetimepicker({
            useCurrent: false,
            format: "YYYY-MM-DD"
        });

        $('#start').datetimepicker().on('dp.change', function (e) {
            var incrementDay = moment(new Date(e.date));
            incrementDay.add(1, 'days');
            $('#end').data('DateTimePicker').minDate(incrementDay);
            $(this).data("DateTimePicker").hide();
        });

        $('#end').datetimepicker().on('dp.change', function (e) {
            var decrementDay = moment(new Date(e.date));
            decrementDay.subtract(1, 'days');
            $('#start').data('DateTimePicker').maxDate(decrementDay);
             $(this).data("DateTimePicker").hide();
        });


  } );
</script>

<!-- Filter Term detail -->
<script>
    $(document).ready(function() {
      $('#tableTrigger').DataTable({
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "deferRender": true,
        "initComplete": function() {
          var column = this.api().column(0);

          var values = [];
          column.data().each(function(d, j) {
            d.split(",").forEach(function(data) {
              data = data.trim();
              
              if (values.indexOf(data) === -1) {
                values.push(data);
              }
            });
          });

          $('<select class="select2_single form-control" style="width: 100%;" tabindex="-1" id="selectTriggerFilter"><option value="">-- Select All --</option></select>')
            .append(values.sort().map(function(o) {
              return '<option value="' + o + '">' + o + '</option>';
            }))
            .on('change', function() {
              column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
            })
            .appendTo('#selectTriggerFilter').select2();
        }
      });
    });
</script>

<!-- Filter Lecture week session-->
<script>
    $(document).ready(function() {
      $('#table_sorttt').DataTable({
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "deferRender": true,
        "initComplete": function() {
          var column = this.api().column(0);

          var values = [];
          column.data().each(function(d, j) {
            d.split(",").forEach(function(data) {
              data = data.trim();
              
              if (values.indexOf(data) === -1) {
                values.push(data);
              }
            });
          });

          $('<select class="select2_single form-control" style="width: 100%;" tabindex="-1" id="selectFilter"><option value="">-- Select All --</option></select>')
            .append(values.sort().map(function(o) {
              return '<option value="' + o + '">' + o + '</option>';
            }))
            .on('change', function() {
              column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
            })
            .appendTo('#selectFilter').select2();
        }
      });
    });
</script>

<!-- Validation Master Term-->
<script>
  $(document).ready(function () {
      $('.forminsert').each(function () {
       $(this).validate({
         errorElement: 'span',
          errorClass: 'desc',
          onclick: false,
          onfocusout: false,
            rules: {
                termcode: {
                  required: true,
                  minlength: 4,
                  maxlength:4
                },
                startdate:{
                  required: true
                },
                enddate:{
                  required: true
                },
                description:{
                  required: true
                }
            },
            message: {
                termcode: {
                  required: true
                },
                startdate:{
                  required: true
                },
                enddate:{
                  required: true
                },
                description: {
                  required: true
                }
            },
            submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
        });
      });

      $('.formupdate').each(function () {
       $(this).validate({
         errorElement: 'span',
          errorClass: 'desc',
          onclick: false,
          onfocusout: false,
            rules: {
                termcode: {
                  required: true,
                  minlength: 4,
                  maxlength:4
                },
                startdate:{
                  required: true
                },
                enddate:{
                  required: true
                },
                description:{
                  required: true
                }
            },
            message: {
                termcode: {
                  required: true
                },
                startdate:{
                  required: true
                },
                enddate:{
                  required: true
                },
                description: {
                  required: true
                }
            },
            submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
        });
      });

      $('.deleteterm').click(function() {
    /* Act on the event */
    alert("Successfull");
    });

  });
</script>

<!-- Validation Term detail -->
<script>
  $('#forminsertdetail').validate({ // initialize the plugin         
    errorElement: 'span',
    errorClass: 'desc',
    rules: {
       termcode: {
           required: true
       },
       startdetail: {
           required: true
       },
       enddetail:{
        required: true
       },
       description:{
        required: true
       },
       includeSchedule:{
        required: true
       }
     },
     submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
    });

 $('.formupdatedetail').each(function () {
       $(this).validate({
         errorElement: 'span',
          errorClass: 'desc',
            rules: {
                termcode: {
                  required: true
                },
                startdetail:{
                  required: true
                },
                enddetail:{
                  required: true
                },
                description:{
                  required: true
                },
                includeSchedule: {
                  required: true
                }
            },
            message: {
                termcode: {
                  required: true
                },
                startdetail:{
                  required: true
                },
                enddetail:{
                  required: true
                },
                description: {
                  required: true
                },
                includeSchedule: {
                  required: true
                }
            },
            submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
        });
      });
 $('.deletedetail').click(function() {
   /* Act on the event */
   alert("Successfull");
 });
</script>

<!-- Validation Lecture Week -->
<script>
  $('#forminsertlecture').validate({ // initialize the plugin
      errorElement: 'span',
      errorClass: 'desc',
          rules: {
              termcode: {
                  required: true,
                
              },
              weeksession:{
                required: true
              },
              day:{
                required: true
              },
              startdateLWS:{
                required: true
              },
              enddateLWS:{
                required: true
              }
          },
              submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
      });
  $('.formupdatelecture').each(function () {
       $(this).validate({
         errorElement: 'span',
          errorClass: 'desc',
            rules: {
                 termcode: {
                  required: true,

              },
              weeksession:{
                required: true
              },
              day:{
                required: true
              },
              startdateLWS:{
                required: true
              },
              enddateLWS:{
                required: true
              }
            },
            message: {
                 termcode: {
                  required: true
              },
              weeksession:{
                required: true
              },
              day:{
                required: true
              },
              startdateLWS:{
                required: true
              },
              enddateLWS:{
                required: true
              }
            },
            submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
        });
      });
  $('.deletelecture').click(function() {
    /* Act on the event */
    alert("Successfull");
  });
</script>
