<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style>
	td{
		color: rgb(51, 51, 51);
	}
</style>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>List Notes</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
      <div class="col-md-4 col-sm-4 col-xs-12">
        <p id="dayfilter"><label><b>Filter Date:</b><br></label></p>
         <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
        <select class="select2_single form-control" tabindex="-1" id="dateFltr"><option value="">-- Select All --</option></select>
         </select>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12">
        <p id="shiftfilter"><label><b>Filter Category:</b><br></label></p>
         <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
        <select class="select2_single form-control" tabindex="-1" id="categoryFltr"><option value="">-- Select All --</option></select>
         </select>
       </div>
       <div class="col-md-4 col-sm-4 col-xs-12">
          <p id="pickupfilter"><label><b>Filter Sub-Category:</b><br></label></p>
         <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
        <select class="select2_single form-control" tabindex="-1" id="subjectFltr"><option value="">-- Select All --</option></select>
         </select>
       </div>
        <br>
       <div class="col-md-4 col-sm-4 col-xs-12">
       <p id="dropofffilter"><label><b>Filter Room:</b><br></label></p>
             <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
          <select class="select2_single form-control" tabindex="-1" id="roomFltr"><option value="">-- Select All --</option></select>
             </select>
       </div>
     	<div class="col-md-4 col-sm-4 col-xs-12">
          <p id="statusfilter"><label><b>Filter Status:</b><br></label></p>
         <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
        <select class="select2_single form-control" tabindex="-1" id="statusFltr"><option value="">-- Select All --</option></select>
         </select>
       </div>
      <div class="col-md-4 col-sm-4 col-xs-12">
          <p id="statusfilter"><label><b>Filter CreatedBy:</b><br></label></p>
         <span style="font-size:18px;font-weight:500;" multiple="true"> </span>
        <select class="select2_single form-control" tabindex="-1" id="createbyFltr"><option value="">-- Select All --</option></select>
         </select>
       </div>
  <div class="x_content">
    <div class="row">
      <div class="col-sm-12">
        <div class="card-box table-responsive">
			<?php if($data->num_rows() > 0){ ?>
         <table id="example" class="display table-bordered" style="width:100%">
	        <thead>
	            <tr>
	            	<th>ID</th>
                <th>Date</th>
                <th>Name</th>
                <th>Category</th>
                <th>Sub-Category</th>
                <th>Subject</th>
                <th>Room</th>
                <th>Description</th>
                <th>Solution</th>
                <th>Follow Up</th>
                <th>Created By</th>
                <th>Status</th>
                <th>Act</th>
	            </tr>
	        </thead>
	        <tbody>
    				<?php foreach($data->result() as $row){ ?>
    				<?php if ($row->status == "On Progress"): ?>
    				<tr class="alert-success">
    					<td>N - <?php echo $row->notesid; ?></td>
    					<td><?php echo date('d-M-Y H:i:s',strtotime($row->date)); ?></td>
    					<td><?php echo $row->usernama; ?></td>
    					<td><?php echo $row->category; ?></td>
              <td><?php echo $row->subject; ?></td>
    					<td><?php echo $row->others; ?></td>
    					<?php if (empty($row->facilityname)): ?>
  						  <td>-</td>
    					<?php else: ?>
  						  <td><?php echo $row->facilityname; ?></td>
    					<?php endif ?>
      					<td><?php echo $row->description; ?></td>
      					<td><?php echo $row->solution; ?></td>
    					<?php if (empty($row->dateline) || $row->dateline == "0000-00-00 00:00:00"): ?>
  						  <td>-</td>
    					<?php else: ?>
  						  <td><?php echo date('d-M-Y H:i:s',strtotime($row->dateline)); ?></td>
    					<?php endif ?>
              <td><?php echo $row->notesuser ?></td>
    					<td><?php echo $row->status; ?></td>
            	<td>
<!--                 <form name="myform" action="<?php echo base_url() ?>notes/updatenotes" method="post">
        					<input type="hidden" name="id" value="<?php echo $row->notesid ?>" />
        					<input type="submit" class="btn btn-xs btn-warning" value="Update" />
    				    </form> -->
                <a class="btn btn-xs btn-info" href="<?php echo base_url() ?>notes/view/<?php echo $row->notesid; ?>">View
                  </a>
                 <a class="btn btn-xs btn-warning" href="<?php echo base_url() ?>notes/updatenotes/<?php echo $row->notesid; ?>">Update
                  </a>
                  <a class="btn btn-xs btn-danger" 
                    data-toggle="modal" 
                    data-target="#modalDel<?php echo $row->notesid; ?>" 
                    data-id="<?php echo $row->notesid; ?>">Delete</a>
            </td>
    				</tr>
    				<?php elseif($row->status == "Open"): ?>
    				<tr class="alert-info">
    					<td>N - <?php echo $row->notesid; ?></td>
    					<td><?php echo date('d-M-Y H:i:s',strtotime($row->date)); ?></td>
    					<td><?php echo $row->usernama; ?></td>
    					<td><?php echo $row->category; ?></td>
    					<td><?php echo $row->subject; ?></td>
              <td><?php echo $row->others; ?></td>
    					<?php if (empty($row->facilityname)): ?>
    						<td>-</td>
    					<?php else: ?>
    						<td><?php echo $row->facilityname; ?></td>
    					<?php endif ?>
    					<td><?php echo $row->description; ?></td>
    					<td><?php echo $row->solution; ?></td>
    					<?php if (empty($row->dateline) || $row->dateline == "0000-00-00 00:00:00"): ?>
    						<td>-</td>
    					<?php else: ?>
    						<td><?php echo date('d-M-Y H:i:s',strtotime($row->dateline));?></td>
    					<?php endif ?>
              <td><?php echo $row->notesuser ?></td>
    					<td><?php echo $row->status; ?></td>
            	<td>
                  <a class="btn btn-xs btn-info" href="<?php echo base_url() ?>notes/updatenotes/<?php echo $row->notesid; ?>">View
                  </a>
                  <a class="btn btn-xs btn-warning" href="<?php echo base_url() ?>notes/updatenotes/<?php echo $row->notesid; ?>">Update
                  </a>
	              <a class="btn btn-xs btn-danger" 
	                data-toggle="modal" 
	                data-target="#modalDel<?php echo $row->notesid; ?>" 
	                data-id="<?php echo $row->notesid; ?>">Delete</a>
	            </td>
    				</tr>
    				<?php else: ?>
    				<tr class="alert-danger">
    					<td>N - <?php echo $row->notesid; ?></td>
    					<td><?php echo date('d-M-Y H:i:s',strtotime($row->date)); ?></td>
    					<td><?php echo $row->usernama; ?></td>
    					<td><?php echo $row->category; ?></td>
    					<td><?php echo $row->subject; ?></td>
              <td><?php echo $row->others; ?></td>
    					<?php if (empty($row->facilityname)): ?>
    						<td>-</td>
    					<?php else: ?>
    						<td><?php echo $row->facilityname; ?></td>
    					<?php endif ?>
    					<td><?php echo $row->description; ?></td>
    					<td><?php echo $row->solution; ?></td>
    					<?php if (empty($row->dateline) || $row->dateline == "0000-00-00 00:00:00"): ?>
    						<td>-</td>
    					<?php else: ?>
    						<td><?php echo date('d-M-Y H:i:s',strtotime($row->dateline));?></td>
    					<?php endif ?>
              <td><?php echo $row->notesuser ?></td>
    					<td><?php echo $row->status; ?></td>
            	<td>
                  <a class="btn btn-xs btn-info" href="<?php echo base_url() ?>notes/updatenotes/<?php echo $row->notesid; ?>">View
                  </a>
                <a class="btn btn-xs btn-warning" href="<?php echo base_url() ?>notes/updatenotes/<?php echo $row->notesid; ?>">Update
                  </a>
	              <a class="btn btn-xs btn-danger" 
	                data-toggle="modal" 
	                data-target="#modalDel<?php echo $row->notesid; ?>" 
	                data-id="<?php echo $row->notesid; ?>">Delete</a>
	            </td>
    				</tr>
    				<?php endif ?>

    				<?php } ?>
    	        </tbody>
    	    </table>
              <?php } else { ?>
                  <div class="well">There is no Notes created yet!</div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<!-- Modal Delete -->
<?php
foreach($data->result_array() as $row):
    $notesid=$row['notesid'];
 ?>   

<div class="modal fade" id="modalDel<?php echo $notesid; ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete notes</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this notes ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('listnotes/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btnDelete">Delete</button>
                    <input type="hidden" name="Notesid" value="<?php echo $notesid ?>">
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>


<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js'); ?>"></script>

<script>
  $('.btnDelete').click(function() {
    alert("Data Deleted");
  });
</script>

<script >
  $(document).ready(function (){
    var table = $('#example').DataTable({
       dom: 'lrtip',
        initComplete: function () {
          this.api().columns([1]).every( function () {
            var column = this;
            var select = $("#dateFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
           this.api().columns([3]).every( function () {
            var column = this;
            var select = $("#categoryFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
           this.api().columns([4]).every( function () {
            var column = this;
            var select = $("#subjectFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
           this.api().columns([6]).every( function () {
            var column = this;
            var select = $("#roomFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
          	this.api().columns([11]).every( function () {
            var column = this;
            var select = $("#statusFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
            this.api().columns([10]).every( function () {
            var column = this;
            var select = $("#createbyFltr"); 
            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' ).select2();
            } );
          } );
          $("#dateFltr,#categoryFltr,#subjectFltr,#roomFltr,#statusFltr,#createbyFltr")
       }
    });
    $('#dateFltr').on('change', function(){
        var search = [];
      $.each($('#dateFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(1).search(search, true, false).draw();  
    });
    $('#categoryFltr').on('change', function(){
        var search = [];
      $.each($('#categoryFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(3).search(search, true, false).draw();
    });
   	$('#subjectFltr').on('change', function(){
        var search = [];

      $.each($('#subjectFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(4).search(search, true, false).draw();  
    });
   	$('#roomFltr').on('change', function(){
        var search = [];

      $.each($('#roomFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(6).search(search, true, false).draw();  
    });
   	$('#statusFltr').on('change', function(){
        var search = [];

      $.each($('#statusFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(11).search(search, true, false).draw();  
    });
    $('#createbyFltr').on('change', function(){
        var search = [];

      $.each($('#createbyFltr option:selected'), function(){
            search.push($(this).val());
      });
      search = search.join('|');
      table.column(10).search(search, true, false).draw();  
    });
});
</script>

