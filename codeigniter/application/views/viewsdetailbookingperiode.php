<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"  rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>"  rel="stylesheet">
 <!-- bootstrap-datetimepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>"  rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.css'); ?>" rel="stylesheet">


<style>
  span.desc {
    color: red;
}
</style>

    <div id="page-wrapper">
    <div class="row" style="min-height: 768px">
       <div class="page-title">
        <div class="title_left">
           <h3>Manage Detail Booking Periode<small></small></h3>
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
            <li class="active">Detail Booking Periode</li>
        </ul>
          </div>
          <div class="title_right">
      </div>
          </div>
          <div class="clearfix"></div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <p id="selectTriggerFilter"><label><b>Filter Booking Periode:</b><br></label></p>
    </div>
                <div class="x_panel">
                <div class="x_title">
                    <h2><i class="glyphicon glyphicon-list-alt"></i> All Passenger Booking Periode <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <?php if($data->num_rows() > 0){ ?>
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th>Day</th>
                          <th>Shift</th>
                          <th>TermCode</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>  
                          <th>Pickup</th>
                          <th>Dropoff</th>
                          <th>OrderPassenger</th>
                          <th>Description</th>
                          <th>Booking By</th>
                          <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php foreach($data->result() as $row){ ?>    
            <tr>
              <td><?php echo $row->day;?></td>
              <td><?php echo $row->shift;?></td> 
              <td><?php echo $row->termcode;?></td>
              <td><?php echo $row->name;?></td>
              <td><?php echo $row->email;?></td> 
              <td><?php echo $row->phone; ?></td>   
              <td><?php echo $row->pickupLoc; ?></td>
              <td><?php echo $row->dropoffLoc; ?></td>
              <td><?php echo $row->name; ?></td>
              <td><?php echo $row->description; ?></td>
              <td><?php echo $row->bookingby; ?></td>
              <td>

                    <a class="btn btn-xs btn-danger" 
                      data-toggle="modal" 
                      data-target="#modalDel" 
                      data-id="<?php echo $row->bookingperiodeid; ?>" 
                      data-delete="<?php echo 'bookingperiodeid';?>">Delete</a>
              </td>
           </tr>
                 <?php } ?>
            </tbody>
        </table>
                 <?php } else { ?>
                    <div class="well">There is no detail booking periode created yet!</div>
                    <?php } ?>
             </div>
             </div> 
          </div>
        </div>
 
<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Booking</h4>
            </div>
            <div class="modal-body">
              <div>
              Are you sure want to delete this Booking Periode?
            </div>
          </div>
            <form role="form" method="post" action="<?php echo base_url('detailbooking/deleteperiode'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deleteperiode" >Delete</button>
                    <input type="hidden" name="bookingperiodeId" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
 <script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap-datetimepicker -->    
<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<!--Jquery Validation-->
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.js'); ?>"></script>


  <script>   
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "lengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
      ],
      dom: "Bfrtip",
                    order: [[ 1, "asc" ]],
                    buttons: [
                         {
                             extend: "copy",
                             className: "btn-sm"
                         },
                         {
                             extend: "csv",
                             className: "btn-sm"
                         },
                         {
                             extend: "excel",
                             className: "btn-sm"
                         },
                         {
                             extend: "pdfHtml5",
                             className: "btn-sm"
                         },
                         {
                             extend: "print",
                             className: "btn-sm"
                         },
                    ],
                    responsive: true,
      "deferRender": true,
      "initComplete": function() {
        var column = this.api().column(0);

        var values = [];
        column.data().each(function(d, j) {
          d.split(",").forEach(function(data) {
            data = data.trim();
            
            if (values.indexOf(data) === -1) {
              values.push(data);
            }
          });
        });

        $('<select class="select2_single form-control" tabindex="-1" id="selectTriggerFilter"><option value="">-- Select All --</option></select>')
          .append(values.sort().map(function(o) {
            return '<option value="' + o + '">' + o + '</option>';
          }))
          .on('change', function() {
            column.search(this.value ? '\\b' + this.value + '\\b' : "", true, false).draw();
          })
          .appendTo('#selectTriggerFilter').select2();
      }
    });

  });
</script> 
<!-- Validation Insert and Update-->
<script>
      $('#deleteperiode').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });
</script>

