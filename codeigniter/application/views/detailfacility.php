 <!-- Bootstrap -->
<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
 <!-- Font Awesome -->
<link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" 
rel="stylesheet">
<!-- NProgress -->
<link href="<?php echo base_url('assets/vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
<!-- Custom styling plus plugins -->
<link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">
<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"  rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> -->


<style>
  span.desc {
    color: red;
}
</style>



<div role="main">
    <div class="page-title">

      <div class="title_left">
       <h3>Detail Facility Room <small></small></h3>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
          <li><a href="<?php echo base_url('facilitycontrol'); ?>">Facility Room</a></li>
          <?php foreach($room->result() as $row){ ?>
          <li class="active"><?php echo $row ->facilityname; ?> </li>
        </ul>
          <?php } ?>
      </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <a class="btn btn-sm btn-info pull-right"
               data-toggle="modal"
               data-for="insert"
               data-target="#modal_insertdetail">Insert
            </a>
        </div>
      </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="row" style="min-height: 100px;">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="glyphicon glyphicon-list-alt"></i> Detail List<small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <?php if($detail->num_rows() > 0){ ?>
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">

            <thead>
                <tr>
                  <th>Item Name</th>
                  <th>Specification</th>
                  <th>Condition</th>
                  <th>Qty</th>
                  <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($detail->result() as $row){ ?>
                <tr>
                  <td><?php echo $row->itemname; ?></td>
                  <td><?php echo $row->specification; ?></td>
                  <td><?php echo $row->condition; ?></td>
                  <td><?php echo $row->qty; ?></td>
                  <td>
                  <a class="btn btn-xs btn-warning"
                  data-for="update"
                  data-toggle="modal"
                  data-target="#modal_edit<?php echo $row->trfacilityitemid; ?>"
                  data-id="<?php echo $row->trfacilityitemid; ?>">
                  Update</a>
                  <a class="btn btn-xs btn-danger"
                  data-toggle="modal" 
                  data-target="#modalDel<?php echo $row->trfacilityitemid; ?>" 
                  data-id="<?php echo $row->trfacilityitemid; ?>" 
                  data-delete="<?php echo $row->itemname; ?>">
                  Delete</a>
                  </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
          <?php } else { ?>
          <div class="well">There is no detail yet in this facility room!</div>
          <?php } ?>

      </div>
    </div>
  </div>
</div>

  <!-- page content facility image-->
<div role="main">
  <div class="">
    <div class="page-title">
        <div class="title_left">
          <h2></h2>
        </div>
        <div class="title_right">
          <div class="left_col" role="main" >
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a class="btn btn-sm btn-info pull-right" 
                  href="<?php echo base_url() ?>facilitycontrol/uploadview/<?php echo $facility; ?>">Upload<i class="fa fa-upload"></i>
                </a>
            </div>
          </div>
        </div>
    </div>   
  </div>
</div>

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Facility Image<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <?php if($image->num_rows() > 0){ ?>
        <div class="row">
          <?php foreach($image->result() as $row){ ?>
          <div class="col-md-55">
            <div class="thumbnail">
              <div class="image view view-first">
                <img style="width: 100%; height: 100%;" id="<?php echo $row->trfacilityimageid; ?>"src="<?php echo base_url()?>assets/images/facility/<?php echo $row->content;?>" alt='Text dollar code part one.' onerror="this.src='<?php echo base_url()?>assets/images/error/noimages.jpg';"/>
                <div class="mask">
                <p></p>
                  <div class="tools tools-bottom">
                  <a class ="viewimg" href="#" data-toggle="modal" data-target="#myModal" src="<?php echo base_url()?>assets/images/facility/<?php echo $row->content;?>" ><i class="fa fa-expand"></i></a>
                  <a class ="updateimg" href="#" data-toggle="modal" data-target=
                  "#modalUpdate<?php echo $row->trfacilityimageid; ?>" data-id="<?php echo $row->trfacilityimageid; ?>"><i class="fa fa-pencil"></i></a>
                  <a class ="delimg" href="#" data-toggle="modal" data-target="#modalDelImg<?php echo $row->trfacilityimageid; ?>" data-id="<?php echo $row->trfacilityimageid; ?>">
                  <i class="fa fa-times"></i></a>
                  </div>
                </div>
              </div>
              <div class="caption">
              <p><?php echo $row->description;?></p>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
          <?php } else { ?>
        <div class="well">There is no Image yet in this facility room!</div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
  <!-- /page content -->

<!-- Modal Insert detail Facility-->
<div id="modal_insertdetail" class="modal fade" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3 class="modal-title" id="myModalLabel">Insert Detail Facility</h3>
          </div>
          <form role="form" method="post" class="form-horizontal form-label-left" action="<?php echo base_url('facilitycontrol/insertDetail') ?>" id="insertDetail" >
          <div class="modal-body">

          <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="itemname">Item Name
        <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="itemname" id="itemForm" class=" form-control select2" 
              required="required" tabindex="-1"  style="width:265px">
                <option value=""></option>
                <?php foreach($dataItem->result() as $row){ ?>
                <option id="<?php echo $row->itemid; ?>" value="<?php echo $row->itemid; ?>"><?php echo $row->itemname;?></option>';
                <?php } ?>
              </select> 
              </div>    
            </div>

            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Specification
              <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control" required="required" name="specification" id="specForm" autocomplete="off" disabled="disabled">
              </div>
            </div>
           <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Condition 
                        <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="condition" class="form-control condition" 
                        required="required">
                        <option value="" selected></option>}
                        <option value="Bad">Bad</option>
                        <option value="Good">Good</option>
                        <option value="In Service">In Service</option>
                        </select>
                            <input type="hidden" name="facility" value="<?php echo $facility; ?>" autocomplete="off">
                        </div>
                    </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" >Qty
              <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input name="qty" class="form-control" required="required" type="text" id="qty" autocomplete="off">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" id="submitInsert" class="btn btn-info" value="Save">
            <input type="hidden" name="InsertDetail" id="Insertdetailfacilityitemid" >
          </div>    
          </form>
    </div>
  </div>
</div>

<!-- Modal Update Detail Facility-->
<?php
  foreach($detail->result_array() as $row):
    $trfacilityitemid=$row['trfacilityitemid'];
    $facilityid=$row['facilityid'];
    $itemid2=$row['itemid'];
    $itemname=$row['itemname'];
    $specification=$row['specification'];
    $condition=$row['condition'];
    $qty=$row['qty']; 
?>
<div class="modal fade" id="modal_edit<?php echo $trfacilityitemid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Edit Detail Facility</h3>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left itemUpdate" action="<?php echo base_url('facilitycontrol/updateDetail') ?>" >
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Itemname</label>
                        <div class="col-xs-8">
                          <select name="itemnameupdate" class="form-control" required="required">
                              <?php foreach($dataItem->result() as $row){ ?>
                              <?php if ($row->itemid == $itemid2): ?>
                                <option value="<?php echo $row->itemid; ?>" selected><?php echo $row->itemname; ?></option>';
                              <?php else: ?>
                                <option value="<?php echo $row->itemid; ?>"><?php echo $row->itemname; ?></option>';
                            <?php endif; ?>
                              <?php } ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Specification</label>
                        <div class="col-xs-8">
                            <input name="specification" value="<?php echo $specification;?>" class="form-control updateformspec" required="required" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Condition</label>
                        <div class="col-xs-8">
                        <select name="condition" class="form-control condition" 
                        required="required">
                          <?php if ($condition == "Bad"): ?>
                            <option value="Bad" selected>Bad</option>
                            <option value="Good">Good</option>
                            <option value="In Service">In Service</option>
                            <?php elseif ($condition == "Good"): ?>
                              <option value="Bad">Bad</option>
                              <option value="Good" selected>Good</option>
                              <option value="In Service">In Service</option>
                            <?php elseif ($condition == "In Service"): ?>
                              <option value="Bad">Bad</option>
                              <option value="Good">Good</option>
                              <option value="In Service" selected>In Service</option>
                            
                          <?php endif ?>
                          
                        </select>
                            <input type="hidden" name="facility" value="<?php echo $facility; ?>" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Quantity</label>
                        <div class="col-xs-8">
                            <input name="qty" value="<?php echo $qty;?>" class="form-control" 
                            required="required" type="text" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                    <input type="submit" class="btn btn-info submitUpdate" value="Save">
                    <input type="hidden" name="updatedetail" value="<?php echo $trfacilityitemid; ?>">
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach;?>

<!-- Modal Update image dari sini ambil modalnya-->
<?php
foreach($image->result_array() as $row):
    $facilityid=$row['facilityid'];
    $trfacilityimageid=$row['trfacilityimageid'];
    $description=$row['description'];
    $content=$row['content'];
 ?>   
<div class="modal fade" id="modalUpdate<?php echo $trfacilityimageid;?>" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Image</h4>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left formupdate" enctype="multipart/form-data" action="<?php echo base_url('facilitycontrol/updateImg') ?>" runat="server" onsubmit='return formValidation()' name="firstform">
                <div class="modal-body">
                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Select File</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type='file' class="inputFile" name="file_name" />
                            <img class="imagepreview" src="<?php echo base_url()?>assets/images/facility/<?php echo $content ?>" style="width: 300px;" alt="your image" />
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="description" class="some-textarea validate[required]" cols="30" rows="10"><?php echo $description ?></textarea> 
                        </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                <input type="hidden" name="Trfacilityimageid" value="<?php echo $trfacilityimageid ?>">
                  <input type="hidden" name="facility" value="<?php echo $facility; ?>">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-info buttonupdate" value="Save">
                   </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<?php
  foreach($detail->result_array() as $row):
    $trfacilityitemid=$row['trfacilityitemid'];
?>
<div class="modal fade" id="modalDel<?php echo $trfacilityitemid;?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Detail</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this item ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('facilitycontrol/deleteDetail'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger deletedetailfacility">Delete</button>
                    <input type="hidden" name="deletedetail" value="<?php echo $trfacilityitemid ?>">
                    <input type="hidden" name="facility" value="<?php echo $facility; ?>">
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>

<!-- Modal Delete Image -->
<?php
  foreach($image->result_array() as $row):
    $trfacilityimageid=$row['trfacilityimageid'];
?>
<div class="modal fade" id="modalDelImg<?php echo $trfacilityimageid;?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Image</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this Image ?
            </div>
          <form role="form" method="post" action="<?php echo base_url('facilitycontrol/deleteImg'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger deleteiImage">Delete</button>
                    <input type="hidden" name="ImgDelete" value="<?php echo $trfacilityimageid ?>">
                    <input type="hidden" name="facility" value="<?php echo $facility; ?>">
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>

<!-- Modal thumbnail -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-body">
                <img style="width: 100%; height: 100%;" class="showimage img-responsive" src="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<!-- JQuery -->
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/vendors/fastclick/lib/fastclick.js'); ?>"></script>
<!-- NProgress -->
<script src="<?php echo base_url('assets/vendors/nprogress/nprogress.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>


<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.min.js'); ?>"></script>

<!--Jqueryvalidate-->
 <script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script> 


<!-- Tinymce-->
<script type="text/javascript">
  tinymce.init({
              selector: "textarea",
              plugins: [
                  "advlist autolink lists link image charmap print preview anchor",
                  "searchreplace visualblocks code fullscreen",
                  "insertdatetime media table contextmenu paste"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
          });
</script>

<!-- Table Button-->
<script>
    $(document).ready(function() {
        $('.select2_single').select2();
        var handleDataTableButtons = function() {
            if ($("#datatable-responsive").length) {
                $("#datatable-responsive").DataTable({
                    dom: "Bfrtip",
                    order: [[ 1, "asc" ]],
                    buttons: [
                        {

                         extend: "copy",
                        className: "btn-sm"

                        },
                        {
                          extend: "csv",
                          className: "btn-sm"
                        },
                         {
                           extend: "excel",
                          className: "btn-sm"
                         },
                         {

                           extend: "pdfHtml5",
                           className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };
        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();
        TableManageButtons.init();
    });
  </script>

<!-- Show Image tumbnahil-->
<script>
  $(document).ready(function () {
      $('.viewimg').on('click', function () {
          var image = $(this).attr('src');
          //alert(image);
          $('#myModal').on('show.bs.modal', function () {
              $(".showimage").attr("src", image);
          });
      });
  });
</script>

<!-- Select2.js dan ajax insert item -->
<script>
  $(function(){
  // turn the element to select2 select style
  $('.select2').select2({
    placeholder: "Select a Item"
  }).on('change', function(e) {
    // var data = $(".select2 option:selected").text();
    // $("#itemname").val(data);
    $.ajax({
     type: 'POST',
     url: "<?php echo base_url(); ?>facilitycontrol/get_spec",
     dataType: "json",
     data: {
     itemdata : $('#itemForm option:selected').attr('id')
     }, // <----send this way
         success: function(data) {
             // console.log("Success",data);
             var yourval = jQuery.parseJSON(JSON.stringify(data));
             // console.log(yourval);
            
            
              for (var i=0 ; i<yourval.length; i++)
              {

                 var spec = yourval[i].specification;                     
             }
                 $('#specForm').val(spec);    
         }

      });
    });
  });
</script>

<!-- Ajax Detail facility update  -->
<script>
    $(document).ready(function () {
        $('select[name=itemnameupdate]').change(function() {
            var a = $(this).val();
            $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>facilitycontrol/get_specUpdate",
            dataType: "json",
            data: {
            itemdata : a
            }, // <----send this way
                success: function(data) {
                    var yourval = jQuery.parseJSON(JSON.stringify(data));
                    
                     for (var i=0 ; i<yourval.length; i++)
                     {
                        var spec = yourval[i].specification;                     
                    }
                        $('.updateformspec').val(spec);    
                }

          });
        });
    });
</script>

<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
              $('.imagepreview').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
      }
  }
  $(".inputFile").change(function () {
      readURL(this);
  });
</script>

<!-- Validation Insert and Update Item-->
<script>
  $('#insertDetail').each(function () {
       $(this).validate({
          errorElement: 'span',
          errorClass: 'desc',
            rules: {
              itemname: {
              required: true,
              },
              specification: {
              required: true,
              },
              qty:{
                required: true
              }
            },
            submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
        });
      });

   $('.itemUpdate').each(function () {
       $(this).validate({
          errorElement: 'span',
          errorClass: 'desc',
            rules: {
              itemname: {
              required: true,
              },
              specification: {
              required: true,
              },
              qty:{
                required: true
              },
            },
            submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
        });
      });

       $('.deletedetailfacility').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });
</script>

<!-- Validation Update and Delete Image-->
<script>
 function formValidation(){

    var isValid = true;
    
      var editorContent = tinyMCE.activeEditor.getContent();   
      if (editorContent == '' || editorContent == null) {
        alert("Description Must be filled out");
        isValid = false;
      } 


    if (!isValid) {
      alert('Submit Fail');
      return false;
    }
    alert('Data Updated');
    return isValid;
  }
  


  $('.deleteiImage').click(function() {
   /* Act on the event */
   alert("Successfull");
  });
</script>
