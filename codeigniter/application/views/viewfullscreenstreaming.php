<style>
.videoContainer {
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
}

iframe {
  /* optional */
  width: 100%;
  height: 100%; 
}
</style>

<div class="videoContainer">
	<?php foreach ($id as $row): ?>
    <iframe class="videoContainer__video" width="1920" height="1080" src="http://www.youtube.com/embed/<?php echo $row->content; ?>" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>
		
	<?php endforeach ?>
</div>