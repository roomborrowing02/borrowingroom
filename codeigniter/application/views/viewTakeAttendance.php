<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style type="text/css">
/* Source: http://tutorialzine.com/2013/06/digital-clock/ */


/*-------------------------
The clocks
--------------------------*/
#clock{
  width:450px;
  padding:40px;
  margin:100px auto 60px;
  position:relative;
}

#clock:after{
  content:'';
  position:absolute;
  width:400px;
  height:20px;
  border-radius:100%;
  left:50%;
  margin-left:-200px;
  bottom:2px;
  z-index:-1;
}


#clock .display{
  text-align:center;
  padding: 40px 20px 20px;
  border-radius:6px;
  position:relative;
}


/*-------------------------
Light color theme
--------------------------*/


#clock.light{
  background-color:#f3f3f3;
  color:#272e38;
}

#clock.light:after{
  box-shadow:0 4px 10px rgba(0,0,0,0.15);
}

#clock.light .digits div span{
  background-color:#272e38;
  border-color:#272e38; 
}

#clock.light .digits div.dots:before,
#clock.light .digits div.dots:after{
  background-color:#272e38;
}

#clock.light .alarm{
  /*background:url('../img/alarm_light.jpg');*/
}

#clock.light .display{
  background-color:#dddddd;
  box-shadow:0 1px 1px rgba(0,0,0,0.08) inset, 0 1px 1px #fafafa;
}


/*-------------------------
Dark color theme
--------------------------*/

#clock {
  border-radius: 10px;
}
#clock.dark{
  background-color:#272e38;
  color:#cacaca;
}

#clock.dark:after{
  box-shadow:0 4px 10px rgba(0,0,0,0.3);
}

#clock.dark .digits div span{
  background-color:#cacaca;
  border-color:#cacaca; 
}

#clock.dark .alarm{
  /*background:url('../img/alarm_dark.jpg');*/
}

#clock.dark .display{
  background-color:#0f1620;
  box-shadow:0 1px 1px rgba(0,0,0,0.08) inset, 0 1px 1px #2d3642;
}

#clock.dark .digits div.dots:before,
#clock.dark .digits div.dots:after{
  background-color:#cacaca;
}

/*-------------------------
The Date
--------------------------*/
#clock .date{
  margin-top:10px;
  font-size: 16px;
}

/*-------------------------
The Digits
--------------------------*/
#clock .digits div{
  text-align:left;
  position:relative;
  width: 28px;
  height:50px;
  display:inline-block;
  margin:0 4px;
}

#clock .digits div span{
  opacity:0;
  position:absolute;

  -webkit-transition:0.25s;
  -moz-transition:0.25s;
  transition:0.25s;
}

#clock .digits div span:before,
#clock .digits div span:after{
  content:'';
  position:absolute;
  width:0;
  height:0;
  border:5px solid transparent;
}

#clock .digits .d1{     height:5px;width:16px;top:0;left:6px;}
#clock .digits .d1:before{  border-width:0 5px 5px 0;border-right-color:inherit;left:-5px;}
#clock .digits .d1:after{ border-width:0 0 5px 5px;border-left-color:inherit;right:-5px;}

#clock .digits .d2{     height:5px;width:16px;top:24px;left:6px;}
#clock .digits .d2:before{  border-width:3px 4px 2px;border-right-color:inherit;left:-8px;}
#clock .digits .d2:after{ border-width:3px 4px 2px;border-left-color:inherit;right:-8px;}

#clock .digits .d3{     height:5px;width:16px;top:48px;left:6px;}
#clock .digits .d3:before{  border-width:5px 5px 0 0;border-right-color:inherit;left:-5px;}
#clock .digits .d3:after{ border-width:5px 0 0 5px;border-left-color:inherit;right:-5px;}

#clock .digits .d4{     width:5px;height:14px;top:7px;left:0;}
#clock .digits .d4:before{  border-width:0 5px 5px 0;border-bottom-color:inherit;top:-5px;}
#clock .digits .d4:after{ border-width:0 0 5px 5px;border-left-color:inherit;bottom:-5px;}

#clock .digits .d5{     width:5px;height:14px;top:7px;right:0;}
#clock .digits .d5:before{  border-width:0 0 5px 5px;border-bottom-color:inherit;top:-5px;}
#clock .digits .d5:after{ border-width:5px 0 0 5px;border-top-color:inherit;bottom:-5px;}

#clock .digits .d6{     width:5px;height:14px;top:32px;left:0;}
#clock .digits .d6:before{  border-width:0 5px 5px 0;border-bottom-color:inherit;top:-5px;}
#clock .digits .d6:after{ border-width:0 0 5px 5px;border-left-color:inherit;bottom:-5px;}

#clock .digits .d7{     width:5px;height:14px;top:32px;right:0;}
#clock .digits .d7:before{  border-width:0 0 5px 5px;border-bottom-color:inherit;top:-5px;}
#clock .digits .d7:after{ border-width:5px 0 0 5px;border-top-color:inherit;bottom:-5px;}


/* 1 */

#clock .digits div.one .d5,
#clock .digits div.one .d7{
  opacity:1;
}

/* 2 */

#clock .digits div.two .d1,
#clock .digits div.two .d5,
#clock .digits div.two .d2,
#clock .digits div.two .d6,
#clock .digits div.two .d3{
  opacity:1;
}

/* 3 */

#clock .digits div.three .d1,
#clock .digits div.three .d5,
#clock .digits div.three .d2,
#clock .digits div.three .d7,
#clock .digits div.three .d3{
  opacity:1;
}

/* 4 */

#clock .digits div.four .d5,
#clock .digits div.four .d2,
#clock .digits div.four .d4,
#clock .digits div.four .d7{
  opacity:1;
}

/* 5 */

#clock .digits div.five .d1,
#clock .digits div.five .d2,
#clock .digits div.five .d4,
#clock .digits div.five .d3,
#clock .digits div.five .d7{
  opacity:1;
}

/* 6 */

#clock .digits div.six .d1,
#clock .digits div.six .d2,
#clock .digits div.six .d4,
#clock .digits div.six .d3,
#clock .digits div.six .d6,
#clock .digits div.six .d7{
  opacity:1;
}


/* 7 */

#clock .digits div.seven .d1,
#clock .digits div.seven .d5,
#clock .digits div.seven .d7{
  opacity:1;
}

/* 8 */

#clock .digits div.eight .d1,
#clock .digits div.eight .d2,
#clock .digits div.eight .d3,
#clock .digits div.eight .d4,
#clock .digits div.eight .d5,
#clock .digits div.eight .d6,
#clock .digits div.eight .d7{
  opacity:1;
}

/* 9 */

#clock .digits div.nine .d1,
#clock .digits div.nine .d2,
#clock .digits div.nine .d3,
#clock .digits div.nine .d4,
#clock .digits div.nine .d5,
#clock .digits div.nine .d7{
  opacity:1;
}

/* 0 */

#clock .digits div.zero .d1,
#clock .digits div.zero .d3,
#clock .digits div.zero .d4,
#clock .digits div.zero .d5,
#clock .digits div.zero .d6,
#clock .digits div.zero .d7{
  opacity:1;
}


/* The dots */

#clock .digits div.dots{
  width:5px;
}

#clock .digits div.dots:before,
#clock .digits div.dots:after{
  width:5px;
  height:5px;
  content:'';
  position:absolute;
  left:0;
  top:14px;
}

#clock .digits div.dots:after{
  top:34px;
}


/*-------------------------
The Alarm
--------------------------*/


#clock .alarm{
  width:16px;
  height:16px;
  bottom:20px;
  position:absolute;
  opacity:0.2;
}

#clock .alarm.active{
  opacity:1;
}


/*-------------------------
Weekdays
--------------------------*/


#clock .weekdays{
  font-size:12px;
  position:absolute;
  width:100%;
  top:10px;
  left:0;
  text-align:center;
}


#clock .weekdays span{
  opacity:0.2;
  padding:0 10px;
}

#clock .weekdays span.active{
  opacity:1;
}


/*-------------------------
AM/PM
--------------------------*/


#clock .ampm{
  position:absolute;
  bottom:20px;
  right:20px;
  font-size:12px;
}


/*-------------------------
Button
--------------------------*/


.button-holder{
  text-align:center;
  padding-bottom:100px;
}

a.button{
  background-color:#f6a7b3;

  background-image:-webkit-linear-gradient(top, #f6a7b3, #f0a3af);
  background-image:-moz-linear-gradient(top, #f6a7b3, #f0a3af);
  background-image:linear-gradient(top, #f6a7b3, #f0a3af);

  border:1px solid #eb9ba7;
  border-radius:2px;

  box-shadow:0 2px 2px #ccc;

  color:#fff;
  text-decoration: none !important;
  padding:15px 20px;
  display:inline-block;
  cursor:pointer;
}

a.button:hover{
  opacity:0.9;
}
</style>
<div class="row" style="min-height: 768px;">
  <div class="col-md-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Take Attendance</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="clock" class="light">
          <div class="display">
            <div class="weekdays"></div>
            <div class="alarm"></div>
            <div class="digits"></div>
            <div class="date"></div>
          </div>
        </div>

        <div class="button-holder">
          <a class="button" id="switch-theme">Switch Theme</a>
          <a class="button" id="take-attendance">Take Attendance</a>
        </div>

      </div>
    </div>
  </div>        
</div>



<!-- JavaScript Includes -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/moment.min.js"></script>
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/iCheck/icheck.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>


<script>
  function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    $('#txt').html(h + ":" + m + ":" + s);
    var t = setTimeout(startTime, 500);
  }
  function checkTime(i) {
if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
return i;
}
</script>

<script type="text/javascript">
  $('.followupdate').hide();
  $('.dateForm').datetimepicker({
    format: "MMM-DD-YYYY h:mm:ss"
  });
  $('.formfollowupdate').datetimepicker({
    format: "MMM-DD-YYYY h:mm:ss"
  });
</script>
<script type="text/javascript">
  tinymce.init({
    selector: "textarea",
    plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    startTime();
    $('.searchinguser').select2({
      placeholder: 'Input a ID Student / Lecturer',
      minimumInputLength: 1,
      allowClear: true,
      ajax:{
        url: "<?php echo base_url(); ?>/notes/search_user",
        dataType: "json",
        delay: 250,
        data: function(params){
          return{
            nimdata: params.term
          };
        },
        processResults: function(data){
          var results = [];

          $.each(data, function(index, item){
            results.push({
              id: item.userid,
              text: item.nim+" - "+item.nama,
            });
          });
          return{
            results: results
          };
        }
      }
    });
  });
</script>
<script>
  $("input[type=checkbox]").click(function() {
    $("input[type=checkbox]").each(function() {
      if ($(this).is(":checked")) {
        $('.followupdate').show();
        $('.formfollowupdate').val('');
      } else {
        $('.formfollowupdate').val('');
        $('.followupdate').hide();
      }
    });
  });

</script>
<script type="text/javascript">
  $('.room').hide();
  $('#subjectroom').select2();
  $('#category').change(function(){
    $('#subject').prop('disabled',false);
    if ($('#category option:selected').attr('id') == "Room") {
      $('.room').show();
    }
    else{
      $('.room').hide();
    }
    $.ajax({
      type: 'POST',
      url: "<?php echo base_url(); ?>notes/getdata",
      dataType: "json",
      data: {
        notesid : $('#category').val()
}, // <----send this way
success: function(data) {
  var yourval = jQuery.parseJSON(JSON.stringify(data));
// console.log(yourval);
var subjecthtml="";
if ($('#category option:selected').attr('id') == 0) {
  subjecthtml += "<option id =\"\" value =\"\"></option>";
  $('#subject').prop('disabled','true');

}
else{
  subjecthtml += "<option id =\"0\" value =\"\">- Please choose the subject -</option>";
  for (var i=0 ; i<yourval.length; i++)
  {
    subjecthtml += "<option value =\""+yourval[i].subjectid+"\" id =\""+ yourval[i].subject +"\">"+yourval[i].subject+"</option>";
  }
}
$('#subject').html(subjecthtml);

}
});
    return false;
  });
</script>

<script>
  function validation(){
    var dateform = document.forms["forminsert"]["dateform"];               
    var user = document.forms["forminsert"]["user"];    
    var category = document.forms["forminsert"]["category"];  
    var subject =  document.forms["forminsert"]["subject"];  
    var room = document.forms["forminsert"]["room"];  
    var others = document.forms["forminsert"]["others"];
    var status = document.forms["forminsert"]["status"];
    var followup = document.forms["forminsert"]["followup"];
    var description = tinyMCE.get('description').getContent();
    var solution = tinyMCE.get('solution').getContent();

// e.stopImmediatePropagation();
if (dateform.value == "")                                  
{ 
  alert("Please fill the date."); 
  return false; 
}
if (user.value == "")                                  
{ 
  alert("Please fill the ID."); 
  return false; 
}
if (category.value == "")                                  
{ 
  alert("Please fill the Category."); 
  return false; 
}
if (subject.value == "")                                  
{ 
  alert("Please fill the Sub-Category."); 
  return false; 
}
if ($("#category option:selected").attr('id') == "Room") {
  if (room.value == "" || room.value == "0")                                  
  { 
    alert("Please fill the Room."); 
    return false; 
  }
} 

if (others.value == "")                                  
{  
  alert("Please fill the Subject."); 
  return false; 
}
if (description == '' || description == null) {
  alert("Description Must be filled out");
  return false;
} 

if (solution == '' || solution == null) {
  alert("Solution Must be filled out");
  return false;
} 
if ($('#derp').is(':checked')) {
  if (followup.value == "")                                  
  {  
    alert("Please fill the Date Followup"); 
    return false; 
  }
} 
alert('Data Inserted');
return true;  
}
</script>

<script type="text/javascript">
  $(function(){

// Cache some selectors

var clock = $('#clock'),
alarm = clock.find('.alarm'),
ampm = clock.find('.ampm'),
date = clock.find('.date');

// Map digits to their names (this will be an array)
var digit_to_name = 'zero one two three four five six seven eight nine'.split(' ');

// This object will hold the digit elements
var digits = {};

// Positions for the hours, minutes, and seconds
var positions = [
'h1', 'h2', ':', 'm1', 'm2', ':', 's1', 's2'
];

// Generate the digits with the needed markup,
// and add them to the clock

var digit_holder = clock.find('.digits');

$.each(positions, function(){

  if(this == ':'){
    digit_holder.append('<div class="dots">');
  }
  else{

    var pos = $('<div>');

    for(var i=1; i<8; i++){
      pos.append('<span class="d' + i + '">');
    }

// Set the digits as key:value pairs in the digits object
digits[this] = pos;

// Add the digit elements to the page
digit_holder.append(pos);
}

});

// Add the weekday names

var weekday_names = 'MON TUE WED THU FRI SAT SUN'.split(' '),
weekday_holder = clock.find('.weekdays');

$.each(weekday_names, function(){
  weekday_holder.append('<span>' + this + '</span>');
});

var weekdays = clock.find('.weekdays span');

// Run a timer every second and update the clock

(function update_time(){

// Use moment.js to output the current time as a string
// hh is for the hours in 12-hour format,
// mm - minutes, ss-seconds (all with leading zeroes),
// d is for day of week and A is for AM/PM

var now = moment().format("HHmmssd");
var dateNow = moment().format("DD MMM YYYY");

digits.h1.attr('class', digit_to_name[now[0]]);
digits.h2.attr('class', digit_to_name[now[1]]);
digits.m1.attr('class', digit_to_name[now[2]]);
digits.m2.attr('class', digit_to_name[now[3]]);
digits.s1.attr('class', digit_to_name[now[4]]);
digits.s2.attr('class', digit_to_name[now[5]]);

// The library returns Sunday as the first day of the week.
// Stupid, I know. Lets shift all the days one position down, 
// and make Sunday last

var dow = now[6];
dow--;

// Sunday!
if(dow < 0){
// Make it last
dow = 6;
}

// Mark the active day of the week
weekdays.removeClass('active').eq(dow).addClass('active');

// Set the am/pm text:
date.text(dateNow);
console.log(now);

// Schedule this function to be run again in 1 sec
setTimeout(update_time, 1000);

})();

// Switch the theme

$('a#switch-theme').click(function(){
  clock.toggleClass('light dark');
});

$('a#take-attendance').click(function(){
  $.ajax({
    type : "POST",
    url: "<?php echo base_url(); ?>takeAttendance/actionTakeAttendance",
    dataType : "JSON",
    success: function(data){
      
    }
  });
  return false;
});

});
</script>