<link href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style>
  span.desc {
    color: red;
}
</style>

<div id="page-wrapper">
  <div class="row" style="min-height: 768px">
    <div class="page-title">
      <div class="title_left">
         <h3>Manage Sub-Category <small></small></h3>
          <ul class="breadcrumb">
              <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
              <li class="active">Sub-Category</li>
          </ul>
      </div>
      <div class="title_right">
        <div class="left_col" role="main" >
            <a class="btn btn-sm btn-info pull-right"
               data-toggle="modal"
               data-for="insert"
               data-target="#modalinsert"
               >Insert
            </a>
        </div>
      </div>
    </div>
    <div class="x_panel"> 
      <div class="x_content">
      <?php if($data->num_rows() > 0){ ?>
      <table id="table_sort" class="cell-border" style="width:100%">
      <thead>
            <tr>
                <th>Category</th>
                <th>Sub-Category</th>
                <th>Act</th>
            </tr>
      </thead>
      <tbody>
             <?php foreach($data->result() as $row){ ?>
           <tr>
             <td><?php echo $row->category; ?></td>
             <td><?php echo $row->subject; ?></td>
             <td>
               <a class="btn btn-xs btn-warning"
                data-for="update"
                data-toggle="modal"
                data-target="#modalupdate<?php echo $row->subjectid; ?>"
                data-ajax="<?php echo base_url('subject/getData') ?>"
                data-id="<?php echo $row->subjectid; ?>"
                >Update</a>||
              <a class="btn btn-xs btn-danger" 
                data-toggle="modal" 
                data-target="#modalDel" 
                data-id="<?php echo $row->subjectid; ?>" 
                data-delete="<?php echo $row->category ?> - <?php echo $row->subject; ?>">Delete</a>
             </td>
           </tr>
           <?php } ?>
      </tbody>
      </table>
            <?php } else { ?>
                <div class="well">There is no subject created yet!</div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Modal Insert -->
<div class="modal fade" id="modalinsert"  tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h3 class="modal-title" id="myModalLabel">Insert Subject</h3>
    </div>
      <form role="form" method="post" id="subjectInsert" class="form-horizontal form-label-left" action="<?php echo base_url('subject/insert') ?>">
        <div class="modal-body">
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="category" class="form-control categoryModal ">
                <option value="">-- Select Category --</option>
               <?php foreach($category->result() as $row){ ?>
                <option value="<?php echo $row->categoryid ?>"><?php echo $row->category ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Subject<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input class="form-control col-md-7 col-xs-12 subjectModal" name="subject" required="required" type="text" autocomplete="off" >
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <input type="submit" class="btn btn-info " id="submitInsert" value="Save">
          <input type="hidden" name="insert" id="insertbutton" >
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Update -->
<?php
foreach($data->result_array() as $row):
    $categoryid=$row['categoryid'];
    $subjectid=$row['subjectid'];
    $subject=$row['subject'];
 ?>
<div class="modal fade" id="modalupdate<?php echo $subjectid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" id="myModalLabel">Edit Subject</h3>
      </div>
      <form role="form" method="post" class="form-horizontal form-label-left subjectupdate" action="<?php echo base_url('subject/update') ?>">
          <div class="modal-body">
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="category" class="form-control category">
                  <option value="">-- Select Category --</option>
                 <?php foreach($category->result() as $row){ ?>
                  <?php if ($row->categoryid == $categoryid): ?>
                    <option value="<?php echo $row->categoryid ?>" selected><?php echo $row->category ?></option>
                    <?php else: ?>
                    <option value="<?php echo $row->categoryid ?>"><?php echo $row->category ?></option>
                  <?php endif ?>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Subject<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12 subjec" name="subject" required="required" type="text" autocomplete="off" value="<?php echo $subject ?>">
              </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-info subjectInsert" value="Save">
              <input type="hidden" name="SubjectId" value="<?php echo $subjectid; ?>">
          </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Subject</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this Subject ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('subject/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deletesubject">Delete</button>
                    <input type="hidden" name="delete" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- <script src="<?php echo base_url('assets/js/jquery.dataTables.js'); ?>"></script>jquery.dataTables.min -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>

<script>
    $(document).ready(function() {
        $('#table_sort').DataTable();
    } );
</script>


<!-- Validation Insert and Update-->
<script>
     $('#subjectInsert').validate({       
            errorElement: 'span',
            errorClass: 'desc',
          
              rules: {
                
              category: {
              required: true,
          },
              subject: {
              required: true,
              minlength:2,
          },
               },
                submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
    });

        $('.subjectupdate').each(function () {
    $(this).validate({
      errorElement: 'span',
      errorClass: 'desc',
      rules: {
          category: {
              required: true,
          },
          subject: {
              required: true,
              minlength:2,
          },
      },
      message: {
          category: {
              required: true
          },
          subject: {
              required: true
          },
      },
      submitHandler: function(form) {
          alert("Successfull");
        // console.log("asdasdas");
        form.submit();
        }
      });
  });

      $('#deletesubject').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });

</script>
