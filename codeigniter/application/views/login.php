<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LSC</title>
	<link rel="icon" href="<?php echo base_url('assets/images/icons/binusicon.png'); ?>" type="image/png">
	<!-- Bootstrap -->
	<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
	<!-- Animate.css -->
	<link href="<?php echo base_url('assets/vendors/animate.css/animate.min.css');?>" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="<?php echo base_url('assets/build/css/custom.min.css');?>" rel="stylesheet">
	<style type="text/css">

	.carousel{
		background: #2f4357;
		margin-top: 20px;
	}
	.carousel .item{
		min-height: 280px; /* Prevent carousel from being distorted if for some reason image doesn't load */
	}
	.carousel .item img{
		margin: 0 auto; /* Align slide image horizontally center */
		height: 300px;
		max-width: 675px;
		width: 100%;
	}
	.bs-example{
		margin: 20px;
	}

	html { 
		background: url("<?php echo base_url('assets/images/bg-login-blur.jpg');?>") no-repeat center center fixed; 
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}
	.login_form{
		background-color: rgba(255, 255, 255, 0.7);
		padding: 10px 30px 10px 30px;
		border-radius: 10px;
	}
	.login_wrapper {
		margin: 10% auto 0;
	}
</style>
</head>
<body class="login">

    <!--<div class="bs-example">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>   
            <div class="carousel-inner">

               <div class="item">
                    <img src="<?php echo base_url('assets/images/anggrek.jpg'); ?>" alt="Fourth Slide">
                </div>
                <div class="item active">
                    <img src="<?php echo base_url('assets/images/syahdan.jpg'); ?>" alt="First Slide">
                </div>
                <div class="item">
                    <img src="<?php echo base_url('assets/images/kijang.jpg'); ?>" alt="Second Slide">
                </div>
                <div class="item">
                    <img src="<?php echo base_url('assets/images/alsut.jpg'); ?>" alt="Third Slide">
                </div>
               
            </div>
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
-->
<div>
	<div class="login_wrapper">
		<div class="animate form login_form">
			<section class="login_content">
				<form role="form" method="post" action="<?php echo base_url()."auth/cek_login" ?>">
					<h1>L O G I N</h1>
					<div>
						<input type="text" name="username" class="form-control" placeholder="Username" required="" />
					</div>
					<div>
						<input type="password" name="password" class="form-control" placeholder="Password" required="" />
					</div>
					<div>
						<button class="btn btn-default submit" type="submit" name="btnLogin">Log in</button>
					</div>
					<div class="clearfix"></div>
					<div class="separator">
						<div>
							<h1>LSC Management System</h1>
							<p>©2018 All Rights Reserved.</p>
						</div>
					</div>
				</form>

			</section>
		</div>
	</div>
</div>
</body>
</html>
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>