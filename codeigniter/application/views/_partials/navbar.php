<!-- top navigation -->
					<div class="top_nav">
						<div class="nav_menu">
							<nav>
								<div class="nav toggle">
									<a id="menu_toggle"><i class="fa fa-bars"></i></a>
								</div>
								<div class="nav toggle">
									<span><img height="20px" src="<?php echo base_url('assets/images/binus-logo.png'); ?>"></span> 
								</div>
								<ul class="nav navbar-nav navbar-right">
									<li class="">
										<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<img src="<?php echo base_url('assets/images/default-photo.png'); ?>" alt=""><?php echo strtoupper($this->session->userdata('username')); ?>
											<span class=" fa fa-angle-down"></span>
										</a>
										<ul class="dropdown-menu dropdown-usermenu pull-right">
											<li>
												<a data-toggle="modal" data-target="#modalPass">
													<i class="fa fa-key pull-right"></i> Change Local Password
												</a>
											</li>
											<li>
												<a href="<?php echo base_url("auth/logout"); ?>">
													<i class="fa fa-sign-out pull-right"></i> Log Out
												</a>
											</li>
										</ul>
									</li>

									<li role="presentation" class="dropdown" ng-controller="messageController" ng-cloak>
										<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
											<i class="fa fa-envelope-o"></i>
											<span class="badge bg-green">{{latestMessageLen}}</span>
										</a>
										<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
											<li class="message-preview" id="has-no-message">
												<div class="media">
													<div class="media-body">
														<span class="media-heading">
															<p class="text-muted">
																Recently, there are no message reported..
															</p>
														</span>
													</div>
												</div>
											</li>

											<li id="has-message" ng-repeat="row in latestMessage">
												<a href="<?php echo base_url('message'); ?>">
													<span>
														<span>{{row.room_name}}</span>
														<span class="time">{{row.inserted_time}}</span>
													</span>
													<span class="message">
														<table>
															<tr>
																<th>Problem</th>
																<td>:</td>
																<td>{{row.problem_name}}</td>
															</tr>
															<tr>
																<th>Problem Type</th>
																<td>:</td>
																<td>{{row.problem_type}}</td>
															</tr>
															<tr>
																<th>Comment</th>
																<td>:</td>
																<td>{{row.comment | limitTo: 25}}...</td>
															</tr>
														</table>
													</span>
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</nav>
						</div>
					</div>
					<!-- /top navigation -->