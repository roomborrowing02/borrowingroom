<!-- sidebar menu -->
						<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
							<div class="menu_section">
								<h3>Menu</h3>
								<ul class="nav side-menu">

									<?php if($this->session->userdata('rolename') == 'admin'){ ?>

									<li <?php if($page == 0) echo 'class="active"'; ?>>
										<a href="<?php echo base_url('dashboard'); ?>"> <i class="fa fa-dashboard"></i> Dashboard </a>
									</li>

									<li>
										<a><i class="fa fa-bullseye"></i> Master <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">

											<li <?php if($page == 2) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('user'); ?>"> <i class="fa fa-users"></i> User List </a>
											</li>
											<li <?php if($page == 4) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('s_term'); ?>"> <i class="fa fa-calendar"></i> Manage Term / Calendar </a>
											</li>

											<li <?php if($page == 5) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('tambahshift'); ?>"> <i class="glyphicon glyphicon-time"></i> Add Shift / Time</a>
											</li>
											<li <?php if($page == 6) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('kendaraan'); ?>"> <i class="fa fa-car"></i>Vehicle</a>
											</li>

											<li <?php if($page == 7) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('driver'); ?>"> <i class="fa fa-id-card-o"></i>Driver</a>
											</li>

											<li <?php if($page == 8) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('campus'); ?>"> <i class="fa fa-university"></i>Campus </a>
											</li>

											<li <?php if($page == 18) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('facilitycontrol'); ?>"> <i class="fa fa-building"></i>Facility Room </a>
											</li>
											<li <?php if($page == 15) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('itemcontrol'); ?>"> <i class="fa fa-keyboard-o"></i>Facility Item </a>
											</li>
											<li <?php if($page == 29) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('roomborrowing'); ?>"> <i class="fa fa-keyboard-o"></i>Room Borrowing </a>
											</li>
											<li <?php if($page == 22) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('role'); ?>"> <i class="fa fa-user"></i> Role </a>
											</li>
											<li <?php if($page == 21) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('weblink'); ?>"> <i class="fa fa-globe glyphicon-user"></i> Weblink </a>
											</li>
										</ul>
									</li>


									<li> 
										<a><i class="fa fa-bullseye"></i> Schedule Configuration <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li <?php if($page == 9) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('cs_master'); ?>"> <i class="fa fa-newspaper-o"></i>
													Configuration Master
												</a>
											</li>
											<li <?php if($page == 10) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('cs_detail'); ?>"> <i class="fa fa-newspaper-o"></i> 
													Configuration Detail 
												</a>
											</li>
										</ul>
									</li>
									<?php } ?>

									<li> 
										<a><i class="fa fa-bus"></i> Booking <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li <?php if($page == 11) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('booking'); ?>"> <i class="fa fa-bus"></i>Booking Date</a>
											</li>
											<li <?php if($page == 17) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('booking/periode'); ?>"> <i class="fa fa-bus"></i>Booking Periode</a>
											</li>
										</ul>
									</li>



									<li> 
										<a><i class="fa fa-print"></i> Detail Booking <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu"> 


											<li <?php if($page == 12) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('detailbooking/bookingperiode'); ?>"> <i class="fa fa-print"></i>Detail Boooking Periode</a>
											</li>


											<li <?php if($page == 12) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('detailbooking'); ?>"> <i class="fa fa-print"></i>Detail Boooking Date</a>
											</li>

										</ul>
									</li>

									<li>
										<a><i class="fa fa-newspaper-o"></i> Notes <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li <?php if($page == 23) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('notes'); ?>"> <i class="fa fa-newspaper-o"></i>
													Notes Report
												</a>
											</li>
											<li <?php if($page == 24) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('listnotes'); ?>"> <i class="fa fa-newspaper-o"></i> 
													List Notes
												</a>
											</li>
											<li <?php if($page == 25) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('category'); ?>"> <i class="fa fa-newspaper-o"></i> 
													Category
												</a>
											</li>
											<li <?php if($page == 26) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('subject'); ?>"> <i class="fa fa-newspaper-o"></i> 
													Sub-Category
												</a>
											</li>
										</ul>
									</li>
									<li>
										<a><i class="fa fa-newspaper-o"></i> Internship <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li <?php if($page == 27) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('takeAttendance'); ?>"> <i class="fa fa-newspaper-o"></i>
													Take Attendance
												</a>
											</li>
											<li <?php if($page == 28) echo 'class="active"'; ?>>
												<a href="<?php echo base_url('attendance'); ?>"> <i class="fa fa-newspaper-o"></i> 
													Attendance
												</a>
											</li>
										</ul>
									</li>
									<li <?php if($page == 16) echo 'class="active"'; ?>>
										<a href="<?php echo base_url('pengumuman'); ?>"> <i class="fa fa-file-picture-o"></i>Pengumuman</a>
									</li>




									<?php if($this->session->userdata('role') == 'member'){ ?>
									<li <?php if($page == 1) echo 'class="active"'; ?>>
										<a href="<?php echo base_url('member'); ?>"> <i class="fa fa-home"></i> Home </a>
									</li>
									<li <?php if($page == 14) echo 'class="active"'; ?>>
										<a href="<?php echo base_url('lecturer'); ?>"> <i class="fa fa-user"></i> Lecturer </a>
									</li>
									<li>
										<li <?php if($page == 13) echo 'class="active"'; ?>>
											<a href="<?php echo base_url('booking'); ?>"> <i class="fa fa-bus"></i>Booking<span class="label label-success pull-right">Coming Soon</span></a>
										</li>
									</li>

									<?php } ?>

								</div>
							</div>

							<!-- /sidebar menu -->