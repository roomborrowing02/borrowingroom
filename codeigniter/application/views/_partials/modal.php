<div class="modal fade" id="modalPass" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Change Local Password</h4>
						</div>
						<form role="form" method="post" action="<?php echo base_url('auth/changePass'); ?>">
							<div class="modal-body">
								<div class="form-group label-floating">
									<label for="pass" class="control-label">Password</label>
									<input class="form-control" type="password" name="pass" id="pass" required>
									<span class="help-block">Input new password</span>
								</div>
								<div class="form-group label-floating">
									<label for="confPass" class="control-label">Confirm Password</label>
									<input class="form-control" type="password" id="confPass" required>
									<span class="help-block">Verify you new password</span>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-success" id="btn-update" disabled>Update</button>
							</div>
						</form>
					</div>
				</div>
			</div>