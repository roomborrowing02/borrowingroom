<!-- menu profile quick info -->
						<div class="profile">
							<div class="profile_pic">
								<img src="<?php echo base_url('assets/images/default-photo.png'); ?>" alt="..." class="img-circle profile_img">
							</div>
							<div class="profile_info">
								<span>Welcome,</span>
								<h2><?php echo strtoupper($this->session->userdata('username')); ?></h2>
							</div>
						</div>
						<!-- /menu profile quick info -->