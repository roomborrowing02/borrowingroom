<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="/">


<title>E-Shuttle</title>
<link rel="icon" href="<?php echo base_url('assets/images/icons/binusicon.png'); ?>" type="image/png">

<!-- Bootstrap -->
<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">

<!-- <link href="<?php echo base_url('assets/css/bootstrap-material-design.min.css'); ?>" rel="stylesheet"> -->
<link href="<?php echo base_url('assets/css/bootstrap-treeview.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/ripples.min.css'); ?>" rel="stylesheet">

<link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">   
<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/ts.css'); ?>" rel="stylesheet"> <!-- ts -->

<!-- Font Awesome -->
<link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="<?php echo base_url('assets/css/custom.min.css'); ?>" rel="stylesheet">