<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style>
 input[type=checkbox]{
    display: none;
  }


 input[type=checkbox] + .derp{
    /*width: 32px;*/
    height: 32px;
    padding-left: 40px;
    /*background-position: 0 0;*/
    background-repeat: no-repeat;
    /*line-height: 32px;*/
    cursor: pointer;
  }

  input[type=checkbox]:checked + .derp{
    background-position: left -32px;
  }

  .derp{
   background-image:url(<?php echo base_url('assets/csscheckbox/csscheckbox.png'); ?>);
   
  }
</style>

<div class="row" style="min-height: 768px;">
    <div class="col-md-12 col-xs-12">
      <div class="x_panel">
        <div class="title_left">
          <h2>Report Notes</h2>
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url('listnotes'); ?>" style="font-weight: bold;">Back</a></li>
            <li class="active">Update List Notes</li>
          </ul>
        </div>
        <div class="x_title">
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left input_mask" method="post" action="<?php echo base_url('notes/update'); ?>" onsubmit='return validation()' name="formupdate">
            <?php
            foreach($list->result_array() as $row):
                $notesid=$row['notesid'];
                $datedate=$row['date'];
                $userid=$row['userid'];
                $categoryid=$row['categoryid'];
                $nama=$row['nama'];
                $nim=$row['nim'];
                $subjectid=$row['subjectid'];
                $subject=$row['subject'];
                $roomid=$row['roomid'];
                $facilityid=$row['facilityid'];
                $facilityname=$row['facilityname'];
                $others=$row['others'];
                $description=$row['description'];
                $solution=$row['solution'];
                $dateline=$row['dateline'];
                $status=$row['status'];
             ?>
             <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label">Date</label>
              <input type="text" class="form-control has-feedback-left dateForm" name="dateform" autocomplete="off" value="<?php echo $datedate ?>" data-date-format="DD-MM-YYYY HH:mm:ss">
              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label col-md-6 col-sm-6 col-xs-12">Kode Dosen</label>
              <select class="searchinguser form-control has-feedback-left" name="user" style="width: 100%;">
                    <option value="<?php echo $userid; ?>"><?php echo $nim; ?> - <?php echo $nama; ?></option>
                </select>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label">Category</label>
              <select name="category" id="category" class="form-control has-feedback-left">
                <option id="" value="">- Please select the category -</option>
                <?php foreach($category->result() as $row){ ?>
                <?php if ($row->categoryid == $categoryid): ?>
                	<option id="<?php echo $row->category ?>" value="<?php echo $row->categoryid ?>" selected><?php echo $row->category ?></option>
                <?php else: ?>
                	<option id="<?php echo $row->category ?>" value="<?php echo $row->categoryid ?>"><?php echo $row->category ?></option>
                <?php endif ?>
                  
                <?php } ?>
              </select>
              <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label">Sub-Category</label>
              <select name="subject" id="subject" class="form-control">
              </select>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback room">
              <label for="control-label col-md-12 col-sm-12 col-xs-12">Room</label>
              <select name="room" id="subjectroom" class="form-control has-feedback-left" style="width: 100%;">
                <option value="">-- Select Room --</option>
               <?php foreach($room->result() as $row){ ?>
               <?php if ($row->facilityid == $facilityid): ?>
   	                <option value="<?php echo $row->facilityid ?>" selected><?php echo $row->facilityname ?> - <?php echo $row->description ?></option>
   	            <?php else: ?>
        	      <option value="<?php echo $row->facilityid ?>"><?php echo $row->facilityname ?> - <?php echo $row->description ?></option>
               <?php endif ?>
          
                <?php } ?>
              </select>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
              <label for="control-label col-md-12 col-sm-12 col-xs-12">Subject</label>
              <input type="text" class="form-control" name="others" value="<?php echo $others ?>">
            </div>
            <br>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="control-label" for="first-name">Description<span class="required">*</span>
              </label>
                <textarea name="description" class="form-control" cols="100" rows="15"><?php echo $description ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="control-label" for="first-name">Solution<span class="required">*</span>
              </label>
                <textarea name="solution" class="form-control" cols="100" rows="15"><?php echo $solution ?></textarea>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label col-md-12 col-sm-12 col-xs-12">Status</label>
              <select name="status" id="" class="form-control">
              	<?php if ($status == "On Progress"): ?>
	                <option value="On Progress" selected>On Progress</option>
	                <option value="Open">Open</option>
	                <option value="Close">Close</option>
              	<?php elseif ($status == "Open"): ?>
		            <option value="On Progress">On Progress</option>
          			<option value="Open" selected>Open</option>
		            <option value="Close">Close</option>
              	<?php elseif ($status == "Close"): ?>
	                <option value="On Progress">On Progress</option>
                	<option value="Open">Open</option>
	                <option value="Close" selected>Close</option>
              	<?php endif ?>
              </select>
            </div>
              <div class="form-group">
                <label class="col-md-1 col-sm-1 col-xs-12 control-label">
                  <br>
                  <small class="text-navy"></small>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <?php if (empty($dateline) || $dateline == '0000-00-00 00:00:00'): ?>
                    <input type="checkbox" id="derp" />
                  <?php else: ?>
                    <input type="checkbox" id="derp" checked />
                  <?php endif ?>
                  <label for="derp" class="derp">Need Follow up ?</label>
                </div>
              </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <label class="followupdate">Date : 
                  <input type="text" style="height: 33px;" class="formfollowupdate" name="followup" autocomplete="off" value="<?php echo $dateline ?>" data-date-format="DD-MM-YYYY HH:mm:ss">
                </label>
                <input type="hidden" name="notesid" value="<?php echo $notesid ?>">
                <!-- <button type="submit" class="btn btn-success">Submit</button> -->
                <input type="submit" class="btn btn-success" value="Submit">
              </div>
            </div>
            <?php endforeach ?>
          </form>
        </div>
      </div>
    </div>        
  </div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/iCheck/icheck.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
 <script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>
 <script type="text/javascript">
    $('.followupdate').hide();
    $('.dateForm').datetimepicker({


    });
    $('.formfollowupdate').datetimepicker({
      useCurrent: false,
    });
</script>
<script type="text/javascript">
  tinymce.init({
              selector: "textarea",
              plugins: [
                  "advlist autolink lists link image charmap print preview anchor",
                  "searchreplace visualblocks code fullscreen",
                  "insertdatetime media table contextmenu paste"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
          });
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $('.searchinguser').select2({
                placeholder: 'Input a ID Student / Lecturer',
                minimumInputLength: 1,
                allowClear: true,
                ajax:{
                    url: "<?php echo base_url(); ?>/notes/search_user",
                    dataType: "json",
                    delay: 250,
                    data: function(params){
                        return{
                            nimdata: params.term
                        };
                    },
                    processResults: function(data){
                        var results = [];

                        $.each(data, function(index, item){
                            results.push({
                                id: item.userid,
                                text: item.nim+" - "+item.nama,
                            });
                        });
                        return{
                            results: results
                        };
                    }
                }
            });
        });
    </script>
<script>
  if ($('#formfollowupdate').val('')) {
        $('.followupdate').hide();	
  }
  else{
  	$("input[type=checkbox]").prop("checked", true);
        $('.followupdate').show();
  }

  $("input[type=checkbox]").click(function() {
    $("input[type=checkbox]").each(function() {

      if ($(this).is(":checked")) {
        $('.followupdate').show();
        $('.formfollowupdate').val('');
      } else {
        $('.formfollowupdate').val('');
        $('.followupdate').hide();
      }
    });
  });
  $("input[type=checkbox]").each(function() {

    if ($(this).is(":checked")) {
      $('.followupdate').show();
    } else {
      $('.followupdate').hide();
    }
  });

</script>
<script type="text/javascript">
  $('.room').hide();
  $('#subjectroom').select2();
  $('#category').change(function(){
    $('#subject').prop('disabled',false);
    if ($('#category option:selected').attr('id') == "Room") {
      $('.room').show();
      // $('#subjectroom').append($("<option value='0' selected>-- Select room --</option>"));
       // $("#subjectroom")[0].selectedIndex = 0;
      $("#subjectroom").prop("selectedIndex", 0).change();
    }
    else{
      $('.room').hide();
    }
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>notes/getdata",
        dataType: "json",
        data: {
        notesid : $('#category').val()
        }, // <----send this way
        success: function(data) {
        var yourval = jQuery.parseJSON(JSON.stringify(data));
        // console.log(yourval);
        var subjecthtml="";
        if ($('#category option:selected').attr('id') == 0) {
          subjecthtml += "<option id =\"\" value =\"\"></option>";
          $('#subject').prop('disabled','true');

        }
        else{
         subjecthtml += "<option id =\"0\" value =\"\">- Please choose the subject -</option>";
            for (var i=0 ; i<yourval.length; i++)
            {
               subjecthtml += "<option value =\""+yourval[i].subjectid+"\" id =\""+ yourval[i].subjectid +"\">"+yourval[i].subject+"</option>";
            }
        }
          $('#subject').html(subjecthtml);
        
      }
      });
      return false;
  });
</script>

<script>
	$('#subject').each(function() {
	    if ($('#category option:selected').attr('id') == "Room") {
	      $('.room').show();
	    }
	    else{
	      $('.room').hide();
	    }
		$.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>notes/getdata",
        dataType: "json",
        data: {
        notesid : $('#category').val()
        }, // <----send this way
        success: function(data) {
        var yourval = jQuery.parseJSON(JSON.stringify(data));
        // console.log(yourval);
        var subjecthtml="";
        if ($('#category option:selected').attr('id') == 0) {
          subjecthtml += "<option id =\"\" value =\"\"></option>";
          $('#subject').prop('disabled','true');

        }
        else{
            for (var i=0 ; i<yourval.length; i++)
            {
               subjecthtml += "<option value =\""+yourval[i].subjectid+"\" id =\""+ yourval[i].subjectid +"\">"+yourval[i].subject+"</option>";
            }
        }
          $('#subject').html(subjecthtml);
        
      }
      });
	});
</script>

<script>
  function validation(){
    var dateform = document.getElementsByName("dateform");
    var user = document.getElementsByName("user");
    var subject = document.getElementsByName("subject");
    var room = document.getElementsByName("room");
    var others = document.getElementsByName("others");
    var followup = document.getElementsByName("followup");
    var dateform = document.getElementsByName("dateform");
    var description = tinyMCE.get('description').getContent();
    var solution = tinyMCE.get('solution').getContent();




    var isValid = true;
    for (var i = 0; i < dateform.length; i++) {
      if (dateform[i].value == "") {
        alert("Date Must be filled out");
        // return false;
        isValid = false;

      }
    }
    for (var i = 0; i < user.length; i++) {
      if (user[i].value == "") {
        alert("User Must be filled out");
        // return false;
        isValid = false;

      }
    }

    for (var i = 0; i < subject.length; i++) {
      if (subject[i].value == "") {
        alert("Sub-Category Must be filled out");
        // return false;
        isValid = false;

      }
    }

    if ($("#category option:selected").attr('id') == "Room") {
      for (var i = 0; i < room.length; i++) {
        if (room[i].value == "" || room[i].value == "0") {
          alert("Room Must be filled out");
          // return false;
          isValid = false;

        }
      }
    } 

    for (var i = 0; i < others.length; i++) {
      if (others[i].value == "") {
        alert("Subject Must be filled out");
        // return false;
        isValid = false;

      }
    }

    if (description == '' || description == null) {
      alert("Description Must be filled out");
      isValid = false;
    } 

    if (solution == '' || solution == null) {
      alert("Solution Must be filled out");
      isValid = false;
    } 
    if ($('#derp').is(':checked')) {
        for (var i = 0; i < followup.length; i++) {
        if (followup[i].value == "") {
          alert("Followup Date Must be filled out");
          // return false;
          isValid = false;

        }
      }
    } 
    if (!isValid) {
      alert('Submit Fail');
      return false;
    }
    alert('Data Updated');
    return isValid;

  }
</script>
