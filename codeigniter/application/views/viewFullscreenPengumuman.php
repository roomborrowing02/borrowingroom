<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LSC</title>
    <link rel="icon" href="<?php echo base_url('assets/images/icons/binusicon.png'); ?>" type="image/png">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url('assets/vendors/animate.css/animate.min.css');?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('assets/build/css/custom.min.css');?>" rel="stylesheet">
    <style type="text/css">
      html,body{
         height:100%;
      }
      .login{
        background-color: black;
      }
      .carousel {
        background-color: black;
        height: 100%;
        margin: 0 auto;
        }

      /* Declare heights because of positioning of img element */
      .carousel .item {
        height: 768px;
        background-color: #777;
      }
      .carousel-inner > .item > img {
        position: absolute;
        margin: 0 auto;
        min-width: 100%;
        height: 100%;
      }   
      .carousel,.item,.active{
         height:100%;
       }
       .carousel-inner{
          height:100%;
      }
      </style>
        </head>
        <body class="login">
  <div class="bs-example">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for carousel items -->
            <div class="carousel-inner">
              <?php $count = 0; 
            $indicators = ''; 
            foreach ($data as $row): 
            $count++; 
            if ($count === 1) { 
               $class = 'active'; 
             }else{ 
               $class = '';}?> 
            <!-- Carousel indicators -->
      <!-- <ol class="carousel-indicators">        -->
                 <!-- <li data-target="#myCarousel" data-slide-to="$count" class="$class"></li><br> -->
      <!-- </ol> -->
      <!-- End Carousel indicatos -->
            <div class="item <?php echo $class; ?>">
                    <img src="<?php echo base_url()?>assets/images/upload/<?php echo $row->content;?>" onerror="this.src='<?php echo base_url()?>assets/images/error/noimages.jpg';">
<!--                     <div class="carousel-caption">
                    <h3><?php echo $row->description; ?></h3>
                    </div> -->
                </div>
                <!-- End Wrapper for carousel items -- >
             <?php endforeach;?> 
            </div>
            <!-- Carousel controls -->
           <!--  <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a> -->
        </div>
    </div>
  </body>
</html>
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<script>
  $("#myCarousel").carousel({
    interval: 10000,
    pause: "false"
  });
</script>
<script>
 setInterval(function() {
    window.location.reload();
  }, 10800000); //3 jam  (1000 = 1s)
</script>