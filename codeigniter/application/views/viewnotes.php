<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style>
 input[type=checkbox]{
    display: none;
  }


 input[type=checkbox] + .derp{
    /*width: 32px;*/
    height: 32px;
    padding-left: 40px;
    /*background-position: 0 0;*/
    background-repeat: no-repeat;
    /*line-height: 32px;*/
    cursor: pointer;
  }

  input[type=checkbox]:checked + .derp{
    background-position: left -32px;
  }

  .derp{
   background-image:url(<?php echo base_url('assets/csscheckbox/csscheckbox.png'); ?>);
   
  }
</style>

<div class="row" style="min-height: 768px;">
    <div class="col-md-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Report Notes</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left input_mask" name="forminsert" method="post" action="<?php echo base_url('notes/insert'); ?>" onsubmit='return validation()'>
               <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label">Date</label>
              <input type="text" class="form-control has-feedback-left dateForm" name="dateform" autocomplete="off" value="<?php echo $date ?>">
              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label col-md-6 col-sm-6 col-xs-12">Kode Dosen</label>
              <select class="searchinguser form-control has-feedback-left" name="user" style="width: 100%;">
                    <option value=""></option>
                </select>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label col-md-6 col-sm-6 col-xs-12">Category</label>
              <select name="category" id="category" class="form-control has-feedback-left">
                <option id="" value="">- Please select the category -</option>
                <?php foreach($category->result() as $row){ ?>
                  <option id="<?php echo $row->category ?>" value="<?php echo $row->categoryid ?>"><?php echo $row->category ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label">Sub-Category</label>
              <select name="subject" id="subject" class="form-control" disabled="">
              </select>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback room">
              <label for="control-label col-md-12 col-sm-12 col-xs-12">Room</label>
              <select name="room" id="subjectroom" class="form-control has-feedback-left" style="width: 100%;">
                <option value="0">-- Select Room --</option>
               <?php foreach($room->result() as $row){ ?>
                <option value="<?php echo $row->facilityid ?>"><?php echo $row->facilityname ?> - <?php echo $row->description ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
              <label for="control-label">Subject</label>
              <input type="text" name="others" class="form-control" placeholder="Subject" autocomplete="off">
            </div>
            <br>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <label class="control-label" for="first-name">Description<span class="required">*</span>
              </label>
                <textarea id="description" name="description" class="form-control" cols="100" rows="15"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
              <label class="control-label" for="first-name">Solution<span class="required">*</span>
              </label>
                <textarea id="solution" name="solution" class="form-control" cols="100" rows="15"></textarea>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              <label for="control-label col-md-12 col-sm-12 col-xs-12">Status</label>
              <select name="status" id="" class="form-control">
                <option value="On Progress">On Progress</option>
                <option value="Open">Open</option>
                <option value="Close">Close</option>
              </select>
            </div>
              <div class="form-group">
                <label class="col-md-1 col-sm-1 col-xs-12 control-label">
                  <br>
                  <small class="text-navy"></small>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="checkbox" id="derp" />
                  <label for="derp" class="derp">Need Follow up ?</label>
                </div>
              </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <label class="followupdate">Date : 
                  <input type="text" style="height: 33px;" class="formfollowupdate" name="followup" autocomplete="off">
                </label>
                <!-- <button type="submit" class="btn btn-success">Submit</button> -->
                <input type="submit" class="btn btn-success" value="Submit">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>        
  </div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/iCheck/icheck.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
 <script src="<?php echo base_url('assets/select2-4.0.6-rc.1/select2-4.0.6-rc.1/dist/js/select2.min.js'); ?>"></script>
 <script type="text/javascript">
    $('.followupdate').hide();
    $('.dateForm').datetimepicker({
      format: "MMM-DD-YYYY h:mm:ss"
    });
    $('.formfollowupdate').datetimepicker({
      format: "MMM-DD-YYYY h:mm:ss"
    });
</script>
 <script type="text/javascript">
  tinymce.init({
      selector: "textarea",
      plugins: [
          "advlist autolink lists link image charmap print preview anchor",
          "searchreplace visualblocks code fullscreen",
          "insertdatetime media table contextmenu paste"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  });
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $('.searchinguser').select2({
                placeholder: 'Input a ID Student / Lecturer',
                minimumInputLength: 1,
                allowClear: true,
                ajax:{
                    url: "<?php echo base_url(); ?>/notes/search_user",
                    dataType: "json",
                    delay: 250,
                    data: function(params){
                        return{
                            nimdata: params.term
                        };
                    },
                    processResults: function(data){
                        var results = [];

                        $.each(data, function(index, item){
                            results.push({
                                id: item.userid,
                                text: item.nim+" - "+item.nama,
                            });
                        });
                        return{
                            results: results
                        };
                    }
                }
            });
        });
    </script>
<script>
$("input[type=checkbox]").click(function() {
  $("input[type=checkbox]").each(function() {
    if ($(this).is(":checked")) {
      $('.followupdate').show();
      $('.formfollowupdate').val('');
    } else {
      $('.formfollowupdate').val('');
      $('.followupdate').hide();
    }
  });
});

</script>
<script type="text/javascript">
  $('.room').hide();
  $('#subjectroom').select2();
  $('#category').select2();
  $('#category').change(function(){
    $('#subject').prop('disabled',false);
    if ($('#category option:selected').attr('id') == "Room") {
      $('.room').show();
    }
    else{
      $('.room').hide();
    }
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>notes/getdata",
        dataType: "json",
        data: {
        notesid : $('#category').val()
        }, // <----send this way
        success: function(data) {
        var yourval = jQuery.parseJSON(JSON.stringify(data));
        // console.log(yourval);
        var subjecthtml="";
        if ($('#category option:selected').attr('id') == 0) {
          subjecthtml += "<option id =\"\" value =\"\"></option>";
          $('#subject').prop('disabled','true');

        }
        else{
         subjecthtml += "<option id =\"0\" value =\"\">- Please choose the subject -</option>";
            for (var i=0 ; i<yourval.length; i++)
            {
               subjecthtml += "<option value =\""+yourval[i].subjectid+"\" id =\""+ yourval[i].subject +"\">"+yourval[i].subject+"</option>";
            }
        }
          $('#subject').html(subjecthtml);
        
      }
      });
      return false;
  });
</script>

<script>
  function validation(){
    var dateform = document.forms["forminsert"]["dateform"];               
    var user = document.forms["forminsert"]["user"];    
    var category = document.forms["forminsert"]["category"];  
    var subject =  document.forms["forminsert"]["subject"];  
    var room = document.forms["forminsert"]["room"];  
    var others = document.forms["forminsert"]["others"];
    var status = document.forms["forminsert"]["status"];
    var followup = document.forms["forminsert"]["followup"];
    var description = tinyMCE.get('description').getContent();
    var solution = tinyMCE.get('solution').getContent();

     // e.stopImmediatePropagation();
    if (dateform.value == "")                                  
    { 
        alert("Please fill the date."); 
        return false; 
    }
    if (user.value == "")                                  
    { 
        alert("Please fill the ID."); 
        return false; 
    }
    if (category.value == "")                                  
    { 
        alert("Please fill the Category."); 
        return false; 
    }
    if (subject.value == "")                                  
    { 
        alert("Please fill the Sub-Category."); 
        return false; 
    }
    if ($("#category option:selected").attr('id') == "Room") {
      if (room.value == "" || room.value == "0")                                  
      { 
          alert("Please fill the Room."); 
          return false; 
      }
    } 

    if (others.value == "")                                  
    {  
        alert("Please fill the Subject."); 
        return false; 
    }
    if (description == '' || description == null) {
      alert("Description Must be filled out");
      return false;
    } 

    if (solution == '' || solution == null) {
      alert("Solution Must be filled out");
      return false;
    } 
    if ($('#derp').is(':checked')) {
      if (followup.value == "")                                  
      {  
          alert("Please fill the Date Followup"); 
          return false; 
      }
    } 
    alert('Data Inserted');
    return true;  
  }
</script>

