<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"  rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>"  rel="stylesheet">
<!-- bootstrap-datetimepicker -->
<link href="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>"  rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">

<style>
span.desc {
    color: red;
}     
</style>

<div id="page-wrapper">
    <div class="row" style="min-height:850px">
        <div class="page-title">
            <div class="title_left">
                <h3>Manage Campus <small></small></h3>
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('index.php/admin'); ?>">Home</a></li>
                    <li class="active">Campus</li>
                </ul>
            </div>
            <div class="title_right">
                <div class="left_col" role="main" >
                    <a class="btn btn-sm btn-info pull-right"
                    data-toggle="modal"
                    data-for="insert"
                    data-target="#modal_insertcampus" 
                    >Insert
                </a>
            </div>
        </div>

        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-university"></i> List Campus<small></small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <?php if($data->num_rows() > 0){ ?>
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>CampusCode</th>
                                <th>CampusName</th>
                                <th>CampusAddress</th>
                                <th>CampusEmail</th>
                                <th>PhoneOffice</th>
                                <th>Detail</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>   
                            <?php foreach($data->result() as $row){ ?>    
                                <tr>
                                    <td><?php echo $row->campuscode; ?></td>
                                    <td><?php echo $row->campusname; ?></td>
                                    <td><?php echo $row->campusaddress; ?></td>
                                    <td><?php echo $row->campusemail; ?></td>
                                    <td><?php echo $row->phoneoffice; ?></td> 
                                    <td><?php echo $row->detail; ?></td>
                                    <td>
                                        <a class="btn btn-xs btn-warning"
                                        data-toggle="modal"
                                        data-for="update"
                                        data-target="#modalupdate<?php echo $row->campusid; ?>"
                                        data-href="<?php echo base_url('index.php/campus/update') ?>"
                                        data-ajax="<?php echo base_url('index.php/campus/getData') ?>"
                                        data-id="<?php echo $row->campusid; ?>">
                                    Update</a>
                                    <a class="btn btn-xs btn-danger" 
                                    data-toggle="modal" 
                                    data-target="#modalDel" 
                                    data-id="<?php echo $row->campusid; ?>" 
                                    data-delete="<?php echo 'Campus';?>">Delete</a>


                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
</div>

<!-- Modal Insert -->
<div class="modal fade" id="modal_insertcampus" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Manage Campus</h4>
            </div>

            <form role="form" method="post" id="campusInsert" class="form-horizontal form-label-left" action="<?php echo base_url('campus/insert') ?>">

                <div class="modal-body">


                    <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campuscode">CampusCode<span class="required" >*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="campuscode" id="campuscode" placeholder="Must be filled" required type="text"  autocomplete="off">
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campusname">CampusName<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="campusname" id="campusnameModal"  placeholder="Must be filled" required="required"  type="text"  autocomplete="off">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campusaddress" >CampusAddress<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="campusaddress" id="campusaddressModal" placeholder="JL.      " required="required" type="text"  autocomplete="off"> 
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campusemail">CampusE-Mail<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="campusemail" id="campusemailModal" placeholder=test123@gmail.com" required="required" type="email"  autocomplete="off">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phoneoffice">Phone Office<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="12" data-validate-words="2" name="phoneoffice" id="phoneofficeModal"  placeholder="999-9999999" pattern="^\d{3}-\d{7}$" required="required" type="text"  autocomplete="off">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="detail">Detail<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="12" data-validate-words="2" name="detail" 
                            id="detailModal" placeholder="Pickup - Dropoff Campus" required="required" type="text"  autocomplete="off">
                        </div>
                    </div>
                    <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-info " id="submitInsert" value="Save"> 
                <input type="hidden" name="CampusId" id="campusid"> 
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- Modal update-->
<?php
foreach($data->result_array() as $row):
    $campusid=$row['campusid'];
    $campuscode2=$row['campuscode'];
    $campusname2=$row['campusname'];
    $campusaddress2=$row['campusaddress'];
    $campusemail2=$row['campusemail'];
    $phoneoffice2=$row['phoneoffice'];
    $detail2=$row['detail'];
    ?>
    <div class="modal fade" id="modalupdate<?php echo $campusid; ?>" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Manage Campus</h4>
                </div>

                <form role="form" method="POST" class="form-horizontal form-label-left campusUpdate" action="<?php echo base_url('campus/update') ?>">

                    <div class="modal-body">

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campuscode">CampusCode<span  class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="campuscode" placeholder="Must be filled" required type="text" value="<?php echo $campuscode2; ?>"  autocomplete="off">
                            </div>
                        </div>


                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campusname">CampusName<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="campusname"   placeholder="Must be filled" required="required"  type="text" value="<?php echo $campusname2; ?>"  autocomplete="off">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campusaddress" >CampusAddress<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="campusaddress" value="<?php echo $campusaddress2; ?>" placeholder="JL.      " required="required" type="text"  autocomplete="off">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campusemail">CampusE-Mail<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="campusemail" value="<?php echo $campusemail2; ?>" placeholder="test123@gmail.com" required="required" type="email"  autocomplete="off">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phoneoffice">Phone Office<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="12" data-validate-words="2" name="phoneoffice" value="<?php echo $phoneoffice2; ?>"  placeholder="999-9999999" pattern="^\d{3}-\d{9}$" required="required" type="text"  autocomplete="off">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="detail">Detail<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input  class="form-control col-md-7 col-xs-12" data-validate-length-range="12" data-validate-words="2" name="detail" value="<?php echo $detail2; ?>" placeholder="Pickup - Dropoff Campus" required="required" type="text"  autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-info submitUpdate" value="Save">
                    <input type="hidden" name="CampusId" value="<?php echo $campusid ?>"> 
                </div>
            </div>
        </form>
    </div>
</div>
</div>

<?php endforeach;?>

<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Campus</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this campus ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('index.php/campus/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deletecampus">Delete</button>
                    <input type="hidden" name="campusId" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- tablesorter -->
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- script -->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<!-- datatables.net -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap-datetimepicker -->    
<script src="<?php echo base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>


<script>
    $(document).ready(function() {
        $('#table_sort').DataTable();
        $('.select2_single').select2();
        var handleDataTableButtons = function() {
            if ($("#datatable-responsive").length) {
                $("#datatable-responsive").DataTable({
                    dom: "Bfrtip",
                    order: [[ 1, "asc" ]],
                    buttons: [
        // {
        //     extend: "copy",
        //     className: "btn-sm"
        // },
        // {
        //     extend: "csv",
        //     className: "btn-sm"
        // },
        // {
        //     extend: "excel",
        //     className: "btn-sm"
        // },
        // {
        //     extend: "pdfHtml5",
        //     className: "btn-sm"
        // },
        // {
        //     extend: "print",
        //     className: "btn-sm"
        // },
],
responsive: true
});
            }
        };
        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        TableManageButtons.init();
    });
</script>

<!-- Validation Insert and Update-->
<script>

  $('#campusInsert').validate({       
            errorElement: 'span',
            errorClass: 'desc',
          
              rules: {
                
               campuscode: {
              required: true,
              minlength:2,
          },
          campusname: {
              required: true,
              minlength:4,
          },
          campusaddress: {
              required: true,
              minlength:5,
          },
          campusemail: {
              required: true,
              minlength:5,
          },
          phoneoffice: {
              required: true,
              minlength:10,
          },
          detail: {
              required: true,
              minlength:5,
          },
               },
                submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
    });
          
  $('.campusUpdate').each(function () {
    $(this).validate({
      errorElement: 'span',
      errorClass: 'desc',
      rules: {
          campuscode: {
              required: true,
              minlength:2,
          },
          campusname: {
              required: true,
              minlength:4,
          },
          campusaddress: {
              required: true,
              minlength:5,
          },
          campusemail: {
              required: true,
              minlength:5,
          },
          phoneoffice: {
              required: true,
              minlength:10,
          },
          detail: {
              required: true,
              minlength:5,
          },
      },
      message: {
          campuscode: {
              required: true
          },
           campusname: {
              required: true
          },
           campusaddress: {
              required: true
          },
           category: {
              required: true
          },
           campusemail: {
              required: true
          },
           phoneoffice: {
              required: true
          },
           detail: {
              required: true
          },

      },
      submitHandler: function(form) {
          alert("Successfull");
        // console.log("asdasdas");
        form.submit();
        }
      });
  });

    $('#deletecampus').click(function() {
        /* Act on the event */
        alert("Sucessfull");
    });
</script>
