<link href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.css'); ?>" rel="stylesheet">
<style>
  span.desc {
    color: red;
}
</style>

<div id="page-wrapper">
  <div class="row" style="min-height: 768px">
       <div class="page-title">
        <div class="title_left">
           <h3>Manage Shift <small></small></h3>
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>">Home</a></li>
            <li class="active">Shift</li>
        </ul>
              </div>
              <div class="title_right">
        <div class="left_col" role="main" >
            <a class="btn btn-sm btn-info pull-right"
               data-toggle="modal"
               data-for="insert"
               data-target="#modal_insertshift"
               >Insert
            </a>
        </div>
      </div>
      <?php if($data->num_rows() > 0){ ?>
<table id="table_sort" class="cell-border" style="width:100%">
      <thead>
            <tr>
                <th>Shift</th>
                <th>Type</th>
                <th>Act</th>
            </tr>
      </thead>
      <tbody>
             <?php foreach($data->result() as $row){ ?>
           <tr>
             <td><?php echo $row->shift; ?></td>
             <td><?php echo $row->shiftType; ?></td>
             <td>
               <a class="btn btn-xs btn-warning"
                data-for="update"
                data-toggle="modal"
                data-target="#modal_edit<?php echo $row->shiftid; ?>"
                data-ajax="<?php echo base_url('tambahshift/getData') ?>"
                data-id="<?php echo $row->shiftid; ?>"
                >Update</a>||
              <a class="btn btn-xs btn-danger" 
                data-toggle="modal" 
                data-target="#modalDel" 
                data-id="<?php echo $row->shiftid; ?>" 
                data-delete="<?php echo $row->shift; ?>">Delete</a>
             </td>
           </tr>
           <?php } ?>
      </tbody>
</table>
            <?php } else { ?>
                <div class="well">There is no shift created yet!</div>
            <?php } ?>
      </div>
      </div>

<!-- Modal Insert -->
<div class="modal fade" id="modal_insertshift"  tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">

  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h3 class="modal-title" id="myModalLabel">Insert Shift</h3>
    </div>
      <form role="form" method="post" id="forminsert" class="form-horizontal form-label-left">
        <div class="modal-body">
         <div class="item form-group">
            <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                <input type="text" class="form-control" name="shift" id="shiftmodal" placeholder="ex: 07:20" autocomplete="off">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-time"></span>
                </span>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="shiftbutton" class="btn btn-info">Save</button>
          <!--  <input type="submit" class="btn btn-info updatebutton" id="shiftbutton" value="Save"> -->
          <input type="hidden" name="Shiftid" id="shiftid" >
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Update -->
        <?php
        foreach($data->result_array() as $row):
            $shiftid=$row['shiftid'];
            $shift=$row['shift'];
         ?>
<div class="modal fade" id="modal_edit<?php echo $shiftid;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Edit Shift</h3>
            </div>
            <form role="form" method="post" class="form-horizontal form-label-left formupdate" action="<?php echo base_url('tambahshift/update') ?>">

                <div class="modal-body">
                    <div class="form-group label-floating">
                    <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                        <input type="text" class="form-control" name="shift" placeholder="ex: 07:20" autocomplete="off" value="<?php echo $shift ?>">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                  </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <!--   <button type="submit" class="btn btn-info updatebutton">Save</button> -->
                      <input type="submit" class="btn btn-info updatebutton" value="Save">
                    <input type="hidden" name="Shiftid" value="<?php echo $shiftid; ?>">
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach;?>

<!-- Modal Delete -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Shift</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete this Shift ?
            </div>
            <form role="form" method="post" action="<?php echo base_url('tambahshift/delete'); ?>">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="deleteshift">Delete</button>
                    <input type="hidden" name="Shiftid" id="deleteId">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/jquery.tablesorter.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tablesorter/tables.js'); ?>"></script>
<!-- <script src="<?php echo base_url('assets/js/jquery.dataTables.js'); ?>"></script>jquery.dataTables.min -->
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/dist/bootstrap-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/validate/dist/jquery.validate.min.js'); ?>"></script>

<script>
    // $(document).ready(function() {
    //     $('#table_sort').DataTable();
    // } );

    $(document).ready(function() {
    $('#table_sort').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );

</script>

<script type="text/javascript">
    $('.clockpicker').clockpicker();
</script>

<script>
  $(document).ready(function () {
        $.validator.addMethod("time", function(value, element) {  
    return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);  
    }, "Please enter a valid time.");
      $('#forminsert').validate({ // initialize the plugin
      errorElement: 'span',
      errorClass: 'desc',
          rules: {
              shift: {
                  required: true,
                  time: true
              }
          }
      });
      
      $('#shiftbutton').click(function() {
        /* Act on the event */
        if($('#forminsert').valid()){
            $.ajax({
              type: 'POST',
              url: "<?php echo base_url(); ?>tambahshift/cekvalidasi",
              dataType: "json",
              data: {
              shift : $('#shiftmodal').val()
              }, // <----send this way
              success: function(data) {
                alert("Sucessfull");
              }
            });
            return false;
        }
      });


 $('.formupdate').each(function () {
       $(this).validate({
         errorElement: 'span',
          errorClass: 'desc',
             rules: {
           shift: {
           required: true,
           time: true
            }
            },
            message: {
                shift: {
                  required: true,
                  time: true
                }
            },
            submitHandler: function(form) {
              alert("Successfull");
              // console.log("asdasdas");
              form.submit();
            }
        });
      });
  });

   $('#deleteshift').click(function() {
        /* Act on the event */
        alert("Sucessfull");
      });
</script>