(function($){
    //Toggle chat button
    $('.chat-nav-button').click(function (e){
        if( $(this).hasClass('close-chat-nav') ){
            $(this).removeClass('close-chat-nav');
            $('.chat-nav').animate({ right: '-200px' });

            //Move chat room and hidden chat when available
            if( $.chat.activeChatUsers.length > 0 ){
                $('.chat-room').animate({ right: '30px' });
                $('#hidden-chat').animate({ right: '230px' });
            }
        } else {
            $(this).addClass('close-chat-nav');
            $('.chat-nav').animate({ right: '0' });

            //Move chat room and hidden chat when available
            if( $.chat.activeChatUsers.length > 0 ){
                $('.chat-room').animate({ right: '230px' });
                $('#hidden-chat').animate({ right: '430px' });
            }
        }
    });

    function updateHiddenChatValue(index){
        $('#hidden-chat tbody').html('');
        $.each($.chat.activeChatUsers, function(idx, key){
            if(idx != index)
                $('#hidden-chat tbody').append('<tr index="' + idx + '" onclick="$.chat.hiddenChatRowClick(' + idx + ')"><td>' + key + '</td></tr>');
        });
    }

    function isWebUserClient(data){
        if( data.indexOf('ASA') > -1 || data.indexOf('ALC') > -1 ) return false;
        else return true;
    }

    function insertTreeNode(tree, data){
        var datas = {text: data};
        var room = ( data.indexOf('ASA') > -1 ) ? data.substring(0,7) : data.substring(0, 4);

        if( tree.length == 0 ) tree.push({ text: room, nodes: [datas] });
        else {
            var index = tree.findIndex(function(data){ 
                            return data.text == room;
                        });

            if( index == -1 ) tree.push({ text: room, nodes: [datas] });
            else tree[index]['nodes'].push( datas );
        }
    }

    $.chat = {
        "activeChatUsers": [], //Active users in array
        "currentChat": 0, //Active user index to chat
        "initial": "", //Current user initial
        "socket": io.connect('10.35.144.12:1515'), //Initialize socket
        init: function(initial){
            $('.chat-nav').animate({ right: '-200px' });

            $.chat.initial = initial.toUpperCase();
            $.chat.socket.emit('userConnected', initial); //Emit this user is online
            $.chat.socket.on('updateUsers', function(data){ //Retrive users online
                if(data.length == 1) $('#treeview').html('<tr><td>There is no user online..</td></tr>');
                else {
                    var tree = [];
                    $.each(data.sort(), function(index, key){
                        if(key != $.chat.initial){
                            if( isWebUserClient(key) ) tree.push({ text: key});
                            else insertTreeNode( tree, key );
                        }
                    });

                    $('#treeview').treeview({
                        levels: 1,
                        expandIcon: 'glyphicon glyphicon-chevron-right',
                        collapseIcon: 'glyphicon glyphicon-chevron-down',
                        selectedBackColor: "#00796B",
                        data: tree
                    });

                    $('#treeview').on('nodeUnselected', function (event, node) {
                        if(node.nodes == undefined) $.chat.chatRoom( node.text );
                    });
                }
            });
            $.chat.socket.on('newMessage', function(data){ //Retrieve new message
                if( $.inArray(data[0], $.chat.activeChatUsers) == -1 ) $.chat.chatRoom(data[0]);

                var index = ($.inArray(data[0], $.chat.activeChatUsers) > -1) ? $.inArray(data[0], $.chat.activeChatUsers) : 0;

                if( $.chat.activeChatUsers.length > 1 ) $.chat.hiddenChatRowClick(index);

                $('#chat-room-' + $.chat.activeChatUsers[ index ] + ' .chat-room-body').append('<div class="chat-messages pull-right"><span>' + data[2] + '</span></div>').animate({ scrollTop: $('.chat-room-body').get(0).scrollHeight });
            });
            $.chat.socket.on('userIsBusy', function(data){
                $('#chat-room-' + $.chat.activeChatUsers[ $.chat.currentChat ] + ' .chat-room-body').append('<div class="user-disconnected"><span>User is currently busy with ' + data + '</span></div>').animate({ scrollTop: $('.chat-room-body').get(0).scrollHeight });
            });
            $.chat.socket.on('userDisconnected', function(data){
                if( $.inArray(data, $.chat.activeChatUsers) > -1 )
                    $('#chat-room-' + $.chat.activeChatUsers[ $.inArray(data, $.chat.activeChatUsers) ] + ' .chat-room-body').append('<div class="user-disconnected"><span>User disconnected</span></div>').animate({ scrollTop: $('.chat-room-body').get(0).scrollHeight });
            });
        },
        chatRoom: function(recipient){ //Start a new chat
            if( $.inArray(recipient, $.chat.activeChatUsers) == -1 ){
                $.chat.activeChatUsers.push(recipient);
                $.chat.currentChat = $.inArray(recipient, $.chat.activeChatUsers); //Update current user index

                if( $.chat.activeChatUsers.length > 1 ) $.chat.initHiddenChat(); 

                var html = '<nav class="chat-room" id="chat-room-' + recipient + '">' +
                                '<div class="chat-room-header close-chat-room">' +
                                   '<span class="from">' + recipient + '</span>' +
                                    '<i class="fa fa-close pull-right closeChat" onclick="$.chat.closeChat(\'' + recipient + '\')"></i>' +
                                    '<i class="fa fa-minus pull-right toggleChat" onclick="$.chat.toggleChat(\'' + recipient + '\')"></i>' +
                                '</div>' +
                                '<div class="chat-room-body"></div>' +
                                '<div class="chat-room-footer">' +
                                    '<form class="sendChat" method="post">' +
                                        '<input class="chat-input" id="chat-input-' + recipient + '" placeholder="Type text..." name="chat-message" autocomplete="off">' +
                                    '</form>' +
                                '</div>' +
                            '</nav>';

                $('#wrapper').append(html);
                $('input[name=chat-message]').focus();
                $('.sendChat').submit(function (e){
                    e.preventDefault();
                    var data = [
                        $.chat.initial, //sender
                        $.chat.activeChatUsers[ $.chat.currentChat ], //recipient
                        $('#chat-input-' + $.chat.activeChatUsers[ $.chat.currentChat ]).val() //message
                    ];

                    $.chat.socket.emit('sendMessages', data);
                    $('input[name=chat-message]').val('');
                    $('.chat-room-body').append('<div class="chat-messages"><span>' + data[2] + '</span></div>').animate({ scrollTop: $('.chat-room-body').get(0).scrollHeight });
                });
            }
        },
        closeChat: function(recipient){ //Close current active chat
            var index = $.inArray(recipient, $.chat.activeChatUsers);
            $.chat.socket.emit('endChat', $.chat.activeChatUsers[ $.chat.currentChat ]);

            $.chat.activeChatUsers.splice(index, 1);
            $.chat.currentChat = $.chat.activeChatUsers.length-1;

            $('#chat-room-' + recipient).remove();
            $.chat.updateHiddenChat();
        },
        toggleChat: function(recipient){ //Animate toggle chat room
            if( $('#chat-room-' + recipient + ' .chat-room-header').hasClass('close-chat-room') ){
                $('#chat-room-' + recipient + ' .chat-room-header').removeClass('close-chat-room');
                $('#chat-room-' + recipient).animate({ bottom: '-225px' });
                $('#chat-room-' + recipient + ' .toggleChat').removeClass('fa-minus');
                $('#chat-room-' + recipient + ' .toggleChat').addClass('fa-plus');
            } else {
                $('#chat-room-' + recipient + ' .chat-room-header').addClass('close-chat-room');
                $('#chat-room-' + recipient).animate({ bottom: '0' });
                $('#chat-room-' + recipient + ' .toggleChat').removeClass('fa-plus');
                $('#chat-room-' + recipient + ' .toggleChat').addClass('fa-minus');
                $('input[name=chat-message').focus();
            }
        },
        initHiddenChat: function(){ //Initialize new hidden chat
            if( $.chat.activeChatUsers.length == 2 ){
                var html = '<nav id="hidden-chat" onclick="$.chat.hiddenChatClick()"><i class="fa fa-list"></i>' +
                                '<table><tbody>' + 
                                    '<tr index="0" onclick="$.chat.hiddenChatRowClick(0)"><td>' + $.chat.activeChatUsers[0] + '</td></tr>' +
                                '</tbody></table></nav>';

                $('#wrapper').append(html);
                $('#hidden-chat table').hide();
            } else updateHiddenChatValue();
            
            $('#chat-room-' + $.chat.activeChatUsers[ $.chat.currentChat ]).hide();
        },
        updateHiddenChat: function(){ //Update hidden chat
            if( $.chat.activeChatUsers.length == 1 ) $('#hidden-chat').remove();
            else updateHiddenChatValue();

            $('#chat-room-' + $.chat.activeChatUsers[ $.chat.currentChat ]).show();
        },
        hiddenChatClick: function(){ $('#hidden-chat table').toggle(); },
        hiddenChatRowClick: function(index){
            updateHiddenChatValue(index);
            $('#chat-room-' + $.chat.activeChatUsers[ $.chat.currentChat ]).hide();

            $.chat.currentChat = index;
            $('#chat-room-' + $.chat.activeChatUsers[ $.chat.currentChat ]).show();
        }
    };    
})(jQuery);