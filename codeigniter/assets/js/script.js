$(document).ready(function(){
    $('#modalDel').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);

        $(this).find('.modal-body').text( 'Are you sure want to delete \"' + button.data('delete') + '\"?' );
        $(this).find('#deleteId').val( button.data('id') );
    });

    $('#modalAct').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);

        $(this).find('.modal-body').text( 'Are you sure want to activate \"' + button.data('delete') + '\"?' );
        $(this).find('#activateId').val( button.data('id') );
    });

    $('#modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('form').attr('action', button.data('href'));

        if( button.data('for') == 'update' ) {
            var temp = $(this);
            $('.modal-body').hide();

            $.post(button.data('ajax'), {id:button.data('id')}, function(data){
                $.each($.parseJSON( data ), function(index, value){
                    temp.find('#' + index).val( value ).trigger('change');
                });

                $('.modal-body').fadeIn(500);
            });
        }
    });

    $('#modal').on('hidden.bs.modal', function (event) {
        $('.form-control').val('');
    });
});