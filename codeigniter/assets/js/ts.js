// var naModule = angular.module('NewAttention', ['ngSanitize','ngRoute']);

var naModule = angular.module('app', ['ngSanitize','ngRoute','content-editable']);




naModule.filter('toArray', function () {
	return function (obj, addKey) {
		if (!angular.isObject(obj)) return obj;
		if ( addKey === false ) {
			return Object.keys(obj).map(function(key) {
				return obj[key];
			});
		} else {
			return Object.keys(obj).map(function (key) {
				var value = obj[key];
				return angular.isObject(value) ?
				Object.defineProperty(value, '$key', { enumerable: false, value: key}) :
				{ $key: key, $value: value };
			});
		}
	};

});

naModule.run(function($rootScope,$location,$route) {
	$rootScope.baseurl = baseurl;
	$rootScope.cpmurl = "http://10.35.144.15/cpm-engine/public/";
	$rootScope.$on( "$routeChangeSuccess", function(event, next, current) {
		if ( current !== undefined ){
			window.location.reload()
		}
	});
	//======================
	//===== Variable
	$rootScope.today = new Date();
	$rootScope.tomorrow = new Date(new Date().setDate( $rootScope.today.getDate()+1 ));
	$rootScope.ajaxCount = 0;
	$rootScope.onAjaxLoadFinished = false;
	$rootScope.overlayOn = false;
	$rootScope.baseurl = baseurl;

    //======================
    //===== Static
    $rootScope.spinnerURI = $rootScope.baseurl+'default.svg';

	//=====================
	//===== Function
	$rootScope.ajaxFactory = function(type,url,param,onsuccess,onfailed=function(data){}){

		$rootScope.ajaxCount +=1;
		$.ajax({
			url : url,
			data : param,     
			crossDomain:true,
			crossOrigin:true,
			type : type,
			async : true,
			cache : false,
			timeout : 30000,  
			error : function(data){
				onfailed(data.responseJSON);
			},
			success : function(data){
				$rootScope.ajaxCount -=1;
				onsuccess(data);
				if ( $rootScope.onAjaxLoadFinished == false && $rootScope.ajaxCount == 0 ){
					$rootScope.onAjaxLoadFinished = true;
					// $rootScope.$apply();
				}
			}
		});
	};

	$rootScope.isObjectEmpty = function(asd){
		return Object.keys(asd).length == 0;
	}

	$rootScope.isObject = function(asd){
		return Object.prototype.toString.call( asd ) == '[object Object]';
	}
	$rootScope.strContain = function(di,kata){
		if (di === null || di === undefined) return true;
		if ( kata == '' ) return true;
		return di.includes(kata);
	}

	$rootScope.openNewWindow = function(url){
		window.open(url);
	}

	$rootScope.openNewTab = function(url){
		window.open(url,'_self');
	}

	$rootScope.redirect = function(url){
		window.location.replace(url);
	}

	$rootScope._tospace= function(argument) {
		return argument.replace('_',' ');
	}

	$rootScope.removeSpace= function(argument) {
		return argument.replace(' ','');
	}

	$rootScope.camelize = function(str) {
		return str.replace(/(\w)(\w*)/g,function(g0,g1,g2){return g1.toUpperCase() + g2.toLowerCase();});
	}

	$rootScope.toObject = function(arr) {
		var rv = {};
		for (var i = 0; i < arr.length; ++i)
			rv[i] = arr[i];
		return rv;
	}	

	$rootScope.getDateFormatSQL = function(d){
		return d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate();
	}

	$rootScope.simulateClick = function(e){
		$(e).trigger('click');
	}

	$rootScope.listen = function($s,$t,$f){
		$s.$watchCollection($t,$f);
	};

	$rootScope.universalModalMessageStatus = 'success';//success or fail
	$rootScope.universalModalMessageValue = [];
	$rootScope.universalModalMessageOpen = function(){
		$("#universalModalMessage").modal('show');
	}
	$rootScope.universalModalMessageClose = function(){
		$("#universalModalMessage").modal('hide');
	}
	$rootScope.universalModalShowMessage = function(val,stat='success'){
		$rootScope.universalModalMessageValue = val;
		$rootScope.universalModalMessageOpen();
		setTimeout($rootScope.universalModalMessageClose,2000);
	}

});

naModule.config(function($routeProvider,$locationProvider) {

	// $locationProvider.html5Mode(true);

	$routeProvider
	.when('/RoomTransaction', {
		templateUrl : baseurl + 'assets/angularTemplate/ClassTransaction.html',
		controller  : 'classTransactionController'
	})
	.when('/cpm', {
		templateUrl : baseurl + 'assets/angularTemplate/CPM.html',
		controller  : 'CPMController'
	})
	.otherwise({
		template : ""
	});

	$locationProvider.html5Mode(true);

});

naModule.directive('onFinishRender',['$timeout', '$parse', function ($timeout, $parse) {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			if (scope.$last === true) {
				$timeout(function () {
					scope.$emit('ngRepeatFinished');
					if(!!attr.onFinishRender){
						$parse(attr.onFinishRender)(scope);
					}
				});
			}
		}
	}
}]);

naModule.directive('boxClassTransaction', function($rootScope) {
	return {
		restrict : 'AEC',
		scope: {
			data: '='
		},
		link: function(scope,element,attrs){

		},
		templateUrl: baseurl + 'assets/angularTemplate/boxClassTransaction.html'
	};
});


naModule.controller('messageController', function ($scope, $http, $interval) {
	$scope.latestMessage = [];
	$scope.latestMessageLen = '';


	$interval(function (){
		/*$http.get( baseurl + 'home/get_latest_message' ).success(function (res) {
			if(res.length == 0){
				$('#has-message, .badge').hide();
				$('#has-no-message').show();
			} else {
				$('#has-message, .badge').show();
				$('#has-no-message').hide();

				//Play notification sound when there is new a problem
				if(res.length > $scope.latestMessageLen && $scope.latestMessageLen != -1
					&& $scope.latestMessageLen != '')
					$('#notif')[0].play();
			}

			$scope.latestMessage  = res;
			$scope.latestMessageLen = res.length;
		});*/
	}, 5000);

});

naModule.controller('classTransactionController',['$scope','$routeParams',function($scope,$param){
	$scope.RoomTransaction = {};
	$scope.DatesTransaction = {};
	$scope.startTime = null;
	$scope.endTime = null;
	$scope.filter = [];
	$scope.filter.Campus = 'ASM';

	$scope.getMore = function(){
		if ( $scope.startTime == null || $scope.endTime == null ) return;

		$scope.ajaxFactory('post', $scope.baseurl + 'WebService/getRoomTrans' ,{
			'endDate':$scope.endTime,
			'startDate':$scope.startTime,
		},function(data){
			$scope.DatesTransaction = data.dates;
			$scope.RoomTransaction = data.Details.RoomTransactionDetail;
			$scope.$apply();
			console.log( data );
		});

	};

	$scope.getMore();


	$scope.$watch('endTime',function(){
		$scope.getMore();
	});

	$scope.$watch('startTime',function(){
		$scope.getMore();
	});

	$('#startTime, #endTime').datetimepicker({
		useCurrent: true,
		format: "MM/DD/YYYY"
	});

	$("#startTime").on("dp.change", function(e) {
		$scope.startTime = e.date.format('MM/DD/YYYY');
		$scope.$apply();
	});
	$("#endTime").on("dp.change", function(e) {
		$scope.endTime = e.date.format('MM/DD/YYYY');
		$scope.$apply();
	});

}]);

naModule.controller('CPMController',['$scope','$routeParams',function($scope,$param){
	$scope.dataSettings = [];
	$scope.dataSettingsOri = [];
	$scope.dataPlan = {};
	$scope.dataHistories = [];
	$scope.isSettingEdited = false;
	$scope.editErrorMessages = [];

	$('[data-toggle="tooltip"]').tooltip();

	$('html').on('switchChange.bootstrapSwitch', '.switch' , function(event, state) {
	  console.log(this.attributes['cpm-class'].value); // DOM element
	  console.log(state); // true | false


		$scope.ajaxFactory('get', $scope.cpmurl + 'set/control' ,{
			class : this.attributes['cpm-class'].value,
			value : state ? 1 : 0
		},function(data){
				$scope.getAllData();
				$scope.editErrorMessages = [];
				$scope.$apply();

				$.snackbar({
					content: data.messages,
					timeout: 3000,
					htmlAllowed: true
				});
		},function(data){
				$scope.editErrorMessages = data;
				$scope.$apply();
		});
	});


	$scope.getAllData = function(){

		$scope.ajaxFactory('get', $scope.cpmurl + 'get/plan' ,{},function(data){
			$scope.dataPlan = data;
			$scope.$apply();
			$(".switch").bootstrapSwitch();
		});

		$scope.updateSettings();

	}

	$scope.updateSettings = function(){

		$scope.ajaxFactory('get', $scope.cpmurl + 'get/settings' ,{},function(data){
			$scope.dataSettings = jQuery.extend( true, {}, data );
			$scope.dataSettingsOri = jQuery.extend( true, {}, data );
			$scope.$apply();
		});

	}

	$scope.updateHistory = function(){

		$scope.ajaxFactory('get', $scope.cpmurl + 'get/histories' ,{},function(data){
			$scope.dataHistories = data;
			$scope.$apply();

			console.log( data );
		});

	}

	$scope.resetSetting = function(){
		$scope.dataSettings.apps = jQuery.extend( true, {}, $scope.dataSettingsOri.apps );
	}

	$scope.submitSetting = function(){

		$scope.ajaxFactory('get', $scope.cpmurl + 'set/setting' , $scope.dataSettings.apps ,
			function(data){

				$scope.getAllData();
				$scope.editErrorMessages = [];
				$scope.$apply();

				$.snackbar({
					content: data.messages,
					timeout: 3000,
					htmlAllowed: true
				});

			},
			function(data){
				$scope.editErrorMessages = data;
				$scope.$apply();
			}
		);

	}


	$scope.$watch('dataSettings',function(){
		$scope.isSettingEdited = JSON.stringify( $scope.dataSettings.apps ) !== JSON.stringify( $scope.dataSettingsOri.apps ) ;
	},true);

	setInterval(function(){
		$scope.updateHistory()
	},60000);

	$scope.updateHistory();
	$scope.getAllData();

}]);

// angular.element(document).ready(function() {
// 	angular.bootstrap(document.getElementById("naModuleApp"), ['NewAttention']);
// });